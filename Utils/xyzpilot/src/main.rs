
extern crate ggez;
extern crate fern;
extern crate log;
#[macro_use] extern crate failure;
extern crate range;
#[macro_use] extern crate lazy_static;
extern crate nalgebra;
extern crate serial;
extern crate dimval;

pub mod xyzstage;
pub mod transform;

use xyzstage::{XYZStage, Axis, Direction/*, AXES*/};
use transform::Transform;
use dimval::DimVal;

use ggez::*;
use ggez::graphics::{Color, Mesh, MeshBuilder, DrawMode, Rect};
use ggez::event::{self, Button, Keycode, MouseButton};
use ggez::nalgebra::{Point2/*, Vector2*/};

use std::env;
use std::fs;
use std::io::{self, BufRead};
use std::collections::HashMap;
use std::time::{Instant};
use std::str::FromStr;
// use std::path::Path;


lazy_static! {
    static ref CONTROLLER_MAPPING: HashMap<Button, (Axis, Direction)> = {
        let mut m = HashMap::new();
        m.insert(Button::DPadLeft, (Axis::X, Direction::Negative));
        m.insert(Button::DPadRight, (Axis::X, Direction::Positive));

        m.insert(Button::DPadDown, (Axis::Y, Direction::Positive));
        m.insert(Button::DPadUp, (Axis::Y, Direction::Negative));

        m.insert(Button::B, (Axis::Z, Direction::Negative));
        m.insert(Button::Y, (Axis::Z, Direction::Positive));
        m
    };
}

struct MainState {
    moving_since: HashMap<Axis, Instant>,
    stage: XYZStage,

    //transformations
    world2screen: Transform,
    screen2world: Transform,
    last_pos: Point2<f32>,

    //ggez stuff
    need_draw: bool,
    stage_mesh: Mesh,
    pointer_mesh: Mesh,
    stage_color: Color,
    pointer_color: Color,
    back_color: Color
}

const SCREEN_WIDTH: u32 = 800;
const SCREEN_HEIGHT: u32 = 800;

const POINTER_WIDTH: f32 = 4f32;

impl MainState {
    fn new(ctx: &mut Context, port: &str) -> GameResult<MainState> {

        let mut stage = XYZStage::new(port)
                                    .map_err(|e|GameError::UnknownError(e.to_string()))?;
        stage.home()
                .map_err(|e|GameError::UnknownError(e.to_string()))?;

        let transf = {
            let limits = stage.get_limits().clone();

            let xrange = limits.get(&Axis::X).ok_or(GameError::UnknownError(format!("No limits for X")))?;
            let yrange = limits.get(&Axis::Y).ok_or(GameError::UnknownError(format!("No limits for Y")))?;

            let world = Rect::new(xrange.min as f32, yrange.min as f32, xrange.size() as f32, yrange.size() as f32);
            let screen = Rect::new(0f32, 0f32, SCREEN_WIDTH as f32, SCREEN_HEIGHT as f32);

            Transform::from_a2b(&world, &screen)
        };
        let invtransf = transf.inverse_transform();

        let stage_mesh = MeshBuilder::new()
                    .polygon(DrawMode::Fill,
                            &[
                                Point2::new(0f32, 0f32),//top left
                                Point2::new(SCREEN_WIDTH as f32, 0f32),//top right
                                Point2::new(SCREEN_WIDTH as f32, SCREEN_HEIGHT as f32),//bottom right
                                Point2::new(0f32, SCREEN_HEIGHT as f32),//bottom left
                            ])
                    .build(ctx)?;

        let pointer_mesh = MeshBuilder::new()
                    .polygon(DrawMode::Fill,
                            &[
                                Point2::new(0f32, 0f32),//top left
                                Point2::new(POINTER_WIDTH, 0f32),//top right
                                Point2::new(POINTER_WIDTH, POINTER_WIDTH),//bottom right
                                Point2::new(0f32, POINTER_WIDTH),//bottom left
                            ])
                    .build(ctx)?;

        let s = MainState { 
            stage, 
            moving_since: HashMap::new(),

            world2screen: transf,
            screen2world: invtransf,
            last_pos: Point2::new(0f32, 0f32),

            need_draw: true,
            stage_mesh,
            pointer_mesh,
            stage_color: Color::from_rgb(200, 0, 0),
            pointer_color: Color::from_rgb(0, 200, 0),
            back_color: Color::from_rgb(20, 20, 20) 
            };
        Ok(s)
    }


    // fn apply_world2screen(&self, mut point: Point2<f32>) -> Point2<f32> {
    //     point.x = point.x*self.world2screen_scale.x + self.world2screen_offset.x;
    //     point.y = point.y*self.world2screen_scale.y + self.world2screen_offset.y;
    //     point
    // }
}

impl event::EventHandler for MainState {
    fn update(&mut self, _ctx: &mut Context) -> GameResult<()> {
        
        let pos = self.stage.get_absolute_position().map_err(|e|GameError::UnknownError(e.to_string()))?;
        self.last_pos.x = pos.x as f32;
        self.last_pos.y = pos.y as f32;

        Ok(())
    }

    fn draw(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::set_color(ctx, self.back_color)?;
        graphics::clear(ctx);
        self.need_draw = false;

        //draw stage
        graphics::set_color(ctx, self.stage_color)?;
        graphics::draw(ctx, &self.stage_mesh, Point2::new(0f32, 0f32), 0.0)?;

        //draw pointer
        graphics::set_color(ctx, self.pointer_color)?;
        let mut screen_pointer = self.last_pos.clone();
        self.world2screen.transform_point(&mut screen_pointer);
        graphics::draw(ctx, &self.pointer_mesh, screen_pointer, 0.0)?;

        graphics::present(ctx);
        Ok(())
    }

    fn key_down_event(&mut self, _ctx: &mut Context, keycode: Keycode, _keymod: event::Mod, _repeat: bool) {
        println!("Keycode {} ", keycode);
    }

    fn key_up_event(&mut self, ctx: &mut Context, keycode: Keycode, _keymod: event::Mod, _repeat: bool) {
        if keycode == Keycode::Escape {
            ctx.quit().unwrap();
        }
    }

    /// A controller button was pressed; instance_id identifies which controller.
    fn controller_button_down_event(
        &mut self,
        _ctx: &mut Context,
        btn: Button,
        _instance_id: i32,
    ) {
       match CONTROLLER_MAPPING.get(&btn) {
           Some((axis, _direction)) => {
                if !self.moving_since.contains_key(axis) {
                    self.moving_since.insert(axis.clone(), Instant::now());
                }
           },
           None => {
            //    println!("Controller button {:?} unknown", btn);
               }
       }

    }
    /// A controller button was released.
    fn controller_button_up_event(&mut self, _ctx: &mut Context, btn: Button, _instance_id: i32) {


        match CONTROLLER_MAPPING.get(&btn) {
           Some((axis, direction)) => {
               //start moving
               if let Some(start_instant) = self.moving_since.get(axis) {
                    let diff = Instant::now() - *start_instant;
                    let result = self.stage.move_dynamic(axis, *direction, diff);
                    if let Err(e) = result {
                        println!("Error for moving axis {:?}: {}", axis, e);
                    }
               }

               self.moving_since.remove(axis);
           },
           None => {
               match btn {
                   Button::Back => {
                       if let Ok(pos) = self.stage.get_absolute_position() {
                           println!("Position is X: {}mm, Y: {}mm, Z: {}mm", pos.x * 1e3, pos.y * 1e3, pos.z * 1e3);
                       }
                       else {
                           println!("Pos abs {:?}", self.stage.get_absolute_position());
                       }
                   },
                   _ => {println!("Controller button {:?} unknown", btn);}
               }
            }
       }

    //    println!("Moving {:?}, direction: {:?}", self.moving, self.directions);
    }

    fn mouse_button_up_event(&mut self, _ctx: &mut Context, _button: MouseButton, x: i32, y: i32) {

        let mut point = Point2::new(x as f32, y as f32);

        self.screen2world.transform_point(&mut point);

        let mut positions: HashMap<Axis, f64> = HashMap::new();
        positions.insert(Axis::X, point.x as f64);
        positions.insert(Axis::Y, point.y as f64);

        if let Err(e) = self.stage.move_absolute_parallel(&positions) {
            println!("Error while moving to {:?}: {}", positions, e);
        }
    }
}


fn main() -> GameResult<()> {

    let stdin = io::stdin();

    //*********************LOG INIT                                          */
    let log_level_stdout =
    // log::LevelFilter::Error;
    // log::LevelFilter::Warn;
    // log::LevelFilter::Info;
    // log::LevelFilter::Debug;
    // log::LevelFilter::Trace;
    log::LevelFilter::Off;
    

    let log_level_log = 
    // log::LevelFilter::Error;
    // log::LevelFilter::Warn;
    // log::LevelFilter::Info;
    // log::LevelFilter::Debug;
    log::LevelFilter::Trace;
    // log::LevelFilter::Off;


    //configure logger
    let log_filename = format!("out.log");

    //remove file if exists
    let _ = fs::remove_file(&log_filename);

    //setup logging
    //stdout level depends on verbosity
    let base_config = fern::Dispatch::new()
    .format(|out, message, record| {
        out.finish(format_args!("[{}] {}",
            record.level(),
            message))
    });

    let stdout_log = fern::Dispatch::new()
                        .level(log_level_stdout)
                        .chain(std::io::stdout());

    let file_log = fern::Dispatch::new()
                        .level(log_level_log)
                        .chain(fern::log_file(log_filename).map_err(|e|format!("{}", e))?);

    base_config.chain(file_log)
                .chain(stdout_log)
                .apply()
                .map_err(|e|format!("{}", e))?;


    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        return Err(GameError::FilesystemError(format!("You must specify the stage port name as an argument to this executable. Ex: xyzpilot /dev/ttyUSB0")));
    }

    //*************** STAGE INIT */

    let stage_port = &args[1];
    

    println!("Please ensure that the stage can move freely, then type O.");
    
    let validation_string = stdin.lock().lines().next().ok_or(GameError::UnknownError(format!("Cannot read validation.")))??;

    if validation_string != "O" {
        println!("Validation incorrect ({}) -> end of program", validation_string);
        return Ok(());
    }

    if args.len() == 2 {
        //*************** ENGINE INIT */
        
        //init 2D engine (ggez)
        print!("Initializing 2D engine...");
        let cb = ContextBuilder::new("chip8", "Ronan")
            .window_setup(conf::WindowSetup::default().title("XYZ Stage"))
            .window_mode(conf::WindowMode::default().dimensions(
                                                        SCREEN_WIDTH, 
                                                        SCREEN_HEIGHT));


        let ctx = &mut cb.build()?;
        let state = &mut MainState::new(ctx, stage_port)?;
        println!("Done");

        //run 2D engine
        print!("Running...");

        event::run(ctx, state)?;
        println!("Done");
        
        Ok(())
    }
    else if args.len() == 4 {

        let x = DimVal::from_str(&args[2])
                        .map_err(|e|GameError::UnknownError(format!("f64 parsing error: {}",e)))?;
        let y = DimVal::from_str(&args[3])
                        .map_err(|e|GameError::UnknownError(format!("f64 parsing error: {}",e)))?;


        let mut newpos: HashMap<Axis, f64> = HashMap::new();
        newpos.insert(Axis::X, x.to_f64().map_err(|_|GameError::UnknownError(format!("Cannot convert to f64")))?);
        newpos.insert(Axis::Y, y.to_f64().map_err(|_|GameError::UnknownError(format!("Cannot convert to f64")))?);

        let mut stage = XYZStage::new(stage_port)
                                    .map_err(|e|GameError::UnknownError(e.to_string()))?;
        stage.home()
                .map_err(|e|GameError::UnknownError(e.to_string()))?;
        
        stage.z_up().map_err(|e|GameError::UnknownError(e.to_string()))?;
        stage.move_absolute_parallel(&newpos).map_err(|e|GameError::UnknownError(e.to_string()))?;
        println!("Moving to {:?}", newpos);
        stage.z_down().map_err(|e|GameError::UnknownError(e.to_string()))?;
        Ok(())
    }
    else if args.len() == 5 {

        let x = DimVal::from_str(&args[2])
                        .map_err(|e|GameError::UnknownError(format!("f64 parsing error: {}",e)))?;
        let y = DimVal::from_str(&args[3])
                        .map_err(|e|GameError::UnknownError(format!("f64 parsing error: {}",e)))?;

        let z = DimVal::from_str(&args[4])
                        .map_err(|e|GameError::UnknownError(format!("f64 parsing error: {}",e)))?;


        let mut newpos: HashMap<Axis, f64> = HashMap::new();
        newpos.insert(Axis::X, x.to_f64().map_err(|_|GameError::UnknownError(format!("Cannot convert to f64")))?);
        newpos.insert(Axis::Y, y.to_f64().map_err(|_|GameError::UnknownError(format!("Cannot convert to f64")))?);

        let mut newz: HashMap<Axis, f64> = HashMap::new();
        newz.insert(Axis::Z, z.to_f64().map_err(|_|GameError::UnknownError(format!("Cannot convert to f64")))?);

        let mut stage = XYZStage::new(stage_port)
                                    .map_err(|e|GameError::UnknownError(e.to_string()))?;
        stage.home()
                .map_err(|e|GameError::UnknownError(e.to_string()))?;
        
        stage.z_up().map_err(|e|GameError::UnknownError(e.to_string()))?;
        stage.move_absolute_parallel(&newpos).map_err(|e|GameError::UnknownError(e.to_string()))?;
        println!("Moving to {:?}", newpos);
        // stage.z_down().map_err(|e|GameError::UnknownError(e.to_string()))?;
        stage.move_absolute_parallel(&newz).map_err(|e|GameError::UnknownError(e.to_string()))?;
        println!("New Z: {:?}", newz);
        Ok(())
    }
    else {
        Err(GameError::UnknownError(format!("Incorrect number of arguments")))
    }

}
