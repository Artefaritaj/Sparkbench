/*
 * File: xyzstage.rs
 * Project: src
 * Created Date: Monday October 22nd 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 5th November 2018 11:47:15 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use std::fmt;
use std::io::{/*Read, */Write, BufRead, BufReader};
use std::collections::HashMap;
use std::thread;
use std::time::Duration;

use range::range::Range;

use serial;
use serial::{SystemPort, SerialPort, PortSettings, BaudRate, CharSize, StopBits, Parity, FlowControl};

use failure::Error;
use nalgebra::Point3;

#[derive(Debug,Clone,Hash,PartialEq,Eq,PartialOrd,Ord,Copy)]
pub enum Axis {
    X,
    Y,
    Z
}

#[derive(Debug,Hash,PartialEq,Eq,PartialOrd,Ord,Clone,Copy)]
pub enum Direction {
    Positive,
    Negative
}

impl fmt::Display for Axis {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Axis::X => write!(f, "1"),
            Axis::Y => write!(f, "2"),
            Axis::Z => write!(f, "3"),
        }
    }
}

const END: &str = "\r\n";
const CMD_HOME: &str = "OR";
const CMD_MOVE_ABS: &str = "PA";
const CMD_MOVE_REL: &str = "PR";
const CMD_GET_POS: &str = "TP";
const CMD_GET_STATUS: &str = "TS";
const CMD_GET_POS_LIM: &str = "SR";
const CMD_GET_NEG_LIM: &str = "SL";

const ZUP_MARGIN: f64 = -0.001f64;


lazy_static! {
    static ref POLL_DELAY_MS: Duration = {
        Duration::from_millis(50)
    };

    pub static ref AXES: Vec<Axis> = {
        vec![Axis::X, Axis::Y, Axis::Z]
    };
}

pub struct XYZStage {
    port: String,
    connection: Option<SystemPort>,
    jog_speeds: HashMap<Axis, f64>,
    limits: HashMap<Axis, Range<f64>>
}

impl XYZStage {
    pub fn new(port: &str) -> Result<XYZStage, Error> {

        let mut jog_speeds = HashMap::new();
        jog_speeds.insert(Axis::X, 1e-2);
        jog_speeds.insert(Axis::Y, 1e-2);
        jog_speeds.insert(Axis::Z, 2e-3);

        let mut limits = HashMap::new();
        limits.insert(Axis::X, Range::new(-25e-3, 25e-3));
        limits.insert(Axis::Y, Range::new(-25e-3, 25e-3));
        limits.insert(Axis::Z, Range::new(-25e-3, 25e-3));
    

        let mut stage = XYZStage { 
            port: port.to_string(), 
            connection: None,
            jog_speeds,
            limits
        };

        stage.connect()?;
        Ok(stage)
    }

    pub fn set_speed(&mut self, axis: &Axis, speed: f64) {
        self.jog_speeds.insert(axis.clone(), speed);
    }

    pub fn get_speed(&self, axis: &Axis) -> Option<f64> {
        let s_opt = self.jog_speeds.get(axis);
        if let Some(s) = s_opt {
            return Some(*s);
        }
        else {
            return None;
        }
    }

    pub fn get_limits(&self) -> &HashMap<Axis, Range<f64>> {
        &self.limits
    }

    fn connect(&mut self) -> Result<(), Error> {
        let mut conn = serial::open(&self.port)?;

        let settings = PortSettings {
            baud_rate: BaudRate::Baud57600,
            char_size: CharSize::Bits8,
            parity: Parity::ParityNone,
            stop_bits: StopBits::Stop1,
            flow_control: FlowControl::FlowSoftware,
        };

        conn.configure(&settings)?;

        self.connection = Some(conn);
        Ok(())
    }

    pub fn home_axis(&mut self, axis: &Axis) -> Result<(), Error> {
        if let Some(ref mut conn) = self.connection {
            conn.write( (axis.to_string() + CMD_HOME + END).as_bytes())?;
        }

        Ok(())
    }

    

    pub fn home(&mut self) -> Result<(), Error> {
        
        for axis in AXES.iter() {
            self.home_axis(axis)?;//find zero
        }        

        thread::sleep(Duration::from_secs(2));

        for axis in AXES.iter() {            
            //mesure max and min values
            self.limits.remove(axis);
            let range = self.get_limits_axis(axis)?;
            self.limits.insert(axis.clone(), range);
        } 

        Ok(())
    }

    fn get_limits_axis(&mut self, axis: &Axis) -> Result<Range<f64>, Error> {
        //positive limit
        self.send_cmd(axis.to_string() + CMD_GET_POS_LIM + "?" + END)?;
        let pos_answer = self.read_answer_until_end()?;
        let pos_prefix = axis.to_string() + CMD_GET_POS_LIM;
        let pos_limit_str = XYZStage::strip_and_check_prefix(pos_answer, &pos_prefix)?;

        let mut pos_limit: f64 = pos_limit_str.parse()?;
        pos_limit *= 1e-3;

        //negavite limit
        self.send_cmd(axis.to_string() + CMD_GET_NEG_LIM + "?" + END)?;
        let neg_answer = self.read_answer_until_end()?;
        let neg_prefix = axis.to_string() + CMD_GET_NEG_LIM;
        let neg_limit_str = XYZStage::strip_and_check_prefix(neg_answer, &neg_prefix)?;

        let mut neg_limit: f64 = neg_limit_str.parse()?;
        neg_limit *= 1e-3;
        Ok(Range::new(neg_limit, pos_limit))
    }

    pub fn move_absolute_axis(&mut self, axis: &Axis, mut pos: f64) -> Result<(), Error> {
        if !self.limits.get(axis).ok_or(format_err!("No limits for axis {:?}", axis))?.is_inside_inclusive(&pos) {
            return Err(format_err!("{} not inside authorized range.", pos));
        }

        pos *= 1e3;//conversion to mm

        self.send_cmd(axis.to_string() + CMD_MOVE_ABS + &pos.to_string() + END)?;
        self.wait_for_move_end(axis)?;
        Ok(())
    }

    pub fn move_absolute(&mut self, positions: &HashMap<Axis, f64>) -> Result<(), Error> {
        for (axis, pos) in positions {
            self.move_absolute_axis(axis, *pos)?;
        }
        Ok(())
    }

    pub fn move_absolute_parallel(&mut self, positions: &HashMap<Axis, f64>) -> Result<(), Error> {

        //first launch all move orders
        for (axis, pos) in positions {
            if !self.limits.get(axis).ok_or(format_err!("No limits for axis {:?}", axis))?.is_inside_inclusive(&pos) {
                return Err(format_err!("{} not inside authorized range.", pos));
            }
            let newpos= pos * 1e3;
            self.send_cmd(axis.to_string() + CMD_MOVE_ABS + &newpos.to_string() + END)?;
            
        }

        //then wait end of move
        for (axis, _) in positions {
            self.wait_for_move_end(axis)?;
        }

        Ok(())
    }

    pub fn move_relative_axis(&mut self, axis: &Axis, mut step: f64) -> Result<(), Error> {
        step *= 1e3;//conversion to mm

        self.send_cmd(axis.to_string() + CMD_MOVE_REL + &step.to_string() + END)?;
        self.wait_for_move_end(axis)?;
        Ok(())
    }

    // fn move_relative_axis_no_wait(&mut self, axis: &Axis, step: f64) -> Result<(), Error> {

    //     self.send_cmd(axis.to_string() + CMD_MOVE_REL + &step.to_string() + END)?;
    //     Ok(())
    // }

    pub fn move_relative(&mut self, steps: &HashMap<Axis, f64>) -> Result<(), Error> {
        for (axis, step) in steps {
            self.move_relative_axis(axis, *step)?;
        }
        Ok(())
    }

    pub fn z_up(&mut self) -> Result<(), Error> {
        self.move_relative_axis(&Axis::Z, ZUP_MARGIN)
    }

    pub fn z_down(&mut self) -> Result<(), Error> {
        self.move_relative_axis(&Axis::Z, -ZUP_MARGIN)
    }

    pub fn move_dynamic(&mut self, axis: &Axis, direction: Direction, delta: Duration) -> Result<(), Error> {
        let ratio: f64 = (delta.subsec_micros() as f64 / 1000000f64) + delta.as_secs() as f64;
        let speed = self.get_speed(axis).ok_or(format_err!("No speed specified for axis {:?}", axis))?;

        let mut step = ratio * speed;

        if direction == Direction::Negative {
            step = -step;
        }

        self.move_relative_axis(axis, step)?;

        Ok(())
    }

    fn send_cmd<S: Into<String>>(&mut self, cmd: S) -> Result<(), Error> {
        if let Some(ref mut conn) = self.connection {
            let cmd_s = cmd.into();
            // println!("Sending {}", cmd_s);
            conn.write( cmd_s.as_bytes())?;
        }
        else {
            return Err(format_err!("Cannot open connection to XYZ stage."));
        }
        Ok(())
    }

    fn wait_for_move_end(&mut self, axis: &Axis) -> Result<(), Error> {
            loop {
                thread::sleep(*POLL_DELAY_MS);
                //send command
                self.send_cmd(axis.to_string() + CMD_GET_STATUS + END)?;
                //read answer
                let status_answer = self.read_answer_until_end()?;
                let prefix = axis.to_string() + CMD_GET_STATUS;
                
                let mut status_complete = XYZStage::strip_and_check_prefix(status_answer, &prefix)?;
                let status = status_complete.split_off(4);

                match status.as_ref() {
                    "32" | "33" => { return Ok(());},
                    "28" => {},//Do nothing (moving)
                    _ => {return Err(format_err!("Incorrect status: {}", status));}
                }
                
            }
    }

    // fn read_connection_answer_until_end<R: Read>(conn: &mut R) -> Result<String, Error> {
    //     let reader = BufReader::new(conn);
    //     let line = reader.lines().next().ok_or(format_err!("Timeout on read"))??;
    //     Ok(line)
    // }

    fn read_answer_until_end(&mut self) -> Result<String, Error> {
        if let Some(ref mut conn) = self.connection {
            let reader = BufReader::new(conn);
            let line = reader.lines().next().ok_or(format_err!("Timeout on read"))??;
            Ok(line)
        }
        else {
            Err(format_err!("No connection opened to read."))
        }
    }

    fn strip_and_check_prefix(mut answer: String, prefix: &str) -> Result<String, Error> {
        //check prefix
        if !answer.starts_with(&prefix) {
            return Err(format_err!("Prefix incorrect : {} instead of {}", answer, prefix));
        }

        let end = answer.split_off(prefix.len());
        Ok(end)
    }

    pub fn get_absolute_position_axis(&mut self, axis: &Axis) -> Result<f64, Error> {
        if let Some(ref mut conn) = self.connection {
            conn.write( (axis.to_string() + CMD_GET_POS + END).as_bytes())?;
        }

        let pos_answer = self.read_answer_until_end()?;
        let prefix = axis.to_string() + CMD_GET_POS;

        let pos_str = XYZStage::strip_and_check_prefix(pos_answer, &prefix)?;
        let mut pos: f64 = pos_str.parse()?;
        pos *= 1e-3;
        Ok(pos)
        
    }

    pub fn get_absolute_position(&mut self) -> Result<Point3<f64>, Error> {
        let x = self.get_absolute_position_axis(&Axis::X)?;
        let y = self.get_absolute_position_axis(&Axis::Y)?;
        let z = self.get_absolute_position_axis(&Axis::Z)?;

        Ok(Point3::new(x,y,z))
    }
}