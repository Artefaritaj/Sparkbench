/*
 * File: transform.rs
 * Project: src
 * Created Date: Tuesday October 30th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 30th October 2018 3:49:42 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */


use ggez::nalgebra::{Point2, Vector2};
use ggez::graphics::Rect;

#[derive(Debug)]
pub struct Transform {
    offset: Vector2<f32>,
    scale: Vector2<f32>
}

impl Transform {
    //center of scaling : (0, 0)
    pub fn from_a2b(a: &Rect, b: &Rect) -> Transform {
        assert!(a.w * a.h != 0f32);

        let scale = Vector2::new(b.w / a.w, b.h / a.h);
        let scaled_a = Point2::new(a.x * scale.x, a.y * scale.y);
        let offset = Vector2::new(b.x - scaled_a.x, b.y - scaled_a.y);

        Transform { offset, scale }
    }

    pub fn transform_rect(&self, r: &mut Rect) {
        let mut origin = Point2::new(r.x, r.y);
        let mut size = Vector2::new(r.w, r.h);

        self.transform_point(&mut origin);
        self.transform_vec(&mut size);

        r.x = origin.x;
        r.y = origin.y;
        r.w = size.x;
        r.h = size.y;
    }

    pub fn transform_point(&self, p: &mut Point2<f32>) {
        p.x = p.x*self.scale.x + self.offset.x;
        p.y = p.y*self.scale.y + self.offset.y;
    }

    pub fn transform_vec(&self, v: &mut Vector2<f32>) {
        v.x = v.x*self.scale.x;
        v.y = v.y*self.scale.y;
    }

    pub fn inverse_transform(&self) -> Transform {
        Transform { 
            offset: Vector2::new(-self.offset.x / self.scale.x, -self.offset.y / self.scale.y),
            scale: Vector2::new(1f32 / self.scale.x, 1f32 / self.scale.y)
        }
    }
}

#[test]
fn test_transform() {
    let screen = Rect::new(0f32, 0f32, 800f32, 600f32);
    let world = Rect::new(-50f32, -50f32, 100f32, 100f32);

    let t = Transform::from_a2b(&world, &screen);
    
    let mut screen2 = world.clone();
    t.transform_rect(&mut screen2);
    assert_eq!(screen, screen2);

    let mut zero = Point2::new(0f32, 0f32);
    t.transform_point(&mut zero);
    assert_eq!(zero, Point2::new(400f32, 300f32));

    let tinv = t.inverse_transform();
    tinv.transform_rect(&mut screen2);
    assert_eq!(screen2, world);
}