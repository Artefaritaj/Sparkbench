# @Author: Lashermes Ronan <ronan>
# @Date:   02-05-2017
# @Email:  ronan.lashermes@inria.fr
# @Last modified by:   ronan
# @Last modified time: 02-05-2017
# @License: MIT

key = "000102030405060708090a0b0c0d0e0f"
[modules]
  [modules.stm32]
    type = "toml_device"
    toml_files = ["STM32L496AG", "AES128", "CIPHER_PROTOCOL1", "OPENOCD"]

  [modules.text_gen]
    type = "vec_u8_generator"
    generator_type = "random"
    len = 16
    output_name = "plaintext"

  [modules.plaintext_saver]
    type = "text_saver"
    path = "plaintexts.hex"
    creation_mode = "overwrite"
    format = "hexstring"

  [modules.ciphertext_saver]
    type = "text_saver"
    path = "ciphertexts.hex"
    creation_mode = "overwrite"
    format = "hexstring"

  [modules.diff_saver]
    type = "text_saver"
    path = "diff.hex"
    creation_mode = "overwrite"
    format = "hexstring"

  [modules.aes]
    type = "aes"
    output_name = "cipher_computed"

  [modules.diff_ciphers]
    type = "diff"

  [modules.console_diff]
    type = "console_display"
    text = "Diff: "
    format = "hexstring"

[actors]
  validate = ["stm32", "test"]
  set_key = ["stm32", "set_key"]
  set_round = ["stm32", "set_round"]
  encrypt = ["stm32", "fast"]
  generate_pt = ["text_gen", "gen"]
  stm32_welcome = ["stm32", "welcome", "post_tempo=500ms"]
  stm32_reset = ["stm32", "reset", "post_tempo=500ms"]
  save_plaintext = ["plaintext_saver", "save"]
  save_ciphertext = ["ciphertext_saver", "save"]
  save_diff = ["diff_saver", "save", "sync_timeout=10ks"]
  compute_cipher = ["aes", "encrypt"]
  diff_ciphers = ["diff_ciphers", "diff", "not_zero"]
  #diff_ciphers = ["diff_ciphers", "diff"]
  show_diff = ["console_diff", "display", "sync_timeout=10ks"]

[constants]
  [constants.compute_cipher]
    key = "$(key:hexstring)"

  [constants.set_key]
    key = "$(key:hexstring)"

  [constants.set_round]
    round = 10


[processes]
   [processes.reset]
    process = ["stm32_reset", "stm32_welcome", "validate"]
    [processes.reset.links]
      reset2welcome = ["stm32_reset", "stm32_welcome"]
      welcome2validate = ["stm32_welcome", "validate"]

  [processes.configure]
    process = ["set_key", "set_round"]
    [processes.configure.links]
      sync = ["set_key", "set_round"]

  [processes.execute]
    process = ["generate_pt", "encrypt", "save_plaintext", "save_ciphertext", "compute_cipher", "diff_ciphers", "show_diff", "save_diff"]
    count = 15000000
    resume_kind = "resume_skip"
    # autolinks = false
    [processes.execute.links]
      plain_save = ["generate_pt", "plaintext", "save_plaintext", "input"]
      ciph_save = ["encrypt", "ciphertext", "save_ciphertext", "input"]
      true_ciph2diff = ["compute_cipher", "cipher_computed", "diff_ciphers", "input1"]
      exp_ciph2diff = ["encrypt", "ciphertext", "diff_ciphers", "input2"]
      diff2display = ["diff_ciphers", "output", "show_diff", "input"]
      diff2save = ["diff_ciphers", "output", "save_diff", "input"]
    [processes.execute.feedx]
      #synchronization to correctly save data when there is an error
      save1 = ["save_ciphertext", "front", "save_plaintext", "front"]
      save2 = ["save_plaintext", "front", "save_ciphertext", "front"]

[experiment]
  entry = ["reset", ""]#special proc name: say where to start experiment
  reset = ["configure", ""]
  configure = ["execute", ""]
  execute = ["", "reset"]
