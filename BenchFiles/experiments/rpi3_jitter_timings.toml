# @Author: ronan
# @Date:   07-10-2016
# @Email:  ronan.lashermes@inria.fr
# @Last modified by:   ronan
# @Last modified time: 20-12-2016

key = "000102030405060708090a0b0c0d0e0f"
plain = "00112233445566778899aabbccddeeff"
reset_duration = 100000
batch = 1

[modules]
  [modules.rpi3]
    type = "toml_device"
    toml_files = ["RPi3", "AES128", "CIPHER_PROTOCOL1"]

  [modules.oscillo]
    type = "toml_device"
    toml_files = ["DSOS404A"]

  [modules.text_gen]
    type = "vec_u8_generator"
    generator_type = "random"
    len = 16
    # generator_type = "fixed"
    # value = "$(plain:hexstring)"
    output_name = "plaintext"

  [modules.key_gen]
    type = "vec_u8_generator"
    generator_type = "fixed"
    value = "$(key:hexstring)"
    output_name = "key"

  [modules.triggy]
    type = "toml_device"
    toml_files = "Triggy"

  [modules.plaintext_saver]
    type = "text_saver"
    path = "plaintexts.hex"
    creation_mode = "overwrite"

  [modules.ciphertext_saver]
    type = "text_saver"
    path = "ciphertexts.hex"
    creation_mode = "overwrite"

  [modules.diff_saver]
    type = "text_saver"
    path = "diff.hex"
    creation_mode = "overwrite"

  [modules.timings_splitter]
    type = "splitter"
    divide_by = "$(batch)"
    input_name = "SAMPLES"
    output_name = "TIMINGS"

  [modules.timings_saver]
    type = "bin_saver"
    path = "timings.bini8"
    creation_mode = "overwrite"
    input_name = "TIMINGS"

  [modules.timings_fault_saver]
    type = "bin_saver"
    path = "timings_fault.bini8"
    creation_mode = "overwrite"
    input_name = "TIMINGS"

  [modules.aes]
    type = "aes"
    output_name = "cipher_computed"

  [modules.diff_ciphers]
    type = "diff"

  [modules.console_diff]
    type = "console_display"
    text = "Diff: "

  [modules.batcher]
    type = "replicator"
    replication_factor = "$(batch)"

  # [modules.xystage]
  #   type = "toml_device"
  #   toml_files = "SMC100"

[actors]
  validate = ["rpi3", "test"]
  set_key = ["rpi3", "set_key"]
  set_round = ["rpi3", "set_round"]
  encrypt = ["rpi3", "fast", "post_tempo=100ms", "timeout=10s"]
  generate_key = ["key_gen", "gen"]
  generate_pt = ["text_gen", "gen"]
  rpi3_welcome = ["rpi3", "welcome"]
  save_plaintext = ["plaintext_saver", "save"]
  save_ciphertext = ["ciphertext_saver", "save"]
  save_diff = ["diff_saver", "save", "sync_timeout=20ks"]
  compute_cipher = ["aes", "encrypt"]
  diff_ciphers = ["diff_ciphers", "diff", "not_zero"]
  show_diff = ["console_diff", "display", "sync_timeout=20ks"]
  triggy_test = ["triggy", "test"]
  triggy_conf0 = ["triggy", "conf0_imm_idle1"]
  triggy_set_width0 = ["triggy", "set_trig0_width"]
  triggy_imm0 = ["triggy", "imm0", "post_tempo=3s"]#this reset the rpi3 chip

  osc_test = ["oscillo", "test"]
  osc_set_channel = ["oscillo", "set_acquisition_channel"]
  osc_run  = ["oscillo", "run"]
  osc_stop  = ["oscillo", "stop"]
  restart_dso = ["oscillo", "restart", "post_tempo=1s"]
  osc_set_time_range = ["oscillo", "set_time_range"]
  osc_set_sample_count = ["oscillo", "set_sample_count"]
  osc_set_segmented = ["oscillo", "set_segmented_mode"]
  osc_get_data = ["oscillo", "get_data"]
  osc_get_data_stop = ["oscillo", "get_data_stop"]
  osc_set_byte_format = ["oscillo", "set_byte_format"]
  save_timings = ["timings_saver", "save", "sync_timeout=20ks"]
  split_timings = ["timings_splitter", "split", "sync_timeout=20ks"]
  save_faulted_timings = ["timings_fault_saver", "save", "sync_timeout=20ks"]
  create_batch = ["batcher", "replicate"]


[constants]
  [constants.compute_cipher]
    key = "$(key:hexstring)"

  [constants.triggy_set_width0]
    TRIG0_WIDTH = "$(reset_duration:u32)"

  [constants.set_round]
    round = 1

  [constants.osc_set_time_range]
    TIME_RANGE = "15us"

  [constants.osc_set_sample_count]
    SAMPLES_COUNT = "10000"

  [constants.osc_set_channel]
    ACQ_CHANNEL = "CHAN3"

  [constants.osc_set_segmented]
    SEG_COUNT = "$(batch:i64)"

[processes]
  [processes.reset]
    #process = ["triggy_test", "triggy_conf0", "triggy_set_width0", "triggy_imm0", "rpi3_welcome", "validate"]
    process = ["triggy_test", "triggy_conf0", "triggy_set_width0", "triggy_imm0", "rpi3_welcome", "validate"]
    [processes.reset.links]
      test2conf = ["triggy_test", "triggy_conf0"]
      conf2width = ["triggy_conf0", "triggy_set_width0"]
      width2imm = ["triggy_set_width0", "triggy_imm0"]
      reset2welcome = ["triggy_imm0", "rpi3_welcome"]
      welcome2validate = ["rpi3_welcome", "validate"]

  [processes.osc_setup]
    # process = ["osc_test", "osc_set_channel", "osc_set_segmented", "osc_set_byte_format"]
    process = ["osc_test", "osc_set_time_range", "osc_set_sample_count", "osc_set_channel", "osc_set_segmented", "osc_set_byte_format"]

  [processes.key_init]
    process = ["validate", "generate_key", "set_key", "set_round"]
    [processes.key_init.links]
      validate = ["validate", "generate_key"]
      validate2 = ["validate", "set_round"]

  [processes.dut]
    process = ["create_batch", "restart_dso", "generate_pt", "encrypt", "save_plaintext",
    "save_ciphertext", "compute_cipher", "diff_ciphers", "show_diff", "save_diff",
    "osc_get_data", "split_timings", "save_timings"]#, "save_faulted_timings"]
    count = 100
    # autolinks = false
    [processes.dut.links]
      run = ["restart_dso", "sync", "create_batch", "input"]
      batching = ["create_batch", "output", "generate_pt", "sync"]
      plain_save = ["generate_pt", "plaintext", "save_plaintext", "input"]
      ciph_save = ["encrypt", "ciphertext", "save_ciphertext", "input"]
      true_ciph2diff = ["compute_cipher", "cipher_computed", "diff_ciphers", "input1"]
      exp_ciph2diff = ["encrypt", "ciphertext", "diff_ciphers", "input2"]
      diff2display = ["diff_ciphers", "output", "show_diff", "input"]
      diff2save = ["diff_ciphers", "output", "save_diff", "input"]
      #diff2savetiming = ["diff_ciphers", "output", "save_faulted_timings", "sync"]
      getdatasync = ["encrypt", "sync", "osc_get_data", "sync", "$(batch)"]

    [processes.dut.feedx]
      #synchronization to correctly save data when there is an error
      save1 = ["save_ciphertext", "front", "save_plaintext", "front"]
      save2 = ["save_plaintext", "front", "save_ciphertext", "front"]
      return = ["osc_get_data", "back", "restart_dso", "back"]
      #save3 = ["save_diff", "save_plaintext,save_ciphertext"]

[experiment]
  entry = ["reset", "", ""]#special proc name: say where to start experiment
  reset = ["osc_setup", "reset", "start_over"]
  osc_setup = ["key_init", "", "start_over"]
  key_init = ["dut", "", "start_over"]
  dut = ["", "reset", "resume"]
