# @Author: ronan
# @Date:   07-10-2016
# @Email:  ronan.lashermes@inria.fr
# @Last modified by:   ronan
# @Last modified time: 20-12-2016

key = "000102030405060708090a0b0c0d0e0f"
plain = "00112233445566778899aabbccddeeff"
reset_duration = 1000

[modules]
  [modules.rpi3]
    type = "toml_device"
    toml_files = ["RPi3", "AES128", "CIPHER_PROTOCOL1"]

  [modules.text_gen]
    type = "vec_u8_generator"
    # generator_type = "random"
    # len = 16
    generator_type = "fixed"
    value = "$(plain:hexstring)"
    output_name = "plaintext"

  [modules.key_gen]
    type = "vec_u8_generator"
    generator_type = "fixed"
    value = "$(key:hexstring)"
    output_name = "key"

  [modules.triggy]
    type = "toml_device"
    toml_files = "Triggy"

  [modules.plaintext_saver]
    type = "text_saver"
    path = "plaintexts.hex"
    creation_mode = "overwrite"
    format = "hexstring"

  [modules.ciphertext_saver]
    type = "text_saver"
    path = "ciphertexts.hex"
    creation_mode = "overwrite"
    format = "hexstring"

  [modules.diff_saver]
    type = "text_saver"
    path = "diff.hex"
    creation_mode = "overwrite"
    format = "hexstring"

  [modules.aes]
    type = "aes"
    output_name = "cipher_computed"

  [modules.diff_ciphers]
    type = "diff"

  [modules.console_diff]
    type = "console_display"
    text = "Diff: "
    format = "hexstring"

  [modules.rep]
    type = "replicator"
    replication_factor = 1
    output_name = "sync"

  # [modules.xystage]
  #   type = "toml_device"
  #   toml_files = "SMC100"

[actors]
  validate = ["rpi3", "test"]
  set_key = ["rpi3", "set_key"]
  set_round = ["rpi3", "set_round"]
  encrypt = ["rpi3", "fast", "post_tempo=50ms"]
  generate_key = ["key_gen", "gen", "wait_validation"]
  generate_pt = ["text_gen", "gen"]
  rpi3_welcome = ["rpi3", "welcome"]
  save_plaintext = ["plaintext_saver", "save"]
  save_ciphertext = ["ciphertext_saver", "save"]
  save_diff = ["diff_saver", "save", "sync_timeout=1ks"]
  compute_cipher = ["aes", "encrypt"]
  diff_ciphers = ["diff_ciphers", "diff", "not_zero"]
  show_diff = ["console_diff", "display", "sync_timeout=1ks"]
  triggy_test = ["triggy", "test"]
  triggy_conf0 = ["triggy", "conf0_imm_idle1"]
  triggy_set_width0 = ["triggy", "set_trig0_width"]
  triggy_imm0 = ["triggy", "imm0", "post_tempo=1s", "wait_validation"]#this reset the rpi3 chip
  # replicate = ["rep", "replicate"]

[constants]
  [constants.compute_cipher]
    key = "$(key:hexstring)"

  [constants.triggy_set_width0]
    TRIG0_WIDTH = "$(reset_duration:u32)"

  [constants.set_round]
    round = 1

[processes]
  [processes.reset]
    #process = ["triggy_test", "triggy_conf0", "triggy_set_width0", "triggy_imm0", "rpi3_welcome", "validate"]
    process = ["triggy_test", "triggy_conf0", "triggy_set_width0", "triggy_imm0", "rpi3_welcome", "validate"]
    [processes.reset.links]
      test2conf = ["triggy_test", "triggy_conf0"]
      conf2width = ["triggy_conf0", "triggy_set_width0"]
      width2imm = ["triggy_set_width0", "triggy_imm0"]
      reset2welcome = ["triggy_imm0", "rpi3_welcome"]
      welcome2validate = ["rpi3_welcome", "validate"]

  [processes.key_init]
    process = ["validate", "generate_key", "set_key", "set_round"]
    [processes.key_init.links]
      validate = ["validate", "generate_key"]
      validate2 = ["validate", "set_round"]

  [processes.dut]
    process = ["generate_pt", "encrypt", "save_plaintext", "save_ciphertext", "compute_cipher", "diff_ciphers", "show_diff", "save_diff"]
    count = 10000
    # autolinks = false
    [processes.dut.links]
      plain_save = ["generate_pt", "plaintext", "save_plaintext", "input"]
      ciph_save = ["encrypt", "ciphertext", "save_ciphertext", "input"]
      true_ciph2diff = ["compute_cipher", "cipher_computed", "diff_ciphers", "input1"]
      exp_ciph2diff = ["encrypt", "ciphertext", "diff_ciphers", "input2"]
      diff2display = ["diff_ciphers", "output", "show_diff", "input"]
      diff2save = ["diff_ciphers", "output", "save_diff", "input"]
    [processes.dut.feedx]
      #synchronization to correctly save data when there is an error
      save1 = ["save_ciphertext", "front", "save_plaintext", "front"]
      save2 = ["save_plaintext", "front", "save_ciphertext", "front"]
      #save3 = ["save_diff", "save_plaintext,save_ciphertext"]

  [processes.triggy_setup]
    process = ["triggy_test", "triggy_conf0", "triggy_set_width0"]
    resume_kind = "start_over"
    [processes.triggy_setup.links]
      #reset
      test2conf = ["triggy_test", "triggy_conf0"]
      conf2width = ["triggy_conf0", "triggy_set_width0"]


  [processes.reset_dut]
    process = [#"triggy_imm0", "rpi3_welcome", "validate",
              # "replicate",
              "generate_key", "set_key", "set_round", "generate_pt", "encrypt", "save_plaintext",
              "save_ciphertext", "compute_cipher", "diff_ciphers", "show_diff", "save_diff"]
    count = 2000000
    resume_kind = "resume"
    [processes.reset_dut.links]
      #reset
      reset2welcome = ["triggy_imm0", "rpi3_welcome"]
      welcome2validate = ["rpi3_welcome", "validate"]
      #key init
      validate = ["validate", "generate_key"]
      # validate = ["validate", "replicate"]
      # rep2genk = ["replicate", "generate_key"]
      sendkey = ["generate_key", "key", "set_key", "key"]
      key2round = ["set_key", "set_round"]
      #dut
      round2pt = ["set_round", "generate_pt"]
      plain_save = ["generate_pt", "plaintext", "save_plaintext", "input"]
      ciph_save = ["encrypt", "ciphertext", "save_ciphertext", "input"]
      true_ciph2diff = ["compute_cipher", "cipher_computed", "diff_ciphers", "input1"]
      exp_ciph2diff = ["encrypt", "ciphertext", "diff_ciphers", "input2"]
      diff2display = ["diff_ciphers", "output", "show_diff", "input"]
      diff2save = ["diff_ciphers", "output", "save_diff", "input"]
    [processes.reset_dut.feedx]
      #synchronization to correctly save data when there is an error
      save1 = ["save_ciphertext", "front", "save_plaintext", "front"]
      save2 = ["save_plaintext", "front", "save_ciphertext", "front"]


[experiment]
  # entry = ["reset", "", ""]#special proc name: say where to start experiment
  # reset = ["key_init", "", "start_over"]
  # key_init = ["dut", "", "start_over"]
  # dut = ["", "reset", "resume"]
  #
  # dut = ["", "", "resume"]

  entry = ["triggy_setup", ""]
  triggy_setup = ["reset_dut", ""]
  reset_dut = ["", "reset_dut"]
