# @Author: Lashermes Ronan <ronan>
# @Date:   02-05-2017
# @Email:  ronan.lashermes@inria.fr
# @Last modified by:   ronan
# @Last modified time: 02-05-2017
# @License: MIT


key = "000102030405060708090a0b0c0d0e0f"
[modules]
  [modules.stm32]
    type = "toml_device"
    toml_files = ["STM32VLDISCOVERY", "AES128", "CIPHER_PROTOCOL1", "OPENOCD"]

  [modules.freqgen]
    type = "toml_device"
    toml_files = ["81160A"]

  [modules.text_gen]
    type = "vec_u8_generator"
    generator_type = "random"
    len = 16
    output_name = "plaintext"

  [modules.status_saver]
    type = "map_saver"
    vars = ["X", "Y", "FREQ", "diff"]
    # format = ["dv:m", "dv:m", "dv:Hz", "hexstring"]
    format = ["f64", "f64", "f64", "hexstring"]
    path = "results.csv"
    creation_mode = "overwrite"

  [modules.display_diff]
    type = "console_display"
    text = "Diff: "
    format = "hexstring"
    input_name = "diff"

  [modules.aes]
    type = "aes"
    output_name = "cipher_computed"

  [modules.diff]
    type = "diff"
    output_name = "diff"
    input_name1 = "ciphertext"
    input_name2 = "cipher_computed"

  [modules.xystage]
    type = "toml_device"
    toml_files = "SMC100"

  [modules.xstepper]
    type = "stepper"
    step_type = "linear"
    min = "0mm"
    max = "0mm"
    step = "1mm"
    # step_type = "fixed"
    # value = "-4mm"

  [modules.ystepper]
    type = "stepper"
    step_type = "snake"
    min = "5mm"
    max = "6mm"
    step = "1mm"
    # step_type = "fixed"
    # value = "4mm"

  [modules.freqstepper]
    type = "stepper"
    step_type = "linear"
    min = "96MHz"
    max = "288MHz"
    step = "6MHz"

  [modules.freqider]
    type = "identity"
    output_name = "FREQ"

  [modules.xider]
    type = "identity"
    output_name = "X"

  [modules.yider]
    type = "identity"
    output_name = "Y"

  [modules.repider]
    type = "identity"
    output_name = "EXE"

  [modules.xpoller]
    type = "poller"
    toml_files = "SMC100"
    command = "getXpos"
    poll_delay = "200ms"
    error_distance = "1um"

  [modules.ypoller]
    type = "poller"
    toml_files = "SMC100"
    command = "getYpos"
    poll_delay = "200ms"
    error_distance = "1um"

  [modules.replicator]
    type = "replicator"
    replication_factor = 150

  [modules.crash_results]
    type = "map_saver"
    vars = ["X", "Y","FREQ"]
    #format = ["dv:m", "dv:m"]
    format = ["f64", "f64","f64"]
    path = "crashs.csv"
    creation_mode = "overwrite"

[actors]
  validate = ["stm32", "test"]
  stm32_welcome = ["stm32", "welcome"]
  generate_pt = ["text_gen", "gen"]
  stm32_reset = ["stm32", "reset"]
  encrypt = ["stm32", "fast", "post_tempo=100ms"]
  set_key = ["stm32", "set_key"]
  set_round = ["stm32", "set_round"]
  show_diff = ["display_diff", "display"]
  diff_cipher = ["diff", "diff"]#, "not_zero"]
  save_status = ["status_saver", "save"]
  compute_cipher = ["aes", "encrypt"]
  setfreq = ["freqgen", "set_frequency"]
  getfreq = ["freqgen", "get_frequency"]

  xystage_test = ["xystage", "test"]
  xystage_reset = ["xystage", "reset", "post_tempo=2s"]
  xystage_homing = ["xystage", "homing", "post_tempo=5s"]
  xstep = ["xstepper", "gen"]
  ystep = ["ystepper", "gen"]
  freqstep = ["freqstepper", "gen"]
  setx = ["xystage", "moveX"]
  getx = ["xystage", "getXpos"]
  gety = ["xystage", "getYpos"]
  get = ["xystage", "get"]
  sety = ["xystage", "moveY"]
  xpoll = ["xpoller", "poll"]
  ypoll = ["ypoller", "poll"]
  xid = ["xider", "replicate", "wait_validation"]
  yid = ["yider", "replicate", "wait_validation"]
  freqid = ["freqider", "replicate", "wait_validation"]
  repid = ["repider", "replicate", "wait_validation"]

  rep = ["replicator", "replicate"]

  save_crash_result = ["crash_results", "save"]

[constants]
  [constants.set_key]
    key = "$(key:hexstring)"

  [constants.compute_cipher]
    key = "$(key:hexstring)"

  [constants.set_round]
    round = 10

[processes]
   [processes.reset]
    process = ["stm32_reset", "stm32_welcome", "validate", "xystage_test", "xystage_homing"]
    [processes.reset.links]
      reset2welcome = ["stm32_reset", "stm32_welcome"]
      welcome2validate = ["stm32_welcome", "validate"]
      test2home = ["xystage_test", "xystage_homing"]

  [processes.init]
    process = ["set_key", "set_round"]
    [processes.init.links]
    key2round = ["set_key", "set_round"]

  [processes.execute]
    process = ["xstep", "xid", "setx", "xpoll", "ystep", "freqstep", "freqid",
      "yid", "sety", "ypoll", "rep", "encrypt", "repid", "compute_cipher",
      "generate_pt", "diff_cipher", "save_status", "show_diff","getx", "gety",
      "setfreq", "getfreq"]
    count = 1
    resume_kind = "resume_skip"
    # autolinks = false
    [processes.execute.links]
      xstep2id = ["xstep", "output", "xid", "input"]
      xid2feed = ["xid", "X", "setx", "X"]
      xid2poll = ["xid", "X", "xpoll", "X"]
      x2y = ["xpoll", "output", "ystep", "sync"]
      ystep2id = ["ystep", "output", "yid", "input"]
      yid2feed = ["yid", "Y", "sety", "Y"]
      yid2poll = ["yid", "Y", "ypoll", "Y"]
      y2freqstep = ["ypoll", "output", "freqstep", "sync"]
      freqstep2id = ["freqstep", "output", "freqid", "input"]
      freq2batch = ["freqid", "FREQ", "rep", "input"]
      setfreq = ["freqid", "FREQ", "setfreq", "FREQ"]

      rep2repid = ["rep", "output", "repid", "input"]
      rep2start = ["repid", "EXE", "generate_pt", "sync"]
      rep2getx = ["repid", "EXE", "getx", "sync"]
      rep2gety = ["repid", "EXE", "gety", "sync"]
      rep2getfreq = ["repid", "EXE", "getfreq", "sync"]
      getx2save = ["getx", "X", "save_status", "X"]
      gety2save = ["gety", "Y", "save_status", "Y"]
      getfreq2save = ["getfreq", "FREQ", "save_status", "FREQ"]

  [processes.crash_report]
    process = ["getx", "gety","getfreq", "save_crash_result"]
    resume_kind = "persistent"

[experiment]
  entry = ["reset", ""]#special proc name: say where to start experiment
  reset = ["init", ""]
  init = ["execute", ""]
  execute = ["", "crash_report"]
  crash_report = ["reset", ""]
