# @Author: Lashermes Ronan <ronan>
# @Date:   02-05-2017
# @Email:  ronan.lashermes@inria.fr
# @Last modified by:   ronan
# @Last modified time: 02-05-2017
# @License: MIT

plaintext = "000102030405060708090a0b0c0d0e0f"
key = "00112233445566778899aabbccddeeff"
correct_cipher = "279fb74a7572135e8f9b8ef6d1eee003"

# plaintext = "00112233445566778899aabbccddeeff"
# key = "000102030405060708090a0b0c0d0e0f"
# correct_cipher = "69c4e0d86a7b0430d8cdb78070b4c55a"
[modules]
  [modules.stm32]
    type = "toml_device"
    toml_files = ["STM32VLDISCOVERY", "AES128", "CIPHER_PROTOCOL1", "OPENOCD", "BO"]

  [modules.cipher_saver]
    type = "map_saver"
    path = "ciphertexts.csv"
    creation_mode = "overwrite"
    vars = ["DELAY", "ciphertext"]
    format = ["f64", "hexstring"]

  [modules.display_diff]
    type = "console_display"
    text = "Diff: "
    input_name = "diff"
    format = "hexstring"

  [modules.diff]
    type = "diff"
    output_name = "diff"
    input_name1 = "ciphertext"
    input_name2 = "correct_cipher"

  [modules.delay_gen]
    type = "stepper"
    step_type = "linear"
    min = "1.90us" # "0.0us"
    max = "2.08us" # 228 us" # "0.4us"
    step = "40ns"
    output_name = "DELAY"

  [modules.replicator]
    type = "replicator"
    replication_factor = 500

  [modules.33509B]
    type = "toml_device"
    toml_files = "33509B"

  [modules.xystage]
    type = "toml_device"
    toml_files = "SMC100"

  [modules.xstepper]
    type = "stepper"
    step_type = "linear"
    min = "0mm"
    max = "0mm"
    step = "1mm"
    # step_type = "fixed"
    # value = "-4mm"

  [modules.ystepper]
    type = "stepper"
    step_type = "snake"
    min = "5mm"
    max = "6mm"
    step = "1mm"
    # step_type = "fixed"
    # value = "4mm"

  [modules.xider]
    type = "identity"
    output_name = "X"

  [modules.yider]
    type = "identity"
    output_name = "Y"

  [modules.xpoller]
    type = "poller"
    toml_files = "SMC100"
    command = "getXpos"
    poll_delay = "200ms"
    error_distance = "1um"

  [modules.ypoller]
    type = "poller"
    toml_files = "SMC100"
    command = "getYpos"
    poll_delay = "200ms"
    error_distance = "1um"

  [modules.crash_results]
    type = "map_saver"
    vars = ["DELAY", "X", "Y"]#, "SP", "ERROR_ADD"]
    format = ["f64","f64","f64"]
    path = "crashs.csv"
    creation_mode = "overwrite"

  [modules.repider]
    type = "identity"

[actors]
  test_33509B = ["33509B", "test"]
  validate = ["stm32", "test"]
  stm32_welcome = ["stm32", "welcome"]
  stm32_reset = ["stm32", "reset"]
  encrypt = ["stm32", "fast"]
  set_key = ["stm32", "set_key"]

  go = ["stm32", "go"]
  send_key = ["stm32", "set_key"]
  send_pt = ["stm32", "set_plaintext"]
  send_get_ct = ["stm32", "get_ciphertext", "post_tempo=50ms"] #

  xystage_test = ["xystage", "test"]
  xystage_reset = ["xystage", "reset", "post_tempo=2s"]
  xystage_homing = ["xystage", "homing", "post_tempo=5s"]
  xstep = ["xstepper", "gen"]
  ystep = ["ystepper", "gen"]
  setx = ["xystage", "moveX"]
  getx = ["xystage", "getXpos"]
  gety = ["xystage", "getYpos"]
  get = ["xystage", "get"]
  sety = ["xystage", "moveY"]
  xpoll = ["xpoller", "poll"]
  ypoll = ["ypoller", "poll"]

  save_ct = ["cipher_saver", "save", "human"]
  show_diff = ["display_diff", "display", "sync_timeout=10ks"]
  diff = ["diff", "diff", "not_zero"]
  gen_delay = ["delay_gen", "gen"]
  set_delay = ["33509B", "set_delay", "post_tempo=100ms", "wait_validation"]
  get_delay = ["33509B", "get_delay"]
  save_crash_result = ["crash_results", "save", "human"]
  rep = ["replicator", "replicate"]
  repid = ["repider", "replicate", "wait_validation"]
  ocd_halt = ["stm32", "halt", "sync_timeout=10s"]
  stm32_show_mem = ["stm32", "show_mem", "sync_timeout=10s"]

[constants]
  [constants.diff]
    correct_cipher = "$(correct_cipher:hex)"

  [constants.set_key]
    key = "$(key:hex)"

  [constants.encrypt]
    plaintext = "$(plaintext:hex)"

  [constants.send_pt]
    plaintext = "$(plaintext:hex)"

  [constants.stm32_show_mem]
    MEM_CNT = 8

[processes]
   [processes.reset]
    process = ["stm32"]
