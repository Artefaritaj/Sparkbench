use rouille::{self, /*Request,*/ Response};
use serde_json::Value;
use failure::Error;
// use std::error::Error as std_error;

use errors::*;

use libmaj::*;
use core_lib::{Core, Experiment, /*ChannelMode*/};

use std::sync::{Arc, Mutex};


fn init_sparkbench() -> Result<Core, Error> {
    let app_data = OwnedAppInfo { name: format!("sparkbench-cli"), author: format!("Artefaritaj")};
    let config_dir = get_app_root(AppDataType::UserConfig, &app_data).map_err(|e|format_err!("{}", e))?;
    Core::setup(&config_dir)
}

fn get_json_data(json: &Value, key: &str) -> Result<String, JsonGetError> {
    match json[key].as_str() {
        Some(s) => Ok(s.to_string()),
        None => Err(JsonGetError::new(key))
    }
}

pub fn run_server() -> Result<(), Error> {
    let core = Arc::new(Mutex::new(init_sparkbench()?));

    rouille::start_server("0.0.0.0:8080", move |request| {

        println!("New sparkbench-server request...");
        
        let json: Value = try_or_400!(rouille::input::json_input(request));

        let command = try_or_400!(get_json_data(&json, "command"));

        match command.as_str() {
            "where" => {
                where_command(&core)
            },
            "database" => {
                database_command(json, &core)
            },
            "experiment" => {
                experiment_command(json, &core)
            },
            _ => Response::text(format!("Command unknown: {}", command))
        }
       
    });

    // Ok(())
}

fn where_command(core: &Arc<Mutex<Core>>) -> Response {
    let core_guard = core.lock().unwrap();
    let bench_files = format!("{}", core_guard.get_toml_manager().get_folder_path().display());

    println!("where -> {}", bench_files);
    
    let response_json = json!({
        "command": "where",
        "path": bench_files,
    });
    Response::text(response_json.to_string())
}

fn database_command(json: Value, core: &Arc<Mutex<Core>>) -> Response {

    let option = try_or_400!(get_json_data(&json, "option"));

    match option.as_str() {
        "path" => database_path_command(json, core),
        _ => Response::text(format!("Command unknown: 'database.{}'", option))
    }
}

fn database_path_command(json: Value, core: &Arc<Mutex<Core>>) -> Response {
    let core_guard = core.lock().unwrap();
    let to_find = try_or_400!(get_json_data(&json, "filename"));
    let path = try_or_400!(core_guard.get_toml_path_by_name(&to_find)
                        .map_err(|f|f.compat()));

    let path_str = try_or_400!(path.to_str()
                        .and_then(|s|Some(s.to_owned()))
                        .ok_or(format_err!("Cannot read path").compat()));

    println!("Path of {} -> {}", to_find, path_str);

    let response_json = json!({
        "command": "database",
        "path": path_str,
    });
    Response::text(response_json.to_string())
}

fn experiment_command(json: Value, core: &Arc<Mutex<Core>>) -> Response {
    let mut core_guard = core.lock().unwrap();
    let filename = try_or_400!(get_json_data(&json, "load"));
    let process_opt: Option<String> = get_json_data(&json, "process").ok();
    let feedback_url_opt: Option<String> = get_json_data(&json, "feedback_url").ok();

    println!("Running {}: process {:?}, url {:?}", filename, process_opt, feedback_url_opt);

    //prepare feedback if any
    core_guard.set_feedback_url(feedback_url_opt);

    //load experiment
    let mut experiment = try_or_400!(Experiment::load_experiment(&filename, &core_guard).map_err(|f|f.compat()));

    match process_opt {
        Some(ref proc_name) => {
            try_or_400!(experiment.launch_process(proc_name, &mut core_guard, None));
        },
        None => {
            try_or_400!(experiment.launch(&mut core_guard).map_err(|f|f.compat()));
        }
    }

    let response_json = json!({
        "command": "experiment",
        "running": filename,
    });
    Response::text(response_json.to_string())
}