/*
 * Created on Tue Mar 20 2018
 *
 * Copyright (c) 2018 INRIA
 */

#[macro_use] extern crate log;

#[macro_use] extern crate failure;
extern crate fern;
extern crate chrono;

extern crate libmaj;
extern crate core_lib;

#[macro_use] extern crate rouille;
#[macro_use] extern crate serde_json;

use failure::Error;
use chrono::Local;

pub mod server;
pub mod errors;

fn init_logger() -> Result<(), Error> {
    //setup logging
    //stdout level depends on verbosity
    let base_config = fern::Dispatch::new()
    .format(|out, message, record| {
        //out.finish(format_args!("{}[{}][{}] {}",
        out.finish(format_args!("{}[{}] {}",
            Local::now()
                .format("[%Y-%m-%d][%H:%M:%S]"),
            //record.target(),
            record.level(),
            message))
    });

    let stdout_log = fern::Dispatch::new()
    .level(log::LevelFilter::Info)
    .chain(std::io::stdout());

    let log_chain = base_config
    .chain(stdout_log)
    .apply()
    .map_err(|e|format_err!("{}", e))?;

    Ok(log_chain)
}



fn run() -> Result<(), Error> {
    init_logger()?;

    info!("Launching server...");

    server::run_server()?;

    info!("Leaving server");

    Ok(())
}


fn main() {
    if let Err(e) = run() {
        println!("Application error: {}", e);
    }
}
