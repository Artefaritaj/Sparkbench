/*
 * Created on Tue Mar 20 2018
 *
 * Copyright (c) 2018 INRIA
 */


use std::fmt;
use std::error::Error;


#[derive(Debug)]
pub struct CommandError {
    cmd_name: String
}


impl CommandError {
    pub fn new(cmd_name: &str) -> CommandError {
        CommandError { cmd_name: cmd_name.to_string() }
    }
}

impl fmt::Display for CommandError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} is not a supported command", self.cmd_name)
    }
}

impl Error for CommandError {
    fn description(&self) -> &str {
        "Unsupported command"
    }
}


#[derive(Debug)]
pub struct JsonGetError {
    key: String
}


impl JsonGetError {
    pub fn new(key: &str) -> JsonGetError {
        JsonGetError { key: key.to_string() }
    }
}

impl fmt::Display for JsonGetError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} key is not found in json value", self.key)
    }
}

impl Error for JsonGetError {
    fn description(&self) -> &str {
        "Key not found"
    }
}