# Commands, Request and Response formats

## Where

Return where benchfiles are located.

### Request

```json
{
    "command": "where"
}
```

### Response

```json
{
    "command": "where",
    "path": path
}
```

## Database

Query the configuration files database.

### Configuration file location

Ask where is located the configuration file with name **filename**.

#### Request

```json
{
    "command": "database",
    "option": "path",
    "filename": filename
}
```

### Response

```json
{
    "command": "database",
    "path": path,
}
```

## Experiment

Run an experiment.

### Run a whole experiment

#### Request

To run the whole experiment
```json
{
    "command": "experiment",
    "load": filename,
    *"process": processname,*
    *"feedback_url": url,*
}
```


### Response

```json
{
    "command": "experiment",
    "running": filename
}
```