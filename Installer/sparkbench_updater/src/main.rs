extern crate libmaj;
extern crate base64;
extern crate fern;
extern crate log;
extern crate chrono;
#[macro_use] extern crate text_io;

use libmaj::{Updater, Packer};
use chrono::Local;
use base64::decode;

use std::process::Command;

const ENDPOINT: &str = "https://gitlab.com/Artefaritaj/Sparkbench/raw/master/Releases/Linux/Packages/versions.json";
const PUB_KEY_B64: &str = "T/VvFDwq1iEO2Ur5LFPGgWNMNDkvERpW13ieYtCISR0=";
const UPDATE_POLICY: &str = "~0";
// const THIS_VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    if let Err(e) = run_main() {
        println!("Error while running updater: {}", e.to_string());
    }
}

fn run_main() -> Result<(), String> {
    prepare_log(3)?;

    if let Ok(this_version) = get_sparkbench_cli_version() {
        match run_self_update(&this_version) {
            Ok(false) => {
                println!("The app is already up-to-date.");
                Ok(())
            },
            Err(e) => Err(format!("Error while updating: {}", e.to_string())),
            _ => Ok(())
        }
    }
    else {
        println!("sparkbench-cli has not been found on your computer. Do you want to install it? Y/n");

        let answer: String = read!("{}\n");

        if answer == "Y" {//web install
            println!("installing...");
            let pub_key = decode(PUB_KEY_B64).map_err(|e|e.to_string())?;
            let version = Packer::install_remote_version(ENDPOINT, &pub_key, true, "~0").map_err(|e|e.to_string())?;
            println!("sparkbench-cli {} has been installed.", version);
        }
        else {
            println!("Cancelling installation.");
        }

        Ok(())
    }
}

fn get_sparkbench_cli_version() -> Result<String, String> {
    let version_output = Command::new("sparkbench-cli")
            .arg("--version")
            .output().map_err(|_|format!("sparkbench-cli is not installed"))?;

    if version_output.status.success() {
        let output_str = String::from_utf8_lossy(&version_output.stdout).to_string();
        let version = output_str.trim().replace("sparkbench-cli ", "");
        Ok(version)
    }
    else {
        Err(format!("sparkbench-cli failed to execute"))
    }
}

fn run_self_update(this_version: &str) -> Result<bool, String> {
    let pub_key = decode(PUB_KEY_B64).map_err(|e|e.to_string())?;
    let updated_version_opt = Updater::update_str(ENDPOINT, &pub_key, this_version, UPDATE_POLICY).map_err(|e|e.to_string())?;
    if let Some(updated_version) = updated_version_opt {
        println!("This app has been updated to version {}. Restart to see the changes.", updated_version);
        Ok(true)
    }
    else {
        Ok(false)
    }
}

fn prepare_log(verbosity: u64) -> Result<(), String> {
    //get verbosity
    let log_level = match verbosity {
        1 => log::LogLevelFilter::Error,
        2 => log::LogLevelFilter::Warn,
        3 => log::LogLevelFilter::Info,
        4 => log::LogLevelFilter::Debug,
        5 => log::LogLevelFilter::Trace,
        _ => log::LogLevelFilter::Off
    };

    //configure logger
    // let log_filename = format!("out.log");

    fern::Dispatch::new()
    .format(|out, message, record| {
        //out.finish(format_args!("{}[{}][{}] {}",
        out.finish(format_args!("{}[{}] {}",
            Local::now()
                .format("[%Y-%m-%d][%H:%M:%S]"),
            //record.target(),
            record.level(),
            message))
    })
    .level(log_level)
    .chain(std::io::stdout())
    // .chain(fern::log_file(log_filename).map_err(|e|e.to_string())?)
    .apply()
    .map_err(|e|e.to_string())?;

    Ok(())
}