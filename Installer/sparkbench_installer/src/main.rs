// @Author: Lashermes Ronan <ronan>
// @Date:   26-07-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-07-2017
// @License: MIT



extern crate libmaj;
extern crate base64;
#[macro_use] extern crate clap;
extern crate fern;
extern crate log;
extern crate chrono;

use libmaj::Packer;
use base64::decode;
use clap::{App, ArgMatches};
use chrono::Local;

const PUB_KEY_B64: &str = "T/VvFDwq1iEO2Ur5LFPGgWNMNDkvERpW13ieYtCISR0=";
const APP_NAME: &str = "sparkbench-cli";

fn main() {
    //load clpa interface
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();


    //The target package is included in the installer here
    let app_package = include_bytes!("../../../Releases/Linux/Packages/sparkbench-cli.0.3.0.package.tar.xz");
    //this is THE public key linked to the private key used to sign app_package

    if let Err(e) = run(matches, app_package, PUB_KEY_B64) {
        println!("Application error: {}", e.to_string());
    }
}

fn run(matches: ArgMatches, app_package: &[u8], public_key_str: &str) -> Result<(), String> {
    //setup log according to verbosity level
    let verbosity = matches.occurrences_of("verbose");
    prepare_log(verbosity)?;

    //clean option to clean user data (default = keep it)
    let keep_user_data = !matches.is_present("clean");


    if matches.is_present("uninstall") {//uninstall
        Packer::uninstall(APP_NAME, keep_user_data).map_err(|e|e.to_string())?;
    }
    else {//install
        let public_key_bytes = decode(public_key_str).map_err(|e|e.to_string())?;
        Packer::install(app_package, &public_key_bytes, keep_user_data).map_err(|e|e.to_string())?;
    }
    Ok(())
}

fn prepare_log(verbosity: u64) -> Result<(), String> {
    //get verbosity
    let log_level = match verbosity {
        1 => log::LogLevelFilter::Error,
        2 => log::LogLevelFilter::Warn,
        3 => log::LogLevelFilter::Info,
        4 => log::LogLevelFilter::Debug,
        5 => log::LogLevelFilter::Trace,
        _ => log::LogLevelFilter::Off
    };

    //configure logger
    // let log_filename = format!("out.log");

    fern::Dispatch::new()
    .format(|out, message, record| {
        //out.finish(format_args!("{}[{}][{}] {}",
        out.finish(format_args!("{}[{}] {}",
            Local::now()
                .format("[%Y-%m-%d][%H:%M:%S]"),
            //record.target(),
            record.level(),
            message))
    })
    .level(log_level)
    .chain(std::io::stdout())
    // .chain(fern::log_file(log_filename).map_err(|e|e.to_string())?)
    .apply()
    .map_err(|e|e.to_string())?;

    Ok(())
}
