Sparkbench is used to pilot an experimental bench to perform physical attacks.

## Documentation

Documentation is available on [its own website](http://artefaritaj.gitlab.io/Sparkbench/).

## Dependencies (Ubuntu)

Before installing Sparkbench, please ensure that the following dependencies are present:

```bash
sudo apt-get install curl git libpcsclite-dev libncurses5-dev libssl-dev
```

## Install

Get installer executable:
- [Linux](https://gitlab.com/Artefaritaj/Sparkbench/raw/master/Releases/Linux/Installer/sparkbench_updater)

and run it.
To update the application, run the web installer again. It will always install or update to the last version.

## Build

Developed in rust (stable). Use cargo to build.

Get rust (stable) toolchain:
```bash
curl https://sh.rustup.rs -sSf | sh
```

Clone the project:
```bash
git clone git@gitlab.com:Artefaritaj/Sparkbench.git
```

Build CLI executable:
```bash
cd Sparkbench/CLI/sparkbench-cli
cargo build --release
```

And its done!

Now move the executable :

```bash
sudo cp target/release/sparkbench-cli /usr/bin
```
