# comment if # is 1st character in the line
copy "$BINARY" "$USER_DATA/$APP_NAME"
copy "sparkbench.toml" "$USER_CONFIG/$FILENAME" "user"
copy "../../BenchFiles" "$USER_CONFIG/BenchFiles" "user"
symlink "/usr/bin/$APP_NAME" "$USER_DATA/$APP_NAME" "user"
