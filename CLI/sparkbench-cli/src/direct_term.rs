// @Author: ronan
// @Date:   19-12-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 20-12-2016

use comms::Link;
use ncurses::*; // watch for globs

use std::io::{Read, Write};
use std::char;

use failure::Error;

#[derive(Clone,Copy,Debug,PartialEq,Eq)]
pub enum TermDataDirection {
    None,
    Link2Term,
    Term2Link
}

pub struct DirectTerm {}

static PAIR_NEUTRAL: i16 = 1;
static PAIR_L2T: i16 = 2;
static PAIR_T2L: i16 = 3;

impl DirectTerm {

    fn display_dir(current_dir: TermDataDirection, byte_dir: TermDataDirection) -> bool {
        ////display direction
        if byte_dir != current_dir {
            addstr("\n");
            let n_attr = COLOR_PAIR(PAIR_NEUTRAL);
            attron(n_attr);
            match byte_dir {
                TermDataDirection::Link2Term => { addstr("-> "); },
                TermDataDirection::Term2Link => { addstr("<- "); },
                _ => {}
            }
            attroff(n_attr);
            true
        }
        else {
            false
        }
    }



    fn diplay_byte(byte_dir: TermDataDirection, hexa: bool, byte_val: u8) {

        let b_attr = match byte_dir {
            TermDataDirection::Link2Term => COLOR_PAIR(PAIR_L2T),
            TermDataDirection::Term2Link => COLOR_PAIR(PAIR_T2L),
            _ => COLOR_PAIR(PAIR_NEUTRAL)
        };
        attron(b_attr);

        //display value
        if !hexa {
            match String::from_utf8(vec![byte_val]) {
                Ok(s) => {
                    addstr(&s);
                },
                _ => {}
            }
        }
        else {
            let s = format!("{:02x} ", byte_val);
            addstr(&s);
        }

        attroff(b_attr);
    }

    ///Init ncurses terminal
    fn init_disp(hexa: bool, channel_name: &str) {
        initscr();
        timeout(10);
        noecho();

        //init colors
        start_color();
        // init_color(COLOR_BACKGROUND, 0, 150, 200);
        // init_color(COLOR_NEUTRAL, 900, 900, 900);
        // init_color(COLOR_T2L, 200, 1000, 200);
        // init_color(COLOR_L2T, 200, 200, 1000);

        init_pair(PAIR_NEUTRAL, 7, 0);
        init_pair(PAIR_T2L, 2, 0);
        init_pair(PAIR_L2T, 1, 0);

        //display instructions
        if hexa {
            addstr("Hexa mode: enter hexa decimal values, then send press [ENTER] to send to channel.\n");
        }
        // addstr("Do not forget to set a short timeout (10ms recommended) in the channel configuration.\n")
        addstr("Press [ESCAPE] to exit the application.\n");
        addstr(&format!("Channel is {}.\n", channel_name));
    }

    fn end_disp() {
        endwin();
    }

    ///Print a hexa digit (byte is ascii value for character), auto add spaces every other 2 digit
    fn print_hexa_digit(byte: u8, vec_len: usize) {
        match String::from_utf8(vec![byte]) {
            Ok(s) => {
                addstr(&s);
            },
            _ => {}
        }

        if vec_len % 2 == 0 {
            addstr(" ");
        }
    }

    //accumulate 4 bits in new u8
    fn acc_hexa_data(vec: &mut Vec<u8>, char_value: u8) -> bool {//return true if to send
        attron(COLOR_PAIR(PAIR_T2L));

        let ret = match char_value {
            v @ 48 ..= 57 => {//number
                vec.push(v-48);
                DirectTerm::print_hexa_digit(v, vec.len());
                false
            },
            v @ 65 ..= 70 => {//upper A ... F
                vec.push(v-55);//v-65+10
                DirectTerm::print_hexa_digit(v+32, vec.len()); // print lower
                false
            },
            v @ 97 ..= 102 => {//lower a ... f
                vec.push(v-87); //v-97+10
                DirectTerm::print_hexa_digit(v, vec.len());
                false
            },
            10 => {//valid signal -> send data
                addstr("! ");
                true
            },
            8 | 127 => {//back
                let back_str = format!("{}", char::from_u32(8).unwrap());
                addstr(&back_str);

                if vec.len() > 0 && vec.len() %2 == 0 {//extra delete for space separator
                    addstr(&back_str);
                }

                let _ = vec.pop();
                false
            },
            _ => {false}
        };

        attroff(COLOR_PAIR(PAIR_T2L));
        ret
    }

    //compress 2n 4bits words in n 8bits words
    fn compress_hexa_data(vec: &mut Vec<u8>) -> Vec<u8> {
        let mut res: Vec<u8> = Vec::new();

        if vec.len() % 2 == 1 {
            vec.push(0);
        }

        let l = vec.len() / 2;

        for i in 0..l {
            let val = (vec[2*i] << 4) | vec[2*i+1];
            res.push(val);
        }

        vec.clear();
        res
    }

    ///Launch the terminal
    pub fn launch(hexa_mode: bool, channel_name: &str, link: Link) -> Result<(), Error> {

        let mut direction = TermDataDirection::None;
        let mut lock = link.lock() .map_err(|e|format_err!("{}", e))?;

        //init special terminal mode (ncurses)
        DirectTerm::init_disp(hexa_mode, channel_name);

        let mut hexa_data: Vec<u8> = Vec::new();

        //loop until escape
        loop {
            //from link to term
            let mut buf = [0u8;1];
            match lock.read(&mut buf) {//read link
                Ok(c) => {
                    if c > 0 {//write to term
                        DirectTerm::display_dir(direction, TermDataDirection::Link2Term);
                        direction = TermDataDirection::Link2Term;
                        DirectTerm::diplay_byte(TermDataDirection::Link2Term, hexa_mode, buf[0]);
                    }
                },
                _ => {}//no data
            }

            //from term to link
            let ch = getch();//read term
            match ch {
                v @ 0 ..= 255 => {//ASCII
                    if v == 27 {//escape
                        break;
                    }
                    else {//send to link
                        DirectTerm::display_dir(direction, TermDataDirection::Term2Link);
                        direction = TermDataDirection::Term2Link;
                        if !hexa_mode {//text mode
                            let byte = v as u8;
                            DirectTerm::diplay_byte(TermDataDirection::Term2Link, hexa_mode, byte);

                            let buf = [byte;1];
                            lock.write(&buf) .map_err(|e|format_err!("{}", e))?;
                        }
                        else {//hexa mode
                            let byte = v as u8;

                            if DirectTerm::acc_hexa_data(&mut hexa_data, byte) {
                                let buf = &DirectTerm::compress_hexa_data(&mut hexa_data);
                                lock.write(&buf) .map_err(|e|format_err!("{}", e))?;
                            }
                        }
                    }
                },
                -1 => {},//TO
                _ => {break;}
            }

            refresh();
        }

        /* Terminate ncurses. */
        DirectTerm::end_disp();

        Ok(())
    }
}
