// @Author: ronan
// @Date:   13-12-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-07-2017


#[macro_use]
extern crate clap;
extern crate ncurses;
extern crate fern;
extern crate log;
extern crate chrono;
extern crate libmaj;

extern crate core_lib;
extern crate comms;
extern crate hconfig;
extern crate config_keys;
extern crate sp_lib;

#[macro_use] extern crate failure;

pub mod direct_term;

use clap::{App, ArgMatches};
use core_lib::{Core, Experiment, ChannelMode};
use hconfig::store::OverwriteRule;
use sp_lib::interfaces::process::DataFeed;

//use std::process;
use std::fs::File;
use std::io::{Read, Write};
use std::fs;
use std::io;
use std::collections::HashMap;
// use std::thread;

use direct_term::DirectTerm;
use chrono::Local;
use libmaj::*;

use config_keys::*;

use failure::Error;


fn main() {

    let app_data = OwnedAppInfo { name: env!("CARGO_PKG_NAME").to_string(), author: format!("Artefaritaj")};

    // The YAML file is found relative to the current file, similar to how modules are found
    let yaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(yaml).get_matches();

    if let Err(e) = run_main(matches, &app_data) {
        println!("Application error: {}", e);
    }
}



#[allow(unused_variables)]
fn run_main(matches: ArgMatches, app_data: &OwnedAppInfo) -> Result<(), Error> {
    //get verbosity
    // Vary the output based on how many times the user used the "verbose" flag
    // (i.e. 'myprog -v -v -v' or 'myprog -vvv' vs 'myprog -v'
    let verbosity = matches.occurrences_of("verbose");
    let log_verbosity = matches.occurrences_of("logverbose");

    let human = !matches.is_present("machine");


    let log_level_stdout = match verbosity {
        1 => log::LevelFilter::Error,
        2 => log::LevelFilter::Warn,
        3 => log::LevelFilter::Info,
        4 => log::LevelFilter::Debug,
        5 => log::LevelFilter::Trace,
        _ => log::LevelFilter::Off
    };

    let log_level_log = match log_verbosity {
        1 => log::LevelFilter::Error,
        2 => log::LevelFilter::Warn,
        3 => log::LevelFilter::Info,
        4 => log::LevelFilter::Debug,
        5 => log::LevelFilter::Trace,
        0 => log::LevelFilter::Info,
        _ => log::LevelFilter::Off
    };

    // println!("Log level is {}", log_level);

    //configure logger
    // let timestamp = Local::now().format("%d-%m-%Y---%H:%M:%S");
    let log_dir_res = fs::create_dir("log");
    let log_filename = format!("log/out.log");

    //remove file if exists
    let _ = fs::remove_file(&log_filename);

    //setup logging
    //stdout level depends on verbosity
    let base_config = fern::Dispatch::new()
    .format(|out, message, record| {
        //out.finish(format_args!("{}[{}][{}] {}",
        out.finish(format_args!("{}[{}] {}",
            Local::now()
                .format("[%Y-%m-%d][%H:%M:%S]"),
            //record.target(),
            record.level(),
            message))
    });

    let stdout_log = fern::Dispatch::new()
    .level(log_level_stdout)
    .chain(std::io::stdout());

    //log file is always at TRACE level
    let file_log = fern::Dispatch::new()
    .level(log_level_log)
    .chain(fern::log_file(log_filename).map_err(|e|format_err!("{}", e))?);

    let log_chain = base_config.chain(file_log).chain(stdout_log)
    .apply()
    .map_err(|e|format_err!("{}", e))?;


    //setup core
    let config_dir = get_app_root(AppDataType::UserConfig, app_data).map_err(|e|format_err!("{}", e))?;
    let mut core = Core::setup(&config_dir)?;

    //manage where subcommands
    if let Some(_) = matches.subcommand_matches("where") {
        run_where(&core, human)?;
    }

    //manage database subcommands
    if let Some(db_matches) = matches.subcommand_matches("database") {
        run_database(db_matches, &mut core, human)?;
    }

    //manage experiment subcommands
    if let Some(exp_matches) = matches.subcommand_matches("experiment") {
        run_experiment(exp_matches, &mut core)?;
    }

    //manage channel subcommands
    if let Some(ch_matches) = matches.subcommand_matches("channel") {
        run_channel(ch_matches, &mut core)?;
    }

    //manage direct subcommands
    if let Some(direct_matches) = matches.subcommand_matches("direct") {
        run_direct(direct_matches, &mut core)?;
    }

    Ok(())
}

fn run_where(core: &Core, human: bool) -> Result<(), Error> {
    let bench_files = core.get_toml_manager().get_folder_path();
    if human == true {
        println!("Bench files are located at: {}", bench_files.display());
    }
    else {
        println!("{}", bench_files.display());
    }
    Ok(())
}

fn run_database(matches: &ArgMatches, core: &mut Core, human: bool) -> Result<(), Error> {
    if matches.is_present("list") {
        if human == true {
            println!("Configuration categories list:");
        }
        
        for cat in core.get_toml_manager().get_categories() {
            println!("- {}", cat);
        }
    }

    if let Some(cat_name) = matches.value_of("available") {
        if human == true {
            println!("Configurations present for category {}:", cat_name);
        }

        for config_name in core.get_toml_manager().get_toml_names_from_category(cat_name) {
            println!("- {}", config_name);
        }
    }

    if matches.is_present("list-all") {
        if human == true {
            println!("Configurations in database:");
        }
        
        let toml_mgr = core.get_toml_manager();
        let categories = toml_mgr.get_categories();
        for cat in categories {
            println!("Category \"{}\":", cat);
            for config_name in toml_mgr.get_toml_names_from_category(&cat) {
                println!("- {}", config_name);
            }
        }
    }

    if let Some(read_file) = matches.value_of("read") {
        let path = core.get_toml_path_by_name(read_file)
                        .map_err(|_|format_err!("Configuration file {} has not been found.", read_file))?;
        let mut file = File::open(path)
                        .map_err(|e|format_err!("Error while reading configuration {}: {}", read_file, e.to_string()))?;

        let mut s = String::new();
        file.read_to_string(&mut s)
            .map_err(|e|format_err!("Failed to read configuration file {}: {}", read_file,e))?;
        println!("{}",s);
    }

    if let Some(to_find) = matches.value_of("path") {
        let path = core.get_toml_path_by_name(to_find)
                        .map_err(|_|format_err!("Configuration file {} has not been found.", to_find))?;
        println!("{}", path.display());
    }

    Ok(())
}

fn run_experiment(matches: &ArgMatches, core: &mut Core) -> Result<(), Error> {

    let mut experiment = if let Some(to_load) = matches.value_of("load") {
        Experiment::load_experiment(to_load, core)?
    }
    else {
        return Err(format_err!("No configuration file specified (-L [CONFIG NAME])"));
    };

    if let Some(values) = matches.values_of("overwrite") {
        let mut overwrites: Vec<OverwriteRule> = Vec::new();
        for ow_str in values {
            let v = OverwriteRule::from_str(ow_str)?;
            overwrites.push(v);
        }
        // let overwrites: Vec<_> = values.map(|s| OverwriteRule::from_str(s)).collect();
        experiment.set_overwrite_rules(&overwrites);
    }

    //if process specified
    let process_opt =  matches.value_of("process");

    // if matches.is_present("launch") {
        match process_opt {
            Some(proc_name) => {    experiment.launch_process(proc_name, core, None).map_err(|e|format_err!("{}", e))?;},
            None => {               experiment.launch(core)?;}
        }
    // }

    Ok(())
}

fn direct_command_mode(experiment: &mut Experiment, core: &mut Core) -> Result<(), Error> {

    //init
    experiment.prepare_direct_command_mode(core);

    println!("Direct commands mode: Enter command name and parameters then [ENTER] to validate. Type \"exit\" to quit.");
    loop {
        print!("> ");
        let _ = io::stdout().flush();
        let mut new_direct_command = String::new();
        io::stdin().read_line(&mut new_direct_command).map_err(|e|format_err!("{}", e))?;
        let trimed = new_direct_command.trim();

        if trimed == "exit" {
            break;
        }
        else {
            match execute_direct_command(trimed, experiment, core) {
                Ok(_) => {},
                Err(e) => {println!("ERROR: {}", e);}
            }
        }
    }
    Ok(())
}

fn display_datafeed(datafeed: DataFeed) {
    let mut resulting_string = String::new();
    for (var_name, vec_data) in datafeed.consume_feed().to_hashmap().iter_mut() {
        match vec_data.pop() {
            Some(d) => {
                resulting_string = format!("{} {}={}", resulting_string, var_name, d.to_string());
            },
            None => {}
        }
    }
    println!("  {}", resulting_string);
}

///execute one command given by the user
fn execute_direct_command(dcmd: &str, exp: &mut Experiment, core: &mut Core) -> Result<(), Error> {
    let splitted: Vec<_> = dcmd.split(" ").collect(); //split on spaces

    if splitted.len() < 1 {
        return Err(format_err!("No command given!"));
    }
    else {
        //get actor's name as first word
        let actor_name = splitted[0];

        //extract vars as "var=value" hashmap.
        let mut vars: HashMap<String, String> = HashMap::new();
        for i in 1..splitted.len() {
            let var_declaration = splitted[i];
            let var_split: Vec<_> = var_declaration.split(ASSIGN_SEP).collect();
            if var_split.len() != 2 {
                return Err(format_err!("Var declaration \"{}\" is incorrect.", var_declaration));
            }
            else {
                vars.insert(var_split[0].to_string(), var_split[1].to_string());
            }
        }

        let datafeed_out = exp.execute_direct_command(core, actor_name, &vars)?;
        display_datafeed(datafeed_out);
    }


    Ok(())
}

fn run_direct(matches: &ArgMatches, core: &mut Core) -> Result<(), Error> {
    let mut experiment = if let Some(to_load) = matches.value_of("load") {
        Experiment::load_experiment(to_load, core)?
    }
    else {
        return Err(format_err!("No configuration file specified (-L [CONFIG NAME])"));
    };

    if let Some(values) = matches.values_of("overwrite") {
        let mut overwrites: Vec<OverwriteRule> = Vec::new();
        for ow_str in values {
            let v = OverwriteRule::from_str(ow_str)?;
            overwrites.push(v);
        }
        // let overwrites: Vec<_> = values.map(|s| OverwriteRule::from_str(s)).collect();
        experiment.set_overwrite_rules(&overwrites);
    }

    direct_command_mode(&mut experiment, core)
}

fn run_channel(matches: &ArgMatches, core: &mut Core) -> Result<(), Error> {

    let mut channels = ChannelMode::new();
    if let Some(values) = matches.values_of("load") {
        for load_str in values {
            channels.load_from_toml(core, load_str)?;
        }
    }

    if matches.is_present("available") {
        let channels_available = channels.list_available_channels();
        println!("Channels available from specified config files:");
        for c in channels_available {
            println!("- {}", c);
        }
    }
    else {

        if let Some(values) = matches.values_of("overwrite") {
            let mut overwrites: Vec<OverwriteRule> = Vec::new();
            for ow_str in values {
                let v = OverwriteRule::from_str(ow_str)?;
                overwrites.push(v);
            }
            channels.set_overwrite_rules(&overwrites);
        }

        channels.set_timeout(Some(10));
        let hexa_mode = matches.is_present("hexa");


        if let Some(selected_chan) = matches.value_of("channel") {
            let link = channels.connect_to(selected_chan, core.get_communication_manager())?;
            DirectTerm::launch(hexa_mode, selected_chan, link)?;
        }
    }

    Ok(())
}
