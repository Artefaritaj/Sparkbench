# Communications

The *comms* lib is dedicated to the management of communication links. A comm resource (access to serial port or ethernet connection...) should be asked to the CommManager.
A connection config StoreNode must be provided and the CommManager will try to create the resource asked.
The format of the connection config is described in [Connection Configuration](./Keys/ConnectionConfig.html)
