# Layers

For communications with devices, one need to differentiate the **link** (i.e. Ethernet, Serial, ...) with the **protocol** (i.e. APDU, I2C).
This distinction is specific to our application. The PC is only able to achieve a limited number of link (Ethernet, Serial-over-USB, ...).
As a result, protocols such as I2C, SPI, etc. are only achievable as a layer over the link. For example, one can achieve an I2C link using the BUS Pirate board and use the Serial-over-USB link to communicate with the board.
