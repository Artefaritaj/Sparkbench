# CSV file

File format to store any two dimensional matrix.
On each line, a vector is stored (separator is '\n').
For each vector values are separated by a special character (separator is ',').
