# BIN format


The *.bin* format is a straightforward one. It stores traces of fixed size.

## File description

Value encoding is unsigned integer (u32) little endian.

| Description | Size (bytes) | Value |
|---|---|---|
|Magic number|4|"TBIN" (ASCII) = 0x5442494E|
|Sample encoding|4| CF encoding table|
|Trace length|4|Number of samples in one trace|
|Trace count|4|Number of traces in the file|
|Data| Trace length * Trace count * Sample byte length (depending on encoding)| The actual data|

Data is stored one complete trace after another.

## Sample encoding

| Encoding description | Data type | Sample byte length | Value in header |
|---|---|---|---|
| Error | | |0|
| Signed byte | i8 | 1 | 1 |
| Unsigned byte | u8 | 1 | 2|
| Signed short | i16 | 2 | 3|
| Unsigned short | u16 | 2 | 4|
| Signed integer | i32 | 4 | 5|
| Unsigned integer| u32 |4|6|
| Signed long| i64| 8|7|
| Unsigned long | u64| 8|8|
| Float | f32| 4|9|
| Double|f64|8|10|
