# BIN format
The *.bin* format is a straightforward one. It stores texts of fixed size.

## File description

Value encoding is unsigned integer (u32) little endian.

| Description | Size (bytes) | Value |
|---|---|---|
|Magic number|4|"MBIN" (ASCII) = 0x4D42494E|
|Message size|4|Size of each message in bytes|
|Message count|4|Number of messages in the file|
|Data| Message size * Message count| The actual data|

Data is stored one complete message after another.

# HEX format

The *.hex* format store texts as human readable data. No header is present.

## File description

Each message in an hexstring on one line.

Example:
```
12AB4E
EF123A
567A89
```

3 messages of 3 bytes.
