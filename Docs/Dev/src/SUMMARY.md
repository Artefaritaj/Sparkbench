<!--
@Author: ronan
@Date:   30-08-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 13-12-2016
@License: GPL
-->



# Summary

- [Overview](./ArchitectureOverview.md)
- [Build] (./Build/Build.md)
- [Distribute] (./Build/Distribute.md)
- [hconfig](./hconfig/Overview.md)
- [Core](./Core/Core.md)
    - [Definitions](./Core/Definitions.md)
    - [Modules](./Core/Modules.md)
    - [TOML Modules](./Core/TomlModules.md)
    - [Commands](./Core/Commands.md)
    - [Variables](./Core/Variables.md)
    - [Configuration files](./Core/ConfFiles.md)
    - [Actors](./Core/Actors.md)
    - [Experiments](./Core/Experiments.md)
- [Communications](./Communications/Communications.md)
    - [Layers](./Communications/Layers.md)
- [File formats](./FileFormats/FileFormats.md)
    - [Text files](./FileFormats/TextFormats.md)
    - [Trace files](./FileFormats/TraceFormats.md)
    - [Unified files](./FileFormats/UnifiedFormats.md)
    - [Configuration files](./FileFormats/ConfigurationFormats.md)
    - [Others](./FileFormats/OtherFormats.md)
- [GUI]()
    - [CLI](./CLI/cli.md)
- [GUI<->Server protocol](./GUIServerProtocol/intro.md)
    - [Commands](./GUIServerProtocol/command.md)
    - [Security](./GUIServerProtocol/security.md)

# Configuration keys

- [Connection configuration](./Keys/ConnectionConfig.md)
- [Openocd configuration](./Keys/OpenOCD.md)
