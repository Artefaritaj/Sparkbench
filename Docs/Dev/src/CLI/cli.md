<!--
@Author: ronan
@Date:   13-12-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 20-12-2016
-->



# Sparkbench Command Line Interface (CLI)

The sparkbench CLI is a way to launch experiments with the Sparkbench platform.

The cli is composed of several subcommands:
- database
- experiment

## Database

### List all configuration files

```
sparkbench-cli database --list-all
```

### List categories

```
sparkbench-cli database --list
sparkbench-cli database -l
```

### List configurations for a given category

```
sparkbench-cli database --available experiments
sparkbench-cli database -a experiments
```

### Read a configuration file

```
sparkbench-cli database --read RPi2
sparkbench-cli database -r RPi2 > save_rpi2.toml
```

## Experiment

An experiment configuration must be loaded and launched in the same command.

```
sparkbench-cli experiment -L rpi2_encrypt --launch
```

## Channel

The channel mode open directly a communication channel with a device. Overwrite rules authorized.

### List available channels in config files RPi2 and DSOS404A

```
sparkbench-cli channel -L RPi2 DSOS404A -a
```

### Open a text-mode communication channel

The -c arg is the channel name (partial match authorized).

```
sparkbench-cli channel -L RPi2 -c RPi2
```

### Open a hexa-mode communication channel

Enter bytes values as hexadecimal digits.

```
sparkbench-cli channel -L RPi2 -c RPi2 -h
```
