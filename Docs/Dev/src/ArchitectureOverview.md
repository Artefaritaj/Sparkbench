# SparkBench Architecture Overview

## Core

The core is the component responsible for managing the bench.
It may apply transformations on data (precomputation, analysis...).
The core is implemented as a tcp server (written in rust) on top of a rust library.

It manages with plugins:
- Devices (oscilloscopes, generators...)
- Targets (smartcard, STM32, ...)
- SCA Leakage Models
- Analysis (distinguishers + data selection + key enumeration, FIA)

The different components are:
- **sp_lib**: contains the interfaces, definitions and helpers function that may be accessed by other libs.
- **algo_lib**: library of algorithms (crypto + other), mainly used as targets of analysis.
- **module_lib**: library of modules (computation elements) (leakage models, distinguishers, devices).
- **comms**: library for abstract communications (Ethernet, Serial, ISO7816/APDU).
- **hconfig**: hierarchical key-value store.
- **core_lib**: uses all other lib to simplify the bench management.
- **core_server**: server interface to core_lib.

![Dependency graph](./Figures/lib_deps.svg)
*Dependency graph for library component*

## GUI

Realized with NW.js (in javascript).
Communicate with the core (server) through a tcp channel.
Rightly impose to limit the amount of data exchanged with the core.

## Build

To activate logging, run with command:
'''
RUST_LOG=XXX cargo run
'''

Where XXX is the lowest logging level to display (error, warning, info, debug, trace).
