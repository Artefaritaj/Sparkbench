<!--
@Author: ronan
@Date:   14-09-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 14-09-2016
@License: GPL
-->



# TOML Modules

Some modules can be created declaratively (mainly for devices).
The description of the module must be written in one or several [TOML](https://github.com/toml-lang/toml) files.

The description should follow a very specific structure wich is directly mapped to a [hconfig](./hconfig/Overview.md) branch.

## When several files are used to describe a device

It can be useful to use several files for the full description of a device.
In this way, the communication protocol can be separated from the commands formats, etc.
All files are loaded at the same node in the config hierarchy, i.e. the instance node.

## Instanced module

An instanced module is a module that can appear several type in the config.
Upon the creation of this module, a unique identifier (UID) is assigned which is the handle to this instance.

All instanced modules are children of the **.instanced** node in the config hierarchy.
For example, I want to load a new device in the hierarchy.
By calling the appropriate *core* method, the file configuration is loaded at node key **.instanced.1dfa2dd251bd3dd4** where **1dfa2dd251bd3dd4** is the UID for this module.
If I want to add additionnal information (from another TOML file) to this instance I must provide the handle **1dfa2dd251bd3dd4** to the core as the key to where to load the new address.

## Imposed structure

The hierarchy from the UID (in our example **1dfa2dd251bd3dd4**) must follow the hereby described structure.

**instanced.$UID.connections.$Connection_NAME** is the key for describing a new connection (serial, ethernet, ...).
The main connection with the targeted application should be named **device_connect**.
Several connections may be described, for example a serial connection with the app and an ethernet connection with the openocd server managing the device.

**(...).$Connection_NAME.connection_config** is the [connection config node](./Keys/ConnectionConfig.html), cf the [linked document](./Keys/ConnectionConfig.html).

**(...).$Connection_NAME.cmds** is the list of available commands for this device.

**instanced.$UID.vars** must contains the list of variables for the device.
