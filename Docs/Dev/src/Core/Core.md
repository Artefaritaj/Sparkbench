# Core

The core contains all the logic necessary to produce and analyze the data.
The data is composed of a succession of execution parameters and results.

For each execution, an *horizontal* chain of modules produces or enriches the data.
An analysis combining data from several executions is said to be *vertical*.
