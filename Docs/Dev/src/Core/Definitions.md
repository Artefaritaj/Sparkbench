# Definitions

To perform an analysis, data from several executions of the target must be gathered.
Such a set of **executions** or **experiments** is called a **campaign**.

Sometimes several dependant or independant **campaigns** are to be performed. Such a set of campaign is called an **evaluation**.

Each **experiment** is described by a graph (DAG) of [**modules**](./Core/Modules.html) specifying the flow of information.

Standard analysis (e.g. CPA) are stored as predefined graph that the user can customize afterwards.
