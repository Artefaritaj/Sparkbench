<!--
@Author: ronan
@Date:   15-09-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 20-12-2016
-->



# Process

The process corresponds to the dynamic behavior of the application. In particular, it includes the logic for threade tasks and the message passing between them.

## Actors

An actor is an independant process task able to manage its own memory and communicating with other actors. It should be dedicated to one specific task only.
See [Actors](./Core/Actors.html) for more precisions.
