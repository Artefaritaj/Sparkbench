<!--
@Author: ronan
@Date:   15-09-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 04-10-2016
@License: GPL
-->

# Variables

Variables are used to fill, extract and format informations in commands.
There are several type of variables detailled below.

## Fixed size

The size is fixed and known in advance.
The size is specified with the **len** keyword.
The len should take into account the type. The byte len of (len=16,type=u8) and (len=4,type=u32) is the same.

```
[instanced.42800e812bd7ba7f.vars.CIPHERTEXT]
len = 16
type = "u8"
```

## Terminated

Some variables end only on a specific sequence of data. These are **termination** variables.

```
[instanced.42800e812bd7ba7f.vars.TEST]
termination = "\n"
type = "String"
```

## Type

Upon reception all data is seen as bytes.
After reception, the byte vector can be converted if possible to the specified type.
Supported types are:
- "string" or "String": an ASCII characters string
- "u8"
- "i8",
- "u16",
- "i16",
- "u32",
- "i32",
- "u64",
- "i64"
