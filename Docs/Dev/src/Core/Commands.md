<!--
@Author: ronan
@Date:   14-09-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 20-10-2016
@License: MIT
-->



# Commands

A command is a sequence of byte to send to a device. The command does not deal with how the byte are transmitted but only what bytes are transmitted.
A command can be described in a TOML file with a couple of strings for (send, receive).

```
[(...).vars.CIPHERTEXT]
type = "u8"
len = 16

[(...).cmds]
get_ciphertext = ["c", "$(CIPHERTEXT)"] # get ciphertext
fast = ["f$(PLAINTEXT)", "$(CIPHERTEXT)"] # send plaintext, expect ciphertext
```

The previous definition describes a command *get_ciphertext*.
To get a ciphertext, send the 'c' ASCII character and expect something to fill the *CIPHERTEXT* variable (i.e. 16 bytes).

Commands description strings can mix any sequence of:
- ASCII characters: "reset".
- Byte sequence: "#(0x20, 34)" corresponds to bytes 20 in hexadecimal followed by 34 in decimal.
- Self reference: "$(key)" or "$(key:convert)". Key may be a variable, or any valid key in module config (ex: $(CIPHERTEXT.len:u8)). Convert is a string describing a convert method to bytes (remember, we send bytes).

Accepted convert format if input is an integer (e.g. len = 16, not len = "16"):
- default is u8
- u8, i8, u16, i16, u32, i32, u64, i64: big endian byte array with fixed lenghth (ex: u32 in 4 bytes)
- string: call a to_string on the value then use ASCII character values
- hexstring, Hexstring: convert an integer value to hexa string (h for lower case letters, H for upper case letters) then use ASCII character values.
- stringX, hexstringX, HexstringX: to number is converted to decimal string and padded with '0' such that the final string lenghth is X (ex: hexstring32).

Accepted convert format if input is a string (e.g. len = "16"), the string is converted to format then to byte vector:
- default is string (string is interpreted as a vec of ASCII values)
- u8, i8, u16, i16, u32, i32, u64, i64
- f32, f64
- hexstring, Hexstring
- stringX, hexstringX, HexstringX

# Internal representation

Internally, a command is a vector of CmdPiece (each one must be convertable to a byte array eventually).
Upon creation, fixed pieces are directly converted to byte arrays.
Undefined values are stored as variable placeholder.

# Command creation

The creation process for a command is done in several step:
- (Extract command strings from store).
- Fill pieces as Data (fixed) or Variable with instance store and list of vars
- Before execution, fill variables' content
