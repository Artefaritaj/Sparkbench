<!--
@Author: ronan
@Date:   15-09-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 14-10-2016
-->



# Actors

Actors are responsible for one action.

## DataFeed

They read from a **DataFeed** and produce a new one.
A datafeed map variables names to data vector (often with one single Data in it).

## Synchronizer

The synchronizer component is used to assemble the datafeed from the different parent actors.
Once all data is here, the datafeed is pushed to the actor (trait).

## Dispatcher

Once a new datafeed has been produced, the result is dispatched to children actors according to the variables (name) they request.

## ThreadedActor

Finally these components (Synchronizer, Actor, Dispatcher) are merged into a ThreadedActor that can be executed in its own thread wich the *ThreadedActor::launch_thread* method.

![ThreadedActor diagram](./Figures/threaded_actor.svg)
*ThreadedActor diagram*

## Controller

The controller is one single element used to pilot all threaded actors together to pause or stop the process.
