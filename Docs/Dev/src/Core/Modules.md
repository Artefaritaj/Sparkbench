<!--
@Author: ronan
@Date:   22-07-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 15-09-2016
@License: GPL
-->



# Modules

Modules are elements in the Processing Graph. This graph describes the operations required for an experiment.
Modules can be:
- Devices
    - Target
    - Apparatus
    - ...
- Parameters
    - ByteVectorGenerator
    - ExplorationAxis
    - ...
- Computation elements
    - CPA
    - MIA
    - Covariance
    - Template
    - ...

## Nodes in a graph

Modules constitute a DAG.
No two outputs of the same module can share the same name.
Each module has an UID. Therefore, it is possible to write to the store with the unique key identifying uniquely one output of one module: **name@id**.

![Example DAG](./Figures/cpa_graph.svg)
*Example DAG*

As a consequence, each node must (at graph creation) be able to associate one input with the associated key. In our DAG for example, the **message** input of **target** must be associated with the key **bv@1**. As such, when the target must read the message value, it will look for the value associated with **bv@1**.


## Implementation

Each module should have its own thread.
We are using the actor model and modules communicate with each other through messages.

## Config structure

Cf [TOML Modules](./Core/TomlModules.html).

## Module interface

A module should define the following features:

|Feature|Description|
|---|---|
|UID|Unique identifier|
