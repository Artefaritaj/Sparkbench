# hconfig

This lib is used to manage a key-value store where keys are hierarichal (hence the 'h').
The store is effectively a tree with the pros and cons of this datastructure.

# StoreKey

A key is of the form "A.B.level3" where '.' is the hierarchy level separator: level3 is child of B in turn child of A.
The store has a mandatory root with key "".
As a consequence, keys "A.B" and ".A.B" are considered equivalent.

To construct an manage a key, use the StoreKey struct and its push methods.

```
//Equivalent to key "A.B.level3"
let key = StoreKey::from("A").push("B").push("level3");

//This is the same
let key2 = StoreKey::from("A.B.level3");
```

Prefer to avoid any direct use of the separator '.'.

# Data

Only a subset of type of data can be stored in the store, namely members of Data enum.

# Add/Get values

Use
```
store.store_update(StoreKey, Data)
```
to insert (or replace) a data in the store.
Use
```
store.get_value(&StoreKey)
```
to get the data corresponding to a key.
Use
```
store.get_node(&StoreKey)
```
to get the node corresponding to a key.

# TOML

The store (or part of) can easily be converted to and from TOML file.
