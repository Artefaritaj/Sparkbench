# Build

Sparkbench is mainly a rust program. To compile rust, you have to use the amazing *cargo* building tool.

## Get rust compile chain and cargo

Get *rustup*, the rust toolchain manager:
```
curl https://sh.rustup.rs -sSf | sh
```

## Dependencies

Some dependencies are required:

- libpcsc
- ...

## Build

To build a lib or a rust binary, go to the corresponding folder (e.g. *CLI/sparkbench-cli*), and use

```
cargo build
```
for a debug build and 

```
cargo build --release
```
for a release build.