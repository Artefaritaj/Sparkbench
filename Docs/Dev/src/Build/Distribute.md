# Distribute the application

At this point, the *sparkbench-cli* application is built in release mode.

## Create a packaqe

Here we create a package using the [maj](https://gitlab.com/Artefaritaj/maj/blob/master/tutorials/maj-cli.md) library.

In *CLI/sparkbench-cli*,
```
cargo-maj pack
```

Copy the package at *Releases/$OS/Packages*.

## Update the *versions.json* manifest

In the *Releases/$OS/Packages* folder, 

```
cargo-maj add-version "https://gitlab.com/Artefaritaj/Sparkbench/raw/master/Releases/Linux/Packages/sparkbench-cli.$VERSION.package.tar.xz" -i $PACKAGE_PATH

```


## Create installer

Go to *Installer/sparkbench_installer*.
Modify the *main.rs* file to point to latest binary.
Build the installer with

```
cargo build
```