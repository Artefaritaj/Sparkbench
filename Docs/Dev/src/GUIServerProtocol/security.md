# Security

To avoid connection hijacking, a simple protection scheme is put in place.
An identical password must be setup on both the server and the client.

On the server side, if 3 authentification attemps have failed, the server must shutdown.

If the password authentification succeed, a proper key exchange is performed to encrypt and authenticate all subsequent communications.
Binding to *libsodium* is used both by the server and by the client.
