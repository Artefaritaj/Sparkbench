# Commands

A command is a packet sent either by the GUI to the core_server or by the core_server to the GUI.
The only mandatory byte in a command is the first one describing the **command type**.

| Command type | First byte value | Description |
|---|---|---|
|Ping|0x01|Sent to ensure the other party is responding|
|File|0x02|A file is being sent in this command|
|Kill|0xFF|This command will kill the connection|
