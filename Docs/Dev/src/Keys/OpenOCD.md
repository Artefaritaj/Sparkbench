# Openocd

Openocd is a software establishing a link (for programming and/or debug) with an ARM device.
*sp_lib* can launch a openocd server with a configuration specific to your device.
Some of these keys (\*) must be defined in the **sparkbench.toml** configuration file as it affect the machine.
Others must be specified in device **BenchFiles** when specific to the device.

|Key|Value|Description|
|---|---|---|
|openocd.device_path|String or Array(String)|Path to the openocd script describing how to connect to the device (-f) (starting from "/scripts/...")|
|openocd.scripts_path|(\*) String|Path to the scripts folder|
|openocd.options|String or Array(String)|List of string to be put after the -c arg|
|openocd.path|(\*) String|Path to the openocd executable|
|openocd.shell_commands|String or Array(String)| Commands (as in shell commands) to be applied before server start|
