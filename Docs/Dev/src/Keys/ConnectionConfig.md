<!--
@Author: ronan
@Date:   23-09-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 23-12-2016
-->



# Connection configuration

The connection is done through the *comms* library. The connection configuration is given to the *connect* method as a StoreNode describing the desired connection.
The provided StoreNode must have the following format.

The root node key must be **connection_config** .

|Node key|Value|Description|
|---|---|---|
|connection_config.link|ethernet/serial/smartcard...|The type of link for the host computer|
|connection_config.protocol|raw/i2c/apdu/...|The format of the data sent and received from the link|
|connection_config.timeout|e.g. 1000|The timeous in ms (alternate key for the various timeouts)|
|---|---|---|
|connection_config.serial.port|e.g. /dev/ttyUSB0 or COM1| The port to connect to|
|connection_config.serial.baudrate|e.g. 115200| The serial baudrate|
|connection_config.serial.databits|7/8| The  data bits|
|connection_config.serial.stopbits|1/2| The stop bits|
|connection_config.serial.parity|odd/even/none, 1/2/0| The parity|
|connection_config.serial.flowcontrol|software/hardware/none, 1/2/0| The flow control|
|connection_config.serial.timeout|e.g. 1000| The timeout in ms|
|---|---|---|
|connection_config.ethernet.ipv4_address|e.g. 127.0.0.1| The IPV4 address|
|connection_config.ethernet.ipv6_address| an IPv6 address | The IPV6 address|
|connection_config.ethernet.port|e.g. 80| The distant port to connect to|
|connection_config.ethernet.read_timeout|e.g. 1000| The read timeout in ms|
|---|---|---|
|connection_config.smartcard.reader| String |The reader name string|
|connection_config.smartcard.protocol| String |The preferred protocol|
|connection_config.smartcard.share| String |How to share the smartcard handle|
