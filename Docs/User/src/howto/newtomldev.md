<!--
@Author: Lashermes Ronan <ronan>
@Date:   23-05-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 24-05-2017
@License: MIT
-->



# How to define a new toml_device module (described by a configuration file)

[TOML devices](./modules/toml_devices.html) is probably the single most useful module type in the Sparkbench software.
It effectively allows to create new modules on the fly from TOML configuration files.

See the [specific doc](./modules/toml_devices.html) for a more thorough description.

In this how to we will describe how to add a custom smartcard to our experiment.
Let's create a new blank toml file: *my-sc.toml*.

We have to include in this configuration file the following information:
- how to connect to the smartcard;
- what variables are required or may be produced by this module commands;
- what are the supported commands.

## Connection information

This is the connection information:
```TOML
[connections.device_connect.connection_config]
link = "smartcard"
protocol = "raw"

[connections.device_connect.connection_config.smartcard]
reader = "auto"
protocol = "auto"
share = "auto"
```

**device_connect** is the name of this connection (arbitrary name). A same module may have several connections (e.g. application connection and jtag connection for STM32).
The general connection information is what type of link is used (among serial, ethernet or smartcard).

*The protocol feature is not yet implemented. Use "raw" for now to avoid later compatibiliy issues.*
The detail of the link parameters must be specified in the
```TOML
[connections.CONNECTION_NAME.connection_config.LINK_NAME]
```
section.
Here we choose the first reader found, and T0/T1 protocol and automatic sharing parameters.

## Commands for this module

We are now going to define some commands (totally made-up), see the [Data Description Format](./configuration/modules.html) for some details:
```TOML
[cmds]
  test = ["test", "#(0x90, 0x00)"]
  select = ["#(0, 1, 2, 3)", "$(SW)"]
```

The first command send "test" over the link (i.e. the values 0x74, 0x65, 0x73, 0x74) and expect the values 0x90, 0x00 as a result. If not the process will fail.
The second command send 0x00, 0x01, 0x02, 0x03 and fill the **SW** variable with the result (see next section). This latter command produces a variable named **SW** that may be used by other actors in the process.

## Variables

The **SW** variable must be defined somewhere to be properly read.

```TOML
[vars.SW]
  type = "u8"
  byte_len = 2
```
The variable must contain 2 bytes. If they cannot be read (before a timeout, that can be changed by the *timeout* attribute), an error occurs and the process fails.
