<!--
@Author: Lashermes Ronan <ronan>
@Date:   23-05-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 24-05-2017
@License: MIT
-->



# How to launch an experiment with the CLI

The Command Line Interface (CLI) is the simple terminal interface to launch experiments. The CLI documentation can be found [here](./CLI/sparkbench-cli.html).

Warning!
All **sparkbench-cli** commands must be executed into a work folder since files will be produced with current folder as root.
*This behaviour may change in the future*

## Bench Files
The CLI works with toml files that describe experiments, devices, protocols, etc. These are called the "bench files" and may be edited to suit your need.
To locate them:
```
sparkbench-cli where
```

## List available configuration

To list all available configuration files: experiments, devices, protocols, ...
```
sparkbench-cli database --list-all
```

## Launch experiment
To launch an experiment (here named *rpi2_encrypt*) with the CLI, use the following command:
```
sparkbench-cli experiment -L rpi2_encrypt
```

## Verbosity
Optionally, you can define a console verbosity level with "-v" and a log verbosity level with "-l":

```
sparkbench-cli -v experiment -L rpi2_encrypt
```
shows only errors.


```
sparkbench-cli -vv experiment -L rpi2_encrypt
```
shows errors and warnings...

Up to
```
sparkbench-cli -vvvvv experiment -L rpi2_encrypt
```
that shows errors, warnings, infos, debugs and traces logging level.

```
sparkbench-cli -v -lll experiment -L rpi2_encrypt
```
shows errors only in the console and errors, warnings and infos in the logging file.
