# How Tos

- [How to design a new experiment](./howto/newexp.html)
- [How to launch an experiment with the CLI](./howto/cli.html)
- [How to define a new toml_device module (described by a configuration file)](./howto/newtomldev.html)
