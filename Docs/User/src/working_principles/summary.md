# Working principles

In this chapter, we we learn the basics behind the Sparkbench application and the definitions required to understand this documentation.

Sparkbench is used to pilot industrial apparatus and manage the resulting data in the context of physical attacks.
For this purpose we define the **host** computer as the computer executing the Sparkbench application that will issue commands to the different devices and ensure that everything has the appropriate timing.


## Modules
A [**module**](./modules/modules.html) represent a device or software component. It has a persistent state associated to it. One can (for some devices) connect to it (e.g. through an ethernet connection).
Examples of modules are:
  - Oscilloscope: it is a device that can be contacted through an ethernet connection.
  - Vector generator: a software component that generate byte vectors to feed to other devices.
  - ...

## Commands

**Commands** are defined per-module. A module may expose one or several commands. A persistent state is stored at the module level, as a result a command effect can depends on the given state of the module. Examples: a xy-stage module may expose commands for "set X position", "set Y position", "get XY position", ...

All module commands consists in a sending command that is sent by the host to the module and a receiving command that is sent by the module to the host.
Either one of these two commands (sending and receiving) can be empty (e.g. there is no communication).

## Actors

An actor is an instance of one command (for a given module) present in a process. It is defined globally in the experiment file.
Actors are the link between instanciated modules and commands and the processes defined globally for the experiment.
The actor must be present only once in each process (to keep DAG property). The same actor can be present in different processes.
```TOML
actor = ["module", "module command"]
actor2 = ["module2", "module command2", "attributes"]
```
Additionnally, one can define attributes for actors (e.g. to specify a timeout for the command).

## Processes

A **process** is a directed acyclic graph (DAG) of actors. It defines the order in which the actors are executed and how the data flows between these actors (data consisting in **variables**). To have a proper synchronization and since cycles are forbidden in the processes' DAG, two mechanisms can used: validations and feedbacks.
A process defines a fixed number of execution of the DAG. It has a start and an end. A process can succeed if all actors have been properly executed. Or it can fail if at least one error occured.

For example, one can define the reset process that will reset all devices in the experiment by sending the corresponding commands (instanciated with actors).

## Experiment

An **experiment** is graph of processes. It defines an entry process, i.e. the first process to be executed. Then, for each process execution, the experiment defines what is the next process to execute according to the previous process result (success or failure).

In this way, if a test process fails, the experiment can go back to the reset process and reexecute the test process afterward. If it succeed, we may execute the shutdown process instead.
