# Commands

Commands may be defined for each [module](./modules/modules.html).
Commands may require data (as variables) and may produce data (as variables).

See each module documentation to see what commands are availables, what they require and what they produce.
