# Experiment

An experiment is a succession of processes. More exactly a directed (potentially cyclic) graph of process.
A process has two possible outcomes: either success or failure.
The experiment section describes the next process depending on the process outcome.


Example:
```TOML
[experiment]
  entry = ["reset", ""] #special proc name: say where to start experiment
  reset = ["key_init", ""]
  key_init = ["dut", ""]
  dut = ["", "reset"]
```

In this experiment, the processes **reset**, **key_init** and **dut** are used.
The special process name "entry" is used to point to the starting process (*reset* in the example).

Then for each process, we have
```TOML
process_name = ["next process if success", "next process if failure"]
```

## Process names

All process names must be valid, i.e. corresponding to processes defined in the "processes" section.
Two special names are used:
 - "entry" is the process launched at start
 - "" (empty string) will interrupt the experiment if called.
