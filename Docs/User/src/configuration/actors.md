# Actors

Actors are instanciated commands, i.e. an actor is an association of a module and a command from this module that can be called from a process.
Additionnally, one can add attributes to an actor to refine its execution. Some attributes are generic (compatible with all actors), but some may be command specific.

## Declaration

Actors are declared in the "actors" section of the experiment file.
An actor is defined as a module, a command from this module and optional attributes.
```TOML
[actors]
  set_key = ["arduino", "set_key"]
  encrypt = ["arduino", "fast", "post_tempo=100ms"]
```
In the above example, two actors are defined for the same *arduino* module.
The second actor define an attribute, here a temporisation after the command (to throttle the encryption speed).
Several attributes may be defined, simply separate them by a comma ','. Example:

```TOML
  encrypt = ["arduino", "fast", "post_tempo=100ms", "pre_tempo=20ms"]
```


## Generic attributes

| Attribute name | Description | Example |
| --- | --- | --- |
| pre_tempo | Wait for the given duration before executing the command when the actor has received its data | ```"pre_tempo=200ms"``` |
| post_tempo | Wait for the given duration after executing the command | ```"post_tempo=200ms"``` |
| wait_validation | Wait the validation of this actor. A validation occurs when the possible consequences of this actor execution have all terminated. | ```wait_validation``` |
| sync_timeout | Fix the timeout duration an actor will wait its input for. If an actor does not receive all necessary input before this duration, the process fails. The value is *120s* by default. | ```sync_timeout=10ks``` |
