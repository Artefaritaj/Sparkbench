# Processes

A process is a directed acyclic graph (DAG) of actors. It describes a graph of operations to execute in order to complete.
A process may have two result, either success or failure.

Here is an example of a complex process:

```TOML
[processes.dut]
  process = ["xstep", "xid", "setx", "xpoll", "ystep",
    "yid", "sety", "ypoll", "delayid", "delaystep", "set_delay", "generate_pt", "encrypt",
    "rep", "save_plaintext", "save_ciphertext", "compute_cipher",
    "diff_ciphers", "show_diff", "save_diff"]
  autolinks = false
  count = 2
  [processes.dut.links]
    xstep2id = ["xstep", "output", "xid", "input"]
    xid2feed = ["xid", "X", "setx", "X"]
    xid2poll = ["xid", "X", "xpoll", "X"]
    x2y = ["xpoll", "output", "ystep", "sync"]
    ystep2id = ["ystep", "output", "yid", "input"]
    yid2feed = ["yid", "Y", "sety", "Y"]
    yid2poll = ["yid", "Y", "ypoll", "Y"]
    y2delstep = ["ypoll", "output", "delaystep", "sync"]

    delstep2id = ["delaystep", "output", "delayid", "input"]
    delid2setdel = ["delayid", "DELAY", "set_delay", "DELAY"]
    delid2rep = ["delayid", "sync", "rep", "input"]

    rep2genpt = ["rep", "output", "generate_pt", "sync"]

    plain2compute = ["generate_pt", "plaintext", "compute_cipher", "plaintext"]
    plain2enc = ["generate_pt", "plaintext", "encrypt", "plaintext"]
    plain_save = ["generate_pt", "plaintext", "save_plaintext", "input"]
    ciph_save = ["encrypt", "ciphertext", "save_ciphertext", "input"]
    true_ciph2diff = ["compute_cipher", "cipher_computed", "diff_ciphers", "input1"]
    exp_ciph2diff = ["encrypt", "ciphertext", "diff_ciphers", "input2"]
    diff2display = ["diff_ciphers", "output", "show_diff", "input"]
    diff2save = ["diff_ciphers", "output", "save_diff", "input"]
  [processes.dut.feedfront]
    save1 = ["save_ciphertext", "save_plaintext"]
    save2 = ["save_plaintext", "save_ciphertext"]
  [processes.dut.feedback]
    delayset = ["set_delay", "rep"]#wait for delay set before generating plaintexts
```

## process

The process key is used to specify all the actors present in this process.

## autolinks

If actors have non ambiguous names for inputs and outputs, the application is able to infer automatically the links (edges of the graph). If not, one must disable this feature with ```autolinks = false``` and specify all the links manually.

## links

A link can be specified in the *links* subsection with the syntax:
```TOML
link_name = ["source actor", "source variable", "destination actor", "destination variable", "expected number of pieces of data by destination actor"]
```
By default, the 5th parameter is set to 1 if not given.
**sync** is a special variable name used to specify an empty variable used for synchronization only.


## count

By default (without this key), each process is executed only once before terminating. But sometimes, we want the process to repeat before terminating (e.g. to have several execution successive executions). 
If *count* is set to 10 for example, the process will be executed 10 times before going (or not) to the next process.

```TOML
count = 10
```

Executions are asynchronous: the next execution does not wait for the end of the previous one but if special care is taken to ensure so.

## resume_kind

The resume_kind option (default if empty is start_over) describes the state of the process if it is called repeatedly:
- **start_over** (default): restart the process as if it is the first execution.
- **persistent**: start this process in the state it was left before *in case of success*.
- **resume**: start this process in the state it was left before (e.g. after a failure, the reset process is called then this process starts in the state triggering the failure).
- **resume_skip**: as **resume** but skip the last process execution (to avoid getting stuck with an incorrect parameters set).

```TOML
resume_kind = "resume"
```