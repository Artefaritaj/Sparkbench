# Constants

Sometimes, one wants to fix a variable to a constant value throughout the experiment.
It is possible to do so with constants: simply select the targeted actor and assign the value to the designated variable.

```TOML
key = "000102030405060708090a0b0c0d0e0f"
reset_duration = 100000

[actors]
  compute_cipher = ["aes", "encrypt"]
  triggy_set_width0 = ["triggy", "set_trig0_width"]

[constants]
  [constants.compute_cipher]
    key = "$(key:hex)"

  [constants.triggy_set_width0]
    TRIG0_WIDTH = "$(reset_duration:u32)"
```

As a consequence there is no need to setup an actor to feed the *key* variable to the *compute_cipher* actor. This variable will always have the fixed value set in the constants section.
