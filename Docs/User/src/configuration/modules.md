# Modules

Modules will always be instanciated. So all modules can be created several times if required. There will be no connection problem if conflicting connections (using the same resources) are identical (it would not make any sense otherwise).

## Data Description Syntax (DDS)

The Data Description Syntax (DDS) can be use in a lot of places: to define commands for [TOML devices](./modules/toml_devices.html) for example or in the [modules](./configuration/modules.md) definitions.

A piece of data using the DDS is always described by a string. It will usually but not in all cases (exceptions are often when only a *dollar token* is authorized) converted to a byte vector at the end.
- **"test"** raw data is interpreted as ascii bytes (here resulting in vector [0x74, 0x65, 0x73, 0x74]).
- **"#(0x45, 0X56)"**, *sharp tokens*  directly specify the byte values (here resulting in vector [0x45, 0x56]).
- **"$(other_key)"**, *dollar tokens* are references to other data defined in the configuration, they are mainly evaluated just-in-time. The key must represent either another piece of data in the configuration or a variable present in this instanciated module.

```TOML
key = "000102030405060708090a0b0c0d0e0f"

[modules]
  [modules.key_gen]
    type = "vec_u8_generator"
    generator_type = "fixed"
    value = "$(key:hexstring)"
    output_name = "key"
```

In this example, we define a constant AES key and instanciate a [vec_u8_generator](./modules/vec_u8_generator.html) module that will always send the byte vector described by the *key* hexadecimal string.

As you have perspically observed, *dollar tokens* can be a bit more complex. It can be
- **"$(key)"**: simply use the specified data.
- **"$(key:format)"** to convert the input piece of data to format. Beware: $(val:string) convert a data to a string (if it is an i64 for example), but $(val:hexstring) convert the val string to a byte vector, interpreting val as an hexadecimal string. **(WILL CHANGE IN THE FUTURE: CONFUSING)**.
- **"$(key:format:selectors)"**: selectors are used to select a few bytes from the byte vector defined by the *dollar token*. For example "$(key:hexstring:2,3,6)" is a 3 bytes vector created from the key hexstring converted to bytes and selecting the 3rd, the 4th and the 7th bytes in this vector (indices are 0 based). If the input data is already to the correct format, the notation $(key::selectors) is possible.
