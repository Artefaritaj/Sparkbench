<!--
@Author: ronan
@Date:   13-12-2016
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 20-12-2016
-->



# Sparkbench Command Line Interface (CLI)

The sparkbench CLI is a way to launch experiments with the Sparkbench platform.

The cli is composed of several subcommands:
- where
- database
- experiment
- channel
- direct

## Where

Locate the folder for bench files (all toml configurations).

```
sparkbench-cli where
```

## Database

### List all configuration files

```
sparkbench-cli database --list-all
```

### List categories

```
sparkbench-cli database --list
sparkbench-cli database -l
```

### List configurations for a given category

```
sparkbench-cli database --available experiments
sparkbench-cli database -a experiments
```

### Read a configuration file

```
sparkbench-cli database --read RPi2
sparkbench-cli database -r RPi2 > save_rpi2.toml
```

## Experiment

An experiment configuration must be loaded and launched in the same command.

```
sparkbench-cli experiment -L rpi2_encrypt
```

If you want to execute only one process in this experiment, it is possible with ```-p [PROCESS_NAME]```:

```
sparkbench-cli experiment -L rpi3_encrypt -p reset
```
This last command launch the *reset* process of the *rpi3_encrypt* experiment.

## Channel

The channel mode open directly a communication channel with a device. Overwrite rules authorized.

### List available channels in config files RPi2 and DSOS404A

```
sparkbench-cli channel -L RPi2 DSOS404A -a
```

### Open a text-mode communication channel

The -c arg is the channel name (partial match authorized).

```
sparkbench-cli channel -L RPi2 -c RPi2
```

### Open a hexa-mode communication channel

Enter bytes values as hexadecimal digits.

```
sparkbench-cli channel -L RPi2 -c RPi2 -h
```

## Direct

In direct modes, you can interact directly with actors defined in the experiment file.

```
sparkbench-cli direct -L xytester
```
opens an interactive prompt.

```
> get
  X=2mm   Y=5mm
> setx X:dv=3mm
> get
  X=3mm   Y=5mm
> move X:dv=0mm Y:dv=0mm
```

In the above example, we use actors *get*, *setx* and *move* from the xytester experiment. Each one of these actors requires or produces variables. Required variables must be fed with the syntax **name:type=value** (no space inside, space is reserved to separate variables). Name, type and value must match the variable requirements and constraints (variable name is case-sensitive).
