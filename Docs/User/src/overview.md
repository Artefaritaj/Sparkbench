# Sparkbench

## TL;DR

Software to pilot experimental benches.
- [Install](./install.html)
- [How tos](./howto/howto.html)

## Description

Sparkbench is a piece of software to pilot industrial apparatus and manage the resulting data.
It is used in the context of physical attacks, to realize experiments.

This software is composed of several bricks.
The core of the application is a library using TOML configuration files to determine what to do.
A simple Command Line Interface (CLI) is also present to properly launch the software.

This document will guide you on how to configure and use the Sparkbench software.
This software is complex, so carefully read the following documentation before starting the application.
In the first part, we will see the working principles behind this application.
We will define what are modules, processes and experiments in the Sparkbench context.
Then we will see what are the modules already present and how to use them.
The main task to design an experiemet is to correctly write the correspondng configuration.
Finally, we will learn how to launch an experiment with the CLI.
