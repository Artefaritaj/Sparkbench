# Install

To install on linux (other platforms have not been tested), download and execute the [offline installer](https://gitlab.com/Artefaritaj/Sparkbench/raw/master/Releases/Linux/Installer/sparkbench_installer) or the [online installer](https://gitlab.com/Artefaritaj/Sparkbench/raw/master/Releases/Linux/Installer/sparkbench_updater).

They can also be used to update the application.
