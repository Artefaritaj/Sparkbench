<!--
@Author: Lashermes Ronan <ronan>
@Date:   26-04-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 29-05-2017
@License: MIT
-->



# Stepper

The stepper module is used to generate data that move in steps. For example to move a XY stage.

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "stepper" ``` |
| step_type | mandatory | fixed, linear, snake, count | ``` step_type = "linear" ``` |
| min | mandatory if linear, count, snake | the value to start from | ``` min = "-20mm" ``` |
| max | mandatory if linear, count, snake | the value to go to | ``` max = "20mm" ``` |
| step | mandatory if linear, snake | value increment between 2 steps | ``` step = "1mm" ``` |
| count | mandatory if count | how many steps between min and max | ``` count = 20 ``` |
| value | mandatory if fixed | fixed value used | ``` value = "1mm" ``` |
| output_name | optional | the name of the output variable | ``` output_name = "Y" ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **gen** |  |  | *output_name* or "output" | Depends on type in *min* or *value* |  |


## Examples

```TOML
[modules]
  [modules.xstepper]
    type = "stepper"
    step_type = "linear"
    min = "-20mm"
    max = "20mm"
    step = "5mm"

[actors]
    xstep = ["xstepper", "gen"]
```
