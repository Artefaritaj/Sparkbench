<!--
@Author: Lashermes Ronan <ronan>
@Date:   20-05-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 30-05-2017
@License: MIT
-->



# Replicator

The replicator copy the data at input some specified number of times.

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "replicator" ``` |
| replication_factor | mandatory | How many copies of input data to make | ``` replication_factor = 3 ``` |
| input_name | optional | the name of the input variable | ``` input_name = "trace_segm" ``` |
| output_name | optional | the name of the output variable | ``` output_name = "trace" ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **replicate** | *input_name* or "input"  | Any  | *output_name* or "output" | Depends on type at input |  |


## Examples

```TOML
[modules]
  [modules.bacher]
    type = "replicator"
    replication_factor = "$(batch)"
    output_name = "sync"

[actors]
      create_batch = ["batcher", "replicate"]
```
