<!--
@Author: Lashermes Ronan <ronan>
@Date:   26-04-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 29-05-2017
@License: MIT
-->



# Delay Controller

Specialized delay calculator for stable timing triggering.

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "delay_controller" ``` |
| coarse_min | mandatory | the minimal delay step on coarse delay generator | ``` coarse_min = "4ns" ``` |


## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **compute** | "user_delay", "period" and "burst_count"  | Data convertible to DimVal, DimVal and u64 | "delay_coarse" and "delay_fine" | DimVal |  |

## Examples

```TOML
[modules]
  [modules.dc]
    type = "delay_controller"
    coarse_min = "4ns"

[actors]
    compute_delays = ["dc", "compute"]
```
