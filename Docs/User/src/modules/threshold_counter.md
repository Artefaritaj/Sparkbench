<!--
@Author: Lashermes Ronan <ronan>
@Date:   26-04-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 29-05-2017
@License: MIT
-->



# Thrshold Counter

Count how many times input data is over a threshold.

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "threshold_counter" ``` |
| datatype | mandatory | how to interpret the input data | ``` datatype = "Array<I8>" ``` |
| input_name | optional | the name of the input variable | ``` input_name = "data" ``` |
| output_name | optional | the name of the output variable | ``` output_name = "count" ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **count** | *input_name* and "threshold"  | Data | "count" or *output_name* | u64 |  |

## Examples

```TOML
[modules]
  [modules.tc]
    type = "threshold_counter"
    datatype = "Array<I8>"

[actors]
    count_over_th = ["tc", "count"]
```
