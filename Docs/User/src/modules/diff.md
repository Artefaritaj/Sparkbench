<!--
@Author: Lashermes Ronan <ronan>
@Date:   26-04-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 29-05-2017
@License: MIT
-->




# Diff

Diff is a module to compute the difference (XOR) of two byte vectors. Allow to check if a result is correct.

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "diff" ``` |
| input_name1 | optional | the name of the first input variable | ``` input_name1 = "plain1" ``` |
| input_name2 | optional | the name of the second input variable | ``` input_name2 = "plain2" ``` |
| output_name | optional | the name of the output variable | ``` output_name = "plains_diff" ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **diff** | "input1" or *input_name1* and "input2" or *input_name2*  | *u8 vectors* (SHOULD CHANGE IN THE FUTURE) | *output_name* or "output" | *u8 vector* | "not_zero" produces an output only if diff result is not zero |



## Examples

```TOML
[modules]
  [modules.diff_ciphers]
    type = "diff"

[actors]
    diff_ciphers = ["diff_ciphers", "diff", "not_zero"]
```
