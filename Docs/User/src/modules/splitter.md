<!--
@Author: Lashermes Ronan <ronan>
@Date:   20-05-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 30-05-2017
@License: MIT
-->



# Splitter

The splitter module is used to cut a vector in several smaller parts.

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "splitter" ``` |
| divide_by | mandatory if *max_size* not present | Cut the input vector in *divide_by* parts | ``` divide_by = 3 ``` |
| max_size | mandatory if *divide_by* not present | All parts at output will have *max_size* elements at the maximum | ``` max_size = 200 ``` |
| input_name | optional | the name of the input variable | ``` input_name = "trace_segm" ``` |
| output_name | optional | the name of the output variable | ``` output_name = "trace" ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **split** | *input_name* or "input"  | *U8 vector* or *F32 vector* or *Array*  | *output_name* or "output" | Depends on type at input |  |


## Examples

```TOML
[modules]
  [modules.batch_splitter]
    type = "splitter"
    divide_by = "$(batch)"

[actors]
    split = ["batch_splitter", "split"]
```
