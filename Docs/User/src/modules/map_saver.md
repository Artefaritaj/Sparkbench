<!--
@Author: Lashermes Ronan <ronan>
@Date:   29-05-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 29-05-2017
@License: MIT
-->



# Map saver

A map saver is a lot like a text saver, it stores data to a file. But unlike, text saver it can store several data per line.

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "map_saver" ``` |
| path | mandatory | path to storage file | ``` path = "plaintexts.hex" ``` |
| creation_mode | optional | overwrite, rename or append (default) | ``` creation_mode = "overwrite" ``` |
| vars | mandatory | the name of the input variables | ``` vars = ["DELAY", "status"] ``` |
| format | mandatory | the display format of the input variables | ``` format = ["dv:s", "hexstring"] ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **save** | Defined by *vars*  | Any string representable format |  | |  |



## Examples

```TOML
[modules]
  [modules.status_saver]
    type = "map_saver"
    vars = ["DELAY", "status"]
    path = "status.csv"
    creation_mode = "overwrite"

[actors]
  save_status = ["status_saver", "save", "human"]
```
