<!--
@Author: Lashermes Ronan <ronan>
@Date:   30-03-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 26-04-2017
@License: MIT
-->



# Vec U8 Generator

This module is used to generate byte vectors (random of a given size or fixed).

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "vec_u8_generator" ``` |
| generator_type | mandatory | random vector or fixed | ``` generator_type = "fixed" ``` |
| output_name | optional | the name of the output variable | ``` output_name = "plaintext" ``` |
| len | mandatory if "random" | the random vector size | ``` len = 16 ``` |
| value | mandatory if "fixed" | the value of the generated vector | ``` value = "000102030405060708090a0b0c0d0e0f" ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **gen** | (sync)  | | *output_name* or "output" | *u8 vector* | |



## Examples

```TOML
key = "000102030405060708090a0b0c0d0e0f"
[modules]
  [modules.text_gen]
    type = "vec_u8_generator"
    generator_type = "random"
    len = 16
    output_name = "plaintext"

  [modules.key_gen]
    type = "vec_u8_generator"
    generator_type = "fixed"
    value = "$(key:hex)" # vector is composed of 16 bytes given in hex format by key node.
    output_name = "key"

[actors]
  generate_pt = ["text_gen", "gen"]
  generate_key = ["key_gen", "gen"]
```
