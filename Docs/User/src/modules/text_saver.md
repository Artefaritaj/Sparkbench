<!--
@Author: Lashermes Ronan <ronan>
@Date:   29-05-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 29-05-2017
@License: MIT
-->



# Text saver

Use this module to store texts (String) into a file.
The string representation of the input data will be written (in particular, the string representation of a byte vector is an hexadecimal string).

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "text_saver" ``` |
| path | mandatory | path to storage file | ``` path = "plaintexts.hex" ``` |
| creation_mode | optional | overwrite, rename or append (default) | ``` creation_mode = "overwrite" ``` |
| input_name | optional | the name of the input variable | ``` input_name = "plaintext" ``` |
| format | optional | the format to convert the input in (default is "hexstring") | ``` format = "hexstring" ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **save** | "input" or *input_name*  | Any string representable format |  | | |



## Examples

```TOML
[modules]
  [modules.plaintext_saver]
    type = "text_saver"
    path = "plaintexts.hex"
    creation_mode = "overwrite"

[actors]
  save_plaintext = ["plaintext_saver", "save"]
```
