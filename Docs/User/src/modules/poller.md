<!--
@Author: Lashermes Ronan <ronan>
@Date:   26-04-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 29-05-2017
@License: MIT
-->




# Poller

A poller is used to block the flow until a target value has been met.
Here the target value is defined as the result of a command from a module.
The variable output to match must have the same name as the input of the poller.

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "poller" ``` |
| toml_files | mandatory | the list of files to load that constitute a module to use for this poller | ``` toml_files = "SMC100" ``` |
| command | mandatory | the name of the polling command in the previous module | ``` command = "getYpos" ``` |
| poll_delay | optional | the minimum delay between two poll commands (in seconds) | ``` poll_delay = "100ms" ``` |
| error_distance | optional | the acceptable error to finish the polling (unit must match result of poll command) | ``` error_distance = "1um" ``` |
| output_name | optional | the name of the output variable | ``` output_name = "Y" ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **poll** | *XXX* where XXX is the variable name of the poll command output  |  | *output_name* or "output" (return an empty data, i.e. a *sync*) | Same as input |  |


## Examples

```TOML
[modules]
  [modules.ypoller]
    type = "poller"
    toml_files = "SMC100"
    command = "getYpos"
    poll_delay = "100ms"
    error_distance = "1um"

[actors]
    ypoll = ["ypoller", "poll"]
```
