<!--
@Author: Lashermes Ronan <ronan>
@Date:   29-05-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 29-05-2017
@License: MIT
-->


# Bin saver

Use this module to store binary data into a file.

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "bin_saver" ``` |
| path | mandatory | path to storage file | ``` path = "traces.bini8" ``` |
| creation_mode | optional | overwrite, rename or append (default) | ``` creation_mode = "overwrite" ``` |
| input_name | optional | the name of the input variable | ``` input_name = "trace" ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **save** | "input" or *input_name*  | *u8 vector* |  | | |



## Examples

```TOML
[modules]
  [modules.traces_saver]
    type = "bin_saver"
    path = "traces.bini8"
    creation_mode = "overwrite"

[actors]
  save_traces = ["traces_saver", "save"]
```
