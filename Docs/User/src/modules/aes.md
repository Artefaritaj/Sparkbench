<!--
@Author: Lashermes Ronan <ronan>
@Date:   26-04-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 29-05-2017
@License: MIT
-->



# AES

AES perform an AES128 encryption. Use it to check results from your target.

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "aes" ``` |
| output_name | optional | the name of the output variable | ``` output_name = "ciphertext" ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **encrypt** | "plaintext" and "key"  | U8 vectors, 16 bytes | "ciphertext" or *output_name* | U8 vector, 16 bytes |  |

DECRYPT should be added

## Examples

```TOML
[modules]
  [modules.aes]
    type = "aes"
    output_name = "cipher_computed"

[actors]
    compute_cipher = ["aes", "encrypt"]
```
