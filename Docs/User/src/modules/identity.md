<!--
@Author: Lashermes Ronan <ronan>
@Date:   26-04-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 29-05-2017
@License: MIT
-->



# Identity

The identity module does nothing. It transmit the data at input to output.
It is useful when one want to convert a vector input to a sequence of output, extra useful in conjonction with *wait_validation* attribute.

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "identity" ``` |
| input_name | optional | the name of the input variable | ``` input_name = "plain" ``` |
| output_name | optional | the name of the output variable | ``` output_name = "plain" ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **replicate** | "input" or *input_name*  | Any | *output_name* or "output" | Same as input |  |



## Examples

```TOML
[modules]
  [modules.xider]
    type = "identity"
    output_name = "X"

[actors]
    xid = ["xider", "replicate", "wait_validation"]
```
