<!--
@Author: Lashermes Ronan <ronan>
@Date:   26-04-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 29-05-2017
@License: MIT
-->



# Console display

Use this module to show something to the console output (stdout).

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "console_display" ``` |
| text | optional | Prefix to add to each display | ``` text = "Ciphertext: " ``` |
| input_name | optional | the name of the input variable | ``` input_name = "ciphertext" ``` |
| format | optional | display format | ``` format = "hexstring" ``` |

## Actor interface

| Command | Input name | Input type | Output name | Output type | Specific attributes |
| --- | --- | --- | --- | --- | --- |
| **display** | "input" or *input_name*  | Any string representable data |  |  |  |



## Examples

```TOML
[modules]
  [modules.console_diff]
    type = "console_display"
    text = "Diff: "

[actors]
    show_diff = ["console_diff", "display"]
```
