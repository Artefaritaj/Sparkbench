# TOML devices

A TOML device is a module defined by a TOML configuration file.

The TOML file describes how to connect to the device, the commands specific to this devices and the associated variables.
The examples given here are real example to connect to the STM32VLDISCOVERY board.

## Connections

First one has to define the connections used to communicate with the module.

```TOML
[connections.device_connect.connection_config]
link = "serial"
protocol = "raw"

[connections.device_connect.connection_config.serial]
port = "/dev/ttyUSB0"
baudrate = 115200
databits = 8
stopbits = 1
parity = "none"
flowcontrol = "none"

[connections.openocd_connect.connection_config.openocd]
args = "-f /usr/local/share/openocd/scripts/board/stm32vldiscovery.cfg"
pre_commands = ["sudo modprobe -r uas", "sudo modprobe -r usb-storage"]
launch_command = "sudo openocd"
wait = 3000

[connections.openocd_connect.connection_config]
link = "ethernet"
protocol = "raw"
require = "openocd"

[connections.openocd_connect.connection_config.ethernet]
ipv4_address = "127.0.0.1"#localhost
port = 4444
```

The previous example describes two connections: *device_connect* and *openocd_connect*.
The first connection is a serial connection with specific parameters.

The second connection is an ethernet connection to a local telnet server. Yet the corresponding server (*openocd*) has to be launched before creating this connection.
This behavior is specified with the **require** key. The required node is looked for in the configuration and defines shell commands to execute to launch the server. First pre_commabds are executed then the launch_command with specified args. Finally, we wait *wait* milliseconds for the server to start.

## Commands

Then the commands are defined, they are attached to the connection they are sent upon.

```TOML
[connections.device_connect.cmds]
test = ["t", "STM32 AES Test OK\n"]
welcome = ["", "Shall we play a game?\n"]
go = ["g", ""] #send 'g', do not expect answer
fast = ["f$(PLAINTEXT)", "$(CIPHERTEXT)"] # send plaintext, expect ciphertext
set_plaintext = ["p$(PLAINTEXT)", ""]
get_ciphertext = ["c", "$(CIPHERTEXT)"]
set_key = ["k$(KEY)", ""]
```

Commands are named and defined by a pair of strings.
The first describes the data to send and the second the data to receive.
The strings must follow the [Data Description Syntax](./configuration/modules.html).
Example 1: above the command *test* will send the ascii character 't' to the connection *device_connect* and expect the answer 'STM32 AES Test OK\n'.
If the expected answer is not met, this command failed. The error will propagate and make the process result a failure.
Example 2: the command *fast* will send the ascii character 'f' followed by the content of the *PLAINTEXT* variable (described in the next section).
The answer must meet the constraints given by the variable *CIPHERTEXT*. This variable could then be sent to other actors.

## Variables

The variables are defined for the module in the **vars** array as described in the [variables](../configuration/variables.html) section.

## Attributes

|Attribute name|Description|
|---|---|
|timeout| Maximum time between command emission and reception. Default is *10s*.|

## Module declaration

| Argument name | Optional/mandatory | Description | Values/Examples |
| --- | --- | --- | --- |
| type | mandatory | | ``` type = "toml_device" ``` |
| toml_files | mandatory | The files to load that constitute this device | ``` toml_files = ["RPi3", "AES128", "CIPHER_PROTOCOL1"] ``` |

## Actor interface

The commands and their requirements have been defined in the previous sections.

### Example

```TOML
[modules]
  [modules.rpi3]
    type = "toml_device"
    toml_files = ["RPi3", "AES128", "CIPHER_PROTOCOL1"]

[actors]
  validate = ["rpi3", "test"]
  set_key = ["rpi3", "set_key"]
  encrypt = ["rpi3", "fast", "post_tempo=100ms"]
```
