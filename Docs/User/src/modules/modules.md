<!--
@Author: Lashermes Ronan <ronan>
@Date:   29-05-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 29-05-2017
@License: MIT
-->



# List of modules

- [AES](./modules/aes.md)
- [TOML devices](./modules/toml_devices.md)
- [Vec U8 Generator](./modules/vec_u8_generator.md)
- [Text saver](./modules/text_saver.md)
- [Bin saver](./modules/bin_saver.md)
- [Map saver](./modules/map_saver.md)
- [Diff](./modules/diff.md)
- [Console display](./modules/console_display.md)
- [Identity](./modules/identity.md)
- [Poller](./modules/poller.md)
- [Replicator](./modules/replicator.md)
- [Splitter](./modules/splitter.md)
- [Stepper](./modules/stepper.md)
- [Threshold counter](./modules/threshold_counter.md)
- [Delay controller](./modules/delay_controller.md)