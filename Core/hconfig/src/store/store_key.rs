// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 15-12-2016



use store::store_node::SEPARATOR;

use std::fmt;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct StoreKey {
    keys: Vec<String>
}

impl StoreKey {
    pub fn new() -> StoreKey {
        StoreKey { keys: Vec::new() }
    }

    pub fn from_key_parts(parts: &Vec<String>) -> StoreKey {
        StoreKey { keys: parts.clone() }
    }

    pub fn from(k: &str) -> StoreKey {
        if k == "" {
            StoreKey::new()
        }
        else {
            //remove starting "."
            let to_parse = trim_start_str(k, SEPARATOR);

            let mut v = Vec::new();
            let splitted: Vec<&str> = to_parse.split(SEPARATOR).collect();

            for s in splitted {
                v.push(s.to_string());
            }

            StoreKey { keys: v}
        }
    }

    ///When you want to chain pushes: let key2 = key.push("l1").push("l2");
    pub fn push(mut self, k: &str) -> StoreKey {

        let splitted = k.split(SEPARATOR);

        for s in splitted {
            self.keys.push(s.to_string());
        }

        self
    }

    pub fn add_prefix(&mut self, prefix: &StoreKey) {
        for kp in prefix.keys.iter() {
            self.keys.insert(0, kp.to_string());
        }
    }

    pub fn add_postfix(&mut self, postfix: &StoreKey) {
        for kp in postfix.keys.iter() {
            self.keys.push(kp.to_string());
        }
    }

    pub fn match_part_key(&self, partial_key: &StoreKey) -> bool {
        let mut start_from = 0usize;
        for key_part in partial_key.keys.iter() {

            let mut is_match = false;
            for (i, k) in self.keys.iter().enumerate() {
                if key_part == k && i >= start_from {
                    is_match = true;
                    start_from = i+1;
                    break;
                }
            }

            if !is_match {
                return false;
            }
        }

        return true;
    }

    pub fn push_in_place(&mut self, k: &str) {
        let splitted = k.split(SEPARATOR);

        for s in splitted {
            self.keys.push(s.to_string());
        }

    }

    ///Push a value to a cloned key. Let self unchanged.
    pub fn push_clone(&self, k: &str) -> StoreKey {
        let cloned = self.clone();
        cloned.push(k)
    }

    ///Get the number of hierarchical levels in this key
    pub fn depth(&self) -> usize {
        self.keys.len()
    }

    ///Queen of Hearts' favorite
    ///Can panic!
    pub fn pop_head(&mut self) -> String {
        self.keys.remove(0)
    }

    pub fn last_sub_key(&self) -> &str {
        self.keys.get(self.keys.len()-1).unwrap()
    }
}

fn trim_start_str(to_parse: &str, to_trim: &str) -> String {
    let mut to_parse_str = to_parse.to_string();
    if to_parse_str.starts_with(to_trim) {
        for _ in 0..to_trim.len() {
            to_parse_str.remove(0);
        }
    }
    to_parse_str
}

impl fmt::Display for StoreKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut to_write = String::new();
        let len = self.keys.len();
        for (i,s) in self.keys.iter().enumerate() {
            to_write.push_str(s);
            if i != (len -1) {
                to_write.push_str(SEPARATOR);
            }
        }
        write!(f, "{}", to_write)
    }
}


#[test]
fn test_store_key() {
    let k1 = StoreKey::from("Test");
    let k2 = k1.push("branch");
    assert!(k2.to_string() == "Test.branch");

    let k3 = StoreKey::from("test2.l2.l3");
    assert!(k3.depth() == 3);
    let k4 = k3.push("l4.l5");
    assert!(k4.depth() == 5);

    let partial = StoreKey::from("rpi2.port");
    let full = StoreKey::from("module.port.rpi2.config.serial.port");
    assert!(full.match_part_key(&partial));
}
