/*
 * Author:   ronan.lashermes@inria.fr
 * Creation: Thu Dec 07 2017
 *
 * Copyright (c) 2017 INRIA
 */

use store::DataType;
use failure::Error;

#[derive(Debug,Clone,PartialEq,Eq,Hash)]
pub enum CompoundDataType {
    None,
    Raw, //Vec<u8>
    Singleton(DataType),//.
    Array(DataType),//Vec<.>
    Table(DataType)//BTreeMap<String, .>
}

impl From<DataType> for CompoundDataType {
    fn from(dt: DataType) -> CompoundDataType {
        CompoundDataType::Singleton(dt)
    }
}

impl Into<DataType> for CompoundDataType {
    fn into(self) -> DataType {
        match self {
            CompoundDataType::None => DataType::None,
            CompoundDataType::Raw => DataType::Raw,
            CompoundDataType::Singleton(dt) => dt,
            CompoundDataType::Array(dt) => dt,
            CompoundDataType::Table(dt) => dt
        }
    }
}

impl CompoundDataType {
     pub fn parse(s: &str) -> Result<CompoundDataType, Error> {
         let matcher = |c: char| {  c == '<' || c == '>' };

         let splitted: Vec<&str> = s.split(matcher).filter(|s| !s.is_empty()).collect();

         if splitted.len() == 0 {
             return Err(format_err!("Cannot parse {} as compound datatype", s));
         }

         match splitted[0] {
            "Raw"|"raw"|"" => Ok(CompoundDataType::Raw),
            "none"|"None" => Ok(CompoundDataType::None),
            "Array"|"array" => {
                if splitted.len() < 2 {
                    Err(format_err!("Cannot parse {} as Array datatype", s))
                }
                else {
                    Ok(CompoundDataType::Array(DataType::parse(splitted[1])?))
                }
            },
            "Table"|"table" => {
                if splitted.len() < 2 {
                    Err(format_err!("Cannot parse {} as Table datatype", s))
                }
                else {
                    Ok(CompoundDataType::Table(DataType::parse(splitted[1])?))
                }
            },
            "Singleton"|"Single"|"singleton"|"single" => {
                if splitted.len() < 2 {
                    Err(format_err!("Cannot parse {} as Singleton datatype", s))
                }
                else {
                    Ok(CompoundDataType::Singleton(DataType::parse(splitted[1])?))
                }
            },
            s => {
                Ok(CompoundDataType::Singleton(DataType::parse(s)?))
            }
         }
     }
}

#[test]
fn test_parse() {
    let correct = vec![ "u8", 
                        "Singleton<u8>", 
                        "Array<i32>", 
                        "String",
                        "raw",
                        "Single<raw>"];
    let correct_truth = vec![   CompoundDataType::Singleton(DataType::U8),
                                CompoundDataType::Singleton(DataType::U8),
                                CompoundDataType::Array(DataType::I32),
                                CompoundDataType::Singleton(DataType::String(None)),
                                CompoundDataType::Raw,
                                CompoundDataType::Singleton(DataType::Raw)];

    for (i, s) in correct.iter().enumerate() {
        let to_test = CompoundDataType::parse(s).unwrap();
        assert_eq!(to_test, correct_truth[i]);
    }
}