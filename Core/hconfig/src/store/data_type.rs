// @Author: ronan
// @Date:   17-11-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 17-11-2016

use std::str::FromStr;

use dimval::Prefix;
use failure::Error;

/// The DataType is the type annotation link with a piece of data.
#[derive(Debug,Clone,PartialEq,Eq,Hash)]
pub enum DataType {
    Raw,//uninterpreted byte
    U8,
    I8,
    U16,
    I16,
    U32,
    I32,
    U64,
    I64,
    F32,
    F64,
    String(Option<usize>),//padding len
    Hexstring(bool, Option<usize>),//(upper, padding len)
    Scistring(bool),//upper
    Boolean,
    Dim(i32, String),//(exponent, unit)
    None
}

impl DataType {
    pub fn parse(s: &str) -> Result<DataType, Error> {
        match s {
            "raw"|""                                => Ok(DataType::Raw),
            "u8"|"U8"|"byte"|"uchar"                => Ok(DataType::U8),
            "i8"|"I8"|"char"                        => Ok(DataType::I8),
            "u16"|"U16"|"ushort"|"unsigned short"   => Ok(DataType::U16),
            "i16"|"I16"|"short"                     => Ok(DataType::I16),
            "u32"|"U32"|"uint"|"unsigned int"       => Ok(DataType::U32),
            "i32"|"I32"|"int"                       => Ok(DataType::I32),
            "u64"|"U64"|"ulong"|"unsigned long"     => Ok(DataType::U64),
            "i64"|"I64"|"long"|"integer"            => Ok(DataType::I64),
            "f32"|"F32"|"float"                     => Ok(DataType::F32),
            "f64"|"F64"|"double"                    => Ok(DataType::F64),
            "string"|"String"|"str"                 => Ok(DataType::String(None)),
            "hexstring"|"hex"|"hexstr"              => Ok(DataType::Hexstring(false, None)),
            "Hexstring"|"Hex"|"Hexstr"              => Ok(DataType::Hexstring(true, None)),
            "e"|"sci"|"scientific"                  => Ok(DataType::Scistring(false)),
            "E"|"Sci"|"Scientific"                  => Ok(DataType::Scistring(true)),
            "bool"|"Bool"|"boolean"|"Boolean"       => Ok(DataType::Boolean),
            x if x.starts_with("string") && x.len() > 6 => {
                let len_str = &x[6..x.len()];
                let len = try!(usize::from_str(len_str) .map_err(|e|format_err!("{}",e)));
                Ok(DataType::String(Some(len)))
            },
            x if x.starts_with("hexstring") && x.len() > 9 => {
                let len_str = &x[9..x.len()];
                let len = try!(usize::from_str(len_str) .map_err(|e|format_err!("{}",e)));
                Ok(DataType::Hexstring(false,Some(len)))
            },
            x if x.starts_with("Hexstring") && x.len() > 9 => {
                let len_str = &x[9..x.len()];
                let len = try!(usize::from_str(len_str) .map_err(|e|format_err!("{}",e)));
                Ok(DataType::Hexstring(true,Some(len)))
            },
            x if x.starts_with("dv") => {//"dv", "dv:m:V", "dv:-3:V"
                    let splitted: Vec<_> = x.split(":").collect();
                    match splitted.len() {
                        1 => {
                            Ok(DataType::Dim(0, String::new()))
                        },
                        2 => {//no prefix
                            Ok(DataType::Dim(0, splitted[1].to_string()))
                        },
                        3 => {
                            let exp = try!(Prefix::from_str(splitted[1]).
                                                        and_then(|p|Ok(p.to_exponent()))
                                                        .or(splitted[1].parse::<i32>()).map_err(|e|format_err!("{}",e)));
                            Ok(DataType::Dim(exp, splitted[2].to_string()))
                        },
                        _ => Err(format_err!("Incorrect dimensioned value format (e.g. dv:m:V)"))
                    }
            },
            _ => Err(format_err!("Incorrect data type: {}", s))
        }
    }

    pub fn byte_size(&self) -> usize {
        match self {
            &DataType::None => 0,
            &DataType::Raw => 1,
            &DataType::U8 => 1,
            &DataType::I8 => 1,
            &DataType::U16 => 2,
            &DataType::I16 => 2,
            &DataType::U32 => 4,
            &DataType::I32 => 4,
            &DataType::U64 => 8,
            &DataType::I64 => 8,
            &DataType::F32 => 4,
            &DataType::F64 => 8,
            &DataType::Dim(_,_) => 1,//undefined
            &DataType::String(_) => 1,//undefined
            &DataType::Hexstring(_,_) => 1,//undefined
            &DataType::Scistring(_) => 1,//undefined
            &DataType::Boolean => 1,
        }
    }
}


#[test]
fn test_parse() {
    let h8 = DataType::parse("hexstring8").unwrap();
    assert_eq!(h8, DataType::Hexstring(false, Some(8)));

    let raw = DataType::parse("").unwrap();
    assert_eq!(raw, DataType::Raw);
}