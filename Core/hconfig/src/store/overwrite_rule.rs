// @Author: ronan
// @Date:   14-12-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 15-12-2016

use store::{Data, StoreKey, StoreNode};
use failure::Error;
use dimval::DimVal;

///An Overwrite rule allow the possibility to change individual key-value pairs before module creation.
#[derive(Debug,Clone)]
pub struct OverwriteRule {
    pattern: StoreKey,
    value:  Data
}

impl OverwriteRule {

    pub fn from_node(n: &StoreNode) -> OverwriteRule {
        OverwriteRule { pattern: StoreKey::from(n.get_node_key()), value: n.get_node_value().clone() }
    }

    ///s must have the format "key_part1.key_part2=value"
    pub fn from_str(s: &str) -> Result<OverwriteRule, Error> {

        let key_val: Vec<_> = s.trim().split("=").collect();
        if key_val.len() != 2 {
            return Err(format_err!("Overwrite rule must have the format key1.key2=value"));
        }
        else {
            if key_val[0].is_empty() {
                return Err(format_err!("Empty key"));
            }

            //get key part for pattern
            let pattern = StoreKey::from(key_val[0]);

            //get value
            let val_str = key_val[1];
            let data: Result<Data, String> = val_str.parse::<bool>().map(|b|Data::create_bool(b)) //try bool
                            .or(val_str.parse::<i64>().map(|i|Data::create_i64(i))) //try int
                            .or(val_str.parse::<f64>().map(|f|Data::create_f64(f))) //try float
                            .or(val_str.parse::<DimVal>().map(|d|Data::create_dim(d))) //try dim
                            .or(Ok(Data::create_str(val_str.trim_matches('\"'))));

            match data {
                Ok(d) => Ok(OverwriteRule { pattern: pattern, value: d }),
                Err(e) => Err(format_err!("Value {} cannot be parsed: {}", val_str, e))
            }
        }
    }

    pub fn get_store_key(&self) -> &StoreKey {
        &self.pattern
    }

    pub fn get_data(&self) -> &Data {
        &self.value
    }

    pub fn apply_to_store_node(ow_rules: &Vec<OverwriteRule>, node: &mut StoreNode) -> Result<(), Error> {
        //convert rules to patial store_keys
        let partial_keys = ow_rules.iter().map(|r|r.get_store_key().clone()).collect();
        let keys_vec = node.get_key_matches_with_partials(&partial_keys);

        if keys_vec.len() != ow_rules.len() {//the number of rules must be equal to the number of results
            return Err(format_err!("Problem when dealing with overwrite rules"));
        }
        else {
            for (i, ow_rule) in ow_rules.iter().enumerate() {
                let ref keys = keys_vec[i];//get the list of results for each rule
                //keys must contain one unique key for non ambiguous overwrite
                //we are defensive here: any error could be critical
                if keys.len() != 1 {
                    return Err(format_err!("The overwrite rule {} is incorrect: {} parameters match this rule instead of exactly one", partial_keys[i], keys.len()));
                }
                else {
                    let key = try!(keys.get(0).ok_or(format_err!("Overwrite rule error")));
                    node.store_update(key.clone(), ow_rule.get_data().clone());
                }
            }
        }
        Ok(())
    }
}

#[test]
fn test_rule_parsing() {
    let tests_ok = vec!["key=true", "k1.k2=\"test\"", "k1.k3=test", "k=", "k1=3.14", "k1.k2.k3=3.14", "k2.k3=3"];
    let tests_fail = vec!["kv", "=v"];

    for t in tests_ok {
        let _ = OverwriteRule::from_str(t).unwrap();
        // println!("Rule for {} is {:?}", t, rule);
    }

    for t in tests_fail {
        let rule = OverwriteRule::from_str(t);
        assert!(rule.is_err(), "Test {} should have failed, instead got {:?}.", t, rule.unwrap());
    }
}
