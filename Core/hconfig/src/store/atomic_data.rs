/*
 * Author:   ronan.lashermes@inria.fr
 * Creation: Thu Dec 07 2017
 *
 * Copyright (c) 2017 INRIA
 */


use std::hash::{Hasher, Hash};
use std::fmt::{Formatter, Display};
use std::fmt;
use std::str::FromStr;
use std::io::Cursor;

use io::byte_convert::ToBytes;

use store::DataType;
use failure::Error;
use toml::Value;
use dimval::{DimVal, Unit, Dimension};

use byteorder::{LittleEndian, ReadBytesExt};
type Endianness = LittleEndian;

use hex;

macro_rules! hexdisp {
    ($i:ident, $upper:ident, $pad:ident) => (
        match ($upper, $pad) {
            (true, Some(pad_len)) => format!("{:01$X}", $i, pad_len),
            (true, None) => format!("{:X}", $i),
            (false, Some(pad_len)) => format!("{:01$x}", $i, pad_len),
            (false, None) => format!("{:x}", $i),
        }
    )
}

macro_rules! to_iuxx {
    ($xx:ty, $xxname:ident) => (
        pub fn $xxname(&self) -> Result<$xx, Error> {
            match self {
                &AtomicData::I8(i)  => Ok(i as $xx),
                &AtomicData::I16(i) => Ok(i as $xx),
                &AtomicData::I32(i) => Ok(i as $xx),
                &AtomicData::I64(i) => Ok(i as $xx),
                &AtomicData::U8(i)  => Ok(i as $xx),
                &AtomicData::U16(i) => Ok(i as $xx),
                &AtomicData::U32(i) => Ok(i as $xx),
                &AtomicData::U64(i) => Ok(i as $xx),
                &AtomicData::F32(i) => Ok(i as $xx),
                &AtomicData::F64(i) => Ok(i as $xx),
                &AtomicData::Dim(ref dv) => Ok(dv.to_i64()? as $xx),
                &AtomicData::String(ref s) => s.parse::<$xx>().map_err(|e|e.into()),
                _ => Err(format_err!("Data is not an integer but is {:?}.", self))
            }
        }
    )
}

macro_rules! to_fxx {
    ($xx:ty, $xxname:ident) => (
        pub fn $xxname(&self) -> Result<$xx, Error> {
            match self {
                &AtomicData::I8(i)  => Ok(i as $xx),
                &AtomicData::I16(i) => Ok(i as $xx),
                &AtomicData::I32(i) => Ok(i as $xx),
                &AtomicData::I64(i) => Ok(i as $xx),
                &AtomicData::U8(i)  => Ok(i as $xx),
                &AtomicData::U16(i) => Ok(i as $xx),
                &AtomicData::U32(i) => Ok(i as $xx),
                &AtomicData::U64(i) => Ok(i as $xx),
                &AtomicData::F32(i) => Ok(i as $xx),
                &AtomicData::F64(i) => Ok(i as $xx),
                &AtomicData::Dim(ref dv) => Ok(dv.to_f64()? as $xx),
                &AtomicData::String(ref s) => s.parse::<$xx>().map_err(|e|e.into()),
                _ => Err(format_err!("Data is not a float but is {:?}.", self))
            }
        }
    )
}

/// Atomic Data: a piece of data handled by actors
#[derive(Clone,PartialEq,Debug,Serialize,Deserialize,PartialOrd)]
pub enum AtomicData {
    Empty,
    U8(u8),
    I8(i8),
    U16(u16),
    I16(i16),
    U32(u32),
    I32(i32),
    U64(u64),
    I64(i64),
    F32(f32),
    F64(f64),
    Boolean(bool),
    Dim(DimVal),
    String(String)
}

impl Hash for AtomicData {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            &AtomicData::Empty => {"".hash(state);},
            &AtomicData::U8(b) => {b.hash(state);},
            &AtomicData::I8(b) => {b.hash(state);},
            &AtomicData::U16(b) => {b.hash(state);},
            &AtomicData::I16(b) => {b.hash(state);},
            &AtomicData::U32(b) => {b.hash(state);},
            &AtomicData::I32(b) => {b.hash(state);},
            &AtomicData::U64(b) => {b.hash(state);},
            &AtomicData::I64(i) => {i.hash(state);},
            &AtomicData::F32(f) => {f.to_string().hash(state);},
            &AtomicData::F64(f) => {f.to_string().hash(state);},
            &AtomicData::Dim(ref dv) => {dv.to_string().hash(state);},
            &AtomicData::Boolean(b) => {b.hash(state);},
            &AtomicData::String(ref s) => {s.hash(state);}
        }
    }
}

impl AtomicData {
    pub fn pow10(&self, exp_10: i32) -> Result<AtomicData, Error> {
        match self {
            &AtomicData::F32(f) => Ok(AtomicData::F32((f as f64 * 10f64.powi(exp_10)) as f32)),
            &AtomicData::F64(f) => Ok(AtomicData::F64(f * 10f64.powi(exp_10))),
            &AtomicData::I64(i) => Ok(AtomicData::I64( (i as f64 * 10f64.powi(exp_10)) as i64)),
            &AtomicData::U64(i) => Ok(AtomicData::U64( (i as f64 * 10f64.powi(exp_10)) as u64)),
            &AtomicData::I32(i) => Ok(AtomicData::I32( (i as f64 * 10f64.powi(exp_10)) as i32)),
            &AtomicData::U32(i) => Ok(AtomicData::U32( (i as f64 * 10f64.powi(exp_10)) as u32)),
            &AtomicData::I16(i) => Ok(AtomicData::I16( (i as f64 * 10f64.powi(exp_10)) as i16)),
            &AtomicData::U16(i) => Ok(AtomicData::U16( (i as f64 * 10f64.powi(exp_10)) as u16)),
            &AtomicData::I8(i) => Ok(AtomicData::I8( (i as f64 * 10f64.powi(exp_10)) as i8)),
            &AtomicData::U8(i) => Ok(AtomicData::U8( (i as f64 * 10f64.powi(exp_10)) as u8)),
            &AtomicData::Dim(ref dv) => Ok(AtomicData::Dim(dv.pow10(exp_10 as i64)?)),
            _ => Err(format_err!("pow10 operation is not possible on this data {:?}.", self))
        }
    }

    pub fn datatype(&self) -> DataType {
        match self {
            &AtomicData::F32(_) => DataType::F32,
            &AtomicData::F64(_) => DataType::F64,
            &AtomicData::I64(_) => DataType::I64,
            &AtomicData::I32(_) => DataType::I32,
            &AtomicData::I16(_) => DataType::I16,
            &AtomicData::I8(_) => DataType::I8,
            &AtomicData::U64(_) => DataType::U64,
            &AtomicData::U32(_) => DataType::U32,
            &AtomicData::U16(_) => DataType::U16,
            &AtomicData::U8(_) => DataType::U8,
            &AtomicData::Boolean(_) => DataType::Boolean,
            &AtomicData::Dim(ref dv) => DataType::Dim(0, dv.get_unit_string().unwrap_or(String::new())),
            &AtomicData::Empty => DataType::None,
            &AtomicData::String(_) => DataType::String(None)
        }
    }

    /// Read a Data of type DataType from a raw (Vec<u8> source)
    pub fn consume_data(cursor: &mut Cursor<Vec<u8>>, dtype: &DataType) -> Result<AtomicData, Error> {
        let data = match dtype {
            &DataType::None => {return Err(format_err!("No datatype to read from"));},
            &DataType::Raw =>   AtomicData::U8(cursor.read_u8()?),
            &DataType::U8 =>    AtomicData::U8(cursor.read_u8()?),
            &DataType::I8 =>    AtomicData::I8(cursor.read_i8()?),
            &DataType::U16 =>   AtomicData::U16(cursor.read_u16::<Endianness>()?),
            &DataType::I16 =>   AtomicData::I16(cursor.read_i16::<Endianness>()?),
            &DataType::U32 =>   AtomicData::U32(cursor.read_u32::<Endianness>()?),
            &DataType::I32 =>   AtomicData::I32(cursor.read_i32::<Endianness>()?),
            &DataType::U64 =>   AtomicData::U64(cursor.read_u64::<Endianness>()?),
            &DataType::I64 =>   AtomicData::I64(cursor.read_i64::<Endianness>()?),
            &DataType::F32 =>   AtomicData::F32(cursor.read_f32::<Endianness>()?),
            &DataType::F64 =>   AtomicData::F64(cursor.read_f64::<Endianness>()?),
            &DataType::String(_)|&DataType::Scistring(_) => AtomicData::String(String::from_utf8(cursor.clone().into_inner())?),
            &DataType::Hexstring(_,_) => AtomicData::String(hex::encode(cursor.clone().into_inner())),
            &DataType::Dim(_, _) => {
                let data_str = String::from_utf8(cursor.clone().into_inner())?;
                AtomicData::Dim(DimVal::from_str(&data_str)?)
            },
            &DataType::Boolean => {
                let b = cursor.read_u8()?;
                AtomicData::Boolean(b!=0)
            },
            // _ => {return Err(format!("Cannot read from data type {:?}", dtype));}
        };
        Ok(data)
    }

   pub fn to_dim(&self) -> Result<DimVal, Error> {
        match self {
            &AtomicData::Dim(ref d) => Ok(d.clone()),
            &AtomicData::String(ref s) => Ok(DimVal::from_str(s)?),
            _ => Err(format_err!("Data is not a DimVal."))
        }
    }


    pub fn to_byte(&self) -> Result<u8, Error> {
        match self {
            &AtomicData::U8(b) => Ok(b),
            _ => Err(format_err!("Data is not a byte."))
        }
    }

    to_iuxx!(i64, to_i64);
    to_iuxx!(u64, to_u64);
    to_iuxx!(i32, to_i32);
    to_iuxx!(u32, to_u32);
    to_iuxx!(i16, to_i16);
    to_iuxx!(u16, to_u16);
    to_iuxx!(i8, to_i8);
    to_iuxx!(u8, to_u8);

    to_fxx!(f64, to_f64);
    to_fxx!(f32, to_f32);

    pub fn to_boolean(&self) -> Result<bool, Error> {
        match self {
            &AtomicData::Boolean(b) => Ok(b),
            &AtomicData::String(ref s) => s.parse::<bool>().map_err(|e|e.into()),
            _ => Err(format_err!("Data is not a boolean."))
        }
    }

    pub fn to_string_result(&self) -> Result<String, Error> {
        match self {
            &AtomicData::I64(i) => Ok(i.to_string()),
            &AtomicData::I32(i) => Ok(i.to_string()),
            &AtomicData::I16(i) => Ok(i.to_string()),
            &AtomicData::I8(i) => Ok(i.to_string()),
            &AtomicData::U64(i) => Ok(i.to_string()),
            &AtomicData::U32(i) => Ok(i.to_string()),
            &AtomicData::U16(i) => Ok(i.to_string()),
            &AtomicData::U8(i) => Ok(i.to_string()),
            &AtomicData::F32(i) => Ok(i.to_string()),
            &AtomicData::F64(i) => Ok(i.to_string()),
            &AtomicData::Boolean(i) => Ok(i.to_string()),
            &AtomicData::String(ref s) => Ok(s.clone()),
            &AtomicData::Dim(ref dv) => Ok(dv.to_string()),
            _ => {return Err(format_err!("Data cannot be displayed as string ({:?})", self)); }
        }
    }

    pub fn to_padded_string(&self, pad: Option<usize>) -> Result<String, Error> {
        let s = self.to_string_result()?;
        match pad {
            None => Ok(s),
            Some(pad_len) => Ok(format!("{:01$}", s, pad_len))
        }
    }

    pub fn to_sci_string(&self, upper: bool) -> Result<String, Error> {
        let f = match self {
            &AtomicData::I64(i) => i as f64,
            &AtomicData::I32(i) => i as f64,
            &AtomicData::I16(i) => i as f64,
            &AtomicData::I8(i) => i as f64,
            &AtomicData::U64(i) => i as f64,
            &AtomicData::U32(i) => i as f64,
            &AtomicData::U16(i) => i as f64,
            &AtomicData::U8(i) => i as f64,
            &AtomicData::F32(i) => i as f64,
            &AtomicData::F64(i) => i as f64,
            &AtomicData::Dim(ref dv) => dv.to_f64()?,
            _ => {return Err(format_err!("Conversion to scistring not possible ({:?})", self)); }
        };

        let s = match upper {
            true => format!("{:E}", f),
            false => format!("{:e}", f)
        };
        Ok(s)
    }

    pub fn to_hex_string(&self, upper: bool, pad: Option<usize>) -> Result<String, Error> {
         let s = match self {
            &AtomicData::I64(i) => hexdisp!(i, upper, pad),
            &AtomicData::I32(i) => hexdisp!(i, upper, pad),
            &AtomicData::I16(i) => hexdisp!(i, upper, pad),
            &AtomicData::I8(i) => hexdisp!(i, upper, pad),
            &AtomicData::U64(i) => hexdisp!(i, upper, pad),
            &AtomicData::U32(i) => hexdisp!(i, upper, pad),
            &AtomicData::U16(i) => hexdisp!(i, upper, pad),
            &AtomicData::U8(i) => hexdisp!(i, upper, pad),
            &AtomicData::String(ref s) => s.to_string(),
            _ => {return Err(format_err!("Conversion to hexstring not possible ({:?})", self)); }
        };
        Ok(s)
    }

    pub fn convert(&self, to: &DataType) -> Result<AtomicData, Error> {
        match to {
            &DataType::U8 => Ok(AtomicData::U8(self.to_u8()?)),
            &DataType::I8 => Ok(AtomicData::I8(self.to_i8()?)),
            &DataType::U16 => Ok(AtomicData::U16(self.to_u16()?)),
            &DataType::I16 => Ok(AtomicData::I16(self.to_i16()?)),
            &DataType::U32 => Ok(AtomicData::U32(self.to_u32()?)),
            &DataType::I32 => Ok(AtomicData::I32(self.to_i32()?)),
            &DataType::U64 => Ok(AtomicData::U64(self.to_u64()?)),
            &DataType::I64 => Ok(AtomicData::I64(self.to_i64()?)),
            &DataType::F32 => Ok(AtomicData::F32(self.to_f32()?)),
            &DataType::F64 => Ok(AtomicData::F64(self.to_f64()?)),
            &DataType::Boolean => Ok(AtomicData::Boolean(self.to_boolean()?)),
            &DataType::String(pad) => Ok(AtomicData::String(self.to_padded_string(pad)?)),
            &DataType::Scistring(upper) =>  Ok(AtomicData::String(self.to_sci_string(upper)?)),
            &DataType::Hexstring(upper, pad) => Ok(AtomicData::String(self.to_hex_string(upper, pad)?)),
            &DataType::Dim(exp, ref unit_str) => {
                match self {
                    &AtomicData::String(ref s) => {
                        let mut dv = DimVal::from_str(s)?;
                        if exp != 0 {
                            dv = dv.pow10(exp as i64)?;
                        }

                        if unit_str != "" {
                            let new_dim = Dimension::parse_dim(unit_str)?;
                            dv.set_dimension(new_dim);
                        }
                        
                        Ok(AtomicData::Dim(dv))
                        
                        // if dv.is_unit(&Unit::from_str(unit_str)?) {
                        //     Ok(AtomicData::Dim(dv))
                        // }
                        // else {
                            // Err(format_err!("Incorrect unit for DimVal: {} instead of {}", dv.get_unit_string()?, unit_str))
                        // }
                    },
                    _ => {
                        let dv = try!(DimVal::from_f64_parts_raw(self.to_f64()?, exp, Unit::from_str(unit_str)?.to_dimension()));
                        Ok(AtomicData::Dim(dv))
                    }
                }
            }
            t => Err(format_err!("Conversion from {:?} to {:?} is not defined", self.datatype(), t))
        }
    }
}

impl FromStr for AtomicData {
    type Err = Error;

    fn from_str(s: &str) -> Result<AtomicData, Error> {
        Ok(AtomicData::String(s.to_string()))
    }
}

impl Into<Value> for AtomicData {
    fn into(self) -> Value {
        match self {
            AtomicData::Empty => Value::String("".to_string()),
            AtomicData::String(s) => Value::String(s),
            AtomicData::U8(b) => Value::Integer(b as i64),
            AtomicData::I8(b) => Value::Integer(b as i64),
            AtomicData::U16(b) => Value::Integer(b as i64),
            AtomicData::I16(b) => Value::Integer(b as i64),
            AtomicData::U32(b) => Value::Integer(b as i64),
            AtomicData::I32(b) => Value::Integer(b as i64),
            AtomicData::U64(b) => Value::Integer(b as i64),
            AtomicData::I64(i) => Value::Integer(i),
            AtomicData::F32(f) => Value::Float(f as f64),
            AtomicData::F64(f) => Value::Float(f),
            AtomicData::Boolean(b) => Value::Boolean(b),
            AtomicData::Dim(dv) => Value::String(dv.to_string())
        }
    }
}

impl From<Value> for AtomicData {
    fn from(val: Value) -> Self {
        match val {
                Value::String(s) => AtomicData::String(s),
                Value::Integer(i) => AtomicData::I64(i),
                Value::Float(f) => AtomicData::F64(f),
                Value::Boolean(b) => AtomicData::Boolean(b),
                Value::Datetime(dt) => AtomicData::String(dt.to_string()),
                //cannot convert compound data types => Data
                _ => AtomicData::Empty
        }
    }
}

impl ToBytes for AtomicData {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        let res = match self {
            &AtomicData::Empty => { Vec::new() },
            &AtomicData::String(ref s) => s.as_bytes().to_vec(),
            &AtomicData::U8(b) => { <u8 as ToBytes>::to_bytes(&b)? },
            &AtomicData::I8(b) => { <i8 as ToBytes>::to_bytes(&b)? },
            &AtomicData::U16(b) => { <u16 as ToBytes>::to_bytes(&b)? },
            &AtomicData::I16(b) => { <i16 as ToBytes>::to_bytes(&b)? },
            &AtomicData::U32(b) => { <u32 as ToBytes>::to_bytes(&b)? },
            &AtomicData::I32(b) => { <i32 as ToBytes>::to_bytes(&b)? },
            &AtomicData::U64(b) => { <u64 as ToBytes>::to_bytes(&b)? },
            &AtomicData::I64(b) => { <i64 as ToBytes>::to_bytes(&b)? },
            &AtomicData::F32(f) => { <f32 as ToBytes>::to_bytes(&f)? },
            &AtomicData::F64(f) => { <f64 as ToBytes>::to_bytes(&f)? },
            &AtomicData::Boolean(b) => <bool as ToBytes>::to_bytes(&b)?,
            &AtomicData::Dim(ref dv) => dv.to_string().as_bytes().to_vec(),
        };
        Ok(res)
    }
}

impl Display for AtomicData {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self.to_string_result() {
            Ok(s) => write!(f, "{}", s),
            Err(e) => write!(f, "Error: {}", e)
        }
    }
}
