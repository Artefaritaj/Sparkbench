// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 15-12-2016
// @License: GPL



use std::vec::Vec;
use std::collections::{HashMap, BTreeMap};

use store::data::Data;
use store::store_key::StoreKey;
use errors::MissingKeyError;

use failure::Error;

use regex::Regex;

pub const SEPARATOR: &'static str = ".";

#[derive(Clone,PartialEq,Debug)]
pub struct StoreNode {
    key: String,
    val: Data,
    children: Vec<StoreNode>
}

lazy_static! {//space: \s
    static ref INDEX_REGEX: Regex = Regex::new(r"^\{[0-9]+\}$").unwrap();
}

impl StoreNode {
    pub fn new(key: String, val: Data) -> StoreNode {
        StoreNode{key: key, val: val, children: Vec::new() }
    }

    pub fn new_empty() -> StoreNode {
        StoreNode{ key: "".to_string(), val: Data::Empty, children: Vec::new()}
    }

    pub fn is_leaf(&self) -> bool {
        return self.children.len() == 0;
    }

    ///Return true if the key is part of an array
    pub fn is_key_index(&self) -> bool {
        INDEX_REGEX.is_match(&self.key)
    }

    pub fn get_node_value(&self) -> &Data {
        return &self.val;
    }

    pub fn get_node_value_array(&self) -> Vec<&Data> {
        let mut ind = 0;
        let mut res = Vec::new();

        while let Ok(ref child) = self.get_node(&StoreKey::from(&format!("{{{}}}", ind))) {
            ind += 1;
            let data = child.get_node_value();
            res.push(data);
        }

        res
    }

    pub fn get_node_key(&self) -> &str {
        return &self.key;
    }

    pub fn get_node_children(&self) -> &Vec<StoreNode> {
        return &self.children;
    }

    pub fn children_count(&self) -> usize {
        self.children.len()
    }

    fn add_child(&mut self, node: StoreNode) {
        self.children.push(node);
    }

    //Create new node as child and return new node pointer
    pub fn create_child(&mut self, key: String, val: Data) {
        let new_node = StoreNode::new(key, val);
        self.children.push(new_node);
    }

    ///Create or update a key value tuple (return number of newly created children != modified values)
    pub fn store_update(&mut self, mut key: StoreKey, val: Data) -> usize {

        if key.depth() == 0 {//leaf found, update here
            self.val = val;
            return 0;
        }

        let next_key = key.pop_head();
        // let (next_key, descendant_key) = separate_head(&key);

        //Search for child with [next_key]
        for child in self.children.iter_mut() {
            if child.key == next_key {//Good child found
                return child.store_update(key.clone(), val);
            }
        }

        //if we are here, the child does not exist yet
        //then we have to create it
        let mut new_child = StoreNode::new(next_key, Data::Empty);
        let nb_created = new_child.store_update(key, val) + 1;
        self.add_child(new_child);
        return nb_created;

    }

    pub fn remove(&mut self, key: &StoreKey) -> Result<StoreNode, Error> {
        let mut descendant_key = key.clone();
        let next_key = descendant_key.pop_head();

        let mut toremove = Some(0_usize);

        //Search for child with [next_key]
        for i in 0..self.children.len() {
            if self.children[i].key == next_key {//Good child found
                toremove = Some(i);
            }
        }

        match toremove {
            Some(c) => {
                if descendant_key.depth() == 0 {//remove here
                    Ok(self.children.remove(c))
                }
                else {
                    match self.children.get_mut(c) {
                        Some(r) => r.remove(&descendant_key),//recursive call
                        None => Err( MissingKeyError::new(key).into())
                    }
                }
            },
            None => Err( MissingKeyError::new(key).into())
        }
    }

    ///Get Data value of designated node
    pub fn get_value(&self, key: &StoreKey) -> Result<Data, Error> {
        if key.depth() == 0 {//leaf found, return here
            return Ok(self.val.clone());
        }

        let mut descendant_key = key.clone();
        let next_key = descendant_key.pop_head();

        //Search for child with [next_key]
        for child in self.children.iter() {
            if child.key == next_key {//Good child found
                return child.get_value(&descendant_key);
            }
        }

        //None found
        return Err( MissingKeyError::new(key).into());
    }

    ///Get node pointer for designated node
    pub fn get_node(&self, key: &StoreKey) -> Result<&StoreNode, Error> {
        if key.depth() == 0 {//leaf found, return here
            return Ok(&self);
        }

        let mut descendant_key = key.clone();
        let next_key = descendant_key.pop_head();

        //Search for child with [next_key]
        for child in self.children.iter() {
            if child.key == next_key {//Good child found
                return child.get_node(&descendant_key);
            }
        }

        //None found
        return Err(MissingKeyError::new(key).into());
    }

    ///The partial key define key parts that must be found in order (not necessarily consecutive) in the matches.
    pub fn get_key_matches_with_partial(&self, partial_key: &StoreKey) -> Vec<StoreKey> {
        let allkeys = self.get_all_store_keys();

        let mut result: Vec<StoreKey> = Vec::new();
        for k in allkeys {
            if k.match_part_key(partial_key) {
                result.push(k.clone());
            }
        }
        result
    }

    ///Try to match different partial keys in a batch for performances
    pub fn get_key_matches_with_partials(&self, partial_keys: &Vec<StoreKey>) -> Vec<Vec<StoreKey>> {
        let allkeys = self.get_all_store_keys();

        //init arrays
        let mut result: Vec<Vec<StoreKey>> = Vec::new();
        for _ in 0..partial_keys.len() {
            result.push(Vec::new());
        }


        for k in allkeys {
            for (i, partial_key) in partial_keys.iter().enumerate() {
                if k.match_part_key(partial_key) {
                    result[i].push(k.clone());
                }
            }

        }
        result
    }

    ///Get a vec of all StoreKeys in this node
    pub fn get_all_store_keys(&self) -> Vec<StoreKey> {
        StoreNode::get_sub_store_keys(&StoreKey::new(), &self)
    }

    fn get_sub_store_keys(prefix: &StoreKey, node: &StoreNode) -> Vec<StoreKey> {
        let mut result: Vec<StoreKey> = Vec::new();

        for c in node.get_node_children() {
            let thiskey = prefix.clone().push(c.get_node_key());
            let mut other = StoreNode::get_sub_store_keys(&thiskey, c);
            result.push(thiskey);
            result.append(&mut other);
        }

        result
    }

    ///Convert tree to hashmap with '.' separated keys
    pub fn to_hashmap(&self) -> HashMap<String, Data> {
        return self.to_hashmap_prefix(&StoreKey::from(&self.key));
    }

    pub fn to_btreemap(&self) -> BTreeMap<String, Data> {
        let hm = self.to_hashmap();
        let mut result: BTreeMap<String, Data> = BTreeMap::new();
        for (k,v) in hm {
            result.insert(k,v);
        }
        result
    }

    pub fn to_hashmap_from(&self, from: &StoreKey) -> Option<HashMap<String, Data>> {
        if from.depth() == 0 {
            return Some(self.to_hashmap());
        }
        else {
            let mut descendant_key = from.clone();
            let next_key = descendant_key.pop_head();
            for child in self.children.iter() {
                if child.key == next_key {//Good child found
                    return child.to_hashmap_from(&descendant_key);
                }
            }
            return None;
        }
    }

    fn to_hashmap_prefix(&self, prefix: &StoreKey) -> HashMap<String, Data> {
        let mut map = HashMap::<String, Data>::new();

        //add this
        //map.insert(prefix.to_string(), self.val.clone());
        map.extend(to_hashmap_recursive_data(prefix, &self.val));

        for child in self.children.iter() {
            let new_prefix = prefix.push_clone(&child.key);
            //add child descendance
            map.extend(child.to_hashmap_prefix(&new_prefix));
        }

        return map;
    }

    ///Add other StoreNode elements to this one. Values from other replace existing ones in case of conflict.
    pub fn merge(&mut self, other: &StoreNode) -> usize {
        self.fill(other.to_hashmap())
    }

    ///Add other StoreNode elements to this one (put at prefix node)
    pub fn merge_prefix(&mut self, other: &StoreNode, prefix: &StoreKey) -> usize {
        self.fill_at(other.to_hashmap(), prefix)
    }

    ///Fill structure with hashmap and return the number of newly created elements.
    ///Values from other replace existing ones in case of conflict.
    pub fn fill(&mut self, map: HashMap<String, Data>) -> usize {

        let mut created = 0usize;
        for (k, v) in map.iter() {
            // let key = StoreKey::from(k);
            // println!("{} -> {}", k, key.to_string());
            let result = self.store_update(StoreKey::from(k), v.clone());
            created += result;
        }

        return created;
    }

    ///Fill structure with hashmap starting from the prefix. Return the number of newly created elements.
    pub fn fill_at(&mut self, map: HashMap<String, Data>, prefix: &StoreKey) -> usize {
        if prefix.depth() == 0 { // if node is found
            return self.fill(map);
        }
        else {//find node
            let mut descendant_key = prefix.clone();
            let next_key = descendant_key.pop_head();
            for child in self.children.iter_mut() {
                if child.key == next_key {//Good child found
                    return child.fill_at(map,&descendant_key);
                }
            }
            // if dont exist, create
            let mut new_child = StoreNode::new(next_key, Data::Empty);
            let nb_created = new_child.fill_at(map,&descendant_key) + 1;
            self.add_child(new_child);
            return nb_created;
        }
    }

    ///Clone the branch starting from the prefix.
    pub fn clone_branch(&self, prefix: &StoreKey) -> StoreNode {
        if prefix.depth() == 0 { // if node is found
            return self.clone();
        }
        else {//find node
            let mut descendant_key = prefix.clone();
            let next_key = descendant_key.pop_head();
            for child in self.children.iter() {
                if child.key == next_key {//Good child found
                    return child.clone_branch(&descendant_key);
                }
            }
            // if dont exist, create
            return StoreNode::new(prefix.to_string(), Data::Empty);
        }
    }
}

fn to_hashmap_recursive_data(prefix: &StoreKey, data: &Data) -> HashMap<String, Data> {
    let mut hm: HashMap<String, Data> = HashMap::new();
    match data {
        &Data::Array(ref v) => {
            //add array root with array len

            hm.insert(prefix.to_string(),Data::create_u64(v.len() as u64));

            for (i,d) in v.iter().enumerate()
            {
                let new_prefix = prefix.push_clone(&("{".to_string()+ &i.to_string() +"}"));
                hm.extend(to_hashmap_recursive_data(&new_prefix, d));
            }
        },
        &Data::Table(ref t) => {
            //add array root with array len
            hm.insert(prefix.to_string(),Data::create_u64(t.len() as u64));
            for (k,d) in t.iter()
            {
                let new_prefix = prefix.push_clone(k);
                hm.extend(to_hashmap_recursive_data(&new_prefix, d));
            }
        },
        d => {hm.insert(prefix.to_string(), d.clone());}
    }
    hm
}


// *************** TESTS ***************************
#[test]
fn test_node_basics() {
    let a_node = StoreNode::new("A".to_string(), Data::create_str("valA"));
    let b_node = StoreNode::new("B".to_string(), Data::create_str("valB"));

    assert!(a_node.key == "A");
    assert!(b_node.key == "B");
    assert!(a_node.val == Data::create_str("valA"));
    assert!(b_node.val == Data::create_str("valB"));
}

#[test]
fn test_node_creation_destruction() {
    let mut root = StoreNode::new("".to_string(), Data::create_str("valRoot"));
    root.store_update(StoreKey::from("A"), Data::create_str("valA"));

    assert!(root.get_value(&StoreKey::from("A")).unwrap() == Data::create_str("valA"));

    root.store_update(StoreKey::from("B.CD.EF.G"), Data::create_str("valG"));

    assert!(root.get_value(&StoreKey::from(".B.CD.EF.G")).unwrap() == Data::create_str("valG"));
    assert!(root.get_value(&StoreKey::from(".A")).unwrap() == Data::create_str("valA"));

    assert!(root.get_node(&StoreKey::from("B.CD"))
        .and_then(|n|n.get_value(&StoreKey::from("EF.G"))).unwrap() == Data::create_str("valG"));


    let _ = root.remove(&StoreKey::from("B.CD"));

    let mut keys = String::new();
    for k in root.to_hashmap().keys() {
        keys = keys + " <" + k + ">";
    }

    assert!(root.get_value(&StoreKey::from("B.CD")).is_err(), "Keys are {}", keys);
    assert!(root.get_value(&StoreKey::from("B.CD.EF")).is_err(), "Keys are {}", keys);
}

#[test]
fn test_hashmaps() {
    let mut root = StoreNode::new("".to_string(), Data::create_str("valRoot"));
    root.store_update(StoreKey::from("A"), Data::create_str("valA"));
    root.store_update(StoreKey::from(".B.CD.EF.G"), Data::create_str("valG"));

    let map = root.to_hashmap();
    assert!(map.len() == 6, "Hashmap length is {}", map.len());

    assert!(map.contains_key("A") == true);
    assert!(map.get("B.CD.EF.G").unwrap() == &Data::create_str("valG"));

    let mut fill_map = HashMap::new();
    fill_map.insert(".C.D".to_string(), Data::create_str("valD"));
    fill_map.insert(".brA.brB".to_string(), Data::create_str("valbrB"));
    fill_map.insert(".C.E".to_string(), Data::create_str("valE"));
    fill_map.insert(".B.CD".to_string(), Data::create_str("valCD"));

    let created = root.fill(fill_map);
    assert!(created == 5);

    let map2 = root.to_hashmap();
    assert!(map2.len() == 11, "Hashmap length is {}", map2.len());

    // for (k,v) in map2 {
    //     println!("{}={:?}", k, v);
    // }

    let map_from = root.to_hashmap_from(&StoreKey::from(".B.CD")).unwrap();
    assert!(map_from.len() == 3, "Hashmap length is {}", map_from.len());
    assert!(map_from.contains_key("CD.EF.G") == true);

    root.fill_at(map_from, &StoreKey::from(".copy.to"));
    let map_from2 = root.to_hashmap_from(&StoreKey::from(".copy.to")).unwrap();

    let mut keys = String::new();
    for k in map_from2.keys() {
        keys = keys + " <" + k + ">";
    }

    assert!(map_from2.len() == 4, "Hashmap length is {} and keys are {}", map_from2.len(), keys);

    let branch = root.clone_branch(&StoreKey::from(".C"));
    let map3 = branch.to_hashmap();

    let mut keys3 = String::new();
    for k in map3.keys() {
        keys3 = keys3 + " <" + k + ">";
    }

    assert!(map3.len() == 3, "Hashmap length is {} and keys are {}", map3.len(), keys3);
}

#[test]
fn test_arrays() {
    use std::collections::btree_map::BTreeMap;

    let mut root = StoreNode::new("".to_string(), Data::create_str("valRoot"));
    root.store_update(StoreKey::from("A"), Data::create_str("valA"));
    root.store_update(StoreKey::from(".B.CD.EF.G"), Data::create_str("valG"));

    let map = root.to_hashmap();
    assert!(map.len() == 6, "Hashmap length is {}", map.len());

    root.store_update(StoreKey::from(".B.Array"), Data::Array(vec![Data::create_str("arr1"),
                                                    Data::Array(vec![Data::create_i64(1),Data::create_u64(2)]),
                                                    Data::create_str("arr3")]));

    let mut treemap: BTreeMap<String, Data> = BTreeMap::new();
    treemap.insert("t1".to_string(), Data::create_i64(1));
    treemap.insert("t1t2".to_string(), Data::create_u16(1));

    root.store_update(StoreKey::from(".C.table"), Data::Table(treemap));

    let map2 = root.to_hashmap();
    let mut keys2 = String::new();
    for k in map2.keys() {
        keys2 = keys2 + " <" + k + ">";
    }

    assert!(map2.len() == 16, "Hashmap length is {} and keys are {}", map2.len(), keys2);


}

#[test]
fn test_root_filled() {
    let mut root = StoreNode::new("root".to_string(), Data::create_str("valRoot"));
    root.store_update(StoreKey::from("A"), Data::create_str("valA"));
    root.store_update(StoreKey::from(".B.CD.EF.G"), Data::create_str("valG"));

    //omit root!
    assert!(root.get_value(&StoreKey::from("A")).unwrap() == Data::create_str("valA"));
}

#[test]
fn test_merge() {
    let mut root = StoreNode::new("".to_string(), Data::create_str("valRoot"));
    root.store_update(StoreKey::from("A"), Data::create_str("valA"));
    root.store_update(StoreKey::from(".B.CD.EF.G"), Data::create_str("valG"));

    let mut root2 = StoreNode::new("".to_string(), Data::create_str("valRoot"));
    root2.store_update(StoreKey::from("A"), Data::create_str("valA2"));
    root2.store_update(StoreKey::from(".B.C.D"), Data::create_str("valD"));


    let added = root.merge(&root2);

    assert!(added == 2, "{} added", added);
    assert!(root.get_value(&StoreKey::from("A")).unwrap() == Data::create_str("valA2"));
    assert!(root.get_value(&StoreKey::from("B.CD")).is_ok());
    assert!(root.get_value(&StoreKey::from("B.C.D")).is_ok());
}

#[test]
fn test_partial_keys() {
    let mut root = StoreNode::new("".to_string(), Data::create_str("valRoot"));
    root.store_update(StoreKey::from("A"), Data::create_str("valA"));
    root.store_update(StoreKey::from(".B.CD.EF.G"), Data::create_str("valG"));
    root.store_update(StoreKey::from("CD.B.G"), Data::create_str("valG2"));

    let mut root2 = StoreNode::new("".to_string(), Data::create_str("valRoot"));
    root2.store_update(StoreKey::from("A"), Data::create_str("valA2"));
    root2.store_update(StoreKey::from(".B.C.D"), Data::create_str("valD"));


    root.merge(&root2);

    // let all_keys = root.get_all_store_keys();
    // for k in all_keys {
    //     println!("{}", k);
    // }

    let partial = StoreKey::from("CD.G");
    let matches = root.get_key_matches_with_partial(&partial);
    assert!(matches.len() == 2);

    let other_partial = StoreKey::from("EF");
    let partials = vec![partial, other_partial];
    let all_matches = root.get_key_matches_with_partials(&partials);
    for (i, pmatch) in all_matches.iter().enumerate() {
        for k in pmatch.iter() {
            println!("{} is a match for {}", k, partials[i])
        }
    }
}
