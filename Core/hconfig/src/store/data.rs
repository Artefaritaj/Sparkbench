// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 17-11-2016
// @License: MIT



use toml::Value;
use store::{DataType, CompoundDataType, AtomicData};
use io::byte_convert::ToBytes;

use std::collections::{BTreeMap};
use std::hash::{Hash, Hasher};
use std::io::Cursor;
use std::fmt::{Display, Formatter};
use std::fmt;
use std::str::FromStr;

use failure::Error;

use hex;
use dimval::DimVal;


#[derive(Clone,PartialEq,Debug,Serialize,Deserialize,PartialOrd)]
pub enum Data {
    Empty,
    Raw(Vec<u8>),
    Singleton(AtomicData),
    Array(Vec<Data>),
    Table(BTreeMap<String, Data>)
}

impl Hash for Data {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self {
            &Data::Empty => {"".hash(state);},
            &Data::Singleton(ref ad) => {ad.hash(state);},
            &Data::Array(ref v) => {
                for d in v {
                    d.hash(state);
                }
            },
            &Data::Table(ref t) => {
                for (k,v) in t {
                    k.hash(state);
                    v.hash(state);
                }
            },
            &Data::Raw(ref v) => {
                v.hash(state);
            }
        }
    }
}

impl Into<Value> for Data {
    fn into(self) -> Value {
        match self {
            Data::Empty => Value::String("".to_string()),
            Data::Raw(tab) => {
                let mut vec : Vec<Value> = Vec::new();
                for e in tab {
                    vec.push(Value::Integer( e as i64));
                }
                Value::Array(vec)
            },
            Data::Singleton(ad) => ad.into(),
            Data::Array(tab) => {
                let mut vec : Vec<Value> = Vec::new();
                for e in tab {
                    vec.push(e.into());
                }
                Value::Array(vec)
            },
            Data::Table(tab) => {
                let mut vec : BTreeMap<String, Value> = BTreeMap::new();
                for (k,v) in tab {
                    vec.insert(k, v.into());
                }
                Value::Table(vec)
            }
        }
    }
}

impl From<Value> for Data {
    fn from(val: Value) -> Self {
        match val {
            Value::Array(tab) =>
            {
                let mut vec : Vec<Data> = Vec::new();
                for e in tab {
                    let val_e: Data = e.into();
                    vec.push(val_e);
                }

                Data::Array(vec)
            },
            Value::Table(t) => {
                let mut vec : BTreeMap<String, Data> = BTreeMap::new();
                for (k,v) in t {
                    vec.insert(k, v.into());
                }
                Data::Table(vec)
            },
            v => {
                let ad: AtomicData = v.into();


                if ad == AtomicData::Empty {
                    Data::Empty
                }
                else {
                    Data::Singleton(ad)
                }
            }
        }
    }
}

impl Data {

    pub fn create_string(s: String) -> Data {
        Data::Singleton(AtomicData::String(s))
    }

    pub fn create_str(s: &str) -> Data {
        Data::Singleton(AtomicData::String(s.to_string()))
    }

    pub fn create_dim(d: DimVal) -> Data {
        Data::Singleton(AtomicData::Dim(d))
    }

    pub fn create_dim_str(s: &str) -> Result<Data, Error> {
        Ok(Data::Singleton(AtomicData::Dim(DimVal::from_str(s)?)))
    }

    pub fn create_i64(i: i64) -> Data {
        Data::Singleton(AtomicData::I64(i))
    }

    pub fn create_u64(i: u64) -> Data {
        Data::Singleton(AtomicData::U64(i))
    }

    pub fn create_i32(i: i32) -> Data {
        Data::Singleton(AtomicData::I32(i))
    }

    pub fn create_u32(i: u32) -> Data {
        Data::Singleton(AtomicData::U32(i))
    }

    pub fn create_i16(i: i16) -> Data {
        Data::Singleton(AtomicData::I16(i))
    }

    pub fn create_u16(i: u16) -> Data {
        Data::Singleton(AtomicData::U16(i))
    }

    pub fn create_i8(i: i8) -> Data {
        Data::Singleton(AtomicData::I8(i))
    }

    pub fn create_u8(i: u8) -> Data {
        Data::Singleton(AtomicData::U8(i))
    }

    pub fn create_f64(i: f64) -> Data {
        Data::Singleton(AtomicData::F64(i))
    }

    pub fn create_f32(i: f32) -> Data {
        Data::Singleton(AtomicData::F32(i))
    }

    pub fn create_bool(i: bool) -> Data {
        Data::Singleton(AtomicData::Boolean(i))
    }

    pub fn create_u8_array(arr: Vec<u8>) -> Data {
        Data::Array( arr.iter().map(|b|Data::Singleton(AtomicData::U8(*b))).collect())
    }

    ///Get number of elements in this Data
    pub fn len(&self) -> usize {
        match self {
            &Data::Empty => 0,
            &Data::Singleton(_) => 1,
            &Data::Array(ref vec) => vec.len(),
            &Data::Table(ref tab) => tab.len(),
            &Data::Raw(ref vec) => vec.len()
        }
    }

    pub fn datatype(&self) -> CompoundDataType {
        match self {
            &Data::Empty => CompoundDataType::None,
            &Data::Singleton(ref ad) => CompoundDataType::Singleton(ad.datatype()),
            &Data::Array(ref vec) => {
                match vec.first() {
                    None => CompoundDataType::Array(DataType::None),
                    Some(ref v) => CompoundDataType::Array(v.datatype().into())
                }
            },
            &Data::Table(ref tab) => {
                match tab.iter().next() {
                    None => CompoundDataType::Table(DataType::None),
                    Some((_,ref v)) => CompoundDataType::Table(v.datatype().into())
                }
            },
            &Data::Raw(_) => CompoundDataType::Raw
        }
    }


    ///Parse from byte vector to type
    pub fn from_raw(vec: Vec<u8>, to: &CompoundDataType) -> Result<Data,Error> {
        let veclen = vec.len();
        let mut cursor = Cursor::new(vec);

        match to {
            &CompoundDataType::Singleton(ref dt) => {
                Ok(Data::Singleton(AtomicData::consume_data(&mut cursor, dt)?))
            },
            &CompoundDataType::Raw => {
                Ok(Data::Raw(cursor.into_inner()))
            },
            &CompoundDataType::Array(ref dt) => {
                let len = if dt.byte_size() > 0 {
                    veclen / dt.byte_size()
                }
                else {
                    1
                };

                match dt {
                    &DataType::String(_)|&DataType::Hexstring(_,_)|&DataType::Scistring(_)|&DataType::Dim(_,_) => {
                        Ok(Data::Singleton(AtomicData::consume_data(&mut cursor, dt)?))
                    },
                    t => {
                        let mut vec_res : Vec<Data> = Vec::new();
                        for _ in 0..len {
                            vec_res.push(Data::Singleton(AtomicData::consume_data(&mut cursor, t)?))
                        }
                        Ok(Data::Array(vec_res))
                    }
                }
            },
            _ => Err(format_err!("Raw to {:?} is not implemented", to))
        }
    }

    pub fn convert(&self, to: &CompoundDataType) -> Result<Data, Error> {

        if to == &CompoundDataType::None {
            return Ok(Data::Empty);
        }

        //Special case: convert from string to hexstring gives Raw
        if let &CompoundDataType::Singleton(DataType::Hexstring(_, _)) = to {
            if let &Data::Singleton(AtomicData::String(ref s)) = self {
                return Ok(Data::Raw(hex::decode(s).map_err(|e|format_err!("Cannot decode hextring {}: {}", s, e))?));
            }
        }

        match self {
            &Data::Empty => {
                match to {
                    &CompoundDataType::None => Ok(Data::Empty),
                    _ => Err(format_err!("Cannot convert from empty data"))?
                }
            },
            &Data::Singleton(ref ad) => {
                match to {

                    &CompoundDataType::Singleton(ref dt) => Ok(Data::Singleton(ad.convert(dt)?)),
                    &CompoundDataType::Raw => Ok(Data::Raw(self.to_bytes()?)),
                    &CompoundDataType::Array(ref dt) => Ok(Data::Array(vec![Data::Singleton(ad.convert(dt)?);1])),
                    _ => Err(format_err!("Cannot convert from {:?} to {:?}", self.datatype(), to))?
                }
            },
            &Data::Raw(ref vec) => {
                Data::from_raw(vec.clone(), to)
            },
            &Data::Array(ref vec) => {
                match to {
                    &CompoundDataType::Singleton(DataType::String(ref padding_opt)) => {
                        let mut strres = String::new();
                        for v in vec {
                            strres = strres + &v.to_string();
                        }

                        if let &Some(pad_len) = padding_opt {
                            while strres.len() < pad_len {
                                strres = strres + " ";
                            }
                        }

                        Ok(Data::Singleton(AtomicData::String(strres)))
                    },
                    &CompoundDataType::Singleton(ref dt) => {
                        match vec.first() {
                            None => Err(format_err!("Cannot convert from empty data"))?,
                            Some(ref v) => {
                                let newdata = v.convert(&dt.clone().into())?;
                                match newdata {
                                    Data::Singleton(ad) => Ok(Data::Singleton(ad)),
                                    _ => Err(format_err!("Not a singleton"))?
                                }
                            }

                        }
                    },
                    &CompoundDataType::Array(ref dt) => {
                        let mut newvec: Vec<Data> = Vec::new();
                        for v in vec.iter() {
                            newvec.push(v.convert(&dt.clone().into())?);
                        }
                        Ok(Data::Array(newvec))
                    },
                    &CompoundDataType::Table(ref dt) => {
                        let mut newtab: BTreeMap<String, Data> = BTreeMap::new();
                        for (i, v) in vec.iter().enumerate() {
                            newtab.insert(i.to_string(), v.convert(&dt.clone().into())?);
                        }
                        Ok(Data::Table(newtab))
                    },
                    &CompoundDataType::Raw => {
                        let mut rawvec: Vec<u8> = Vec::new();
                        for v in vec.iter() {
                            let mut innerraw = v.to_bytes()?;
                            rawvec.append(&mut innerraw);
                        }
                        Ok(Data::Raw(rawvec))
                    },
                    &CompoundDataType::None => Ok(Data::Empty)
                }
            },
            &Data::Table(ref tab) => {
                match to {
                    &CompoundDataType::Singleton(ref dt) => {
                        match tab.iter().next() {
                            None => Err(format_err!("Cannot convert from empty data"))?,
                            Some((_, ref v)) => {
                                let newdata = v.convert(&dt.clone().into())?;
                                match newdata {
                                    Data::Singleton(ad) => Ok(Data::Singleton(ad)),
                                    _ => Err(format_err!("Not a singleton"))?
                                }
                            }

                        }
                    },
                    &CompoundDataType::Array(ref dt) => {
                        let mut vec: Vec<Data> = Vec::new();
                        for (_, v) in tab.iter() {
                            vec.push(v.convert(&dt.clone().into())?);
                        }
                        Ok(Data::Array(vec))
                    },
                    &CompoundDataType::Table(ref dt) => {
                        let mut newtab: BTreeMap<String, Data> = BTreeMap::new();
                        for (k, v) in tab.iter() {
                            newtab.insert(k.clone(), v.convert(&dt.clone().into())?);
                        }
                        Ok(Data::Table(newtab))
                    },
                    &CompoundDataType::Raw => {
                        let mut rawvec: Vec<u8> = Vec::new();
                        for (_, v) in tab.iter() {
                            let mut innerraw = v.to_bytes()?;
                            rawvec.append(&mut innerraw);
                        }
                        Ok(Data::Raw(rawvec))
                    },
                    &CompoundDataType::None => Ok(Data::Empty)
                }
            }
        }
    }

    pub fn to_string_result(&self) -> Result<String, Error> {
        let dt_str = self.convert(&CompoundDataType::Singleton(DataType::String(None)))?;

        if let Data::Singleton(AtomicData::String(ref s)) = dt_str {
            Ok(s.to_string())
        }
        else {
            Err(format_err!("Cannot convert to String"))
        }
    }

    pub fn to_atomic_data(&self) -> Result<AtomicData, Error> {
        if let &Data::Singleton(ref ad) = self {
            Ok(ad.clone())
        }
        else {
            Err(format_err!("Cannot convert {:?} to atomic data", self.datatype()))
        }
    }

    pub fn pow10(&self, exp_10: i32) -> Result<Data, Error> {
        match self {
            &Data::Singleton(ref ad) => Ok(Data::Singleton(ad.pow10(exp_10)?)),
            &Data::Array(ref vec) => {
                let mut newvec: Vec<Data> = Vec::new();
                for d in vec {
                    newvec.push(d.pow10(exp_10)?);
                }
                Ok(Data::Array(newvec))
            },
            &Data::Table(ref tab) => {
                let mut newtable: BTreeMap<String, Data> = BTreeMap::new();
                for (k, v) in tab.iter() {
                    newtable.insert(k.to_string(), v.pow10(exp_10)?);
                }
                Ok(Data::Table(newtable))
            },
            &Data::Empty => Ok(Data::Empty),
            _ => Err(format_err!("Cannot exponentiate this type of data"))
        }
    }
}

impl ToBytes for Data {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        match self {
            &Data::Empty => Ok(Vec::new()),
            &Data::Singleton(ref d) => d.to_bytes(),
            &Data::Array(ref vec) => {
                let mut new_vec: Vec<u8> = Vec::new();
                for v in vec.iter() {
                    let mut inner_vec = v.to_bytes()?;
                    new_vec.append(&mut inner_vec);
                }
                Ok(new_vec)
            },
            &Data::Table(ref tab) => {
                let mut new_vec: Vec<u8> = Vec::new();
                for (_, v) in tab.iter() {
                    let mut inner_vec = v.to_bytes()?;
                    new_vec.append(&mut inner_vec);
                }
                Ok(new_vec)
            },
            &Data::Raw(ref v) => Ok(v.clone())
        }
    }
}

impl Display for Data {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        match self.to_string_result() {
            Ok(s) => write!(f, "{}", s),
            Err(e) => write!(f, "Error: {}", e)
        }
    }
}

#[test]
fn test_convert() {
    let test_vec_u8 = vec![Data::create_u8(1), Data::create_u8(2), Data::create_u8(255)];

    let test_data = Data::Array(test_vec_u8);
    let i8data = test_data.convert(&CompoundDataType::Array(DataType::I8)).unwrap();

    let truth = Data::Array(vec![Data::create_i8(1), Data::create_i8(2), Data::create_i8(-1)]);
    assert_eq!(truth, i8data);

    let test_single = Data::create_str("42");
    let single_array = test_single.convert(&CompoundDataType::Array(DataType::U64)).unwrap();
    let truth = Data::Array(vec![Data::Singleton(AtomicData::U64(42));1]);
    assert_eq!(single_array, truth);

    let hexstr = Data::create_str("000102030405060708090a0b0c0d0e0f");
    let hexconv = hexstr.convert(&CompoundDataType::Singleton(DataType::Hexstring(false, None))).unwrap();
    let hex2raw = hexconv.convert(&CompoundDataType::Raw).unwrap();
    let hextruth: Vec<u8> = vec![0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
    assert_eq!(hextruth, hex2raw.to_bytes().unwrap());

    let back2hex = hex2raw.convert(&CompoundDataType::Singleton(DataType::Hexstring(false, None))).unwrap();
    assert_eq!(back2hex, hexstr);

    let estr = Data::create_str("-3.5e-3");
    let ef = estr.convert(&CompoundDataType::Singleton(DataType::F64)).unwrap();
    let ecdt = CompoundDataType::parse("e").unwrap();
    let estr2 = ef.convert(&ecdt).unwrap();

    let truth = Data::Singleton(AtomicData::F64(-0.0035f64));
    assert_eq!(ef, truth);
    assert_eq!(estr, estr2);

    let dim_str = Data::create_str("1.000009E-7");
    let convdi = dim_str.convert(&CompoundDataType::Singleton(DataType::Dim(0,"Hz".to_string()))).unwrap();
    assert_eq!(convdi, Data::Singleton(AtomicData::Dim(DimVal::from_str("1.000009E-7Hz").unwrap())));
}
