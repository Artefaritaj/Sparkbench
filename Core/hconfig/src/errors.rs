/*
 * Author:   ronan.lashermes@inria.fr
 * Creation: Mon Nov 27 2017
 *
 * Copyright (c) 2017 INRIA
 */

use std;
use store::store_key::StoreKey;


#[derive(Fail, Debug)]
#[fail(display = "A key is missing: {}", missing_key)]
pub struct MissingKeyError {
    missing_key: StoreKey,
}

impl MissingKeyError {
    pub fn new(missing_key: &StoreKey) -> MissingKeyError {
        MissingKeyError { missing_key: missing_key.clone() }
    }
}

#[derive(Fail, Debug)]
pub enum ParsingError {
    #[fail(display = "{}", _0)]
    Bool (#[cause] std::str::ParseBoolError ),

    #[fail(display = "{}", _0)]
    Float (#[cause] std::num::ParseFloatError),

    #[fail(display = "{}", _0)]
    Int (#[cause] std::num::ParseIntError)
}

impl From<std::str::ParseBoolError> for ParsingError {
    fn from(err: std::str::ParseBoolError) -> ParsingError {
        ParsingError::Bool(err)
    }
}

impl From<std::num::ParseFloatError> for ParsingError {
    fn from(err: std::num::ParseFloatError) -> ParsingError {
        ParsingError::Float(err)
    }
}

impl From<std::num::ParseIntError> for ParsingError {
    fn from(err: std::num::ParseIntError) -> ParsingError {
        ParsingError::Int(err)
    }
}