// @Author: ronan
// @Date:   14-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 21-10-2016
// @License: GPL

use byteorder::{LittleEndian, WriteBytesExt};
use failure::Error;

type Endianness = LittleEndian;

pub trait ToBytes {
    fn to_bytes(&self) -> Result<Vec<u8>, Error>;
}

pub trait FromBytes<T> {
    fn from_bytes(vec: Vec<u8>) -> Result<T, Error>;
}

impl ToBytes for bool {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        let mut v: Vec<u8> = Vec::new();
        if *self == true {
            v.push(1);
        }
        else {
            v.push(0);
        }
        Ok(v)
    }
}

impl ToBytes for u8 {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        let mut v: Vec<u8> = Vec::new();
        v.write_u8(*self)?;
        Ok(v)
    }
}

impl ToBytes for i8 {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        let mut v: Vec<u8> = Vec::new();
        v.write_i8(*self)?;
        Ok(v)
    }
}

impl ToBytes for u16 {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        let mut v: Vec<u8> = Vec::new();
        v.write_u16::<Endianness>(*self)?;
        Ok(v)
    }
}

impl ToBytes for i16 {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        let mut v: Vec<u8> = Vec::new();
        v.write_i16::<Endianness>(*self)?;
        Ok(v)
    }
}


impl ToBytes for u32 {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        let mut v: Vec<u8> = Vec::new();
        v.write_u32::<Endianness>(*self)?;
        Ok(v)
    }
}



impl ToBytes for i32 {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        let mut v: Vec<u8> = Vec::new();
        v.write_i32::<Endianness>(*self)?;
        Ok(v)
    }
}

impl ToBytes for u64 {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        let mut v: Vec<u8> = Vec::new();
        v.write_u64::<Endianness>(*self)?;
        Ok(v)
    }
}

impl ToBytes for i64 {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        let mut v: Vec<u8> = Vec::new();
        v.write_i64::<Endianness>(*self)?;
        Ok(v)
    }
}

impl ToBytes for f32 {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        let mut v: Vec<u8> = Vec::new();
        v.write_f32::<Endianness>(*self)?;
        Ok(v)
    }
}

impl ToBytes for f64 {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        let mut v: Vec<u8> = Vec::new();
        v.write_f64::<Endianness>(*self)?;
        Ok(v)
    }
}

impl ToBytes for String {
    fn to_bytes(&self) -> Result<Vec<u8>, Error> {
        Ok(self.as_bytes().to_vec())
    }
}

#[test]
fn test_byteorder() {
    let tu8 = 14u8.to_bytes().unwrap();
    assert!(tu8[0] == 14u8);

    let ti16 = 4555u16.to_bytes().unwrap();
    assert!(ti16[0] == 203 && ti16[1] == 17);
}
