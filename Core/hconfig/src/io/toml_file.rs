// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 14-11-2016
// @License: MIT



use toml;

use std::path::Path;
use std::collections::HashMap;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::str::FromStr;

use toml::value::Table;
use toml::Value;

use store::store_node::*;
use store::data::Data;

use failure::Error;

///Load a toml file as a StoreNode tree
pub fn toml_file_to_storenode<T: AsRef<Path>>(path: T) -> Result<StoreNode, Error> {
    let toml_table = try!(load_from_toml_file(path.as_ref()));
    let file_hm = toml_to_store_hashmap(toml_table);
    let mut root = StoreNode::new_empty();
    root.fill(file_hm);
    Ok(root)
}

pub fn toml_to_storenode(toml_data: &str) -> Result<StoreNode, Error> {
    let toml_table = toml_data.parse::<Value>()
                        .map_err(|e| format_err!("TOML file parsing failed: {}", e.to_string()))?;
    let file_hm = toml_to_store_hashmap(toml_table);
    let mut root = StoreNode::new_empty();
    root.fill(file_hm);
    Ok(root)
}

///Save a StoreNode tree in a toml file
pub fn storenode_to_toml_file(path: &Path, root: &StoreNode) -> Result<(), Error> {
    match save_to_toml_file(path,  &store_to_toml(&root)) {
        true => Ok(()),
        false => Err(format_err!("Unable to save file to disk."))
    }
}

fn load_from_toml_file(path: &Path) -> Result<Value, Error> {
    let mut s = String::new();
    File::open(path)?.read_to_string(&mut s)?;
    s.parse::<Value>()
        .map_err(|e| format_err!("TOML file parsing failed: {}", e.to_string()))
}

// fn contains_separator(s: &str) -> bool {
//     return s.contains(SEPARATOR);
// }

fn toml_to_store_hashmap(val: Value) -> HashMap<String, Data> {
    return toml_to_store_hashmap_prefix(val, "");
}

fn store_to_toml(store: &StoreNode) -> Value {
    let val = store_to_toml_value(store);
    let tab: Table = match val {
        toml::Value::Table(t) => t,
        x => {
            let mut t = Table::new();
            t.insert("".to_string(), x);
            t
        }
    };
    Value::Table(tab)
}

fn store_to_toml_value(store: &StoreNode) -> toml::Value {

    if store.is_leaf() {//value not table
        return store.get_node_value().clone().into();
    }
    else {//table
        let mut tab = Table::new();
        let mut arr = toml::value::Array::new();
        let mut create_arr: bool = false;
        for child in store.get_node_children() {
            if child.is_key_index() {//array
                if create_arr == false {//reserve space
                    create_arr = true;
                    let arr_len = store.get_node_children().len();
                    arr.resize(arr_len, toml::Value::Boolean(true));
                }


                let ind = match get_index_from_key(child.get_node_key()) {
                    Err(_) => 0,
                    Ok(i) => i
                };

                // println!("Insert at {}/{}", ind, store.get_node_chidren().len());
                arr[ind] = store_to_toml_value(child);
            }
            else {
                tab.insert(child.get_node_key().to_string(), store_to_toml_value(child));
            }
        }

        if arr.len() > 0 {
            return toml::Value::Array(arr);
        }
        else {
            return toml::Value::Table(tab);
        }

    }
}

//Index keys have format "{i}"
fn get_index_from_key(s: &str) -> Result<usize, Error> {
    let len = s.len();
    let ind_srt = &s[1..len-1];
    usize::from_str(ind_srt).map_err(|e|e.into())
}

fn toml_to_store_hashmap_prefix(val: Value, prefix: &str) -> HashMap<String, Data> {

    let mut result: HashMap<String, Data> = HashMap::new();

    match val {
        toml::Value::Table(tab) => {
                for (k,v) in tab {
                    let new_prefix = prefix.to_string() + SEPARATOR + &k ;
                    let child_map = toml_to_store_hashmap_prefix(v.clone(), &new_prefix);
                    result.extend(child_map);
                }
            },
        toml::Value::Array(arr) => {//add index as key
            let mut index = 0u64;
            for v in arr {
                let new_prefix = prefix.to_string() + SEPARATOR + "{"+&index.to_string()+"}";
                index += 1;
                let child_map = toml_to_store_hashmap_prefix(v.clone(), &new_prefix);
                result.extend(child_map);
            }
        },
        x => {
            result.insert(prefix.to_string(), x.into());
            }
    };

    return result;
}

pub fn save_to_toml_file(path: &Path, val: &Value) -> bool {
    match File::create(path) {//Open file
        Err(_) => false,
        Ok(mut file) => {//if success
            // let s = toml::Value::Table(tab.clone()).to_string();
            let s = val.to_string();
            match file.write_all(s.as_bytes()) {
                Err(_) => false,
                Ok(_) => true
            }
        }
    }
}



#[test]
fn test_toml_lib() {
    use store::store_key::StoreKey;

    let toml = r#"
        [test]
        foo = "bar"

        [other]
        ab = 1
        cd = 2
        ef = 3

        [test2]
        nothing = true
        arr2 = [ "red", "yellow", "green" ]

        [again.other2.other3]
        bidon = "oui"
        arr3 = [ [ 1, 2 ], [3, 4, 5] ]

        [[vars]]
        name = "PLAINTEXT"
        type = "[u8;16]"

        [[vars]]
        name = "CIPHERTEXT"
        type = "[u8;16]"

        [[vars]]
        name = "KEY"
        type = "[u8;16]"

        [[vars]]
        name = "TEST"
        type = "String"
    "#;

    let _ = toml_to_storenode(&toml).unwrap();

    let path = Path::new("test.toml");

    // let mut parser = toml::Parser::new(toml);
    let value = toml.parse::<Value>() .map_err(|e| format!("TOML file parsing failed: {}", e.to_string())).unwrap();
    assert!(save_to_toml_file(&path,&value), "found toml: {:?}", value);

    let parsed = load_from_toml_file(&path).unwrap();
    let file_hm = toml_to_store_hashmap(parsed);

    let mut keys_file_hm = String::new();
    for k in file_hm.keys() {
        keys_file_hm = keys_file_hm + " <" + k + ">";
    }
    assert!(file_hm.len() == 22, "Length is {}, keys are {}", file_hm.len(), keys_file_hm);
    assert!(file_hm.contains_key(".other.ab") == true, "keys are {}", keys_file_hm);

    let mut root = StoreNode::new_empty();
    root.fill(file_hm);

    let store_hm = root.to_hashmap();
    let mut keys_store_hm = String::new();
    for k in store_hm.keys() {
        keys_store_hm = keys_store_hm + " <" + k + ">";
    }

    assert!(store_hm.len() == 38, "Length is {}, keys are {}", store_hm.len(), keys_store_hm);
    assert!(store_hm.contains_key("again.other2.other3") == true);

    root.store_update(StoreKey::from(".test2.arr2.{1}"), Data::create_str("blue"));


    save_to_toml_file(&Path::new("test2.toml"),  &store_to_toml(&root));
}
