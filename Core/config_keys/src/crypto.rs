// @Author: Lashermes Ronan <ronan>
// @Date:   10-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 10-04-2017
// @License: MIT



pub const KEY: &'static str = "key";
pub const KEY_TO_FIND: &'static str = "key_to_find";
pub const PLAINTEXT: &'static str = "plaintext";
pub const CIPHERTEXT: &'static str = "ciphertext";
pub const FAULTY_CIPHERTEXT: &'static str = "faulty_ciphertext";