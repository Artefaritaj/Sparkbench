// @Author: ronan
// @Date:   22-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 22-09-2016
// @License: MIT



//Openocd
pub const OPENOCD: &'static str = "openocd";
pub const DEVICE_PATH: &'static str = "device_path";
pub const SCRIPTS_PATH: &'static str = "scripts_path";
pub const SHELL_COMMANDS: &'static str = "shell_commands";
pub const PRE_COMMANDS: &'static str = "pre_commands";
pub const POST_COMMANDS: &'static str = "post_commands";
pub const LAUNCH_COMMAND: &'static str = "launch_command";
pub const END_COMMANDS: &'static str = "end_commands";
pub const ARGS: &'static str = "args";
pub const WAIT: &'static str = "wait";
