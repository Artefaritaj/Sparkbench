// @Author: Lashermes Ronan <ronan>
// @Date:   16-03-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 16-03-2017
// @License: MIT



pub const RENAME: &'static str = "rename";
pub const APPEND: &'static str = "append";
pub const OVERWRITE: &'static str = "overwrite";
pub const BATCH: &'static str = "batch";