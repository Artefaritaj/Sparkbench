// @Author: Lashermes Ronan <ronan>
// @Date:   03-03-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 03-03-2017
// @License: MIT



pub const WAIT_VALIDATION: &'static str = "wait_validation";
pub const PRE_TEMPO: &'static str = "pre_tempo";
pub const INTER_TEMPO: &'static str = "inter_tempo";
pub const POST_TEMPO: &'static str = "post_tempo";

pub const SYNC_TIMEOUT: &'static str = "sync_timeout";
pub const NOT_ZERO: &'static str = "not_zero";
pub const POLL_TIMEOUT: &'static str = "poll_timeout";
