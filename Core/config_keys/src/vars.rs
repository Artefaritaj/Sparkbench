// @Author: ronan
// @Date:   22-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 17-11-2016
// @License: MIT



//vars
pub const VARS: &'static str = "vars";
pub const TERMINATION: &'static str = "termination";
pub const DURATION: &'static str = "duration";
pub const CONVERT_TO: &'static str = "convert_to";
pub const MIN: &'static str = "min";
pub const MAX: &'static str = "max";

// pub const INCMD_TYPE: &'static str = "incmd_type";
// pub const SHAPER_EXP: &'static str = "shaper_exp";
// pub const SHAPER_TYPE: &'static str = "shaper_type";
// pub const EXT_TYPE: &'static str = "ext_type";
// pub const TYPE: &'static str = "type";
pub const EXPONENT: &'static str = "exponent";
pub const SELECTORS: &'static str = "selectors";
pub const BYTE_LENGTH: &'static str = "byte_length";
pub const BYTE_LEN: &'static str = "byte_len";