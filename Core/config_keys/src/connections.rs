// @Author: ronan
// @Date:   22-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 23-12-2016
// @License: MIT

pub const CONNECTIONS: &'static str = "connections";

///Root key for a connection configuration
pub const CONNECTION_CONFIG: &'static str = "connection_config";

///Key for the link node
pub const CONNECTION_LINK: &'static str = "link";
///Key for the protocol node
pub const CONNECTION_PROTOCOL: &'static str = "protocol";
pub const LINK_SERIAL: &'static str = "serial";
pub const LINK_ETHERNET: &'static str = "ethernet";
pub const LINK_SMARTCARD: &'static str = "smartcard";

pub const REQUIRE: &'static str = "require";

///Port to connect to (either for serial or ethernet)
pub const PORT: &'static str = "port";

//timeouts
pub const TIMEOUT: &'static str = "timeout";
pub const READ_TIMEOUT: &'static str = "read_timeout";
pub const WRITE_TIMEOUT: &'static str = "write_timeout";

//Serial config
pub const BAUD_RATE: &'static str = "baudrate";
pub const DATABITS: &'static str = "databits";
pub const STOPBITS: &'static str = "stopbits";
pub const PARITY: &'static str = "parity";
pub const FLOW_CONTROL: &'static str = "flowcontrol";

pub const SERIAL_RTS: &'static str = "serial_rts";
pub const SERIAL_DTR: &'static str = "serial_dtr";
pub const SERIAL_CTS: &'static str = "serial_cts";
pub const SERIAL_DSR: &'static str = "serial_dsr";
pub const SERIAL_RI: &'static str = "serial_ri";
pub const SERIAL_CD: &'static str = "serial_cd";

//Ethernet config
pub const IPV4_ADDRESS: &'static str = "ipv4_address";
pub const IPV6_ADDRESS: &'static str = "ipv4_address";
pub const TRANSPORT: &'static str = "transport";

//Smartcard config
pub const SMARTCARD: &'static str = "smartcard";
pub const READER: &'static str = "reader";

pub const EXCLUSIVE: &'static str = "exclusive";
pub const SHARED: &'static str = "shared";
pub const DIRECT: &'static str = "direct";

pub const T0: &'static str = "T0";
pub const T1: &'static str = "T1";

//Protocols
pub const PROTOCOL_RAW: &'static str = "protocol_raw";
