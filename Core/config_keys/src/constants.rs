// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 23-09-2016
// @License: MIT


//fixed paths
pub const SPARKBENCH_CONFIG_FILE: &'static str = "sparkbench.toml";
pub const SPARKBENCH: &'static str = "sparkbench";

//command parser
pub const VAR_START: &'static str = "$(";//to have "$" in command: #(0x24) -> 0x24 is ASCII for $
pub const VEC_START: &'static str = "#(";//to have "#" in command: #(0x23) -> 0x24 is ASCII for $
pub const VAR_SEP: &'static str = ":";
pub const VEC_SEP: &'static str = ",";
pub const ASSIGN_SEP: &'static str = "=";
