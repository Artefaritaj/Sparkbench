// @Author: ronan
// @Date:   22-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 23-12-2016
// @License: MIT



//Useful generics keys
pub const PATH: &'static str = "path";
pub const COMMAND: &'static str = "command";
pub const OPTION: &'static str = "option";
pub const PROTOCOL: &'static str = "protocol";
pub const DEVICE: &'static str = "device";
pub const ALGORITHM: &'static str = "algorithm";
pub const NAME: &'static str = "name";
pub const MODULE: &'static str = "module";
pub const LABEL: &'static str = "label";
pub const ERROR: &'static str = "error";
pub const CONFIG: &'static str = "config";
pub const VALUE: &'static str = "value";
pub const LEN: &'static str = "len";
pub const TYPE: &'static str = "type";
pub const SHARE: &'static str = "share";
pub const AUTO: &'static str = "auto";
pub const ANY: &'static str = "any";
pub const RAW: &'static str = "raw";
pub const UNDEFINED: &'static str = "undefined";
pub const COUNT: &'static str = "count";

//plural
pub const COMMANDS: &'static str = "commands";
pub const CMDS: &'static str = "cmds";
pub const ACTORS: &'static str = "actors";
pub const OPTIONS: &'static str = "options";
pub const MODULES: &'static str = "modules";
pub const ERRORS: &'static str = "errors";
pub const CONFIGS: &'static str = "configs";
pub const VALUES: &'static str = "values";
