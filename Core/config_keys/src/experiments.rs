// @Author: ronan
// @Date:   17-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016


pub const EXPERIMENT: &'static str = "experiment";
pub const RESUME_KIND: &'static str = "resume_kind";
pub const PROCESS: &'static str = "process";
pub const PROCESSES: &'static str = "processes";
pub const RESET: &'static str = "reset";
pub const RULES: &'static str = "rules";
pub const EXECUTE: &'static str = "execute";
pub const SHUTDOWN: &'static str = "shutdown";
pub const LINKS: &'static str = "links";
pub const FEEDBACK: &'static str = "feedback";
pub const FEEDFRONT: &'static str = "feedfront";
pub const FEEDX: &'static str = "feedx";
pub const BACK: &'static str = "back";
pub const FRONT: &'static str = "front";
pub const DUT: &'static str = "dut";
pub const SOURCES: &'static str = "sources";
pub const SINK: &'static str = "sink";
pub const SYNC: &'static str = "sync";
pub const AUTOLINKS: &'static str = "autolinks";
pub const ENTRY: &'static str = "entry";
pub const PULSE: &'static str = "pulse";
pub const CONSTANTS: &'static str = "constants";
