// @Author: ronan
// @Date:   22-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 22-09-2016
// @License: MIT



//Benchfiles
pub const BENCHFILES: &'static str = "benchfiles";

//Modules
pub const INSTANCED: &'static str = "instanced";
