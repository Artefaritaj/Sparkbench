// @Author: Lashermes Ronan <ronan>
// @Date:   25-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 25-04-2017
// @License: MIT



pub const TOML_FILES: &'static str = "toml_files";
pub const INPUT_NAME: &'static str = "input_name";
pub const OUTPUT_NAME: &'static str = "output_name";
pub const CREATION_MODE: &'static str = "creation_mode";
pub const TEXT: &'static str = "text";
pub const POLL_DELAY: &'static str = "poll_delay";
pub const ERROR_DISTANCE: &'static str = "error_distance";
pub const REPLICATION_FACTOR: &'static str = "replication_factor";
pub const DIVIDE_BY: &'static str = "divide_by";
pub const MAX_SIZE: &'static str = "max_size";
pub const STEP_TYPE: &'static str = "step_type";
pub const STEP: &'static str = "step";
pub const GENERATOR_TYPE: &'static str = "generator_type";
pub const HUMAN: &str = "human";
pub const FIXED: &str = "fixed";
pub const FORMAT: &str = "format";
pub const OVERWRITE_RULES: &str = "ow";
pub const URL: &str = "url";
pub const BIAS: &str = "bias";
pub const CONTINUE: &str = "continue";
pub const CONTROLLER: &str = "controller";

//XYZ stage
pub const X: &str = "x";
pub const Y: &str = "y";
pub const Z: &str = "z";
pub const XSTEP: &'static str = "xstep";
pub const YSTEP: &'static str = "ystep";
pub const MOVE: &str = "move";

//Mapper
pub const AXES: &str = "axes";

