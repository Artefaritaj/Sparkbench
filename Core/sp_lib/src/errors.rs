/*
 * Author:   ronan.lashermes@inria.fr
 * Creation: Tue Nov 28 2017
 *
 * Copyright (c) 2017 INRIA
 */

#[derive(Fail, Debug)]
#[fail(display = "Failed to create a command {}: {}", cmd_name, info)]
pub struct CommandCreationError {
    cmd_name: String,
    info: String
}

impl CommandCreationError {
    pub fn new(cmd_name: &str, info: &str) -> CommandCreationError {
        CommandCreationError { cmd_name: cmd_name.to_string(), info: info.to_string() }
    }
}

#[derive(Fail, Debug)]
#[fail(display = "The data received does not fit the {} command definition: {}", cmd_name, info)]
pub struct CommandNotValidated {
    cmd_name: String,
    info: String
}

impl CommandNotValidated {
    pub fn new(cmd_name: &str, info: &str) -> CommandNotValidated {
        CommandNotValidated { cmd_name: cmd_name.to_string(), info: info.to_string() }
    }
}

#[derive(Fail, Debug)]
#[fail(display = "The command {} is unknown", cmd_name)]
pub struct CommandUnknown {
    cmd_name: String
}

impl CommandUnknown {
    pub fn new(cmd_name: &str) -> CommandUnknown {
        CommandUnknown { cmd_name: cmd_name.to_string()}
    }
}

#[derive(Fail, Debug)]
#[fail(display = "The data received for command {} is incorrect: {}", cmd_name, info)]
pub struct DataReceivedError {
    cmd_name: String,
    info: String
}

impl DataReceivedError {
    pub fn new(cmd_name: &str, info: &str) -> DataReceivedError {
        DataReceivedError { cmd_name: cmd_name.to_string(), info: info.to_string() }
    }
}

#[derive(Fail, Debug)]
#[fail(display = "The data sent for command {} is incorrect: {}", cmd_name, info)]
pub struct DataSentError {
    cmd_name: String,
    info: String
}

impl DataSentError {
    pub fn new(cmd_name: &str, info: &str) -> DataSentError {
        DataSentError { cmd_name: cmd_name.to_string(), info: info.to_string() }
    }
}

#[derive(Fail, Debug)]
#[fail(display = "Failed to format value: {}", info)]
pub struct FormattingError {
    info: String
}

impl FormattingError {
    pub fn new(info: &str) -> FormattingError {
        FormattingError { info: info.to_string()}
    }
}

#[derive(Fail, Debug)]
#[fail(display = "Failed to create module: {}", info)]
pub struct ModuleCreationError {
    info: String
}

impl ModuleCreationError {
    pub fn new(info: &str) -> ModuleCreationError {
        ModuleCreationError { info: info.to_string()}
    }
}

#[derive(Fail, Debug)]
#[fail(display = "Failed to execute module: {}", info)]
pub struct ModuleExecutionError {
    info: String
}

impl ModuleExecutionError {
    pub fn new(info: &str) -> ModuleExecutionError {
        ModuleExecutionError { info: info.to_string()}
    }
}

#[derive(Fail, Debug)]
#[fail(display = "Timeout upon data reception ({})", source)]
pub struct RxTimeout {
    source: String
}

impl RxTimeout {
    pub fn new(source: &str) -> RxTimeout {
        RxTimeout { source: source.to_string()}
    }
}

#[derive(Fail, Debug)]
#[fail(display = "Variable missing {}: {}", var_name, info)]
pub struct VariableMissingError {
    var_name: String,
    info: String
}

impl VariableMissingError {
    pub fn new(var_name: &str, info: &str) -> VariableMissingError {
        VariableMissingError { var_name: var_name.to_string(), info: info.to_string() }
    }
    pub fn get_variable_name(&self) -> &str {
        &self.var_name
    }
}