/*
 * Author:   ronan.lashermes@inria.fr
 * Creation: Tue Nov 28 2017
 *
 * Copyright (c) 2017 INRIA
 */


pub enum LinkType {
    Ethernet,
    Serial,
    Executable,
    Proprietary
}
