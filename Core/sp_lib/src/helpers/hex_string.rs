// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 21-10-2016



use rustc_serialize::hex::FromHex;
use rustc_serialize::hex::ToHex;
use crate::helpers::byte_vectors::generate_random_byte_vector;

pub fn to_hex_string(bytes: &Vec<u8>) -> String {
  bytes.to_hex()
}

pub fn from_hex_string(s: &str) -> Result<Vec<u8>, String> {
    match s.from_hex() {
        Ok(arr) => Ok(arr),
        Err(e) => Err(e.to_string())
    }
}

pub fn generate_random_hex_str(byte_size: usize) -> String {
    let byte_vector = generate_random_byte_vector(byte_size);
    to_hex_string(&byte_vector)
}

#[test]
fn tets_hex_strings() {
    let hs = generate_random_hex_str(32);
    let v = from_hex_string(&hs).unwrap();
    let hs2 = to_hex_string(&v);
    assert!(hs == hs2);
}
