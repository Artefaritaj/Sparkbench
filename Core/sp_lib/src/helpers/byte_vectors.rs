//use hconfig::store::data::*;
//use helpers::hex_string::from_hex_string;
use rand;
use rand::Rng;

// pub fn data_to_byte_vector(d: &Data) -> Option<Vec<u8>> {
//     match d {
//         &Data::String(ref s) => from_hex_string(s),
//         x => x.to_vec_u8()
//     }
// }

pub fn generate_random_byte_vector(size: usize) -> Vec<u8> {
    let mut result : Vec<u8> = Vec::new();

    let mut rng = rand::thread_rng();
    for _ in 0..size {
        result.push(rng.gen::<u8>());
    }

    result
}

#[test]
fn test_bv() {
    let v = generate_random_byte_vector(12);
    assert!(v.len() == 12);
}
