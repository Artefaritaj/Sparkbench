// @Author: ronan
// @Date:   30-08-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 13-12-2016
// @License: GPL


use std::collections::HashMap;
use std::collections::hash_map::{IterMut, Iter};

#[derive(Debug)]
///Used to manage TOML files in BenchFiles
pub struct NameMapper<T> {
    name_mapping: HashMap<String, T>
}

impl<T> NameMapper<T> {
    pub fn new() -> NameMapper<T> {
        NameMapper { name_mapping: HashMap::new() }
    }

    pub fn clear(&mut self) {
        self.name_mapping.clear();
    }

    pub fn insert(&mut self, name: &str, res: T) {
        self.name_mapping.insert(name.to_string(), res);
    }

    pub fn insert_if_not_here(&mut self, name: &str, res: T) -> Result<(), String> {
        if self.contains_name(name) {
            Err(format!("Key {} already present.", name))
        }
        else {
            self.insert(name, res);
            Ok(())
        }
    }

    pub fn contains_name(&self, name: &str) -> bool {
        self.name_mapping.contains_key(name)
    }

    pub fn remove(&mut self, name: &str) -> Option<T> {
        self.name_mapping.remove(name)
    }

    pub fn get(&self, name: &str) -> Option<&T> {
        self.name_mapping.get(name)
    }

    pub fn get_mut(&mut self, name: &str) -> Option<&mut T> {
        self.name_mapping.get_mut(name)
    }

    pub fn get_whole_mapping(&self) -> &HashMap<String, T> {
        &self.name_mapping
    }

    pub fn get_values(&self) -> Vec<&T> {
        self.name_mapping.values().collect()
    }

    pub fn get_keys(&self) -> Vec<&String> {
        self.name_mapping.keys().collect()
    }

    pub fn iter(&self) -> Iter<String, T> {
        self.name_mapping.iter()
    }

    pub fn iter_mut(&mut self) -> IterMut<String, T> {
        self.name_mapping.iter_mut()
    }

    pub fn len(&self) -> usize {
        self.name_mapping.len()
    }

    pub fn is_empty(&self) -> bool {
        self.name_mapping.is_empty()
    }

    pub fn insert_string(&mut self, name: String, res: T) {
        self.name_mapping.insert(name, res);
    }

    pub fn to_hashmap(self) -> HashMap<String, T> {
        self.name_mapping
    }
}

impl<T:Clone> Clone for NameMapper<T> {
    fn clone(&self) -> Self {
        NameMapper { name_mapping: self.name_mapping.clone() }
    }
}

impl<T:Clone> NameMapper<T> {

    pub fn get_clone_by_name(&self, name: &str) -> Option<T> {
        match self.get(name) {
            Some(c) => Some(c.clone()),
            None => None
        }
    }

    pub fn merge(&mut self, other: &NameMapper<T>) {
        for (k,v) in other.get_whole_mapping().iter() {
            self.insert(k, v.clone());
        }
    }
}
