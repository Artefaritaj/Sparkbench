// @Author: ronan
// @Date:   14-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 14-10-2016

use crate::errors::RxTimeout;

use chrono::prelude::*;

pub fn check_timeout(end_time: DateTime<Utc>, error_info: String) -> Result<(), RxTimeout> {
    //check timeout
    let now = Utc::now();
    if now > end_time {
        Err(RxTimeout::new(&error_info))
    }
    else {
        Ok(())
    }
}
