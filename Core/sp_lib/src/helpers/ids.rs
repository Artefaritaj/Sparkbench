// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 14-11-2016



use rand;
use rand::Rng;
use std::hash::{Hash, Hasher};
use std::collections::hash_map::DefaultHasher;

pub fn gen_rng_id() -> u64 {
    rand::thread_rng().gen::<u64>()
}

pub fn next_id(prev_id: u64) -> u64 {
    let mut s = DefaultHasher::new();
    prev_id.hash(&mut s);
    s.finish()
}

#[test]
fn test_id() {
    let i1 = gen_rng_id();
    let i2 = next_id(i1);
    assert!(i1 != i2);
}
