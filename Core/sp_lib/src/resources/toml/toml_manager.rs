// @Author: ronan
// @Date:   30-08-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 13-12-2016
// @License: GPL



use std::path::{Path, PathBuf};
use std::collections::HashMap;
use std::collections::hash_map::IterMut;

use crate::helpers::name_mapper::*;


pub struct TOMLManager {
    folder_path: PathBuf,
    mapper: NameMapper<PathBuf>,
    types: NameMapper<Vec<String>>, //map a type string (key) to a list of toml file name of that type
}

// pub type TOMLManager = NameMapper<PathBuf>;
// pub trait TOMLManagerTrait {
//     fn verify_all(&mut self) -> usize;
// }
//

impl TOMLManager {

    pub fn clear(&mut self) {
        self.mapper.clear();
    }

    pub fn insert<P: AsRef<Path>>(&mut self, name: &str, res: P) {
        let pathbuf: PathBuf = res.as_ref().canonicalize().unwrap_or(PathBuf::from(res.as_ref()));
        self.mapper.insert(name, pathbuf);
    }

    pub fn insert_if_not_here<P: AsRef<Path>>(&mut self, name: &str, res: P) -> Result<(), String> {
        if self.contains_name(name) {
            Err(format!("Key {} already present.", name))
        }
        else {
            let pathbuf: PathBuf = res.as_ref().canonicalize().unwrap_or(PathBuf::from(res.as_ref()));
            self.insert(name, pathbuf);
            Ok(())
        }
    }

    pub fn contains_name(&self, name: &str) -> bool {
        self.mapper.contains_name(name)
    }

    pub fn remove(&mut self, name: &str) -> Option<PathBuf> {
        self.mapper.remove(name)
    }

    pub fn get(&self, name: &str) -> Option<&Path> {
        self.mapper.get(name).and_then(|pb|Some(pb.as_path()))
    }

    pub fn get_mut(&mut self, name: &str) -> Option<&mut PathBuf> {
        self.mapper.get_mut(name)
    }

    pub fn get_whole_mapping(&self) -> &HashMap<String, PathBuf> {
        &self.mapper.get_whole_mapping()
    }

    pub fn get_values(&self) -> Vec<&PathBuf> {
        self.mapper.get_values()
    }

    pub fn get_keys(&self) -> Vec<&String> {
        self.mapper.get_keys()
    }

    pub fn iter_mut(&mut self) -> IterMut<String, PathBuf> {
        self.mapper.iter_mut()
    }

    pub fn len(&self) -> usize {
        self.mapper.len()
    }

    pub fn get_folder_path(&self) -> &Path {
        self.folder_path.as_path()
    }

    pub fn new(folder: &Path) -> TOMLManager {
        let folder_path = folder.canonicalize().unwrap_or(PathBuf::from(folder));
        TOMLManager { mapper: NameMapper::new(), types: NameMapper::new(), folder_path: folder_path}
    }

    pub fn merge(&mut self, other: &TOMLManager) {
        self.mapper.merge(&other.mapper);
    }

    pub fn get_clone_by_name(&self, name: &str) -> Option<PathBuf> {
        self.mapper.get_clone_by_name(name)
    }

    pub fn get_categories(&self) -> Vec<String> {
        let mut res: Vec<String> = Vec::new();
        for c in self.types.get_keys() {
            res.push(c.clone());
        }
        res
    }

    pub fn get_toml_names_from_category(&self, cat: &str) -> Vec<String> {
        self.types.get(cat).map_or(Vec::new(), |v|v.clone())
    }


    ///Verify that all path are still valid. Delete entry othewise. Return number of invalid path found.
    pub fn verify_all(&mut self) -> usize {

        let mut to_del = Vec::new();
        for (k,v) in self.mapper.get_whole_mapping().iter() {
            if !v.exists() {
                to_del.push(k.clone());
            }
        }

        for k in to_del.iter() {
            self.mapper.remove(&k.to_string());
        }

        self.generate_all_types();

        to_del.len()
    }

    fn generate_type(types: &mut NameMapper<Vec<String>>, name: &str, p: &Path) {
        p.parent()
            .and_then(|parent|parent.file_name())
            .and_then(|pname|pname.to_str())
            .map(|pname|{
                    let ok = match types.get_mut(pname) {
                        Some(cat) => {
                            cat.push(name.to_string());
                            true
                        },
                        None => false
                    };
                    if !ok {
                        types.insert(pname, vec![name.to_string()]);
                    }
            });
    }

    fn generate_all_types(&mut self) {
        self.types.clear();
        let mut types: NameMapper<Vec<String>> = NameMapper::new();

        for (k,v) in self.mapper.iter_mut() {
            TOMLManager::generate_type(&mut types, &k, &v);
        }

        self.types = types;
    }
}
