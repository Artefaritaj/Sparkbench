// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 21-10-2016
// @License: GPL

use config_keys::*;

use std::cmp::min;

use hconfig::store::{StoreNode, StoreKey, Data, CompoundDataType};
use hconfig::io::byte_convert::*;

use failure::Error;

#[derive(PartialEq,Eq,Clone,Debug)]
pub enum ExpressionToken {
    Fixed(String),
    Dollar(String),//$(VAR) for b variable
    Sharp(String)//#(v1,v2) for byte vectors
}

///(token, rest)
pub fn next_token(s: &str) -> Result<(ExpressionToken, String), Error> {

    let mut rest = String::new();

    //var
    let token = if s.starts_with(VAR_START) {//token is a var
        let end_token = match s.find(")") {
            Some(i) => i,
            None => {return Err(format_err!("Variable has no end. Syntax is $(VAR:formatting) with optionnal :formatting."));}
        };

        rest = s[(end_token+1)..s.len()].to_string();
        ExpressionToken::Dollar(s[2..end_token].to_string())
    }
    else if s.starts_with(VEC_START) {//token is a vec
        let end_token = match s.find(")") {
            Some(i) => i,
            None => {return Err(format_err!("Vector has no end. Syntax is #(VAL1,VAL2)."));}
        };

        rest = s[(end_token+1)..s.len()].to_string();
        ExpressionToken::Sharp(s[2..end_token].to_string())
    }
    else {//token is a litteral

        let var_i = s.find(VAR_START);
        let vec_i = s.find(VEC_START);
        let min_i = match var_i {
            Some(i1) => {
                match vec_i {
                    Some(i2) => {
                        Some(min(i1,i2))
                    },
                    None => {
                        Some(i1)
                    }
                }
            },
            None => {
                match vec_i {
                    Some(i2) => {
                        Some(i2)
                    },
                    None => {
                        None
                    }
                }
            }
        };

        match min_i {
            Some(i) => {
                rest = s[i..s.len()].to_string();
                ExpressionToken::Fixed(s[0..i].to_string())
            },
            None => {
                ExpressionToken::Fixed(s.to_string())
            }
        }
    };

    Ok((token, rest))
}

///Parse a sharp token (string inside: #(here))
///A sharp token represent a fixed byte vector
pub fn parse_sharp_token(s: &str) -> Result<Vec<u8>, Error> {
    let splitted: Vec<&str> = s.split(VEC_SEP).collect();
    let mut vector: Vec<u8> = Vec::new();
    for val in splitted {

        let trimmed = val.trim();
        if trimmed.starts_with("0x") || trimmed.starts_with("0X") {
            vector.push(u8::from_str_radix(&trimmed[2..trimmed.len()], 16).map_err(|e| format_err!("{}", e))?)
        }
        else {
            vector.push(u8::from_str_radix(trimmed, 10).map_err(|e| format_err!("{}", e))?)
        }
    }
    Ok(vector)
}

///Parse a fixed expression and convert to byte vector
pub fn parse_fixed(s: &str) -> Result<Vec<u8>, Error> {

    if s == "" {
        return Ok(Vec::new());
    }

    let mut tokens: Vec<ExpressionToken> = Vec::new();
    let mut rest = s.to_string();

    //read all tokens
    while rest.len() > 0 {
        let (tok, working) = match next_token(&rest) {
            Ok((t,r)) => (t,r),
            Err(s) => {return Err(s);}//fail to read token
        };

        rest = working;
        tokens.push(tok);
    }

    //transform tokens in CmdPieces
    let mut vector: Vec<u8> = Vec::new();
    for tok in tokens.iter() {
        let mut to_append = create_vec_from_token(&tok)?;
        vector.append(&mut to_append);
    }

    Ok(vector)
}

fn create_vec_from_token(tok: &ExpressionToken) -> Result<Vec<u8>, Error> {
    match tok {
        &ExpressionToken::Fixed(ref s) => Ok(s.as_bytes().to_vec()),
        &ExpressionToken::Sharp(ref s) => parse_sharp_token(s),
        &ExpressionToken::Dollar(_) => Err(format_err!("Expression must be constant."))
    }
}

///Parse a fixed expression and convert to i64
pub fn parse_static_i64(s: &str, node: &StoreNode) -> Result<i64, Error> {

    if s == "" {
        return Err(format_err!("Empty string cannot be parsed"));
    }

    //only 1 token considered
    let (tok, _) = next_token(s)?;

    match tok {
        ExpressionToken::Dollar(key) => {
            let data = parse_dollar_static_data(&key,  node)?
                       .to_atomic_data()?;
            data.to_i64()
        },
        ExpressionToken::Fixed(val) => {
            val.parse::<i64>().map_err(|e|format_err!("{}", e))
        },
        _ => Err(format_err!("Unable to parse {}", s))
    }

}



///Parse a fixed expression and convert to string
pub fn parse_static_string(s: &str, node: &StoreNode) -> Result<String, Error> {
    let mut rest = s.to_string();
    let mut tokens: Vec<ExpressionToken> = Vec::new();

    //read all tokens
    while rest.len() > 0 {
        let (tok, working) = match next_token(&rest) {
            Ok((t,r)) => (t,r),
            Err(s) => {return Err(s);}//fail to read token
        };

        rest = working;
        tokens.push(tok);
    }

    let mut result = String::new();

    //transform tokens in strings
    for tok in tokens.iter() {
        let new_part = match tok {
            &ExpressionToken::Dollar(ref key_str) => {
                let data = parse_dollar_static_data(&key_str, node)?;
                data.to_string_result()?
            },
            &ExpressionToken::Fixed(ref val) => {
                val.clone()
            },
            _ => { return Err(format_err!("Unable to parse {}", s)); }
        };
        result.push_str(&new_part);
    }

    Ok(result)
}

///Parse a fixed expression and convert to Data
pub fn parse_static_data(s: &str, node: &StoreNode) -> Result<Data, Error> {

    if s == "" {
        return Err(format_err!("Empty string cannot be parsed"));
    }

    //only 1 token considered
    let (tok, _) = next_token(s)?;

    match tok {
        ExpressionToken::Dollar(key_str) => {
            parse_dollar_static_data(&key_str, node)
        },
        ExpressionToken::Fixed(val) => {
            Ok(Data::create_string(val))
        },
        _ => Err(format_err!("Unable to parse {}", s))
    }
}

///Parse a fixed expression and convert to byte vector
pub fn parse_static_vecu8(s: &str, node: &StoreNode) -> Result<Vec<u8>, Error> {

    if s == "" {
        return Ok(Vec::new());
    }

    let mut tokens: Vec<ExpressionToken> = Vec::new();
    let mut rest = s.to_string();

    //read all tokens
    while rest.len() > 0 {
        let (tok, working) = match next_token(&rest) {
            Ok((t,r)) => (t,r),
            Err(s) => {return Err(s);}//fail to read token
        };

        rest = working;
        tokens.push(tok);
    }

    //transform tokens in CmdPieces
    let mut vector: Vec<u8> = Vec::new();
    for tok in tokens.iter() {
        let mut to_append = create_vec_from_token_static(&tok, node)?;
        vector.append(&mut to_append);
    }

    Ok(vector)
}

fn create_vec_from_token_static(tok: &ExpressionToken, node: &StoreNode) -> Result<Vec<u8>, Error> {
    match tok {
        &ExpressionToken::Fixed(ref s) => Ok(s.as_bytes().to_vec()),
        &ExpressionToken::Sharp(ref s) => parse_sharp_token(s),
        &ExpressionToken::Dollar(ref s) => parse_dollar_static_vecu8(s, node)
    }
}

pub fn parse_dollar_static_data(cmd_str: &str, node: &StoreNode) -> Result<Data, Error> {
    let mut splitted: Vec<&str> = cmd_str.split(VAR_SEP).collect();
    if splitted.len() == 0 {
        return Err(format_err!("Command piece empty."));
    }
    else {
        while splitted.len() != 3 {
            splitted.push("");//add default formatting and selector
        }
    }

    let key = StoreKey::from(splitted[0]);
    let format = splitted[1];
    //let selector = splitted[2];

    parse_dollar_static_data_splitted(&key, format, node)
}

pub fn parse_dollar_static_data_splitted(key: &StoreKey, format: &str, config: &StoreNode) -> Result<Data, Error> {
    // get the data present in the config
    let config_data = config.get_value(key) .map_err(|e|format_err!("{}", e))?;


    match CompoundDataType::parse(format) {//get datatype to convert to from format string
        Ok(conv_to_datatype) => config_data.convert(&conv_to_datatype),//convert to data according to datatype
        Err(_) => Ok(config_data)// keep data as is without data type info
    }

}

pub fn parse_selectors(selectors: &str, node: Option<&StoreNode>) -> Result<Option<Vec<usize>>, Error> {
    if selectors == "" || selectors == "all" {
        Ok(None)
    }
    else {
        let mut selected: Vec<usize> = Vec::new();
        let selected_str = selectors.split(VEC_SEP);
        match node {
            Some(n) => {
                for sel in selected_str {
                    let ind = parse_static_i64(sel, n)? as usize;
                    selected.push(ind);
                }
            },
            None => {
                for sel in selected_str {
                    let ind = sel.parse::<i64>().map_err(|e|format_err!("{}", e))? as usize;
                    selected.push(ind);
                }
            }
        }

        Ok(Some(selected))
    }
}

pub fn parse_dollar_static_vecu8(cmd_str: &str, node: &StoreNode) -> Result<Vec<u8>, Error> {

    let mut splitted: Vec<&str> = cmd_str.split(VAR_SEP).collect();
    if splitted.len() == 0 {
        return Err(format_err!("Command piece empty."));
    }
    else {
        while splitted.len() != 3 {
            splitted.push("");//add default formatting and selector
        }
    }

    let key = StoreKey::from(splitted[0]);
    let format = splitted[1];
    let selector = splitted[2];

    let out_vec = parse_dollar_static_data_splitted(&key, format, node)?.to_bytes()?;
    let selectors = parse_selectors(selector, Some(node))?;

    match selectors {
        None => Ok(out_vec),
        Some(indices) => {
            let mut new_out_vec: Vec<u8> = Vec::new();
            for ind in indices {
                new_out_vec.push(*out_vec.get(ind).ok_or(format_err!("Index {} is not valid", ind))?);
            }
            Ok(new_out_vec)
        }
    }
}

///Return Vec["v1", "v2", "v3"] for a node: n = ["v1", "v2", "v3"]
pub fn parse_string_array(node: &StoreNode) -> Result<Vec<String>, Error> {
    let mut result: Vec<String> = Vec::new();
    let max = node.get_node_children().len();

    if max == 0 {
        match node.get_node_value().to_string_result() {
            Ok(ref s) => { result.push(s.to_string()); },
            _ => {}
        }
    }
    else {
        for i in 0..max {
            let cnode = node.get_node(&StoreKey::from(&format!("{{{}}}", i))).map_err(|e|format_err!("{}", e))?;
            let cstr = cnode.get_node_value().to_string_result()?;
            result.push(cstr);
        }
    }
    Ok(result)
}

pub fn parse_static_string_array(array_node: &StoreNode, vars_node: &StoreNode) -> Result<Vec<String>, Error> {
    let arr_str = parse_string_array(array_node)?;
    let mut res: Vec<String> = Vec::new();
    for s in arr_str.iter() {
        let r = parse_static_string(s, vars_node)?;
        res.push(r);
    }
    Ok(res)
}

///Return Vec[Data::String("v1"), Data::Int64(1)] for a node: n % ["v1", 1]
pub fn parse_data_array(node: &StoreNode) -> Result<Vec<Data>, Error> {
    let mut result: Vec<Data> = Vec::new();
    let max = node.get_node_children().len();
    for i in 0..max {
        let cnode = node.get_node(&StoreKey::from(&format!("{{{}}}", i))).map_err(|e|format_err!("{}", e))?;
        let cval = cnode.get_node_value();
        result.push(cval.clone());
    }
    Ok(result)
}

pub fn parse_string(node: &StoreNode) -> Result<String, Error> {
    node.get_node_value().to_string_result()
}



#[test]
fn test_format() {
    assert!(Data::create_i64(0xab12).convert(&CompoundDataType::parse("Hexstring8").unwrap()).unwrap() == Data::create_string(format!("0000AB12")));
    // assert!(format_integer_val(0xab12, "u32").unwrap().len() == 4);
    // assert!(format_integer_val(0xab12, "hexstring").unwrap() == "ab12".as_bytes().to_vec());
    // assert!(format_integer_val(0xab12, "Hexstring8").unwrap() == "0000AB12".as_bytes().to_vec());
    // assert!(format_integer_val(0xab12, "string8").unwrap() == "00043794".as_bytes().to_vec());
    //
    // assert!(format_string_var("16", "u8").unwrap()[0] == 16);
    // assert!(format_string_var("ab12", "hexstring").unwrap()[1] == 0x12, "{} instead of 18", format_string_var("ab12", "hexstring").unwrap()[0]);
    // assert!(format_string_var("ab12", "hexstring").unwrap()[0] == 0xab);
}

#[test]
fn test_parser() {
    let test = "abc#(0x21, 34)";
    let parsed_res = parse_fixed(test);

    assert!(parsed_res.is_ok() == true, "Parsing error.");
    let parsed = parsed_res.unwrap();

    let mut vec_check = String::from("abc").as_bytes().to_vec();
    vec_check.push(0x21);
    vec_check.push(34);

    assert!(vec_check.len() == parsed.len());
    for i in 0..parsed.len() {
        assert!(vec_check[i] == parsed[i], "Byte {} is incorrect.", i);
    }
}

#[test]
fn test_string_parser() {
    //create store first
    let mut root = StoreNode::new_empty();
    root.store_update(StoreKey::from("key"), Data::create_string(format!("1122")));
    root.store_update(StoreKey::from("postfix"), Data::create_string(format!("pf")));

    let test = "abc$(key)def$(postfix)ghi";
    let result = parse_static_string(test, &root).unwrap();

    assert_eq!(result, "abc1122defpfghi");
}

#[test]
fn test_parse_string_array() {
    use hconfig::io::toml_file::toml_to_storenode;

    let toml = r#"
        a = [ "red", "yellow", "green" ]
        b = "blue"
    "#;

    let store = toml_to_storenode(&toml).unwrap();
    let anode = store.get_node(&StoreKey::from("a")).unwrap();
    let bnode = store.get_node(&StoreKey::from("b")).unwrap();

    let vec1 = parse_string_array(&anode).unwrap();
    let truth1 = vec!["red", "yellow", "green"];
    assert_eq!(vec1, truth1);

    let vec2 = parse_string_array(&bnode).unwrap();
    let truth2 = vec!["blue";1];
    assert_eq!(vec2, truth2);
}