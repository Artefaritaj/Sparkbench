// @Author: ronan
// @Date:   19-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016


use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use crate::interfaces::modules::Module;
use crate::interfaces::process::{Actor, DataFeed};

use failure::Error;

///Actor to execute a command from a module
pub struct ModCmdActor {
    actor_name: String,
    parent_mod: Arc<Mutex<dyn Module>>,
    cmd_name: String
}

impl ModCmdActor {
    pub fn new(actor_name: &str, parent_mod: Arc<Mutex<dyn Module>>, cmd_name: &str) -> ModCmdActor {
        ModCmdActor { actor_name: actor_name.to_string(), parent_mod: parent_mod, cmd_name: cmd_name.to_string() }
    }
}

//Only accept cardinality = 1
impl Actor for ModCmdActor {

    fn get_name(&self) -> &str {
        &self.actor_name
    }

    fn act(&mut self, datafeed: DataFeed, attributes: &HashMap<String, String>) -> Result<DataFeed, Error> {
        let mut out_datafeed = DataFeed::new();
        match self.parent_mod.lock() {
            Ok(mut module) => {

                //send command
                let res_vars = module.execute_cmd(&self.cmd_name, datafeed.consume_feed(), attributes)?;

                out_datafeed.merge_mapper(res_vars);
            },
            Err(_) => {
                return Err(format_err!("Lock failed"));
            }
        }
        Ok(out_datafeed)
    }

    fn closing(&mut self, validated: &HashMap<String, usize>) {
        match self.parent_mod.lock() {
            Ok(mut module) => {
                module.actor_closing(&self.actor_name, validated);
            },
            Err(_) => {

            }
        }
    }


}
