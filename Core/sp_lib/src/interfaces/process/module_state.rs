// @Author: Lashermes Ronan <ronan>
// @Date:   22-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 22-02-2017
// @License: MIT


///Structure to save the intermediary state of a Module
#[derive(Debug,Clone)]
pub struct ModuleState {
    name: String,//module name
    data: Vec<u8>
}

impl ModuleState {
    pub fn empty(name: &str) -> ModuleState {
        ModuleState { data: Vec::new(), name: name.to_string() }
    }

    pub fn new(name: &str, data: Vec<u8>) -> ModuleState {
        ModuleState { data: data, name: name.to_string() }
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn set_name(&mut self, new_name: &str) {
        self.name = new_name.to_string();
    }

    pub fn set_data(&mut self, data: Vec<u8>) {
        self.data = data;
    }

    pub fn get_data(&self) -> &Vec<u8> {
        &self.data
    }
}
