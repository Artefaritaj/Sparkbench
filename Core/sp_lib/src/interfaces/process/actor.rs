// @Author: ronan
// @Date:   10-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

use crate::interfaces::process::DataFeed;
use std::collections::HashMap;
use failure::Error;

pub trait Actor: Send + 'static {
    fn get_name(&self) -> &str;

    fn act(&mut self, datafeed: DataFeed, attributes: &HashMap<String, String>) -> Result<DataFeed, Error>;

    fn closing(&mut self, validated: &HashMap<String, usize>);
}
