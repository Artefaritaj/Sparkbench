// @Author: ronan
// @Date:   10-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 09-11-2016

use crate::helpers::name_mapper::NameMapper;

use hconfig::store::Data;

///The datafeed is the struct that can feed an actor and returned by it
#[derive(Debug)]
pub struct DataFeed {
    feed: NameMapper<Vec<Data>>//(var_name, accumulated data)
}

impl DataFeed {
    pub fn new() -> DataFeed {
        DataFeed {feed: NameMapper::new()}
    }

    pub fn clear(&mut self) {
        self.feed.clear();
    }

    pub fn is_empty(&self) -> bool {
        self.feed.is_empty()
    }

    pub fn get_feed(&self) -> &NameMapper<Vec<Data>> {
        &self.feed
    }

    pub fn consume_feed(self) -> NameMapper<Vec<Data>> {
        self.feed
    }

    pub fn merge_mapper(&mut self, other: NameMapper<Vec<Data>>) {
        for (k, vec) in other.to_hashmap() {
            for v in vec {
                self.add_data(k.clone(), v);
            }
        }
    }

    ///add a new data to the datafeed according to var_name
    pub fn add_data(&mut self, var_name: String, data: Data) {

        let add2hm = match self.feed.get_mut(&var_name) {
            Some(vec) => {//this var already exists, append to existing vec
                vec.push(data);
                None
            },
            None => {//new var, to please to borrow checker, we add the new var outside of the match.
                let mut vec : Vec<Data> = Vec::new();
                vec.push(data);
                Some(vec)
            }
        };

        //add the new var with the first data in the hashmap
        if let Some(vec) = add2hm {
            self.feed.insert_string(var_name, vec);
        }
    }

    pub fn extend_data(&mut self, var_name: &str, data: Vec<Data>) {
        let add2hm = match self.feed.get_mut(var_name) {
            Some(vec) => {//this var already exists, append to existing vec
                vec.extend(data);
                None
            },
            None => {//new var, to please to borrow checker, we add the new var outside of the match.
                let mut vec : Vec<Data> = Vec::new();
                vec.extend(data);
                Some(vec)
            }
        };

        //add the new var with the first data in the hashmap
        if let Some(vec) = add2hm {
            self.feed.insert(var_name, vec);
        }
    }

    pub fn get_var_data(&self, var_name: &str) -> Option<&Vec<Data>> {
        self.feed.get(var_name)
    }

    pub fn get_var_data_mut(&mut self, var_name: &str) -> Option<&mut Vec<Data>> {
        self.feed.get_mut(var_name)
    }

    pub fn move_var_data(&mut self, var_name: &str) -> Option<Vec<Data>> {
        self.feed.remove(var_name)
    }

    // pub fn move_first_var_data(&mut self, var_name: &str) -> Option<Data> {
    //     self.feed.get_mut(var_name).and_then(|v|v.first())
    // }

    ///Count how many data available for var
    pub fn count_var_data(&self, var_name: &str) -> usize {
        match self.feed.get(var_name) {
            Some(ref v) => v.len(),
            None => 0
        }
    }
}
