// @Author: ronan
// @Date:   22-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016
// @License: MIT

use crate::interfaces::modules::{Variable, VariableDefinition, VariableDelimiter};
use crate::errors::*;
use crate::helpers::timeout::*;
use crate::helpers::name_mapper::NameMapper;
use hconfig::io::byte_convert::ToBytes;
use hconfig::store::Data;

use comms::connections::Connected;

use chrono::prelude::*;
use chrono::Duration;

use std::sync::MutexGuard;
use std::io::Read;
use std::time;

use failure::Error;

#[derive(Debug,Clone)]
pub enum CmdPiece {
    Data(Vec<u8>),
    VariableDefinition(VariableDefinition),
    Variable(Variable)
}

impl CmdPiece {

    ///Receive a piece of data.
    pub fn receive(&self, input_vars: &NameMapper<Vec<Data>>, output_vars: &NameMapper<Data>,
        link: &mut MutexGuard<Box<dyn Connected>>, end_time: DateTime<Utc>, cmd_name: &str) 
        -> Result<Option<Variable>, Error> {
        let res_var = match self {
            &CmdPiece::Data(ref v) => {//we must receive a vector identical to v
                let buffer = CmdPiece::read_n_bytes(cmd_name, v.len(), link, end_time)?;

                //check size
                if buffer.len() != v.len() {
                    return Err(
                        DataReceivedError::new(cmd_name, &format!("{} bytes read instead of {}", buffer.len() , v.len())).into());
                }

                //check data
                for (i,check) in v.iter().enumerate() {
                    if buffer[i] != *check {
                        return Err(
                            CommandNotValidated::new(cmd_name, &format!("Byte {} incorrect ({} instead of {})", i, buffer[i] , check)).into());
                    }
                }

                //Result is not a variable so return None
                None
            },
            &CmdPiece::VariableDefinition(ref def) => {//we must receive a variable
                let delimiter = def.get_delimiter().clone();//how the variable end

                let var = match delimiter {//manage the var according to its delimiter
                    Some(VariableDelimiter::FixedSize(size)) => {//fixed byte len
                        CmdPiece::fixed_size_read(size, link, end_time, cmd_name, def)?
                    },
                    Some(VariableDelimiter::FixedByVariable(ref len_var_name)) => {//len_var_name is the name of the var giving the len
                        //try input var
                        if let Some(ivar_vec) = input_vars.get(len_var_name) {
                            //must have one and only one data in var
                            if ivar_vec.len() == 1 {
                                let size: usize = ivar_vec[0].to_atomic_data()?.to_u64()? as usize;
                                CmdPiece::fixed_size_read(size, link, end_time, cmd_name, def)?
                            }
                            else {
                                return Err(
                                    VariableMissingError::new(len_var_name,
                                        &format!("cannot get dynamic length for {} from {} (must have only 1 data point but got {})",
                                            cmd_name, len_var_name, ivar_vec.len())).into()
                                );
                            }
                        }
                        else if let Some(ovar) = output_vars.get(len_var_name) {
                            let size: usize = ovar.to_atomic_data()?.to_u64()? as usize;
                            CmdPiece::fixed_size_read(size, link, end_time, cmd_name, def)?
                        }
                        else {
                            // trace!("Vars available: \nInput -> {:?}\nOutput -> {:?}", input_vars.get_keys(), output_vars.get_keys());

                            return Err(
                                VariableMissingError::new(len_var_name,
                                    &format!("cannot get dynamic length for {} from {}", cmd_name, len_var_name)).into());
                        }
                    },
                    Some(VariableDelimiter::Terminated(ref termination)) => {
                        let payload = CmdPiece::find_termination(def.get_name(), termination, link, end_time)?;
                        Variable::new_fill(def.clone(), payload)
                    },
                    Some(VariableDelimiter::WaitFor(duration)) => {
                        let payload = CmdPiece::read_for_duration(def.get_name(), link, duration)?;
                        Variable::new_fill(def.clone(), payload)
                    },
                    None => { return Err(
                            VariableMissingError::new(def.get_name(), "infinite variables cannot be received to").into());}
                };

                Some(var)
            },
            &CmdPiece::Variable(ref v) => {
                return Err(VariableMissingError::new(v.get_name(), "variable is already filled").into());
            }
        };

        Ok(res_var)
    }

    fn fixed_size_read(size: usize, link: &mut MutexGuard<Box<dyn Connected>>, end_time: DateTime<Utc>, cmd_name: &str, def: &VariableDefinition) -> Result<Variable, Error> {
        let buffer = CmdPiece::read_n_bytes(cmd_name, size, link, end_time)?;

        //check size
        if buffer.len() != size {
            return Err(
                DataReceivedError::new(cmd_name, 
                    &format!("{} bytes read instead of {}.", buffer.len() , size)).into());
        }

        Ok(Variable::new_fill(def.clone(), buffer))
    }

    fn read_n_bytes(cmd_name: &str, n: usize, link: &mut MutexGuard<Box<dyn Connected>>, end_time: DateTime<Utc>) -> Result<Vec<u8>, Error> {
        //debug!("read_n_bytes ({}) for {}", n, cmd_name);


        let mut bytes_received = 0;
        let mut res_buffer : Vec<u8> = Vec::new();

        while bytes_received < n {
            let mut buf = vec![0;n-bytes_received];

            //check timeout
            let now = Utc::now();
            if now > end_time {
                return Err(
                    RxTimeout::new(&format!("{} received {}/{} bytes", cmd_name, bytes_received, n)).into()
                );
            }

            let size = link.read(&mut buf)
                                .map_err(|e|DataReceivedError::new(cmd_name,
                                    &format!("{} while receiving data ({} bytes already)", e, bytes_received)
                                ))?;
            res_buffer.extend_from_slice(&buf[0..size]);
            bytes_received += size;
        }

        Ok(res_buffer)
    }

    fn read_for_duration(cmd_name: &str, link: &mut MutexGuard<Box<dyn Connected>>, duration: Duration) -> Result<Vec<u8>, Error> {
        let mut now = Utc::now();
        let end = now.checked_add_signed(duration).ok_or(format_err!("Cannot compute end time for command {}", cmd_name))?;
        let time_duration = time::Duration::from_millis(duration.num_milliseconds() as u64);

        let mut res_buffer : Vec<u8> = Vec::new();

        let mut buf = vec![0;4096];

        //reset link timeout (avoid 5s timeout when waiting for 100ms)
        let old_to = link.get_timeout()?;
        link.set_timeout(Some(time_duration))?;

        //wait for duration
        while now < end {
            if let Ok(size) = link.read(&mut buf) {
                res_buffer.extend_from_slice(&buf[0..size]);
            }
            now = Utc::now();
        }

        //set link timeout back to old value
        link.set_timeout(old_to)?;
        Ok(res_buffer)
    }

    fn find_termination(cmd_name: &str, term: &Vec<u8>, link: &mut MutexGuard<Box<dyn Connected>>, end_time: DateTime<Utc>) -> Result<Vec<u8>, Error> {

        let mut vec : Vec<u8> = Vec::new();
        let mut buf = [0u8;1];
        let term_len = term.len();

        let mut counter = 0; //count the number of consecutive bytes in the link agreeing with termination. If counter == term_len, we are good.

        while counter != term_len {

            check_timeout(end_time, format!("Cannot find termination for {}", cmd_name))?;

            let size = link.read(&mut buf).map_err(|e|DataReceivedError::new("?", &e.to_string()))?;

            if size >= 1 {
                if buf[0] == term[counter] {
                    counter += 1;
                }
                else {
                    counter = 0;
                }
            }

            vec.push(buf[0]);
        }

        //remove termination
        //TODO optionnal keep
        for _ in 0..term_len {
            vec.pop();
        }

        Ok(vec)
    }
}

impl ToBytes for CmdPiece {
     fn to_bytes(&self) -> Result<Vec<u8>, Error> {
         match self {
             &CmdPiece::Data(ref vec) => {
                Ok(vec.clone())
            },
            &CmdPiece::Variable(ref v) => {
                Ok(v.get_raw_content().clone())
            },
            &CmdPiece::VariableDefinition(ref def) => {
                Err(format_err!("Variable {} has not been filled.", def.get_name()))
            }
         }
     }
 }
