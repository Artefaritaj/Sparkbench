// @Author: ronan
// @Date:   04-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 04-10-2016

use comms::connections::CommManager;
use comms::errors::ConnectionError;

pub trait ConnectionPreparer {
    ///Ensure that connections are opened (from configs), return the number of created connections
    fn prepare_connections(&mut self, comm_mgr: &mut CommManager) -> Result<usize, ConnectionError>;
}
