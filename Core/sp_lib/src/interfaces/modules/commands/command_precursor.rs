// @Author: ronan
// @Date:   22-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 19-10-2016
// @License: MIT

use crate::errors::CommandCreationError;
use crate::interfaces::modules::commands::cmd_piece::CmdPiece;
use crate::interfaces::modules::commands::parser::parse_str;
use crate::interfaces::modules::VariableDefinition;
use crate::helpers::name_mapper::*;

use hconfig::store::{StoreNode, StoreKey};

use failure::Error;

//Dedicated types are used to smoothen the creation flow
pub struct CmdPrecursor {
    name: String,
    send_str: String,
    ans_str: String
}

impl CmdPrecursor {
    ///Extract command name, send string and answer string from cmd_node
    pub fn new(cmd_node: &StoreNode) -> Result<CmdPrecursor, Error> {
        let send_cmd_key = StoreKey::from("{0}");
        let ans_cmd_key = StoreKey::from("{1}");

        let cmd_name = cmd_node.get_node_key();
        let send_data = cmd_node.get_value(&send_cmd_key).map_err(|_| CommandCreationError::new(cmd_name, &format!("Missing data to be sent")))?;
        let ans_data = cmd_node.get_value(&ans_cmd_key).map_err(|_| CommandCreationError::new(cmd_name, &format!("Missing data to be received")))?;

        let send_str = send_data.to_string_result()?;
        let ans_str = ans_data.to_string_result()?;

        Ok(CmdPrecursor { name: cmd_node.get_node_key().to_string(),
                        send_str: send_str,
                        ans_str: ans_str})
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn parse_send(&self, instance_node: &StoreNode, vars: &NameMapper<VariableDefinition>) -> Result<Vec<CmdPiece>, Error> {
        parse_str(&self.send_str, instance_node, vars)
    }

    pub fn parse_ans(&self, instance_node: &StoreNode, vars: &NameMapper<VariableDefinition>) -> Result<Vec<CmdPiece>, Error> {
        parse_str(&self.ans_str, instance_node, vars)
    }
}
