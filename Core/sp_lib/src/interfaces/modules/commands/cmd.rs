// @Author: ronan
// @Date:   22-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016
// @License: MIT

use crate::interfaces::modules::commands::command_precursor::CmdPrecursor;
use crate::interfaces::modules::commands::cmd_piece::CmdPiece;
use crate::interfaces::modules::{VariableDefinition};
use crate::errors::*;
use crate::helpers::name_mapper::NameMapper;

use hconfig::store::{StoreNode, Data};

use comms::connections::Connected;
use comms::errors::*;

use chrono::prelude::*;
use chrono::Duration;

use std::sync::MutexGuard;

use crate::helpers::hex_string::to_hex_string;
use failure::Error;

///A command is a list of cmd piece to be sent to a device through a connection
#[derive(Debug,Clone)]
pub struct Cmd {
    ///Connection name (must map to a Link)
    conn: String,
    ///Command name
    name: String,
    ///Pieces for command to be sent
    send_pieces: Vec<CmdPiece>,
    ///Pieces for command to be received
    ans_pieces: Vec<CmdPiece>,

    /// Save var definitions for future request about what this command reuires and produces
    var_defs: NameMapper<VariableDefinition>,

    //receive timeout
    rx_to: Duration
}

impl Cmd {
    ///Create a cmd from the relevant data
    pub fn create(conn_name: &str, cmd_node: &StoreNode, instance_node: &StoreNode, vars: &NameMapper<VariableDefinition>) -> Result<Cmd, Error> {
        let precursor = CmdPrecursor::new(cmd_node)?;
        let send_pieces = precursor.parse_send(instance_node, vars)?;
        let ans_pieces = precursor.parse_ans(instance_node, vars)?;

        let mut cmd = Cmd {
            conn: conn_name.to_string(),
            name: precursor.get_name().to_string(),
            send_pieces: send_pieces,
            ans_pieces: ans_pieces,
            var_defs: vars.clone(),
            rx_to: Duration::seconds(10) };

        cmd.rationalize();
        Ok(cmd)
    }

    fn collect_bytes(&self, vars: &NameMapper<Vec<Data>>) -> Result<Vec<u8>, Error> {
        let mut result : Vec<u8> = Vec::new();
        for p in self.send_pieces.iter() {
            let mut vec = match p {
                &CmdPiece::Data(ref v) => v.clone(),
                &CmdPiece::Variable(ref v) => v.get_raw_content().clone(),
                &CmdPiece::VariableDefinition(ref def) => {
                    let data_vec = vars.get(def.get_name()).ok_or(VariableMissingError::new(def.get_name(), "variable has not been feeded"))?;
                    let mut v : Vec<u8> = Vec::new();
                    for d in data_vec {
                        let mut vec = def.get_raw_from_extdata(d) 
                                            .map_err(|e|VariableMissingError::new(def.get_name(), &e.to_string()))?;
                        v.append(&mut vec);
                    }
                    v
                }
            };
            result.append(&mut vec);
        }
        Ok(result)
    }

    pub fn send(&self, vars: &NameMapper<Vec<Data>>, link: &mut MutexGuard<Box<dyn Connected>>) -> Result<usize, Error> {
        let sent = if !self.is_send_empty() {


            //retrieve data to send
            let to_send = self.collect_bytes(vars).map_err(|e| CommandCreationError::new(&self.name, &e.to_string()))?;

            trace!("{}.{} sending {}", self.conn, self.name, to_hex_string(&to_send));
            //debug!("{}.{} send {}", self.conn, self.name, to_hex_string(&to_send));

            //send data
            let bytes_sent = link.write(&to_send).map_err(|e| ConnectionError::new(&self.conn, &e.to_string()))?;

            //check that all bytes have been sent
            if bytes_sent != to_send.len() {
                return Err(DataSentError::new(&self.name, &format!("{} bytes sent instead of {}.", bytes_sent, to_send.len())).into());
            }

            bytes_sent
        }
        else {
            0
        };

        Ok(sent)
    }

    pub fn set_rx_timeout(&mut self, to: Duration) {
        self.rx_to = to;
    }

    pub fn receive(&self, vars: &NameMapper<Vec<Data>>, link: &mut MutexGuard<Box<dyn Connected>>) -> Result<NameMapper<Data>, Error> {
        let mut mapping : NameMapper<Data> = NameMapper::new();
        let name = self.name.to_string();

        if !self.is_expected_answer_empty() {

            let start: DateTime<Utc> = Utc::now();
            let end = start + self.rx_to;

            //we receive data piece by piece
            for p in self.ans_pieces.iter() {
                let var_opt = p.receive(vars, &mapping, link, end, &self.name)?;

                match var_opt {
                    Some(v) => {
                        let vname = v.get_name().to_string();
                        let vdata = v.to_data();
                        // trace!("Received variable {} with data {:?}", vname, vdata);
                        mapping.insert(&vname,
                                        vdata
                                            .map_err(|e| DataReceivedError::new(&name, &e.to_string()))?);

                    },
                    None => {
                        trace!("Received {:?} for  {}.{}", p, self.conn, self.name);
                    }//not a var
                }

            }

        }

        Ok(mapping)
    }

    ///Combine successive pieces if possible to minimize the number of pieces
    fn rationalize(&mut self) {
        minimize_pieces_count(&mut self.ans_pieces);
        minimize_pieces_count(&mut self.send_pieces);
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn get_connection_name(&self) -> &str {
        &self.conn
    }

    pub fn is_send_empty(&self) -> bool {
        self.send_pieces.len() == 0
    }

    pub fn is_expected_answer_empty(&self) -> bool {
        self.ans_pieces.len() == 0
    }

    fn get_var_names_from_pieces(pieces: &Vec<CmdPiece>) -> Vec<String> {
        let mut names : Vec<String> = Vec::new();
        for p in pieces.iter() {
            match p {
                &CmdPiece::Variable(ref v) => {
                    names.push(v.get_name().to_string());
                },
                &CmdPiece::VariableDefinition(ref def) => {
                    names.push(def.get_name().to_string());
                },
                _ => {}//Do nothing
            }
        }
        names
    }

    pub fn get_required_var_names(&self) -> Vec<String> {
        Cmd::get_var_names_from_pieces(&self.send_pieces)
    }

    pub fn get_required_var_definitions(&self) -> Vec<VariableDefinition> {
        self.get_required_var_names()
                .iter()
                .filter_map(|name|self.var_defs.get(name))//get only var defs if exists
                .map(|v|v.clone())//allocate new var def from pointer
                .collect()
    }

    pub fn get_produced_var_names(&self) -> Vec<String> {
        Cmd::get_var_names_from_pieces(&self.ans_pieces)
    }

    pub fn get_produced_var_definitions(&self) -> Vec<VariableDefinition> {
        self.get_produced_var_names()
                .iter()
                .filter_map(|name|self.var_defs.get(name))//get only var defs if exists
                .map(|v|v.clone())//allocate new var def from pointer
                .collect()
    }
}

///Minimze the number of pieces in a pieces vector
///Cumulate all successive data pieces in one data piece
fn minimize_pieces_count(pieces: &mut Vec<CmdPiece>) {
    let mut new_vec: Vec<CmdPiece> = Vec::new();
    let mut data_vec: Vec<u8> = Vec::new();//last data vec

    for p in pieces.iter() {
        match p {
            &CmdPiece::Data(ref v) => {
                let mut clone = v.clone();
                data_vec.append(&mut clone);
            },
            &CmdPiece::Variable(ref v) => {
                if data_vec.len() > 0 {//create piece
                    new_vec.push(CmdPiece::Data(data_vec.clone()));
                    data_vec.clear();
                }
                new_vec.push(CmdPiece::Variable(v.clone()));
            },
            &CmdPiece::VariableDefinition(ref def) => {
                if data_vec.len() > 0 {//create piece
                    new_vec.push(CmdPiece::Data(data_vec.clone()));
                    data_vec.clear();
                }
                new_vec.push(CmdPiece::VariableDefinition(def.clone()));
            }
        }
    }

    //create last piece is needed
    if data_vec.len() > 0 {//create piece
        new_vec.push(CmdPiece::Data(data_vec.clone()));
        data_vec.clear();
    }

    pieces.clear();
    pieces.append(&mut new_vec);
}
