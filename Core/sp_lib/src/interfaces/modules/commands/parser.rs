// @Author: ronan
// @Date:   30-08-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 21-10-2016
// @License: MIT

use crate::interfaces::modules::commands::cmd_piece::CmdPiece;
use crate::interfaces::modules::VariableDefinition;
use crate::interfaces::expressions::parser::*;
use crate::helpers::name_mapper::*;
use crate::config_keys::*;

use hconfig::store::{StoreNode, Data};
use failure::Error;

pub fn parse_str(s: &str, instance_node: &StoreNode, vars: &NameMapper<VariableDefinition>) -> Result<Vec<CmdPiece>, Error> {

    if s == "" {
        return Ok(Vec::new());
    }

    let mut tokens: Vec<ExpressionToken> = Vec::new();
    let mut rest = s.to_string();

    //read all tokens
    while rest.len() > 0 {
        let (tok, working) = match next_token(&rest) {
            Ok((t,r)) => (t,r),
            Err(s) => {return Err(s);}//fail to read token
        };

        rest = working;
        tokens.push(tok);
    }

    //transform tokens in CmdPieces
    let mut cmd_pieces: Vec<CmdPiece> = Vec::new();
    for tok in tokens.iter() {
        cmd_pieces.push(create_cmd_piece_from_token(&tok, instance_node, vars)?);
    }

    Ok(cmd_pieces)
}


///Translate to bytes, Fixed and Sharp. Do not fill Dollar
pub fn create_cmd_piece_from_token(tok: &ExpressionToken, instance_node: &StoreNode, vars: &NameMapper<VariableDefinition>) -> Result<CmdPiece, Error> {
    match tok {
        &ExpressionToken::Fixed(ref s) => Ok(CmdPiece::Data(s.as_bytes().to_vec())),
        &ExpressionToken::Sharp(ref s) => parse_sharp_cmd(&s),
        &ExpressionToken::Dollar(ref s) => parse_dollar_static_cmdpiece(s,instance_node)
                                            .or(parse_dollar_variable(s,vars))
    }
}

fn parse_dollar_static_cmdpiece(cmd_str: &str, node: &StoreNode) -> Result<CmdPiece, Error> {
    let vec = parse_dollar_static_vecu8(cmd_str, node)?;
    Ok(CmdPiece::Data(vec))
}

fn parse_sharp_cmd(s: &str) -> Result<CmdPiece, Error> {
    let vector = parse_sharp_token(s)?;
    Ok(CmdPiece::Data(vector))
}

pub fn parse_dollar_variable(var_name: &str, vars: &NameMapper<VariableDefinition>) -> Result<CmdPiece, Error> {
    let mut splitted: Vec<&str> = var_name.split(VAR_SEP).collect();
    while splitted.len() < 3 {
        splitted.push("");
    }

    let key = splitted[0];
    let format = splitted[1];
    let selectors_str = splitted[2];
    let selectors = parse_selectors(selectors_str, None)?;


    match vars.get(key) {
        Some(ref v) => {
            let mut vdef = (*v).clone();
            if format != "" {
                //either "f32"  or "type=f32,exponent=10"
                let attrs: Vec<_> = format.split(VEC_SEP).collect();
                for attr in attrs {
                    let pair_vec: Vec<_> = attr.split(ASSIGN_SEP).collect();

                    if pair_vec.len() == 1 {
                        vdef.add_attribute(TYPE, Data::create_str(pair_vec[0]));
                    }
                    else if pair_vec.len() == 2 {
                        vdef.add_attribute(pair_vec[0], Data::create_str(pair_vec[1]));
                    }
                }
            }

            match selectors {
                Some(sel_vec) => {
                    let mut data_selectors: Vec<Data> = Vec::new();
                    for sel in sel_vec.iter() {
                        data_selectors.push(Data::create_u64(*sel as u64));
                    }

                    vdef.add_attribute(SELECTORS, Data::Array(data_selectors));
                },
                None => {}
            }

            Ok(CmdPiece::VariableDefinition(vdef))
        },
        None => Err(format_err!("{} is not a known variable.", var_name))
    }
}


#[test]
fn test_next_token() {
    let tests =         vec!["abc$(P)def",  "$(abc)def",    "abc$(def)",    "abc$def",  "abc$("];
    let first_token=    vec![ExpressionToken::Fixed("abc".to_string()),
                                ExpressionToken::Dollar("abc".to_string()),
                                ExpressionToken::Fixed("abc".to_string()),
                                ExpressionToken::Fixed("abc$def".to_string()),
                                ExpressionToken::Fixed("abc".to_string())];

    for (i,v) in tests.iter().enumerate() {
        let (tok, _) = next_token(v).unwrap();
        assert!(tok == first_token[i]);
    }

    let tst1 = "abc$(P)def";
    let (tok, rest) = next_token(tst1).unwrap();
    assert!(tok == ExpressionToken::Fixed("abc".to_string()));
    let (tok, rest) = next_token(&rest).unwrap();
    assert!(tok == ExpressionToken::Dollar("P".to_string()), "token is {:?}", tok);
    let (tok, _)  = next_token(&rest).unwrap();
    assert!(tok == ExpressionToken::Fixed("def".to_string()), "token is {:?}", tok);

    let tst2 = "abc$(VAR";
    let (tok, rest) = next_token(tst2).unwrap();
    assert!(tok == ExpressionToken::Fixed("abc".to_string()));
    let res = next_token(&rest);
    assert!(res.is_err());
}
