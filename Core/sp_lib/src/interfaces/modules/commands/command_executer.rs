// @Author: ronan
// @Date:   04-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 26-10-2016

use crate::helpers::name_mapper::NameMapper;
use failure::Error;

use std::collections::HashMap;

use hconfig::store::Data;

pub trait CommandExecuter {
    fn execute_cmd(&mut self, cmd_name: &str, vars: NameMapper<Vec<Data>>, attributes: &HashMap<String, String>) -> Result<NameMapper<Vec<Data>>,Error>;
}

#[test]
fn test_trait_object() {//this is actually a test at the compilation step
    let _ : Option<Box<dyn CommandExecuter>> = None;
}
