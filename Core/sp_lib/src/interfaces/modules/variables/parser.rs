// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016
// @License: MIT

use crate::interfaces::modules::VariableDefinition;
use crate::interfaces::expressions::parser::{parse_static_vecu8, parse_static_data, parse_string_array, parse_data_array};

use hconfig::store::store_node::StoreNode;
use hconfig::store::store_key::StoreKey;
use hconfig::store::{Data, AtomicData, CompoundDataType};

use config_keys::*;

use failure::Error;

pub fn parse_variables(vars: &StoreNode, instance_node: &StoreNode) -> Result<Vec<VariableDefinition>, Error> {

    let mut parsed : Vec<VariableDefinition> = Vec::new();

    //each child of node [vars] must describe a var
    for v in vars.get_node_children().iter() {
        parsed.push(parse_variable(v, instance_node).map_err(|e|format_err!("Failed to parse var {}: {}", v.get_node_key(), e))?);
    }

    Ok(parsed)
}

fn parse_variable(var: &StoreNode, instance_node: &StoreNode) -> Result<VariableDefinition, Error> {
    let name = String::from(var.get_node_key());

    //read type
    let type_key = StoreKey::from(TYPE);
    let type_list: Vec<CompoundDataType> = match var.get_node(&type_key) {
        Ok(ref d) => parse_string_array(d)?.iter().filter_map(|s|CompoundDataType::parse(s).ok()).collect(),
        _ => Vec::new()
    };
    let mut def = VariableDefinition::new(&name,type_list);
    // trace!("VariableDefinition for var {} is {:?}", name, def);

    for node in var.get_node_children().iter() {
        let node_name = node.get_node_key();
        // trace!("Parsing node {} for var {}", node_name, name);
        match node_name {
            TERMINATION => {
                match node.get_node_value() {
                    &Data::Singleton(AtomicData::String(ref s)) => {
                        let vec = parse_static_vecu8(&s, instance_node)?;
                        def.add_attribute(TERMINATION, Data::Raw(vec));
                    },
                    _ => {}
                }
            },
            TYPE => {},//do nothing
            n @ MIN| n @ MAX => {
                match node.get_node_value() {
                    &Data::Singleton(AtomicData::String(ref s)) => {
                        let final_data = match parse_static_data(s, instance_node) {
                            Ok(d2) => {
                                d2
                            },
                            Err(_) => {
                                Data::Singleton(AtomicData::String(s.to_string()))
                            }
                        };
                        // trace!("Converting {:?} to datatype {:?}", final_data, def.get_ext_type());
                        let to_add = final_data.convert(&def.get_ext_type())?;
                        // trace!("Conversion ok: {:?}", to_add);
                        def.add_attribute(n, to_add);
                    },
                    d => {
                        let to_add = d.convert(&def.get_ext_type())?;
                        def.add_attribute(n, to_add);
                    }
                }
            },
            n => {
                // trace!("Other var attribute: {:?}", node);
                let node_value = node.get_node_value().clone();
                match node_value {
                    Data::Singleton(AtomicData::String(s)) => {
                        let final_data = match parse_static_data(&s, instance_node) {
                            Ok(d2) => {
                                d2
                            },
                            Err(_) => {
                                Data::Singleton(AtomicData::String(s))
                            }
                        };
                        def.add_attribute(n, final_data);
                    },
                    Data::Empty => {//if empty => assume an array
                        let data_vec = parse_data_array(node)?;
                        let concat = Data::Array(data_vec);
                        def.add_attribute(n, concat);
                    },
                    d => {
                        def.add_attribute(n, d);
                    }
                }

            }
        }
    }

    Ok(def)
}
