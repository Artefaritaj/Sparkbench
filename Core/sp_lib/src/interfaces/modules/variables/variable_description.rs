// @Author: ronan
// @Date:   18-11-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

use crate::interfaces::modules::VariableDirection;
use hconfig::store::CompoundDataType;

#[derive(Debug,Clone,PartialEq,Eq,Hash)]
pub struct VariableDescription {
    var_name: String,
    direction: VariableDirection,
    allowed_datatypes: Vec<CompoundDataType>
}

impl VariableDescription {
    pub fn new(name: &str, dir: VariableDirection) -> VariableDescription {
        VariableDescription { var_name: name.to_string(), direction: dir, allowed_datatypes: Vec::new()}
    }

    pub fn with_datatypes(name: &str, dir: VariableDirection, allowed_datatypes: Vec<CompoundDataType>) -> VariableDescription {
        VariableDescription { var_name: name.to_string(), direction: dir, allowed_datatypes: allowed_datatypes}
    }

    pub fn get_var_name(&self) -> &str {
        &self.var_name
    }

    pub fn get_direction(&self) -> VariableDirection {
        self.direction
    }

    pub fn get_allowed_datatypes(&self) -> &Vec<CompoundDataType> {
        &self.allowed_datatypes
    }

    pub fn set_allowed_datatypes(&mut self, new_allowed_datatypes: Vec<CompoundDataType>) {
        self.allowed_datatypes = new_allowed_datatypes;
    }

    pub fn is_allowed_datatype(&self, dt: CompoundDataType) -> bool {
        self.allowed_datatypes.contains(&dt)
    }

    pub fn first_allowed_datatype(&self) -> Option<&CompoundDataType> {
        self.allowed_datatypes.first()
    }
}
