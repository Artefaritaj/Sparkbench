// @Author: ronan
// @Date:   18-11-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

use chrono::Duration;

///How to determine the end of the var in the dataflow.
#[derive(Clone,Debug)]
pub enum VariableDelimiter {
    ///Byte count is fixed
    FixedSize(usize),
    ///termination sequence
    Terminated(Vec<u8>),
    //Infinite cannot be read from device only sent to
    //Infinite,
    ///Byte count is given by specified variable
    FixedByVariable(String),
    ///All bytes received during some duration are part of the var
    WaitFor(Duration)
}
