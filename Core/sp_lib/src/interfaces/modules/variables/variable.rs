// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016
// @License: MIT

use crate::interfaces::modules::{VariableDefinition, VariableDelimiter};

use std::fmt;

use hconfig::store::{Data, CompoundDataType};

use failure::Error;

#[derive(Clone,Debug,Hash)]
pub struct Variable {
    def: VariableDefinition,
    content: Vec<u8>
}

impl fmt::Display for Variable {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let (def, content) = (self.def.clone(), self.content.clone());
        let d = def.get_extdata_from_raw(content);

        match d {
            Ok(d) => write!(f, "{}: {}", self.def.get_name(), d),
            Err(e) => write!(f, "{}: Cannot display var ({})", self.def.get_name(), e)
        }
    }
}

impl Variable {


    pub fn get_name(&self) -> &str {
        &self.def.get_name()
    }

    pub fn get_delimiter(&self) -> Option<VariableDelimiter> {
        self.def.get_delimiter()
    }

    pub fn get_ext_type(&self) -> CompoundDataType {
        self.def.get_ext_type()
    }


    pub fn get_raw_content(&self) -> &Vec<u8> {
        &self.content
    }

    pub fn set_content(&mut self, vec: Vec<u8>) {
        self.content = vec;
    }


    pub fn new_fill(def: VariableDefinition, content: Vec<u8>) -> Variable {
        Variable { def: def, content: content}
    }


    ///This is where we interpret bytes as Data
    pub fn to_data(self) -> Result<Data, Error> {
        let (def, content) = (self.def, self.content);
        def.get_extdata_from_raw(content)
    }
}
