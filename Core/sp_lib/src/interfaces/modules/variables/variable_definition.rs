// @Author: ronan
// @Date:   18-11-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

use crate::interfaces::modules::VariableDelimiter;

use std::collections::HashMap;
use std::hash::{Hash, Hasher};

use config_keys::*;

use hconfig::store::{DataType, Data, CompoundDataType};
use hconfig::io::byte_convert::ToBytes;

use failure::Error;

use dimval::Prefix;
use chrono::Duration;

#[derive(Clone,Debug)]
pub struct VariableDefinition {
    name: String,
    types: Vec<CompoundDataType>, //first is type after raw, last is extdata type
    attrs: HashMap<String, Data>

    //delimiter: Option<VariableDelimiter>,
    // presence_constraint: VariablePresenceConstraint,
    // var_type: DataType,
    // convert_to: Option<DataType>,
    // selectors: Option<Vec<usize>>,
    // min: Option<Data>,
    // max: Option<Data>
}

impl Hash for VariableDefinition {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);

        for cdt in self.types.iter() {
            cdt.hash(state);
        }

        for (k, v) in self.attrs.iter() {
            k.hash(state);
            v.hash(state);
        }
    }
}

impl VariableDefinition {

    pub fn new(var_name: &str, types: Vec<CompoundDataType>) -> VariableDefinition {
        VariableDefinition { name: var_name.to_string(), types: types, attrs: HashMap::new() }
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn add_attribute(&mut self, att_name: &str, att_data: Data) {
        self.attrs.insert(att_name.to_string(), att_data);
    }

    pub fn get_delimiter(&self) -> Option<VariableDelimiter> {
        //try fixed by constant
        match self.attrs.get(BYTE_LEN) {
            Some(d) => {
                match d.to_atomic_data().and_then(|ad|ad.to_i64()) {
                    //BYTE LENGTH is given as a constant
                    Ok(i) => {return Some(VariableDelimiter::FixedSize(i as usize));},
                    Err(_) => {
                        if let Ok(s) = d.to_string_result() { //BYTE LENGTH is given as a reference to another var
                            return Some(VariableDelimiter::FixedByVariable(s.to_string()));
                        }
                    }
                }
            },
            None => {}
        }

        //try terminated (end of var is given by a sequence of bytes)
        match self.attrs.get(TERMINATION) {
            Some(d) => {
                match d.to_bytes() {
                    Ok(vec) => {return Some(VariableDelimiter::Terminated(vec.clone()));},
                    _ => {}
                }
            },
            None => {}
        }

        //try by duration (end of var is after a given time)
        match self.attrs.get(DURATION) {
            Some(d) => {
                match d.to_atomic_data().and_then(|ad|ad.to_dim()) {
                    //DURATION is given as a constant
                    Ok(dv) => {
                        //convert dv to milliseconds
                        if let Ok(millis) = dv.to_i64_prefix(Prefix::Milli) {
                            return Some(VariableDelimiter::WaitFor(Duration::milliseconds(millis)));
                        }
                    },
                    _ => {}
                }
            },
            None => {}
        }

        None
    }

    fn get_exponents(&self) -> Result<Option<Vec<i32>>, Error> {
        match self.attrs.get(EXPONENT) {
            Some(ref d) => {
                let expdata = d.convert(&CompoundDataType::Array(DataType::U64))?;
                match expdata {
                    Data::Array(vec) => {
                        let mut resvec: Vec<i32> = Vec::new();
                        for v in vec {
                            resvec.push(v.to_atomic_data()?.to_i32()? as i32);
                        }
                        Ok(Some(resvec))
                    },
                    _ => Err(format_err!("Cannot convert {:?} to exponent(s)", self))
                }
            },
            None => Ok(None)
        }
    }

    pub fn get_ext_type(&self) -> CompoundDataType {
        match self.types.iter().rev().next() {
            Some(cdt) => cdt.clone(),
            None => CompoundDataType::Raw
        }
    }

    fn get_selectors(&self) -> Option<Vec<usize>> {
        match self.attrs.get(SELECTORS) {
            Some(&Data::Array(ref arr)) => {
                let mut res: Vec<usize> = Vec::new();
                for a in arr {
                    if let Ok(u) = a.to_atomic_data().and_then(|ad|ad.to_u64()) {
                        res.push(u as usize);
                    }
                    else {
                        return None;
                    }
                }
                Some(res)
            },
            Some(&Data::Singleton(ref ad)) => {
                if let Ok(i) = ad.to_u64() {
                    Some(vec![i as usize;1])
                }
                else if let Ok(s) = ad.to_string_result() {
                    let splitted: Vec<_> = s.split(VEC_SEP).collect();
                    let mut res: Vec<usize> = Vec::new();
                    for s in splitted {
                        match s.parse::<usize>() {
                            Ok(i) => {res.push(i); },
                            Err(_) => { return None; }
                        }
                    }
                    Some(res)
                }
                else {
                    None
                }
            }
            _ => None
        }
    }

    pub fn check_minmax(&self, d: &Data) -> Result<(), Error> {
        if let Some(min) = self.get_min() {
            if d < min {
                return Err(format_err!("Min constraint not respected {:?} is < min {:?}", d, min));
            }
        }

        if let Some(max) = self.get_max() {
            if d > max {
                return Err(format_err!("Max constraint not respected max {:?} is < {}", max, d));
            }
        }

        Ok(())
    }


    pub fn get_min(&self) -> Option<&Data> {
        self.attrs.get(MIN)
    }

    pub fn get_max(&self) -> Option<&Data> {
        self.attrs.get(MAX)
    }

    ///Get ext_data from raw incmd byte vector
    pub fn get_extdata_from_raw(&self, raw_content: Vec<u8>) -> Result<Data, Error> {
        let exponents = self.get_exponents()?;
        let mut data = Data::Raw(raw_content);

        //convert data in order from first to last
        for (i, cdt) in self.types.iter().enumerate() {
            //convert
            data = data.convert(cdt)?;

            //apply exponent
            match exponents {
                Some(ref exps) => {
                    if i < exps.len() && exps[i] != 0 {
                        data = data.pow10(exps[i])?;
                    }
                },
                None => {}
            }
        }

        //check min max
        self.check_minmax(&data)?;

        Ok(data)
    }

    ///Get raw incmd bytevector from ext_data
    pub fn get_raw_from_extdata(&self, ext_data: &Data) -> Result<Vec<u8>, Error> {
        //filter with minmax
        self.check_minmax(ext_data)?;

        let exponents = self.get_exponents()?;
        let mut data = ext_data.clone();

        //convert data in reverse order
        for (i, cdt) in self.types.iter().enumerate().rev() {
            //convert
            data = data.convert(cdt)?;

            //apply exponent
            match exponents {
                Some(ref exps) => {
                    if i < exps.len() && exps[i] != 0 {
                        data = data.pow10(-exps[i])?;
                    }
                },
                None => {}
            }
        }

        //get byte vector
        let vec = data.to_bytes()?;

        //use selectors if any
        let res = match self.get_selectors() {
            None => vec,
            Some(ref sels) => {
                let mut v: Vec<u8> = Vec::new();
                for ind in sels.iter() {
                    v.push(*vec.get(*ind) .ok_or(format_err!("Incorrect selector: {}", ind))?);
                }
                v
            }
        };

        Ok(res)
    }
}
