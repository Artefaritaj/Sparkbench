// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016
// @License: GPL



pub mod variable;
pub mod parser;
pub mod variable_definition;
pub mod variable_description;
pub mod variable_delimiter;
pub mod variable_direction;
