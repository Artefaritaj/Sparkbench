// @Author: ronan
// @Date:   18-11-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016




#[derive(Debug,Clone,Copy,PartialEq,Eq,Hash)]
pub enum VariableDirection {
    Produced,
    Required
}
