// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016
// @License: GPL


// use helpers::name_mapper::NameMapper;
use crate::interfaces::modules::VariableDescription;
use crate::interfaces::modules::commands::command_executer::CommandExecuter;
use crate::interfaces::process::ModuleState;

use std::collections::HashMap;

use comms::connections::CommManager;
use failure::Error;

pub trait Module : CommandExecuter + Send {

    fn get_uid(&self) -> &str;
    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription>;
    fn load_state(&mut self, previous_state: Option<&ModuleState>) -> Result<(), Error>;
    fn save_state(&mut self) -> Result<ModuleState, Error>;
    fn actor_closing(&mut self, actor_name: &str, validated: &HashMap<String, usize>);

    ///Prepare is called after load state
    fn prepare(&mut self, comms: &mut CommManager) -> Result<(), Error>;

    // fn execute(&mut self, args: &Vec<String>) -> Result<(), ModuleExecutionError>;
    // fn set_vars(&mut self, vars: Vec<Variable>);
    // fn get_vars(&self) -> Vec<&Variable>;
}

#[test]
fn test_module_trait_object() {
    use std::sync::{Arc, Mutex};
    let _ : Option<Box<dyn Module>> = None;
    let _ : Option<Arc<Mutex<dyn Module>>> = None;
}
