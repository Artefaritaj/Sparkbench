// @Author: Lashermes Ronan <ronan>
// @Date:   23-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 23-02-2017
// @License: MIT

use crate::resources::toml::toml_manager::TOMLManager;
use hconfig::store::{StoreKey,StoreNode,OverwriteRule};
use std::sync::{Arc, Mutex};

use crate::interfaces::modules::Module;
use failure::Error;

pub trait ModuleBuilder {
    /// Build a module
    /// 
    /// # Arguments
    /// 
    /// * 'config' - The whole config hierarchy at this point.
    /// * 'at' - Where this module config is in the whole hierarchy.
    /// * 'args' - Arguments for this particular module creation.
    /// * 'ow_rules' - Overwriting rules to overwrite at the last instant data in the config.
    /// * 'toml_mgr' - The manager in charge of finding where config files (TOML format) are.
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, ow_rules: &Vec<OverwriteRule>, toml_mgr: &TOMLManager) -> Result<Arc<Mutex<dyn Module>>, Error>;
}
