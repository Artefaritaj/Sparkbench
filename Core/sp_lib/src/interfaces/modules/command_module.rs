// @Author: ronan
// @Date:   21-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016
// @License: MIT

use crate::helpers::name_mapper::NameMapper;
use crate::interfaces::modules::Cmd;
use crate::interfaces::modules::commands::command_executer::CommandExecuter;
use crate::interfaces::modules::commands::connection_preparer::ConnectionPreparer;
use crate::errors::*;

use config_keys::*;

use dimval::{DimVal, Prefix};
use failure::Error;

use chrono::Duration;

use hconfig::store::{StoreNode, Data};

use comms::connections::{CommManager, Link};
use comms::errors::*;

use std::{thread, time};
use std::collections::HashMap;

///A command module is able to send commands to a connection
pub struct CommandModule {
    ///The available commands for this module; format is connection_name.cmd_name for mapping
    cmds: NameMapper<Cmd>,
    ///The configs of the various connections to this module
    conn_configs: NameMapper<StoreNode>,
    ///Links are connection handles
    links: NameMapper<Link>
}

impl CommandExecuter for CommandModule {
    fn execute_cmd(&mut self, cmd_name: &str, vars: NameMapper<Vec<Data>>, attributes: &HashMap<String, String>) -> Result<NameMapper<Vec<Data>>, Error> {
        //get command
        let cmd = self.cmds.get_mut(cmd_name)
                                .ok_or(CommandUnknown::new(cmd_name))?;
        //debug!("Start for {}", cmd_name);
        //check that required vars are present
        for v in cmd.get_required_var_names().iter()
        {
            if !vars.contains_name(v) {
                warn!("{} misses variable {}", cmd_name, v);
                return Err(VariableMissingError::new(v, "for command execution").into());
            }
        }

        //get timeout attribute
        match attributes.get(TIMEOUT) {
            Some(val_str) => {
                match val_str.parse::<DimVal>().and_then(|x|x.to_f64_prefix(Prefix::Milli)) {
                    Ok(millis) => {
                        let timeout = Duration::milliseconds(millis as i64);
                        cmd.set_rx_timeout(timeout);//set rx timeout
                    },
                    Err(_) => {

                    }
                }

            },
            None => {}//stay with default
        }

        //get connection link
        let conn_name = cmd.get_connection_name().to_string();
        let link =  self.links.get_mut(&conn_name).ok_or(ConnectionMissing::new(&conn_name))?;

        //debug!("Trying to open link {} for {}", conn_name, cmd_name);

        //open link
        let mut link_guard = link.lock().map_err(|_| ConnectionError::new(&conn_name, "The connection is already open somewhere else."))?;
        //debug!("Link {} opened for {}", conn_name, cmd_name);


        //send
        cmd.send(&vars, &mut link_guard)?;

        // debug!("Cmd {} sent!", cmd_name);

        //inter temporisation
        if let Some(inter_str) = attributes.get(INTER_TEMPO) {
            match inter_str.parse::<DimVal>()
                            .and_then(|x|x.filter_unit_str("s"))
                            .and_then(|x|x.to_f64_prefix(Prefix::Milli)) {
                Ok(inter_ms) => {
                    let tempor = time::Duration::from_millis(inter_ms as u64);
                    thread::sleep(tempor);
                },
                Err(_) => {}
            }
        }

        //receive
        let produced_vars = cmd.receive(&vars, &mut link_guard)?;

        // debug!("Cmd {} received!", cmd_name);
        let mut out_vars : NameMapper<Vec<Data>> = NameMapper::new();

        for (k,v) in produced_vars.to_hashmap() {
            out_vars.insert_string(k, vec![v]);
        }

        // debug!("End for {}", cmd_name);
        Ok(out_vars)

    }
}

impl ConnectionPreparer for CommandModule {
    ///Ensure that connections are opened (from configs), return the number of created connections
    fn prepare_connections(&mut self, comm_mgr: &mut CommManager) -> Result<usize, ConnectionError> {
        let mut conn_created = 0usize;
        for (conn_name, conn_config) in self.conn_configs.get_whole_mapping() {
            //existing?
            let link_exists = self.links.contains_name(conn_name);

            if !link_exists {//link does not exist -> create
                let new_link = comm_mgr.connect_to(conn_config) .map_err(|e|ConnectionError::new(conn_name, &e.to_string()))?;
                self.links.insert(conn_name, new_link);
                conn_created += 1;
            }
        }

        Ok(conn_created)
    }
}

impl CommandModule {

    pub fn new() -> CommandModule {
        CommandModule {cmds: NameMapper::new(), conn_configs: NameMapper::new(), links: NameMapper::new()}
    }

    //Commands
    pub fn list_available_commands(&self) -> Vec<String> {
        let mut av_cmds : Vec<String> = Vec::new();
        for k in self.cmds.get_whole_mapping().keys() {
            av_cmds.push(k.to_string());
        }
        av_cmds
    }

    pub fn get_commands(&self) -> &NameMapper<Cmd> {
        &self.cmds
    }

    pub fn get_command(&self, cmd_name: &str) -> Option<&Cmd> {
        self.cmds.get(cmd_name)
    }

    pub fn add_command(&mut self, cmd: Cmd) {
        let name = String::from(cmd.get_name());
        self.cmds.insert(&name, cmd);
    }

    pub fn add_commands(&mut self, cmds: Vec<Cmd>) {
        for c in cmds {
            self.add_command(c);
        }
    }

    pub fn remove_command_by_name(&mut self, name: &str) -> Option<Cmd> {
        self.cmds.remove(name)
    }

    //Connection configs
    pub fn add_connection_config(&mut self, conn_name: &str, conn_config: StoreNode) {
        self.conn_configs.insert(conn_name, conn_config);
    }


    pub fn remove_connection_config_by_name(&mut self, name: &str) -> Option<StoreNode> {
        self.conn_configs.remove(name)
    }
}
