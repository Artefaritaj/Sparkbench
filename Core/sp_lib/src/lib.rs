// @Author: ronan
// @Date:   19-08-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 21-10-2016
// @License: GPL



extern crate rand;
extern crate rustc_serialize;
extern crate hconfig;
extern crate comms;
extern crate config_keys;
extern crate chrono;
extern crate byteorder;
extern crate dimval;

/*#[macro_use]*/ extern crate lazy_static;

#[macro_use] extern crate log;

#[macro_use] extern crate failure;

pub mod interfaces;
pub mod helpers;
pub mod definitions;
pub mod errors;
pub mod resources;
