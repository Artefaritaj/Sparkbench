// @Author: ronan
// @Date:   21-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 15-11-2016
// @License: MIT


#[derive(Debug,Fail)]
#[fail(display = "The thread has been killed")]
pub struct ThreadKilled {}

impl ThreadKilled {
    pub fn new() -> ThreadKilled {
        ThreadKilled { }
    }
}
