/*
 * Created on Fri Mar 23 2018
 *
 * Copyright (c) 2018 INRIA
 */

#[derive(Debug,Serialize,Deserialize)]
pub enum FeedbackNotification {
    // ExperimentCrashed(String), //Error msg
    ExperimentStarted,
    ExperimentEnded,
    ProcessStarted(String),
    ProcessEnded(String),
    ProcessCrashed(String),
    Tick(u64, u64), //(current, total)
}