// @Author: ronan
// @Date:   10-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

use std::sync::mpsc::*;
use std::collections::HashMap;
use std::thread;
use std::time;

use chrono::prelude::*;
use chrono::Duration;

use hconfig::store::Data;

use sp_lib::helpers::timeout::*;
use sp_lib::interfaces::process::datafeed::DataFeed;

use process::actors::actor_message::*;
use process::controller_message::ControllerMessage;
use process::actors::validation::validator::Validator;

use failures::RecvFailure;
use failure::Error;

///The synchronizer read the data from the channels, regroup it and when ready can send to actor.
pub struct Synchronizer {
    rxs: HashMap<String, (Receiver<ActorMessage>, usize)>,
    constants: HashMap<String, (Data, usize)>,
    timeout: Duration
}

impl Synchronizer {

    pub fn new(constants: HashMap<String, (Data, usize)>) -> Synchronizer {
        Synchronizer { rxs: HashMap::new(), timeout: Duration::days(1), constants: constants}
    }

    pub fn add_receiver(&mut self, var_name: &str, rx: Receiver<ActorMessage>, count: usize) -> Result<(), Error> {
        let new_count = if count > 0 { count } else { 1 };
        if self.rxs.contains_key(var_name) {
            return Err(format_err!("{} already has a receiver.", var_name));
        }
        else {
            self.rxs.insert(var_name.to_string(), (rx, new_count));
            Ok(())
        }
    }

    pub fn len(&self) -> usize {
        self.rxs.len()
    }

    pub fn set_timeout(&mut self, new_to: Duration) {
        self.timeout = new_to;
    }

    pub fn get_channel_names(&self) -> Vec<&str> {
        let mut res = Vec::new();
        for (name, _) in self.rxs.iter() {
            res.push(name.as_ref());
        }
        res
    }

    pub fn get_constants(&self) -> &HashMap<String, (Data, usize)> {
        &self.constants
    }

    ///Blocking with timeout
    pub fn recv(&mut self, actor_name: &str, valid: &mut Validator, ctr: &Receiver<ControllerMessage>) -> Result<DataFeed, RecvFailure> {
        let mut datafeed = DataFeed::new();
        let mut end = Utc::now() + self.timeout;
        // let mut validated_tot = 0usize;
        while !self.validate(&datafeed) {//while all data is not there

            check_timeout(end, format!("Synchronizer timeout for actor {}", actor_name)).map_err(|e| RecvFailure::Any(e.into()))?;

            //is there a message from the controller?
            match ctr.try_recv() {
                Ok(m) => {
                    debug!("{} received {:?} from the controller", actor_name, m);
                    return Err(RecvFailure::ControllerMessage(m));
                },
                _ => {}
            }

            //is there a pending validation?
            try!(valid.update_validations());

            //feed constants
            for (var_name, pair) in self.constants.iter() {
                let (data, count) = pair.clone();
                for _ in datafeed.count_var_data(var_name)..count {
                    datafeed.add_data(var_name.to_string(), data.clone());
                }
            }

            let mut all_channels_empty = true;
            //receive messages from other actors
            for (name, pair) in self.rxs.iter() {//try to receive on all channel
                let &(ref rx, count) = pair;

                let current_count = datafeed.count_var_data(name);

                if current_count < count {//if we are expecting data on that channel
                    match rx.try_recv() {
                       Ok(ActorMessage::EndSignal) => {
                           return Err(RecvFailure::ControllerMessage(ControllerMessage::End));
                       },
                       Ok(ActorMessage::Data(d)) => {
                           trace!("{}.{} received data {:?}", actor_name, name, d);
                           //push timeout
                           all_channels_empty = false;
                           end = Utc::now() + self.timeout;
                           datafeed.add_data(name.to_string(), d);//add data received to
                       },
                       Ok(ActorMessage::Error(e)) => {
                           return Err(RecvFailure::Any(format_err!("Error at input: {}", e)));
                       },
                       Ok(ActorMessage::SyncSignal) => {
                        //    trace!("{}.{} received SyncSignal", actor_name, name);
                           //push timeout
                           all_channels_empty = false;
                           end = Utc::now() + self.timeout;
                           datafeed.add_data(name.to_string(), Data::Empty);//add empty data to sync channel
                       },
                       _ => {}//do nothing
                   }
                }
            }

            // auto throttling
            if all_channels_empty == true {
                thread::sleep(time::Duration::from_millis(10))
            }
        }
        // trace!("Datafeed in {:?}", datafeed);

        Ok(datafeed)
    }

    pub fn try_purge(&self) {
        for (_, pair) in self.rxs.iter() {//try to receive on all channel
            let &(ref rx, _) = pair;
            let _ = rx.try_recv();//drop message
        }
    }

    pub fn validate(&self, feed: &DataFeed) -> bool {
        for (name, pair) in self.rxs.iter() {
            let &(_, count) = pair;
            if feed.count_var_data(name) != count {
                return false;
            }
        }
        true
    }

}
