// @Author: ronan
// @Date:   10-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 15-11-2016

use config_keys::*;

use process::actors::actor_message::ActorMessage;
use sp_lib::interfaces::process::datafeed::DataFeed;
use process::controller_message::ControllerMessage;
use process::actors::validation::validation_step::ValidationStep;
use process::actors::validation::validator::Validator;

// use std::sync::mpsc::{TrySendError};
use std::collections::HashMap;
use std::sync::mpsc::Receiver;
// use std::{thread, time};
use std::fmt::Display;

use process::actors::SBSender;
use failure::Error;


pub struct Dispatcher {
    ///tx channel to next actors
    txs: HashMap<String, Vec<SBSender<ActorMessage>>> //(source var_name, all senders linked with this var)
}

impl Dispatcher {
    pub fn new() -> Dispatcher {
        Dispatcher { txs: HashMap::new() }
    }

    ///Add a sender endpoint linked with a var_name
    pub fn add_sender(&mut self, source_var: &str, tx: SBSender<ActorMessage>) {
            let mut prev = match self.txs.remove(source_var) {// remove to edit or create empty vec
                Some(v) => v,
                None => Vec::new()
            };

            prev.push(tx);//add this sender
            self.txs.insert(source_var.to_string(), prev);//wrap in hashmap
    }

    fn deal_with_controller(ctr_rx: &Receiver<ControllerMessage>) -> Result<(), Error> {
        if let Ok(m) = ctr_rx.try_recv() {
            // warn!("Controller intercepted dispatched message");
            Err(format_err!("Received message {:?} from Controller in Dispatcher", m))
        }
        else {
            Ok(())
        }
    }

    //send this datafeed where is needed
    pub fn send(&self, actor_name: &str, mut datafeed: DataFeed, valid: &mut Validator, ctr_rx: &Receiver<ControllerMessage>) 
    -> Result<(), Error> {//unpack datafeed

        let mut msg_count: HashMap<String, usize> = HashMap::new();

        //create validation step
        //1. count data to validate
        for (source_var, _) in self.txs.iter() {
            let mut data_count = datafeed.count_var_data(source_var);
            if data_count == 0 && source_var == SYNC {
                data_count = 1;
            }
            if data_count != 0 {
                msg_count.insert(source_var.to_string(), data_count);
            }
        }
        //2. Memorize these counts
        let valid_step = ValidationStep::new(msg_count);
        valid.add_to_validate(valid_step);

        //send the datafeed
        for (source_var,txs) in self.txs.iter() {
            let data_opt = datafeed.move_var_data(source_var);//get data vector for this var
            match data_opt {
                Some(vec) => {//if data vector present
                    for d in vec.iter() {//for each data
                        for tx in txs.iter() {//for each channel
                            let mess = ActorMessage::Data(d.clone());//only 1 clone because d can be big

                            try!(Dispatcher::deal_with_controller(ctr_rx));

                            //is there a pending validation ?
                            try!(valid.update_validations());

                            // trace!("Sending {}.{}", actor_name, source_var);
                            tx.send(mess)?;
                        }
                    }
                },
                None => {
                     if source_var == SYNC {
                        //try!(self.send_mess(ActorMessage::SyncSignal, ctr_rx));
                        // trace!("{}.SyncSignal sent to {:?}", actor_name, source_var);
                         for tx in txs.iter() {//for each channel
                             let _ = try!(tx.send(ActorMessage::SyncSignal));//send all data into channel
                         }
                     }
                     else {
                         debug!("{}.{} has no data", actor_name, source_var);
                     }
                }
            }
        }

        Ok(())
    }

    fn send_mess(&self, mess: ActorMessage, ctr_rx: &Receiver<ControllerMessage>) -> Result<(), Error> {
        for (_,txs) in self.txs.iter() {
            for tx in txs.iter() {//for each channel

                // loop {
                    try!(Dispatcher::deal_with_controller(ctr_rx));
                    tx.send(mess.clone())?;
                    // let send_res = tx.try_send(mess.clone());
                    // match send_res {
                    //     Ok(_) => { break;}
                    //     Err(e) => {
                    //         match e {
                    //             TrySendError::Disconnected(_) => { return Err(format_err!("Cannot send message {:?} on disconnected channel", mess)); },
                    //             TrySendError::Full(_) => {//try again
                    //                 let closing_delay = time::Duration::from_millis(5);
                    //                 thread::sleep(closing_delay);
                    //             }
                    //         }

                    //     }
                    // }
                // }
            }
        }
        Ok(())
    }

    pub fn send_error<S: Display>(&self, e: S, ctr_rx: &Receiver<ControllerMessage>) -> Result<(), Error> {
        self.send_mess(ActorMessage::Error(e.to_string()), ctr_rx)
    }

    pub fn send_end(&self, ctr_rx: &Receiver<ControllerMessage>) -> Result<(), Error> {
        self.send_mess(ActorMessage::EndSignal, ctr_rx)
    }
}
