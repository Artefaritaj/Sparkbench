// @Author: ronan
// @Date:   11-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 16-11-2016

use hconfig::store::Data;


#[derive(Debug,Clone)]
pub enum ActorMessage {
    ///Transmit data
    Data(Data),
    ///This is a sync signal
    SyncSignal,
    ///End of the experiment
    EndSignal,
    ///An error occured
    Error(String)
}
