// @Author: Lashermes Ronan <ronan>
// @Date:   04-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 04-04-2017
// @License: MIT


use process::actors::actor_message::ActorMessage;
use process::monitor_message::MonitorMessage;
use process::controller_message::ControllerMessage;

use std::sync::mpsc::*;
use std::collections::HashMap;

use std::time;
use std::thread;

use failure::Error;


pub struct FeedXManager {
    //rxs: Vec<CountingReceiver>,
    rxs: HashMap<String, HashMap<String, (Receiver<ActorMessage>, usize)>>, //(dest_type, (var_name, (Receiver, cardinality)))
    txs: HashMap<String, Vec<Sender<ActorMessage>>>, //(source_type, Vec senders for that type)
    counters: HashMap<String, HashMap<String, usize>> //(dest_type, (var_name, count))
}

impl FeedXManager {
    pub fn new() -> FeedXManager {
        FeedXManager { rxs: HashMap::new(), txs: HashMap::new(), counters: HashMap::new() }
    }

    pub fn add_receiver(&mut self, var_name: &str, rx: Receiver<ActorMessage>, dest_type: &str, count: usize) 
    -> Result<(), Error> {
        let new_count = if count > 0 { count } else { 1 };

        let mut hash_for_this_type = if let Some(prev) = self.rxs.remove(dest_type) {
            prev
        }
        else {
            HashMap::new()
        };

        if hash_for_this_type.contains_key(var_name) {
            return Err(format_err!("{} already has a receiver.", var_name));
        }
        else {
            hash_for_this_type.insert(var_name.to_string(), (rx, new_count));
        }

        self.rxs.insert(dest_type.to_string(), hash_for_this_type);
        Ok(())
    }

    pub fn add_sender(&mut self, tx: Sender<ActorMessage>, source_type: &str) {
        let mut vec_for_this_type = if let Some(prev) = self.txs.remove(source_type) {
            prev
        } else {
            Vec::new()
        };

        vec_for_this_type.push(tx);

        self.txs.insert(source_type.to_string(), vec_for_this_type);
    }

    // pub fn len(&self) -> usize {
    //     self.rxs.len()
    // }

    fn deal_with_monitor(mon_rx: &Receiver<MonitorMessage>) -> Result<(), Error> {
        //treat all upcoming monitor messages
        while let Ok(m) = mon_rx.try_recv() {
            // debug!("Message {:?} received at execution from Monitor", m);
            match m {
                MonitorMessage::Error(e) => {//abort!
                    return Err(format_err!("Error during the process: {}", e));
                },
                MonitorMessage::End => {//should not end here!
                    return Err(format_err!("Process is ending prematurely"));
                },
                MonitorMessage::Undefined => {
                    return Err(format_err!("Process is ending prematurely (reason undefined)"));
                },
                _ => {}
            }
        }
        Ok(())
    }

    fn deal_with_controller(ctr_rx: &Receiver<ControllerMessage>) -> Result<(), Error> {
        match ctr_rx.try_recv() {
            Ok(m) => {Err(format_err!("Message {:?} from the controller while waiting for validations", m))},
            _ => {Ok(())}
        }
    }

    pub fn update_feedx(&mut self, feed_type: &str, ctr: Option<&Receiver<ControllerMessage>>, mon: Option<&Receiver<MonitorMessage>>) 
    -> Result<(), Error> {
        try!(self.send_feedx(feed_type));
        self.wait_for_feedx(feed_type, ctr, mon)
    }

    pub fn send_feedx(&self, source_type: &str) -> Result<(), Error> {
        if let Some(txs) = self.txs.get(source_type) {
            for tx in txs.iter() {
                try!(tx.send(ActorMessage::SyncSignal));
            }
        }

        Ok(())
    }

    pub fn wait_for_feedx(&mut self, dest_type: &str, ctr: Option<&Receiver<ControllerMessage>>, mon: Option<&Receiver<MonitorMessage>>) 
    -> Result<(), Error> {

        while !self.validate(dest_type) {
            match mon {//if monitor
                Some(mon_rx) => {try!(FeedXManager::deal_with_monitor(mon_rx));},
                None => {}
            }

            match ctr {//if controller
                Some(ctr_rx) => {try!(FeedXManager::deal_with_controller(ctr_rx));},
                None => {}
            }

            //not validated, receive new messages
            let mut counter_update: HashMap<String, usize> = if let Some(prev) = self.counters.remove(dest_type) {
                prev
            }
            else {
                HashMap::new()
            };

            if let Some(rxs) = self.rxs.get(dest_type) {
                for (name, pair) in rxs.iter() {
                    let &(ref rx, _) = pair;

                    match rx.try_recv() {
                        Ok(ActorMessage::SyncSignal) => {
                            //end = UTC::now() + self.timeout;
                            let prev = if let Some(val) = counter_update.get(name) {
                                *val
                            }
                            else {
                                0
                            };//previous value

                            counter_update.insert(name.to_string(), prev + 1);
                        },
                        Ok(x) => { return Err(format_err!("Feedback manager received message {:?} which is forbidden", x)); },
                        Err(_) => { thread::sleep(time::Duration::from_millis(10));}
                    }
                }
            }

            //update counters before next validate
            self.counters.insert(dest_type.to_string(), counter_update);
        }

        //validated, update counters
        let mut new_count: HashMap<String, usize> = HashMap::new();//counters for this dest_type
        if let Some(rxs) = self.rxs.get(dest_type) {
            if let Some(counters) = self.counters.get(dest_type) {
                for (name, pair) in rxs.iter() {
                    let &(_, count) = pair;
                    if let Some(val) = counters.get(name) {
                        if *val >= count {
                            new_count.insert(name.to_string(), *val-count);
                        }
                        else {
                            return Err(format_err!("Feed{} count cannot be negative (counter is {}, value to remove is {})", dest_type, *val, count));
                        }
                    }
                    else {
                        new_count.insert(name.to_string(), 0);
                    }
                }
            }
        }
        self.counters.insert(dest_type.to_string(), new_count);


        Ok(())
    }

    fn validate(&mut self, dest_type: &str) -> bool {
        let mut validated = true;
        if let Some(rxs) = self.rxs.get(dest_type) {
            if let Some(counters) = self.counters.get(dest_type) {
                for (name, pair) in rxs.iter() {
                    let &(_, cr_count) = pair;
                    if let Some(count) = counters.get(name) {
                        if *count < cr_count {
                            validated = false;
                        }
                    }
                    else {
                        validated = false;
                    }
                }
            }
            else {
                validated = false;
            }
        }

        validated
    }
}
