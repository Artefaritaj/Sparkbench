// @Author: ronan
// @Date:   10-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

use process::actors::{Synchronizer, Dispatcher, SBSender, FeedXManager, ActorMessage};
use process::actors::validation::validator::Validator;
use process::actors::validation::validation_message::ValidationMessage;

use process::{Core, ControllerMessage, MonitorMessage};

use dimval::{DimVal, Prefix};

use chrono;
use failure::Error;
use failures::*;

use config_keys::*;

use std::sync::mpsc::*;
use std::{thread, time};
use std::collections::HashMap;

use sp_lib::interfaces::process::Actor;
use sp_lib::helpers::ids::gen_rng_id;


use hconfig::store::Data;

// const RDV_CHAN_SIZE: usize = 1;
// const BUFFER_SIZE : usize = 10;

#[derive(Debug)]
enum LoopFSMState {
    Idle,
    ActingOnData,
    Ending,
    ///Close thread
    Closing,
    Error
}

pub struct ThreadedActor {
    name: String,
    sync: Synchronizer,//receive data
    actor: Box<dyn Actor>,//act on data, trait object
    disp: Dispatcher,//send (modified?) data down the chain
    valid: Validator,
    feedx_mgr: FeedXManager,
    attributes: HashMap<String, String>,
    ///Do we force validate upon an error, so as to not get stuck
    /// true => ResumeSkip,
    /// false => Resume
    force_on_error: bool
}

impl ThreadedActor {
    pub fn new<T>(name: &str, actor: T, constants: HashMap<String, (Data, usize)>, attributes: HashMap<String, String>, force: bool) 
    -> ThreadedActor where T: Actor {
        ThreadedActor {name: name.to_string(),
            actor: Box::new(actor),
            sync: Synchronizer::new(constants),
            disp: Dispatcher::new(),
            valid: Validator::new(),
            feedx_mgr: FeedXManager::new(),
            attributes: attributes,
            force_on_error: force}
    }

    pub fn dismantle(self) -> (String, Synchronizer, Box<dyn Actor>, Dispatcher, Validator, HashMap<String, String>, FeedXManager, bool) {
        (self.name, self.sync, self.actor, self.disp, self.valid, self.attributes, self.feedx_mgr, self.force_on_error)
    }

    pub fn get_synchronizer_mut(&mut self) -> &mut Synchronizer {
        &mut self.sync
    }

    pub fn get_dispatcher_mut(&mut self) -> &mut Dispatcher {
        &mut self.disp
    }

    pub fn get_validator_mut(&mut self) -> &mut Validator {
        &mut self.valid
    }

    pub fn get_actor_mut(&mut self) -> &mut Box<dyn Actor> {
        &mut self.actor
    }

    pub fn get_feedx_mut(&mut self) -> &mut FeedXManager {
        &mut self.feedx_mgr
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn get_sources_count(&self) -> usize {
        self.sync.len()
    }

    pub fn feedx_link(&mut self, to: &mut ThreadedActor, source_type: &str, dest_type: &str, cardinality: usize) -> Result<(), Error> {
        let (tx, rx): (Sender<ActorMessage>, Receiver<ActorMessage>) = channel();
        let var_name = format!("FB_{}_{:x}", self.get_name(), gen_rng_id());
        self.get_feedx_mut().add_sender(tx, source_type);
        try!(to.get_feedx_mut().add_receiver(&var_name, rx, dest_type, cardinality));
        trace!("New feedback from {}.{} to {}.{} ({})", self.get_name(), source_type, to.get_name(), dest_type, cardinality);
        Ok(())
    }

    ///Establish a synchronization link (i.e. rendez vous channel) with another ThreadedActor
    pub fn sync_link(&mut self, to: &mut ThreadedActor) -> Result<(), Error> {
        // let (tx,rx): (SBSender<ActorMessage>, Receiver<ActorMessage>) = sync_channel(RDV_CHAN_SIZE);
        let (tx,rx): (SBSender<ActorMessage>, Receiver<ActorMessage>) = channel();
        let (tx_valid,rx_valid): (Sender<ValidationMessage>, Receiver<ValidationMessage>) = channel();

        let var_name = format!("SYNC_{:x}", gen_rng_id());
        // self.get_synchronizer_mut().add_validation_channel(&var_name, rx_next);
        self.get_validator_mut().add_validation_rx(&var_name, rx_valid);
        self.get_dispatcher_mut().add_sender(&var_name, tx);
        to.get_validator_mut().add_validation_tx(tx_valid, 1);
        try!(to.get_synchronizer_mut().add_receiver(&var_name, rx, 1));
        trace!("New link from {}.{} to {}.{}", self.get_name(), var_name, to.get_name(), var_name);
        Ok(())
    }

    pub fn self_sync_link(&mut self) -> Result<(), Error> {
        // let (tx,rx): (SBSender<ActorMessage>, Receiver<ActorMessage>) = sync_channel(RDV_CHAN_SIZE);
        let (tx,rx): (SBSender<ActorMessage>, Receiver<ActorMessage>) = channel();
        let (tx_valid,rx_valid): (Sender<ValidationMessage>, Receiver<ValidationMessage>) = channel();

        let var_name = format!("SYNC_{:x}", gen_rng_id());
        self.get_validator_mut().add_validation_rx(&var_name, rx_valid);
        self.get_dispatcher_mut().add_sender(&var_name, tx);
        self.get_validator_mut().add_validation_tx(tx_valid, 1);
        try!(self.get_synchronizer_mut().add_receiver(&var_name, rx, 1));
        trace!("New link from {}.{} to {}.{}", self.get_name(), var_name, self.get_name(), var_name);
        Ok(())
    }

    ///Establish a data link between 2 ThreadedActor.
    ///[cardinality] is the number of data from this link [to] is expecting before action.
    pub fn link(&mut self, to: &mut ThreadedActor, source_var: &str, dest_var: &str, cardinality: usize) -> Result<(), Error> {
        // let this_buffer_size = if cardinality > BUFFER_SIZE { BUFFER_SIZE } else { cardinality };
        // let (tx,rx): (SBSender<ActorMessage>, Receiver<ActorMessage>) = sync_channel(this_buffer_size);
        let (tx,rx): (SBSender<ActorMessage>, Receiver<ActorMessage>) = channel();
        let (tx_valid,rx_valid): (Sender<ValidationMessage>, Receiver<ValidationMessage>) = channel();

        self.get_validator_mut().add_validation_rx(source_var, rx_valid);
        self.get_dispatcher_mut().add_sender(source_var, tx);
        to.get_validator_mut().add_validation_tx(tx_valid, cardinality);
        try!(to.get_synchronizer_mut().add_receiver(dest_var, rx, cardinality));
        trace!("New link from {}.{} to {}.{}", self.get_name(), source_var, to.get_name(), dest_var);
        Ok(())
    }

    pub fn self_link(&mut self, source_var: &str, dest_var: &str, cardinality: usize) -> Result<(), Error> {
        // let this_buffer_size = if cardinality > BUFFER_SIZE { BUFFER_SIZE } else { cardinality };
        // let (tx,rx): (SBSender<ActorMessage>, Receiver<ActorMessage>) = sync_channel(this_buffer_size);
        let (tx,rx): (SBSender<ActorMessage>, Receiver<ActorMessage>) = channel();
        let (tx_valid,rx_valid): (Sender<ValidationMessage>, Receiver<ValidationMessage>) = channel();

        self.get_validator_mut().add_validation_rx(source_var, rx_valid);
        self.get_dispatcher_mut().add_sender(source_var, tx);
        self.get_validator_mut().add_validation_tx(tx_valid, cardinality);
        try!(self.get_synchronizer_mut().add_receiver(dest_var, rx, cardinality));
        trace!("New link from {}.{} to {}.{}", self.get_name(), source_var, self.get_name(), dest_var);
        Ok(())
    }

    pub fn write_to_link(&mut self, var_name: &str, rx: Receiver<ActorMessage>, tx_valid: Sender<ValidationMessage>, cardinality: usize) -> Result<(), Error> {
        try!(self.get_synchronizer_mut().add_receiver(var_name, rx, cardinality));
        self.get_validator_mut().add_validation_tx(tx_valid, cardinality);
        Ok(())
    }


    //Finite state machine transitions
    fn idle_state(monitor_tx: &Sender<MonitorMessage>, controller_rx: &Receiver<ControllerMessage>) -> Result<LoopFSMState, Error> {
        try!(monitor_tx.send(MonitorMessage::Idle));
        //wait for start message from controller
        loop {
            match controller_rx.try_recv() {
                Ok(ControllerMessage::Start) => { return Ok(LoopFSMState::ActingOnData);},
                Ok(ControllerMessage::Kill) => { return Ok(LoopFSMState::Closing);},
                Ok(x) => {
                    return Err(format_err!("FSM error: receive {:?} message while in Idle state.", x));
                }
                _ => {
                    let tempor = time::Duration::from_millis(10);
                    thread::sleep(tempor);
                }
            }
        }
    }

    fn working_state(name: &str, sync: &mut Synchronizer, actor: &mut Box<dyn Actor>, disp: &Dispatcher,
                    valid: &mut Validator, attrs: &HashMap<String, String>,
                    monitor_tx: &Sender<MonitorMessage>, controller_rx: &Receiver<ControllerMessage>,
                    feedx_mgr: &mut FeedXManager) -> Result<LoopFSMState, ActorFailure> {

        try!(monitor_tx.send(MonitorMessage::Working) .map_err(|e|ActorFailure::Propagated(format_err!("{}", e))));



        //read datafeed from previous actors
        let datafeed_in = match sync.recv(name, valid, controller_rx) {
                Ok(d) => {
                    d
                },
                Err(RecvFailure::ControllerMessage(m)) => {//we did not receive data but a signal instead
                    // info!("{} rx {:?}", threaded_actor.get_name(), m);
                    match m {
                        ControllerMessage::End => {
                            return Ok(LoopFSMState::Ending);
                        },
                        ControllerMessage::Kill => {
                            return Ok(LoopFSMState::Closing);
                        },
                        x => {
                            return Err(ActorFailure::Propagated(format_err!("FSM error: receive {:?} message while working.", x)));
                        }
                    }
                },
                Err(e) => {
                    return Err(ActorFailure::Propagated(format_err!("{} ({})", e, name)));
                }
        };
        trace!("{} received input", name);
        //trace!("{} datafeed_in: {:?}", name, datafeed_in);

        //if forced, do not act
        if valid.is_one_forced() {
            let tempor = time::Duration::from_millis(100 as u64);
            thread::sleep(tempor);
            info!("{} has been stopped because of forcing", name);
            return Err(ActorFailure::Propagated(format_err!("{} has been stopped because of forcing", name)));
        }

        //pre temporisation
        if let Some(pre_str) = attrs.get(PRE_TEMPO) {
            match pre_str.parse::<DimVal>()
                        //.map_err(|s| ActorError::Step(s))
                        .and_then(|x|x.filter_unit_str("s"))
                        .and_then(|x|x.to_f64_prefix(Prefix::Milli)) {
                Ok(pre_ms) => {
                    let tempor = time::Duration::from_millis(pre_ms as u64);
                    thread::sleep(tempor);
                },
                Err(_) => {}
            }
        }

        //feedfront update
        try!(feedx_mgr.update_feedx(FRONT, Some(controller_rx), None) .map_err(|s|ActorFailure::Propagated(s)));

        //act on datafeed
        // trace!("{} before acting.", name);
        let datafeed_out = try!(actor.act(datafeed_in, attrs) .map_err(|s|ActorFailure::Step(s)));
        // trace!("{} datafeed_out: {:?}", name, datafeed_out);

        //post temporisation
        if let Some(post_str) = attrs.get(POST_TEMPO) {
            match post_str.parse::<DimVal>()
                        .and_then(|x|x.filter_unit_str("s"))
                        .and_then(|x|x.to_f64_prefix(Prefix::Milli)) {
                Ok(post_ms) => {
                    let tempor = time::Duration::from_millis(post_ms as u64);
                    thread::sleep(tempor);
                },
                Err(_) => {}
            }
        }

        //send datafeed to next actors, synchronizer is needed to update validation while the output buffer is full
        try!(disp.send(name, datafeed_out, valid, controller_rx) .map_err(|s|ActorFailure::Propagated(s)));
        // trace!("{} sent to dispatcher", name);

        //feedback update
        try!(feedx_mgr.update_feedx(BACK, Some(controller_rx), None) .map_err(|s|ActorFailure::Propagated(s)));
        // trace!("{} feedback", name);

        //synchronization attributes:
        //wait for all outgoing messages to be completed before acting on the next message
        if attrs.contains_key(WAIT_VALIDATION) {
            try!(valid.wait_for_total_validation(Some(controller_rx), None) .map_err(|s|ActorFailure::Propagated(s)));
            trace!("Validation wait for {} ended.", name);
        }

        // trace!("{} after validation", name);

        //keep working
        Ok(LoopFSMState::ActingOnData)
    }

    fn end_state(disp: &Dispatcher, valid: &mut Validator, monitor_tx: &Sender<MonitorMessage>, controller_rx: &Receiver<ControllerMessage>) -> Result<LoopFSMState, Error> {
        //propagate end
        try!(disp.send_end(controller_rx));

        //wait for complete validation
        //number of validation to send
        let valid_count = try!(valid.wait_for_total_validation(Some(controller_rx), None));

        if valid_count > 0 {
            //send the validation
            try!(valid.send_validation(valid_count));
        }

        //signal to monitor
        try!(monitor_tx.send(MonitorMessage::End));

        //wait for kill
        loop {
            match controller_rx.try_recv() {
                Ok(ControllerMessage::Kill) => { return Ok(LoopFSMState::Closing); },
                Ok(x) => {
                    return Err(format_err!("FSM error: receive {:?} message while in End state.", x));
                }
                _ => {
                    let tempor = time::Duration::from_millis(50);
                    thread::sleep(tempor);
                }
            }
        }
    }

    fn error_state(name: &str, disp: &Dispatcher, monitor_tx: &Sender<MonitorMessage>, controller_rx: &Receiver<ControllerMessage>, error: &Error) 
    -> Result<LoopFSMState, Error> {
        //signal to monitor
        debug!("An error occured in {}: {}", name, error);
        let _ = monitor_tx.send(MonitorMessage::Error(error.to_string()));
        let _ = disp.send_error(error, controller_rx);

        //Ok(LoopFSMState::Idle)
        Ok(LoopFSMState::Closing)
    }

    fn closing_state(name: &str, actor: &mut Box<dyn Actor>, valid: &mut Validator, monitor_tx: &Sender<MonitorMessage>) {
        //debug!("Thread actor {} trying to close.", name);
        let max_step = valid.get_max_step();
        actor.closing(&max_step.to_hashmap());

        //signal to monitor
        let _ = monitor_tx.send(MonitorMessage::Closing);
        debug!("Thread actor {} closing.", name);


    }

    fn attr4init(attrs: &HashMap<String, String>, sync: &mut Synchronizer) {
        //is there a timeout attribute for this actor?
        match attrs.get(SYNC_TIMEOUT) {
            Some(val_str) => {//can unwrap because UNITS_SYMBOLS is statically defined (but error not catch at compilation if any)
                match val_str.parse::<DimVal>()
                        .and_then(|x|x.filter_unit_str("s"))
                        .and_then(|x|x.to_f64_prefix(Prefix::Milli)) {
                    Ok(millis) => {
                        let timeout = chrono::Duration::milliseconds(millis as i64);
                        sync.set_timeout(timeout);
                    },
                    Err(e) => {
                        warn!("An error occured while parsing SYNC_TIMEOUT attribute: {}", e.to_string())
                    }
                }

            },
            None => {}//stay with default
        }
    }

    ///Launch actor in its own thread
    ///This method is too long and complex TODO refactor
    pub fn launch_thread(threaded_actor: ThreadedActor, core: &mut Core) {

        let controller_rx = core.get_controller_mut().add_controlled();
        let monitor_tx = core.get_monitor_mut().add_monitored();
        debug!("{} is now monitored.", threaded_actor.get_name());

        thread::spawn(move|| {
            let mut state = LoopFSMState::Idle;
            // let mut threaded_actor = threaded_actor;
            let (name, mut sync, mut actor, disp, mut valid, attributes, mut feedx_mgr, force_on_error) = threaded_actor.dismantle();
            let controller_rx = controller_rx;
            let monitor_tx = monitor_tx;
            let mut error_msg = format_err!("");

            //manage attributes that have an impact at init
            ThreadedActor::attr4init(&attributes, &mut sync);

            loop {//infinite loop with FSM
                let result = match state {
                    LoopFSMState::Idle => {
                        ThreadedActor::idle_state(&monitor_tx, &controller_rx)
                    },
                    LoopFSMState::ActingOnData => {
                        let wres = ThreadedActor::working_state(&name, &mut sync, &mut actor, &disp,
                                                    &mut valid, &attributes, &monitor_tx,
                                                    &controller_rx, &mut feedx_mgr);

                       
                        match wres {
                            Err(act_failure) => {
                                if let ActorFailure::Step(_) = act_failure {
                                     //if fail, we send the validation so as to not get stuck at this position
                                    if force_on_error {
                                        let _ = valid.forced_validation(1);
                                        thread::sleep(time::Duration::from_millis(10));
                                    }
                                }
                                Err(act_failure.to_error())
                            },
                            Ok(state) => Ok(state)
                        }

                    },
                    LoopFSMState::Ending => {
                        ThreadedActor::end_state(&disp, &mut valid, &monitor_tx, &controller_rx)
                    },
                    LoopFSMState::Error => {
                        ThreadedActor::error_state(&name, &disp, &monitor_tx, &controller_rx, &error_msg)
                    },
                    LoopFSMState::Closing => {
                        ThreadedActor::closing_state(&name, &mut actor, &mut valid, &monitor_tx);
                        break;
                    }
                };

                match result {
                    Ok(s) => { state = s; },
                    Err(e) => {
                        state = LoopFSMState::Error;
                        error_msg = e;
                    }
                }
            }

            // println!("Thread closed.");
            //let some alive time before closing channels -> time to propagate ultimate message
            let closing_delay = time::Duration::from_millis(20);
            thread::sleep(closing_delay);
            debug!("Thread actor {} closed", name);
        });
    }
}
