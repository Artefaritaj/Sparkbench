// @Author: Lashermes Ronan <ronan>
// @Date:   04-05-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 04-05-2017
// @License: MIT


#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord)]
pub enum ValidationMessage {
    Std(usize),
    Forced(usize)//force validation on crashes
}

impl ValidationMessage {
    pub fn get_count(&self) -> usize {
        match self {
            &ValidationMessage::Std(u) => u,
            &ValidationMessage::Forced(u) => u
        }
    }

    pub fn new_std(u: usize) -> ValidationMessage {
        ValidationMessage::Std(u)
    }

    pub fn new_forced(u: usize) -> ValidationMessage {
        ValidationMessage::Forced(u)
    }
}
