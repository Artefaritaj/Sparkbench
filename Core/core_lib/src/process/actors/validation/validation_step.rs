// @Author: Lashermes Ronan <ronan>
// @Date:   24-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 24-02-2017
// @License: MIT

use config_keys::*;
use std::collections::HashMap;

use failure::Error;

#[derive(Debug,Clone)]
///This structure represents the validation messages (from next actors) required to send a new validation (from this actor)
pub struct ValidationStep {
    step: HashMap<String, usize>
}

impl ValidationStep {
    pub fn new(hm: HashMap<String, usize>) -> ValidationStep {
        ValidationStep { step: hm }
    }

    pub fn from_pulse(count: usize) -> ValidationStep {
        let mut hm: HashMap<String, usize> = HashMap::new();
        hm.insert(SYNC.to_string(), count);
        ValidationStep { step: hm }
    }

    pub fn is_empty(&self) -> bool {
        self.step.is_empty()
    }

    ///all names of self must be included in other and all counts must be lower or equal
    pub fn included_in(&self, other: &ValidationStep) -> bool {
        for (name, count) in self.step.iter() {
            let other_count = match other.step.get(name) {
                Some(c) => c,
                None => {return false;}
            };

            if other_count < count {
                return false;
            }
        }
        true
    }

    pub fn get_count(&self, name: &str) -> Result<usize, Error> {
        match self.step.get(name) {
            Some(u) => Ok(*u),
            None => Err(format_err!("No name {} present", name))
        }
    }

    pub fn to_hashmap(self) -> HashMap<String, usize> {
        self.step
    }
}
