// @Author: Lashermes Ronan <ronan>
// @Date:   24-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 24-02-2017
// @License: MIT

use std::sync::mpsc::*;

use process::actors::validation::validation_message::ValidationMessage;

#[derive(Debug)]
pub struct ValidationRx {
    rx: Receiver<ValidationMessage>,
    count: usize,//counter of how many validation received
    forced: bool//is this rx forced
}

impl ValidationRx {
    pub fn get_count(&self) -> usize {
        self.count
    }

    pub fn set_count(&mut self, count: usize) {
        self.count = count;
    }

    pub fn get_forced(&self) -> bool {
        self.forced
    }

    pub fn set_forced(&mut self, forced: bool) {
        self.forced = forced;
    }

    pub fn get_rx(&self) -> &Receiver<ValidationMessage> {
        &self.rx
    }

    // pub fn new(rx: Receiver<usize>, count: usize) -> ValidationRx {
    //     ValidationRx { rx: rx, count: count }
    // }

    pub fn from_channel(chan: Receiver<ValidationMessage>) -> ValidationRx {
        ValidationRx { rx: chan, count: 0, forced: false}
    }
}
