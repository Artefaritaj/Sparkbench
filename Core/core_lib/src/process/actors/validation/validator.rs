// @Author: Lashermes Ronan <ronan>
// @Date:   24-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 24-02-2017
// @License: MIT

// use chrono::prelude::*;
// use sp_lib::helpers::timeout::*;

use std::sync::mpsc::*;
use std::collections::{HashMap, VecDeque};
use std::{thread, time};

use process::actors::validation::validation_rx::ValidationRx;
use process::actors::validation::validation_tx::ValidationTx;
use process::monitor_message::MonitorMessage;
use process::actors::validation::validation_step::ValidationStep;
use process::actors::validation::validation_message::ValidationMessage;
use process::controller_message::ControllerMessage;

use failure::Error;

pub struct Validator {
    to_validate: VecDeque<ValidationStep>,
    ///for each var store all ingoing validation channel as well as the current validation count
    validation_counters: HashMap<String, Vec<ValidationRx>>,//count is incremented when validations are received
    validation_senders: Vec<ValidationTx>//count is the number of message to send on validation
}

impl Validator {

    pub fn new() -> Validator {
        Validator { to_validate: VecDeque::new(), validation_counters: HashMap::new(), validation_senders: Vec::new()}
    }

    fn deal_with_monitor(mon_rx: &Receiver<MonitorMessage>) -> Result<(), Error> {
        //treat all upcoming monitor messages
        while let Ok(m) = mon_rx.try_recv() {
            // debug!("Message {:?} received at execution from Monitor", m);
            match m {
                MonitorMessage::Error(e) => {//abort!
                    return Err(format_err!("Error during the process: {}", e));
                },
                MonitorMessage::End => {//should not end here!
                    return Err(format_err!("Process is ending prematurely"));
                },
                MonitorMessage::Undefined => {
                    return Err(format_err!("Process is ending prematurely"));
                },
                _ => {}
            }
        }
        Ok(())
    }

    fn deal_with_controller(ctr_rx: &Receiver<ControllerMessage>) -> Result<(), Error> {
        match ctr_rx.try_recv() {
            Ok(m) => {Err(format_err!("Message {:?} from the controller while waiting for validations", m))},
            _ => {Ok(())}
        }
    }


    //this validator receives validations from rx
    pub fn add_validation_rx(&mut self, var_name: &str, rx: Receiver<ValidationMessage>) {
        let chan = ValidationRx::from_channel(rx);

        //strange way to do that, thank you borrow checker :(
        let to_create = !self.validation_counters.contains_key(var_name);

        if to_create {
            self.validation_counters.insert(var_name.to_string(), vec![chan]);
        }
        else {
            match self.validation_counters.get_mut(var_name) {
                Some(vec) => { vec.push(chan); }
                None => { }
            }
        }
    }

    //this validator send validations to tx
    pub fn add_validation_tx(&mut self, tx: Sender<ValidationMessage>, count: usize) {
        let chan = ValidationTx::new(tx, count);
        self.validation_senders.push(chan);
    }

    pub fn substact_validation_step(&mut self, step: &ValidationStep) -> Result<(), Error> {
        for (var_name, vec_channels) in self.validation_counters.iter_mut() {
            let to_remove = match step.get_count(var_name) {
                Ok(u) => u,
                Err(_) => 0
            };

            for channel in vec_channels.iter_mut() {
                let mut count = channel.get_count();
                if count >= to_remove {
                    count -= to_remove;
                    channel.set_count(count);
                }
                // else {
                //     return Err(format!("Removing {} elements which is more than the maximum {} for {}", to_remove, count, var_name));
                // }
            }
        }
        Ok(())
    }



    ///This is the maximum step that can be use to try to shorten the queue
    ///E.g. this synchronizer has 1 var "input" with two channels ch1 ch2 with two attached validation_counters vc1 vc2.
    ///min_step is the validation step {"input", min(vc1,vc2)}
    pub fn get_max_step(&self) -> ValidationStep {
        if self.is_one_forced() {
            self.get_max_step_forced()
        }
        else {
            self.get_max_step_std()
        }
    }

    pub fn is_one_forced(&self) -> bool {
        for (_, vec_channels) in self.validation_counters.iter() {
            for channel in vec_channels.iter() {
                if channel.get_forced() == true {
                    return true;
                }
            }
        }
        false
    }

    ///as get_max_step_std but keep only for forced
    fn get_max_step_forced(&self) -> ValidationStep {
        let mut current_validation_counterss = HashMap::new();
        //get validation messages
        for (var_name, vec_channels) in self.validation_counters.iter() {
            let mut min_count = <usize>::max_value();
            for channel in vec_channels.iter() {
                if channel.get_forced() == true {
                    let count = channel.get_count();

                    if count < min_count {
                        min_count = count;
                    }
                }
            }
            current_validation_counterss.insert(var_name.to_string(), min_count);
        }
        ValidationStep::new(current_validation_counterss)
    }


    fn get_max_step_std(&self) -> ValidationStep {
        let mut current_validation_counterss = HashMap::new();
        //get validation messages
        for (var_name, vec_channels) in self.validation_counters.iter() {
            let mut min_count = <usize>::max_value();
            for channel in vec_channels.iter() {
                let count = channel.get_count();

                if count < min_count {
                    min_count = count;
                }
            }
            current_validation_counterss.insert(var_name.to_string(), min_count);
        }
        ValidationStep::new(current_validation_counterss)
    }

    pub fn try_shorten_queue(&mut self, step: &ValidationStep) -> bool {
        let max_step = self.get_max_step();

        if step.included_in(&max_step) {
            match self.substact_validation_step(step) {
                Ok(_) => true,
                Err(_) => false
            }
        }
        else {
            false
        }
    }

    ///Read validation messages if any, then try to shorten the queue as much as possible
    ///Return the number of time to validate
    pub fn try_validate(&mut self) -> Result<usize, Error> {
        //get validation messages
        for (_, vec_channels) in self.validation_counters.iter_mut() {
            for channel in vec_channels.iter_mut() {
                let mut count = channel.get_count();
                while let Ok(u) = channel.get_rx().try_recv() {
                    count += u.get_count();
                    match u {
                        ValidationMessage::Std(_) => {/*channel.set_forced(false);*/},
                        ValidationMessage::Forced(_) => {
                            channel.set_forced(true);
                        },
                    }
                }

                channel.set_count(count);
            }
        }

        let mut validation_count = 0usize;
        loop {
            let to_remove = match self.to_validate.front() {
                Some(s) => s.clone(),
                None => {break;}
            };

            if self.try_shorten_queue(&to_remove) {//validation!
                self.to_validate.pop_front();
                validation_count += 1;
            }
            else {
                break;
            }
        }

        Ok(validation_count)
    }

    pub fn wait_for_total_validation(&mut self, ctr: Option<&Receiver<ControllerMessage>>, mon: Option<&Receiver<MonitorMessage>>) 
    -> Result<usize, Error> {
        // let end = UTC::now() + self.timeout;

        let mut validation_count = 0usize;
        while self.to_validate.len() > 0 {

            match mon {//if monitor
                Some(mon_rx) => {try!(Validator::deal_with_monitor(mon_rx));},
                None => {}
            }

            match ctr {//if controller
                Some(ctr_rx) => {try!(Validator::deal_with_controller(ctr_rx));},
                None => {}
            }

            validation_count += try!(self.update_validations());
            if validation_count == 0 {//auto throttling
                let tempor = time::Duration::from_millis(5);
                thread::sleep(tempor);
            }
        }
        Ok(validation_count)
    }


    pub fn send_validation(&self, validation_count: usize) -> Result<(), Error> {
        for vtx in self.validation_senders.iter() {
            try!(vtx.get_tx().send(ValidationMessage::new_std(vtx.get_count() * validation_count)));
        }
        Ok(())
    }

    pub fn forced_validation(&self, validation_count: usize) -> Result<(), Error> {
        for vtx in self.validation_senders.iter() {
            try!(vtx.get_tx().send(ValidationMessage::new_forced(vtx.get_count() * validation_count)));
        }
        Ok(())
    }

    pub fn update_validations(&mut self) -> Result<usize, Error> {
        //is there a validation to do?
        //number of validation to send
        let valid_count = match self.try_validate() {
            Ok(u) => u,
            Err(e) => {
                return Err(e);
            }
        };

        // validated_tot += valid_count;

        if valid_count > 0 {
            //send the validation
            if self.is_one_forced() {
                try!(self.forced_validation(valid_count));
            }
            else {
                try!(self.send_validation(valid_count));
            }

        }
        Ok(valid_count)
    }

    pub fn add_to_validate(&mut self, step: ValidationStep)  {
        self.to_validate.push_back(step);
    }

    pub fn get_unvalidated_queue(&self) -> &VecDeque<ValidationStep> {
        &self.to_validate
    }

    pub fn is_totally_validated(&self) -> bool {
        self.to_validate.len() == 0
    }


}
