// @Author: Lashermes Ronan <ronan>
// @Date:   02-03-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 02-03-2017
// @License: MIT

use std::sync::mpsc::*;

use process::actors::validation::validation_message::ValidationMessage;

pub struct ValidationTx {
    tx: Sender<ValidationMessage>,
    count: usize//how many validation to send
}

impl ValidationTx {
    pub fn new(tx: Sender<ValidationMessage>, count: usize) -> ValidationTx {
        let actual_count = if count > 0 { count } else { 1 };
        ValidationTx { tx: tx, count: actual_count }
    }

    pub fn get_tx(&self) -> &Sender<ValidationMessage> {
        &self.tx
    }

    pub fn get_count(&self) -> usize {
        self.count
    }
}
