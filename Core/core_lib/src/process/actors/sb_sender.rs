/*
 * File: sender.rs
 * Project: actors
 * Created Date: Friday November 9th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 9th November 2018 2:54:23 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */
use std::sync::mpsc::Sender;

pub type SBSender<T> = Sender<T>;