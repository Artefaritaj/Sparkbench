// @Author: ronan
// @Date:   16-11-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

///May easily contain data in the future
#[derive(Debug)]
pub enum MonitorMessage {
    Undefined,
    Idle,
    Working,
    Error(String),
    End,
    Paused,
    Closing,
    FinalState,
    Kill
}

#[derive(Debug,Clone,Copy,PartialEq,Eq,Hash)]
pub enum MonitorSignal {
    Undefined,
    Idle,
    Working,
    Error,
    End,
    Paused,
    Closing
}

impl MonitorMessage {
    pub fn to_signal(&self) -> MonitorSignal {
        match self {
            &MonitorMessage::Undefined   => MonitorSignal::Undefined,
            &MonitorMessage::Idle        => MonitorSignal::Idle,
            &MonitorMessage::Working     => MonitorSignal::Working,
            &MonitorMessage::Error(_)    => MonitorSignal::Error,
            &MonitorMessage::End         => MonitorSignal::End,
            &MonitorMessage::Paused      => MonitorSignal::Paused,
            &MonitorMessage::Closing  => MonitorSignal::Closing,
            _                            => MonitorSignal::Undefined
        }
    }

    pub fn unanime_signal_state(states: &Vec<MonitorMessage>) -> Option<MonitorSignal> {
        let mut agg_state: Option<MonitorSignal> = None;

        for st in states {
            match agg_state {
                Some(st2) => {
                    if (*st).to_signal() != st2 {
                        return None;
                    }
                },
                None => {agg_state = Some(st.to_signal());}
            }
        }
        agg_state
    }
}

impl MonitorSignal {
    pub fn unanime_state(states: &Vec<MonitorSignal>) -> Option<MonitorSignal> {
        let mut agg_state: Option<MonitorSignal> = None;

        for st in states {
            match agg_state {
                Some(st2) => {
                    if *st != st2 {
                        return None;
                    }
                },
                None => {agg_state = Some(st.clone());}
            }
        }
        agg_state
    }
}
