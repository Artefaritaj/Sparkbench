// @Author: ronan
// @Date:   07-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 16-11-2016

///These are messages sent to
#[derive(Debug,Clone,Copy,Hash,PartialEq,Eq)]
pub enum ControllerMessage {
    Start,
    End,
    Kill
}
