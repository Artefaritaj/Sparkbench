// @Author: ronan
// @Date:   07-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016


use std::sync::mpsc::{channel, Sender, SyncSender, Receiver};
use std::{thread, time};

use process::actors::actor_message::ActorMessage;
use process::controller_message::ControllerMessage;
use process::actors::validation::validator::Validator;
use process::monitor_message::MonitorMessage;

use failure::Error;

pub struct Controller {
    ctr_txs: Vec<Sender<ControllerMessage>>,
    sources: Vec<SyncSender<ActorMessage>>,
    validator: Validator,
}

impl Controller {
    pub fn new() -> Controller {
        Controller { ctr_txs: Vec::new(), sources: Vec::new(), validator: Validator::new()}
    }

    pub fn clear(&mut self) {
        self.ctr_txs.clear();
        self.sources.clear();
    }

    pub fn add_controlled(&mut self) -> Receiver<ControllerMessage> {
        let (tx, rx) = channel();
        self.ctr_txs.push(tx);
        rx
    }

    pub fn send_message_all(&self, mess: ControllerMessage) -> Result<(), Error> {
        for tx in self.ctr_txs.iter() {
            let _ = try!(tx.send(mess.clone()));
        }
        Ok(())
    }

    pub fn clear_sources(&mut self) {
        self.sources.clear();
    }

    pub fn add_source(&mut self, link: SyncSender<ActorMessage>) {
        self.sources.push(link);
    }

    pub fn set_sources(&mut self, links: Vec<SyncSender<ActorMessage>>) {
        self.sources = links;
    }

    pub fn set_validator(&mut self, valid: Validator) {
        self.validator = valid;
    }

    pub fn get_validator_mut(&mut self) -> &mut Validator {
        &mut self.validator
    }

    fn deal_with_monitor(mon_rx: &Receiver<MonitorMessage>) -> Result<(), Error> {
        //treat all upcoming monitor messages
        while let Ok(m) = mon_rx.try_recv() {
            debug!("Message {:?} received at execution from Monitor", m);
            match m {
                MonitorMessage::Error(e) => {//abort!
                    return Err(format_err!("Error during the process: {}", e.to_string()));
                },
                MonitorMessage::End => {//should not end here!
                    return Err(format_err!("Process is ending prematurely"));
                },
                MonitorMessage::Undefined => {
                    return Err(format_err!("Process is ending prematurely"));
                },
                _ => {}
            }
        }
        Ok(())
    }

    ///Send a message through source channel, we must check errors on mon_rx to avoid blocking
    pub fn send_message_sources(&self, mess: ActorMessage, mon_rx: &Receiver<MonitorMessage>) -> Result<(), Error> {
        debug!("Send {:?} to all sources.", mess);
        for link in self.sources.iter() {
            loop {
                try!(Controller::deal_with_monitor(mon_rx));
                let send_res = link.try_send(mess.clone());
                match send_res {
                    Ok(_) => {break;}
                    Err(_) => {//try again
                        let closing_delay = time::Duration::from_millis(10);
                        thread::sleep(closing_delay);
                    }
                }
            }
        }
        Ok(())
    }

    ///Send a sync signal through source channel
    pub fn pulse(&self, mon_rx: &Receiver<MonitorMessage>) -> Result<(), Error> {
        self.send_message_sources(ActorMessage::SyncSignal, mon_rx)
    }

    ///Send end signal through source channel
    pub fn end(&self, mon_rx: &Receiver<MonitorMessage>) -> Result<(), Error> {
        self.send_message_sources(ActorMessage::EndSignal, mon_rx)
    }

    pub fn start_all(&self) -> Result<(), Error> {
        self.send_message_all(ControllerMessage::Start)
    }

    fn is_vec_all_true(vec: &[bool]) -> bool {
        for b in vec {
            if *b == false {
                return false;
            }
        }
        true
    }

    pub fn kill_all(&mut self) -> Result<(), Error> {
        debug!("Controller is sending kill signal ({}).", self.ctr_txs.len());
        //let some alive time before closing channels -> time to propagate ultimate message
        let closing_delay = time::Duration::from_millis(20);
        thread::sleep(closing_delay);

        let mut destroyed_vec = vec![false;self.ctr_txs.len()];

        for (i,tx) in self.ctr_txs.iter().enumerate() {
            match tx.send(ControllerMessage::Kill) {//may fail because the actor already closed
                Ok(_) => {},
                Err(_) => {destroyed_vec[i] = true;}
            }
        }

        //wait for channel deletion (rx has been destroyed)
        while Controller::is_vec_all_true(&destroyed_vec) == false {
            //wait for channel deletion (rx has been destroyed)
            for (i,tx) in self.ctr_txs.iter().enumerate() {
                if destroyed_vec[i] == false {
                    match tx.send(ControllerMessage::Kill) {
                        Ok(_) => {

                        },
                        Err(_) => {
                            destroyed_vec[i] = true;
                        }
                    };
                }
            }
            let tempor = time::Duration::from_millis(30);
            thread::sleep(tempor);
        }

        debug!("All actors have been killed.");

        Ok(())
    }

    pub fn get_controlled_count(&self) -> usize {
        self.ctr_txs.len()
    }
}
