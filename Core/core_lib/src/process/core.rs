// @Author: ronan
// @Date:   31-08-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 15-12-2016
// @License: GPL

use config_keys::*;

use hconfig::store::{StoreNode, StoreKey, Data, AtomicData};
use hconfig::io::toml_file::*;

use sp_lib::helpers::ids::*;

use comms::connections::{CommManager, FeedbackNotifier};
use process::{Controller, Monitor};
use sp_lib::resources::toml::toml_manager::TOMLManager;

use std::path::{Path, PathBuf};
use std::ffi::OsStr;

use failure::Error;

///The Core stores the state of the application
pub struct Core {
    root_path: PathBuf,
    ///Last configuration (applied in direct mode)
    static_config: StoreNode,
    ///Locked if an automatic campaign is currently being performed
    locked: bool,
    ///Mapping between module names and TOML files path
    toml_manager: TOMLManager,
    ///Communication Manager
    comm_manager: CommManager,
    ///Source for next id
    last_inst_id: u64,
    ///Controller to manage actors
    controller: Controller,
    ///Monitor to get info from actors
    monitor: Monitor,
}



impl Core {

    //************************************************************************************
    // PROCESS
    //************************************************************************************

    ///Load (or update) the app config
    pub fn load_app_config(&mut self, root_path: &Path) -> Result<(), Error> {

        let local_root_path = Path::new(SPARKBENCH_CONFIG_FILE).to_path_buf();
        let local_root = if local_root_path.exists() {
            Some(local_root_path)
        }
        else {
            None
        };

        let mut config_path = root_path.to_path_buf();
        config_path.push(SPARKBENCH_CONFIG_FILE);

        if config_path.exists() == false {
            if let Some(local) = local_root {
                config_path = local;
            }
            else {
                return Err(format_err!("No config file @ {}", config_path.display()));
            }
        }

        debug!("Root config file at {}", config_path.display());

        let config = toml_file_to_storenode(config_path.as_path());
        match config {
            Ok(c) => {self.static_config.merge_prefix(&c, &StoreKey::from(CONFIG));},
            Err(_) => {}//do nothing
        }

        Ok(())
    }

    ///Reset clear the config and load only app config. Arrive at the same state as after a setup
    pub fn reset(&mut self, root_path: &Path) -> Result<(), Error> {
        //clear previous config_
        self.static_config = StoreNode::new_empty();
        // self.toml_manager = TOMLManager::new();
        self.comm_manager = CommManager::new();
        self.locked = false;
        self.last_inst_id = gen_rng_id();

        //load default config -> specify TOML files root folder
        try!(self.load_app_config(root_path));

        //List available modules (TOML files)
        self.toml_manager = try!(self.get_supported_configs());
        self.toml_manager.verify_all();//verify and create categories

        //Everything went well
        Ok(())
    }

    ///This is where the core is created
    ///Ensure that static config is loaded if present
    pub fn setup<T: AsRef<Path>>(root_path: T) -> Result<Core, Error> {

        //constructor
        let mut core = Core {
            root_path: root_path.as_ref().to_path_buf(),
            static_config: StoreNode::new_empty(),
            locked: false,
            toml_manager: TOMLManager::new(Path::new("")),
            last_inst_id: 0u64,
            comm_manager: CommManager::new(),
            controller: Controller::new(),
            monitor: Monitor::new()};

        //reset
        try!(core.reset(root_path.as_ref()));

        //return setup struct
        Ok(core)
    }

    //************************************************************************************
    // FEEDBACK URL
    //************************************************************************************

    pub fn create_feedback_notifier(&self) -> FeedbackNotifier {
        self.comm_manager.create_feedback_notifier()
    }

    pub fn set_feedback_url(&mut self, url: Option<String>) {
        self.comm_manager.set_feedback_url(url);
    }

    //************************************************************************************
    // CONTROLLERS
    //************************************************************************************

    pub fn get_controller_mut(&mut self) -> &mut Controller {
        &mut self.controller
    }

    pub fn get_controller(&self) -> &Controller {
        &self.controller
    }

    //************************************************************************************
    // MONITORS
    //************************************************************************************

    pub fn get_monitor_mut(&mut self) -> &mut Monitor {
        &mut self.monitor
    }

    pub fn get_monitor(&self) -> &Monitor {
        &self.monitor
    }

    //************************************************************************************
    // MODULES
    //************************************************************************************

    ///Generate a new id (without rng, use a hash chain)
    pub fn gen_next_id(&mut self) -> u64 {
        let new = next_id(self.last_inst_id);
        self.last_inst_id = new;
        new
    }

    pub fn get_toml_path_by_name(&self, name: &str) -> Result<PathBuf, Error> {
        match self.toml_manager.get_clone_by_name(name) {
            Some(p) => Ok(p),
            None => Err(format_err!("Module {} not found", name))
        }
    }

    ///Remove all descendant node from the specified branch
    pub fn remove_config_at(&mut self, at: &StoreKey) -> Result<StoreNode, Error> {
        self.static_config.remove(at)
    }


    ///The TOML manager maps module name to corresponding TOML Files
    pub fn get_toml_manager(&self) -> &TOMLManager {
        &self.toml_manager
    }

    pub fn verify_toml_manager(&mut self) -> usize {
        self.toml_manager.verify_all()
    }

    ///Get the list of config names supported by the application
    fn get_supported_configs(&self) -> Result<TOMLManager, Error> {
        //find benchfiles root path
        let key = StoreKey::from(CONFIG).push(BENCHFILES).push(PATH);
        let benchfiles_str = match self.static_config.get_value(&key) {
            Ok(Data::Singleton(AtomicData::String(s))) => s,
            _ => { return Err(format_err!("Folder path {} has not been found.", key)); }//return early
        };

        let mut root_path = self.root_path.clone();
        root_path.push(benchfiles_str);

        acc_toml_name_recursive(root_path)
    }

    //************************************************************************************
    // COMMUNICATIONS
    //************************************************************************************

    pub fn get_communication_manager(&mut self) -> &mut CommManager {
        &mut self.comm_manager
    }

    //************************************************************************************
    // GLOBAL LOAD/SAVE
    //************************************************************************************

    ///Save static config to file
    pub fn save_config_to(&self, path: &Path) -> Result<(), Error> {
        storenode_to_toml_file(path, &self.static_config)
    }

    ///Load static config from file (merged with current static corfig, i.e. app config at the minimum)
    pub fn load_config_from(&mut self, path: &Path) -> Result<(), Error> {
        match toml_file_to_storenode(path) {
            Ok(s) => {
                self.static_config.merge(&s);
                Ok(())
            },
            Err(e) => Err(e)
        }
    }
}


///Recursively search for toml file in specified folder and add "name" property value to manager
fn acc_toml_name_recursive(folder_path: PathBuf) -> Result<TOMLManager, Error> {
    let mut toml_mgr = TOMLManager::new(folder_path.as_path());
    if folder_path.exists() {
        for files in try!( folder_path.read_dir()) {//read files
            let file = try!(files);
            let path = file.path();
            if path.is_file() && path.extension() == Some(&OsStr::new("toml"))  {//if it is a file and extension is "toml"
                //now we can load file
                try!(toml_file_to_storenode(path.as_path()));//return error if file cannot be parsed

                path.file_stem()//select file name without extension
                    .and_then(|name|name.to_str())//convert to str
                    .map(|s|toml_mgr.insert(s, path.clone()));//use the str to add to mapping
            }
            else if path.is_dir() {//recursive call if it is dir
                let mut v = try!(acc_toml_name_recursive(path));
                toml_mgr.merge(& mut v);
            }
        }

        Ok(toml_mgr)
    }
    else {
        match folder_path.to_str() {
            Some(p) => Err(format_err!("Folder {} does not exist.", p)),
            None => Err(format_err!("Folder does not exist."))
        }

    }

}

#[test]
fn test_core_init() {
    let mut core = Core::setup("").unwrap();

    assert!(core.verify_toml_manager() == 0);
    assert!(core.get_toml_manager().get_categories().len() > 0, "No categories!");

    // for cat in core.get_toml_manager().get_categories() {
    //     println!("Category: {}", cat);
    // }
}
