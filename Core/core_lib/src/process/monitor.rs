// @Author: ronan
// @Date:   16-11-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

use std::sync::mpsc::{Receiver, Sender, channel};
use std::thread;

use process::monitor_message::{MonitorMessage, MonitorSignal};

// use sp_lib::interfaces::process::ActorState;
// use sp_lib::helpers::NameMapper;

pub struct Monitor {
    mon_rxs: Vec<Receiver<MonitorMessage>>,//channel rx for all actors
}

impl Monitor {
    pub fn new() -> Monitor {
        Monitor { mon_rxs: Vec::new()}
    }

    pub fn clear(&mut self) {
        self.mon_rxs.clear();
    }

    pub fn get_nb_monitored(&self) -> usize {
        self.mon_rxs.len()
    }

    pub fn add_monitored(&mut self) -> Sender<MonitorMessage> {
        let (tx, rx) = channel();
        self.mon_rxs.push(rx);
        tx
    }

    ///lift the monitor in its own thread: it will aggregate the messages from the actor and send the result in the output channel
    pub fn lift_to_aggregate_channel(&mut self) -> (Sender<MonitorMessage>, Receiver<MonitorMessage>) {
        let (txi, rxo) = channel();
        let (txo, rxi) = channel();//this thread pilot
        //detach receivers from monitor
        let mut removed: Vec<Receiver<MonitorMessage>> = Vec::new();
        let mon_rxs_len = self.mon_rxs.len();
        for _ in 0..mon_rxs_len {
            match self.mon_rxs.pop() {
                Some(mrx) => removed.push(mrx),
                None => {}
            }
        }

        thread::spawn(move|| {
            let txi = txi;
            let rxs = removed;
            let mut states: Vec<MonitorSignal> = Vec::new();
            // let mut state_saves: NameMapper<ActorState> = NameMapper::new();//save the states of actors
            let mut aggregate_state = MonitorSignal::Idle;
            for _ in 0..rxs.len() {
                states.push(MonitorSignal::Idle);
            }
            let mut kill = false;
            loop { //TODO redo from scratch with something coherent
                if kill == true {
                    break;//kill thread
                }

                //do I need to pilot this thread?
                match rxi.try_recv() {
                    Ok(MonitorMessage::Kill) => {
                        break;//kill thread
                    },
                    _ => {}
                }

                //deal with messages from actors
                for (i, mrx) in rxs.iter().enumerate() {
                    match mrx.try_recv() {
                        Ok(m) => {
                            states[i] = m.to_signal();
                            // debug!("Message {:?} received by monitor for actor {}", m, i);
                            match m {
                                MonitorMessage::Error(e) => {
                                    if aggregate_state != MonitorSignal::Error {
                                        aggregate_state = MonitorSignal::Error;
                                        let _ = txi.send(MonitorMessage::Error(e));
                                    }
                                },
                                MonitorMessage::Closing => {
                                    if MonitorSignal::unanime_state(&states) == Some(MonitorSignal::Closing) {
                                        aggregate_state = MonitorSignal::Closing;
                                        let _ = txi.send(MonitorMessage::FinalState);
                                        kill = true;
                                        break;//kill thread
                                    }
                                },
                                x => {  //for other state, update only on unanimity
                                        if aggregate_state != x.to_signal() && MonitorSignal::unanime_state(&states) == Some(x.to_signal()) {
                                            aggregate_state = x.to_signal();
                                            let _ = txi.send(x);
                                        }

                                }
                            }
                        },
                        Err(_) => {}
                    }
                }
            }
            debug!("Monitor killed.");
        });
        (txo, rxo)
    }
}
