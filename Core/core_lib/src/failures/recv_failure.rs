// @Author: ronan
// @Date:   14-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 16-11-2016

use process::controller_message::ControllerMessage;

use std::fmt;
use failure::Error;

#[derive(Debug)]
pub enum RecvFailure {
    ControllerMessage(ControllerMessage),
    Any(Error)
}


impl fmt::Display for RecvFailure {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &RecvFailure::ControllerMessage(ref m) => write!(f, "Failed to receive datafeed: {:?}", m),
            &RecvFailure::Any(ref s) => write!(f, "Failed to receive datafeed: {}", s),
        }

    }
}



impl From<Error> for RecvFailure {
    fn from(err: Error) -> RecvFailure {
        RecvFailure::Any(err)
    }
}

// impl From<RxTimeout> for RecvFailure {
//     fn from(err: RxTimeout) -> RecvFailure {
//         RecvFailure::RxTimeout(err)
//     }
// }

// impl From<ThreadKilled> for RecvFailure {
//     fn from(err: ThreadKilled) -> RecvFailure {
//         RecvFailure::ThreadKilled(err)
//     }
// }

// impl From<ControllerMessage> for RecvFailure {
//     fn from(mess: ControllerMessage) -> RecvFailure {
//         RecvFailure::ControllerMessage(mess)
//     }
// }
