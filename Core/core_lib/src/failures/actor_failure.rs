// @Author: ronan
// @Date:   14-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 15-11-2016

use std::fmt;
use failure::Error;

#[derive(Debug)]
pub enum ActorFailure {
    Step(Error),
    Propagated(Error),
    Any(Error)
}



impl fmt::Display for ActorFailure {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &ActorFailure::Step(ref s) => write!(f, "Actor step failure: {}", s),
            &ActorFailure::Propagated(ref s) => write!(f, "Actor propagated failure: {}", s),
            &ActorFailure::Any(ref s) => write!(f, "Actor failure: {}", s),
        }

    }
}

impl ActorFailure {
    pub fn new_step(e: Error) -> ActorFailure {
        ActorFailure::Step(e)
    }

    pub fn new_propagated(e: Error) -> ActorFailure {
        ActorFailure::Propagated(e)
    }

    pub fn to_error(self) -> Error {
        match self {
            ActorFailure::Step(e) => e,
            ActorFailure::Propagated(e) => e,
            ActorFailure::Any(e) => e,
        }
    }
}

impl From<Error> for ActorFailure {
    fn from(err: Error) -> ActorFailure {
        ActorFailure::Any(err)
    }
}
