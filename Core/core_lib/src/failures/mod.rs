/*
 * Author:   ronan.lashermes@inria.fr
 * Creation: Tue Nov 28 2017
 *
 * Copyright (c) 2017 INRIA
 */


pub mod actor_failure;
pub mod process_failure;
pub mod recv_failure;

pub use self::actor_failure::ActorFailure;
pub use self::process_failure::ProcessFailure;
pub use self::recv_failure::RecvFailure;