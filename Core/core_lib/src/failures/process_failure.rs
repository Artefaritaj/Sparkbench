// @Author: Lashermes Ronan <ronan>
// @Date:   22-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 22-02-2017
// @License: MIT


use std::error;
use std::fmt;

use experiments::experiment_state::ExperimentState;
use failure::Error;

#[derive(Debug)]
pub struct ProcessFailure {
    error: Error,
    state: ExperimentState
}

impl ProcessFailure {
    pub fn new(e: Error, state: ExperimentState) -> ProcessFailure {
        ProcessFailure { error: e, state: state}
    }

    pub fn new_opt(e: Error, state: Option<ExperimentState>) -> ProcessFailure {
        match state {
            Some(s) => ProcessFailure { error: e, state: s},
            None => ProcessFailure { error: e, state: ExperimentState::empty()}
        }

    }

    pub fn error_only(e: Error) -> ProcessFailure {
        ProcessFailure { error: e, state: ExperimentState::empty()}
    }

    pub fn dismantle(self) -> (Error, ExperimentState) {
        (self.error, self.state)
    }

    pub fn to_state(self) -> ExperimentState {
        self.state
    }

    pub fn to_error(self) -> Error {
        self.error
    }
}

impl error::Error for ProcessFailure {
    fn description(&self) -> &str {
        "Process failure"
    }
}

impl fmt::Display for ProcessFailure {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Process failure: {}", self.error)
    }
}

impl From<Error> for ProcessFailure {
    fn from(err: Error) -> ProcessFailure {
        ProcessFailure::error_only(err)
    }
}
