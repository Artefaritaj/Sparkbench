// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 15-12-2016
// @License: MIT



// #![feature(mpsc_select)]

extern crate byteorder;
extern crate hconfig;
extern crate rustc_serialize;
extern crate rand;
extern crate sp_lib;
extern crate comms;
extern crate module_lib;
extern crate config_keys;
extern crate chrono;
extern crate dimval;

#[macro_use] extern crate failure;

#[macro_use] extern crate serde_derive;
extern crate serde;
extern crate serde_json;

#[macro_use]
extern crate log;

pub mod process;
pub mod errors;
pub mod experiments;
pub mod helpers;
pub mod failures;

pub use self::process::core::Core;
pub use self::experiments::experiment::Experiment;
pub use self::experiments::channel_mode::ChannelMode;
