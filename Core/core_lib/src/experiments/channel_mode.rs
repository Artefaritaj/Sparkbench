// @Author: ronan
// @Date:   15-12-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 20-12-2016

use config_keys::*;

use hconfig::store::{StoreNode, StoreKey, Data, OverwriteRule};
use hconfig::io::toml_file::*;

use process::Core;
use comms::{Link, CommManager};

use failure::Error;

use std::collections::HashMap;

#[derive(Debug)]
///The channel mode is used to open communication channels directly
pub struct ChannelMode {
    conn_configs: HashMap<String, StoreNode>, //(channel name, connection config)
    timeout: Option<i64>,
    ow_rules: Vec<OverwriteRule>
}

impl ChannelMode {
    pub fn new() -> ChannelMode {
        ChannelMode { conn_configs: HashMap::new(), timeout: None, ow_rules: Vec::new() }
    }

    ///Load a toml file in static config from name (file in BenchFiles) at a prefix
    pub fn load_from_toml(&mut self, core: &Core, name: &str) -> Result<(), Error> {
        let path = try!(core.get_toml_path_by_name(name));

        let config = try!(toml_file_to_storenode(&path));
        let connections_key = StoreKey::from(CONNECTIONS);
        match config.get_node(&connections_key) {
            Ok(connections_node) => {
                //Connection node should contains all channels
                for c in connections_node.get_node_children() {//c is channel
                    let chan_name = c.get_node_key();//extract channel name
                    let conn_config_key = StoreKey::from(CONNECTION_CONFIG);
                    match c.get_node(&conn_config_key) {
                        Ok(conn_config) => {
                            self.conn_configs.insert(format!("{}.{}", name, chan_name), conn_config.clone());
                        },
                        Err(_) => {}//do nothing
                    }
                }
            },
            Err(_) => {}//do nothing
        }

        Ok(())
    }



    ///List all available channels
    pub fn list_available_channels(&self) -> Vec<String> {
        let mut result: Vec<String> = Vec::new();
        for (k,_) in self.conn_configs.iter() {
            result.push(k.clone());
        }
        result
    }

    pub fn set_overwrite_rules(&mut self, ow_rules: &Vec<OverwriteRule>) {
        self.ow_rules = ow_rules.clone();
    }

    pub fn set_timeout(&mut self, to_ms: Option<i64>) {
        self.timeout = to_ms;
    }

    ///Get the config matching the given partial name
    pub fn get_matching_config(&self, partial_channel_name: &str) -> Result<StoreNode, Error> {
        let mut matching: Vec<String> = Vec::new();
        for k in self.list_available_channels() {
            if k.contains(partial_channel_name) {
                matching.push(k.clone());
            }
        }

        if matching.len() == 1 {
            match matching.pop() {
                Some(k) => {
                    Ok( try!(self.conn_configs.get(&k).ok_or(format_err!("{} is not a valid channel.", partial_channel_name))) .clone())
                },
                None => {
                    Err(format_err!("Failure"))
                }
            }
        }
        else {
            Err(format_err!("{} maches {} channel names", partial_channel_name, matching.len()))
        }
    }

    pub fn connect_to(&mut self, channel_name: &str, comm_mgr: &mut CommManager) -> Result<Link, Error> {
        // let mut conn_config = try!(self.conn_configs.get(channel_name).ok_or(format!("{} is not a valid channel.", channel_name))) .clone();
        let mut conn_config = try!(self.get_matching_config(channel_name));

        match self.timeout {
            Some(to) => {
                let timeout_key = StoreKey::from(TIMEOUT);
                conn_config.store_update(timeout_key, Data::create_i64(to));//set timeout
            },
            _ => {}
        }


        //apply overwrite rules
        try!(OverwriteRule::apply_to_store_node(&self.ow_rules, &mut conn_config));

        //start connection
        comm_mgr.connect_to(&conn_config)
    }
}

#[test]
fn test_channel_mode() {
    let core = Core::setup("").unwrap();
    let mut channels = ChannelMode::new();

    channels.load_from_toml(&core, "RPi2").unwrap();
    let list_ch = channels.list_available_channels();
    println!("Channels: {:?}", list_ch);
}
