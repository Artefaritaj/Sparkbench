// @Author: ronan
// @Date:   17-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 15-12-2016

use chrono::prelude::*;
use chrono::Duration;

use failure::Error;

use config_keys::*;

use hconfig::store::{StoreNode, StoreKey,OverwriteRule, Data, AtomicData, DataType, CompoundDataType};
use hconfig::io::toml_file::*;

use std::{thread, time};
use std::path::Path;
use std::collections::HashSet;
use std::sync::{Arc, Mutex};
use std::sync::mpsc::*;
use std::collections::HashMap;

use sp_lib::helpers::timeout::*;
use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::{Module, VariableDirection, VariableDescription};
use sp_lib::interfaces::expressions::parser::parse_static_data;
use sp_lib::interfaces::ModCmdActor;
use sp_lib::interfaces::process::{DataFeed, Actor};

use module_lib::helpers::creator::create_instanced_module;

use process::{Core, FeedbackNotification};
use process::actors::threaded_actor::ThreadedActor;
use process::actors::actor_message::ActorMessage;
use process::actors::validation::validator::Validator;
use process::monitor_message::MonitorMessage;
use process::actors::validation::validation_step::ValidationStep;
use process::actors::validation::validation_message::ValidationMessage;
use experiments::module_command_pair_description::ModuleCommandPairDescription;
use experiments::link_description::LinkDescription;
use experiments::feedx_description::FeedXDescription;
use experiments::module_creation_description::ModuleCreationDescription;
use experiments::exp_process_desc::ExpProcDesc;
use experiments::process_decision::ProcessDecisison;
use experiments::experiment_state::ExperimentState;

use failures::process_failure::ProcessFailure;
// use failures::ProcessFailure;

#[derive(Debug)]
///An experiment should contain all data to perform one experiment (including reset procedure, DUT process, ...)
pub struct Experiment {
    ///Config
    static_config: StoreNode,
    ///Modules descriptions (corresponds to section [module])
    module_descriptions: NameMapper<ModuleCreationDescription>,
    ///Commands (corresponds to section [cmd])
    actors_mapping: NameMapper<ModuleCommandPairDescription>,
    ///Description of all processes (corresponds to section [process])
    processes: NameMapper<ExpProcDesc>,
    //these are the rules to modify the config at the last moment (JIT)
    ow_rules: Vec<OverwriteRule>,
    ///Describe the decision graph between processes
    decision_graph: NameMapper<ProcessDecisison>,
    ///Constants are used to fix some variables
    constants: NameMapper<NameMapper<(Data, usize)>> //(actor, (var, (data, usize)))
}

impl Experiment {
    pub fn new(static_config: StoreNode) -> Experiment {
        Experiment {
            static_config: static_config,
            module_descriptions: NameMapper::new(),
            actors_mapping: NameMapper::new(),
            processes: NameMapper::new(),
            ow_rules: Vec::new(),
            decision_graph: NameMapper::new(),
            constants: NameMapper::new()}
    }

    //************************************************************************************
    // CONFIG
    //************************************************************************************

    ///Remove all descendant node from the specified branch
    pub fn remove_config_at(&mut self, at: &StoreKey) -> Result<StoreNode, Error> {
        self.static_config.remove(at)
    }

    ///Load a toml file in static config form extern file path at a prefix
    pub fn load_from_toml_extern(&mut self, path: &Path, at: &StoreKey) -> Result<(), Error> {
        let config = try!(toml_file_to_storenode(path));
        self.static_config.merge_prefix(&config, at);
        Ok(())
    }

    ///Load a toml file in static config from name (file in BenchFiles) at a prefix
    pub fn load_from_toml(&mut self, core: &Core, name: &str, at: &StoreKey) -> Result<(), Error> {
        let path = try!(core.get_toml_path_by_name(name));
        self.load_from_toml_extern(&path, at)
    }


    //************************************************************************************
    // EXPERIMENT LOADING
    //************************************************************************************

    pub fn load_experiment(name: &str, core: &Core) -> Result<Experiment, Error> {
        let path = try!(core.get_toml_path_by_name(name));
        Experiment::load_experiment_extern(&path)
    }

    pub fn load_experiment_extern(path: &Path) -> Result<Experiment, Error> {
        //create experiment
        let mut exp = Experiment::new(StoreNode::new_empty());

        //load experement from toml file
        try!(exp.load_from_toml_extern(path, &StoreKey::new()));

        //now we can start to parse
        try!(exp.parse_modules());
        try!(exp.parse_actors());
        try!(exp.parse_constants());
        try!(exp.parse_processes());
        try!(exp.parse_decision_graph());

        trace!("New experiment parsed: {:?}", exp);
        // println!("Decision graph parsed: {:?}", exp.decision_graph);

        Ok(exp)
    }

    //************************************************************************************
    // PARSING METHODS
    //************************************************************************************

    fn parse_decision_graph(&mut self) -> Result<(), Error> {
        let exp_graph_node = try!(self.static_config.get_node(&StoreKey::from(EXPERIMENT))).clone();

        // println!("Decision graph node: {:?}", exp_graph_node);

        for decision in exp_graph_node.get_node_children() {
            let proc_dec = try!(ProcessDecisison::parse(decision));
            let proc_name = proc_dec.get_name().to_string();
            self.decision_graph.insert(&proc_name, proc_dec);
        }

        Ok(())
    }

    ///Parse all modules from modules node
    fn parse_modules(&mut self) -> Result<(), Error> {
        let modules_configs = try!(self.static_config.get_node(&StoreKey::from(MODULES))).clone();

        for module in modules_configs.get_node_children() {
            try!(self.parse_instanced_module(&module));
        }

        Ok(())
    }

    ///Parse one module from module node
    fn parse_instanced_module(&mut self, module_node: &StoreNode) -> Result<(), Error> {
        let mod_name = module_node.get_node_key();
        let children_len = module_node.children_count();

        if children_len >= 1 {
            //extract kind of module (key TYPE)
            let mod_type_str: String = module_node.get_value(&StoreKey::from(TYPE))?.to_string_result()?;

            self.module_descriptions.insert(mod_name, ModuleCreationDescription::new(mod_type_str, module_node.clone()));

            Ok(())
        }
        else {
            Err(format_err!("The module {} has no definition", mod_name))
        }
    }

    fn parse_actors(&mut self) -> Result<(), Error> {
        let cmds_configs = try!(self.static_config.get_node(&StoreKey::from(ACTORS)));

        for cmd in cmds_configs.get_node_children() {
            let (actor_name, mcp) = try!(ModuleCommandPairDescription::parse_mcp(cmd));//for each process command, extract the pair (module, module command)
            self.actors_mapping.insert(&actor_name, mcp);
        }
        Ok(())
    }

    fn parse_constants(&mut self) -> Result<(), Error> {
        self.constants.clear();
        let constants_node = match self.static_config.get_node(&StoreKey::from(CONSTANTS)) {
            Ok(n) => n,
            Err(_) => {return Ok(()); }//no constant is not an error
        };


        for cmd_cst_node in constants_node.get_node_children() {//one child per cmd
            let cmd_name = cmd_cst_node.get_node_key();

            let mut cmd_constants: NameMapper<(Data, usize)> = NameMapper::new();
            for cst_node in cmd_cst_node.get_node_children() {//one child per constant
                let var_name = cst_node.get_node_key();

                //get cardinality for this constant if any (default to 1)
                let count = if cst_node.get_node_children().len() == 2 {
                    cst_node.get_value(&StoreKey::from("{1}"))?.to_atomic_data()?.to_u64()? as usize
                }
                else {
                    1usize
                };

                //get the constant data node
                let data_node = if cst_node.get_node_children().len() == 2 {
                    cst_node.get_node(&StoreKey::from("{0}"))?
                }
                else {
                    cst_node
                };

                //extract data from constant
                let var_data = match data_node.get_node_value() {
                    &Data::Singleton(AtomicData::String(ref s)) => {

                        try!(parse_static_data(s, &self.static_config))
                    },
                    d => {
                        d.clone()
                    }
                };

                //add this new constant in experiment
                cmd_constants.insert(var_name, (var_data, count));
            }
            self.constants.insert(cmd_name, cmd_constants);
        }

        Ok(())
    }

    ///parse all processes from exp toml file
    fn parse_processes(&mut self) -> Result<(), Error> {
        let processes_node = self.static_config.get_node(&StoreKey::from(PROCESSES))?;
        self.processes.clear();
        for process_node in processes_node.get_node_children() {
            let process = try!(Experiment::parse_process(process_node, &self.static_config));
            self.processes.insert(process_node.get_node_key(), process);
        }
        Ok(())
    }

    ///parse one process from exp toml file
    fn parse_process(node: &StoreNode, config: &StoreNode) -> Result<ExpProcDesc, Error> {
        ExpProcDesc::parse(node, config)
    }

    //************************************************************************************
    // EXPERIMENT BUILDING AND LAUNCHING
    //************************************************************************************

    fn autolinks(process: &ExpProcDesc,
                actors_mapping: &NameMapper<ModuleCommandPairDescription>,
                modules: &mut NameMapper<Arc<Mutex<dyn Module>>>,
                constants: &NameMapper<NameMapper<(Data, usize)>>)
            -> Result<Vec<LinkDescription>, Error> {
        let mut new_links: Vec<LinkDescription> = Vec::new();
        let mut producers: NameMapper<String> = NameMapper::new();//(var_name, actor name)
        let mut consumers: NameMapper<Vec<String>> = NameMapper::new();//(actor name, Vec<var_name>)

        //for each process
        if process.is_autolinks() {
            //for each actor
            for actor_name in process.get_process() {//for each actor...
                //get command module pair
                let mod_cmd_pair = try!(actors_mapping.get(&actor_name)
                                        .ok_or(format_err!("Actor {} not found.", actor_name)))
                                        .clone();

                //clone and get module (handle)
                let arc_mod = try!(modules.get_mut(mod_cmd_pair.get_module_name())
                                        .ok_or(format_err!("Module {} not found.", mod_cmd_pair.get_module_name())))
                                        .clone();
                //get handle
                let locked_mod = try!(arc_mod.lock().map_err(|e|format_err!("{}", e)));

                //get var descriptions for this actor
                let vars_desc = locked_mod.get_variable_descriptions(mod_cmd_pair.get_command_name());

                let mut forbidden_vars: Vec<String> = Vec::new();

                //send vars in producers or consumer
                for var_desc in vars_desc {
                    match var_desc.get_direction() {
                        //only one producer for a var, else => ERROR: need manual links
                        VariableDirection::Produced => {
                            // try!(producers.insert_if_not_here(var_desc.get_var_name(), actor_name.clone())
                            //                 .map_err(|e| format!("{} You need to manually define the links (produced).", e.to_string())));
                            if !forbidden_vars.contains(&var_desc.get_var_name().to_owned()) {
                                match producers.insert_if_not_here(var_desc.get_var_name(), actor_name.clone()) {
                                    Ok(_) => {},//continue
                                    Err(_) => {
                                        info!("Several producers for {}, you need to manually manage links handling variable {}", var_desc.get_var_name(), var_desc.get_var_name());
                                        //remove this var
                                        producers.remove(var_desc.get_var_name());
                                        //prevent from using it in the future
                                        forbidden_vars.push(var_desc.get_var_name().to_string());
                                    }
                                }
                            }
                        },


                        VariableDirection::Required => {

                            //if a constant is defined, the var is not required
                            let is_constant_present = constants.get(actor_name).and_then(|x| x.get(var_desc.get_var_name())).is_some();
                            if is_constant_present == false {
                                let mut prev = match consumers.remove(actor_name) {
                                    Some(v) => v,
                                    None => Vec::new()
                                };

                                prev.push(var_desc.get_var_name().to_string());//prev contains all consumed vars for this actor
                                consumers.insert(actor_name, prev);
                            }
                            else {
                                trace!("{}.{} constant present", actor_name, var_desc.get_var_name());
                            }

                            // try!(consumers.insert_if_not_here(actor_name, var_desc.get_var_name().to_string())
                            //                 .map_err(|e| format!("{} You need to manually define the links (required).", e.to_string())));
                        }
                    }
                }
            }

            //now we have producers and consumers
            for (consumer_actor, vec_var_name) in consumers.iter_mut() {
                for var_name in vec_var_name.iter() {
                    match producers.get(var_name) {
                        Some(producer_actor) => {
                            trace!("New automatic link: {}.{} -> {}.{}", producer_actor, var_name, consumer_actor, var_name);
                            new_links.push(LinkDescription::new(producer_actor, var_name, consumer_actor, var_name, 1));//only card 1 with autolink (for now?)
                        },
                        None => {}//here we cannot build the link
                    }
                }
            }

        }

        Ok(new_links)
    }

    fn merge_links(process_desc: &mut ExpProcDesc, new_links: Vec<LinkDescription>) {
        for new_link in new_links {
            if !Experiment::is_link_present(process_desc.get_links(), &new_link) {
                process_desc.add_link(new_link);
            }
        }
    }

    fn is_link_present(links: &Vec<LinkDescription>, new_link: &LinkDescription) -> bool {
        for link in links {//O(n): would be better with hashmap
            if link.is_similar(new_link) {
                return true;
            }
        }
        return false;
    }

    pub fn set_overwrite_rules(&mut self, ow_rules: &Vec<OverwriteRule>) {
        self.ow_rules = ow_rules.clone();
    }

    ///Init the DUT process for this experiment
    fn init_process(&mut self, process_name: &str, core: &mut Core, previous_state: Option<ExperimentState>)
        -> Result<NameMapper<Arc<Mutex<dyn Module>>>, ProcessFailure> {

        let previous_state_err = previous_state.clone();

        let mut process_desc = self.processes.get(process_name)
                                                .ok_or(ProcessFailure::new_opt(
                                                    format_err!("Unknown process {}", process_name),
                                                    previous_state_err.clone()
                                                ))?
                                                .clone();
        let rkind = process_desc.get_resume_kind();
        //create modules
        let mut mod_mapping = self.create_modules_in_process(core, process_desc.get_process())
                                        .map_err(|e|ProcessFailure::new_opt(e, previous_state_err.clone()))?;

        //autolinking: create links from var names
        let new_links = Experiment::autolinks(&process_desc, &self.actors_mapping, &mut mod_mapping, &self.constants)
                                        .map_err(|e|ProcessFailure::new_opt(e, previous_state_err.clone()))?;
        //merge new links if not already defined
        Experiment::merge_links(&mut process_desc, new_links);

        //load previous states
        Experiment::load_previous_state(&mut mod_mapping, previous_state)
                        .map_err(|e|ProcessFailure::new_opt(e, previous_state_err.clone()))?;

        //prepare (connections ...)
        Experiment::prepare(&mut mod_mapping, core)
                        .map_err(|e|ProcessFailure::new_opt(e, previous_state_err.clone()))?;

        //lift modules to threaded actors
        let mut th_actors = self.create_threaded_actors(&mut mod_mapping, process_desc.get_process(), rkind.is_force())
                                        .map_err(|e|ProcessFailure::new_opt(e, previous_state_err.clone()))?;

        //links actors properly
        Experiment::create_links(&mut th_actors, process_desc.get_links())//between actors
                        .map_err(|e|ProcessFailure::new_opt(e, previous_state_err.clone()))?;

        //add feedX
        Experiment::create_feedx(&mut th_actors, process_desc.get_feedx())//between actors
                        .map_err(|e|ProcessFailure::new_opt(e, previous_state_err.clone()))?;

        let (validator, sources) = Experiment::link_sources(&mut th_actors, process_desc.get_sources())//add sources
                                                    .map_err(|e|ProcessFailure::new_opt(e, previous_state_err.clone()))?;
        core.get_controller_mut().set_sources(sources);
        core.get_controller_mut().set_validator(validator);

        Experiment::lift_actors_to_threads(core, th_actors);

        Ok(mod_mapping)
    }

    fn wait_for_final_state(mon_rx: &Receiver<MonitorMessage>) -> Result<(), Error> {
        let end = Utc::now() + Duration::seconds(3);
        //treat all upcoming monitor messages
        loop {
            try!(check_timeout(end, format!("Wait for final state timeout")));
            match mon_rx.try_recv() {
                Ok(m) => match m {
                    MonitorMessage::FinalState => {
                        return Ok(())
                    },
                    _ => {}
                },
                Err(_) => {}//wait again
            }
        }
    }

    fn modules2exp_state(modules: &mut NameMapper<Arc<Mutex<dyn Module>>>, pulses: u64) -> Result<ExperimentState, Error> {
        let mut estate = ExperimentState::new(pulses);
        for (mod_name, module) in modules.iter_mut() {
            let mut mod_guard = try!(module.lock().map_err(|e|format_err!("{}", e)));
            let mstate = try!(mod_guard.save_state());
            estate.add_module_state(mod_name, mstate);
        }
        Ok(estate)
    }

    ///Prepare experiment for direct command mode
    pub fn prepare_direct_command_mode(&self, core: &mut Core) {
        //clear actors from previous experiments
        core.get_controller_mut().clear();
        core.get_monitor_mut().clear();
    }

    pub fn execute_direct_command(&mut self, core: &mut Core, actor_name: &str, args: &HashMap<String, String>) -> Result<DataFeed, Error> {
        //build module for command
        let cmd_mod_pair = try!(self.actors_mapping.get(actor_name).ok_or(format_err!("Command {} does not exists", actor_name))).clone();
        let mod_name = cmd_mod_pair.get_module_name();
        let mod_desc = try!(self.module_descriptions.get(&mod_name)
                            .ok_or(format_err!("Module {} not found.", mod_name)))
                            .clone();//... get description and ...
        let module = try!(self.create_exp_instanced_module(&mod_name, &mod_desc, core));//...build module from description

        let expected_var_descriptions_vec = {
            //prepare the module (open connections...)
            let mut mod_guard = try!(module.lock().map_err(|e|format_err!("{}", e)));
            try!(mod_guard.prepare(core.get_communication_manager()));
            mod_guard.get_variable_descriptions(cmd_mod_pair.get_command_name())
        };

        //create actor
        //create an actor from the module

        //extract variable definitions and put in hashmap for usability
        let mut expected_var_descriptions: HashMap<String, VariableDescription> = HashMap::new();
        for var in expected_var_descriptions_vec {
            expected_var_descriptions.insert(var.get_var_name().to_string(), var);
        }

        let mut mod_act = ModCmdActor::new(actor_name, module, cmd_mod_pair.get_command_name());

        //get arguments
        let datafeed_in = try!(Experiment::create_datafeed_from_cmd_args(args, &expected_var_descriptions));

        //start command
        mod_act.act(datafeed_in, cmd_mod_pair.get_attributes())
    }

    fn create_datafeed_from_cmd_args(args: &HashMap<String, String>, expected_var_descriptions: &HashMap<String, VariableDescription>) -> Result<DataFeed, Error> {
        let mut datafeed = DataFeed::new();
        for (arg_name, arg_content) in args.iter() {
            let splitted: Vec<_> = arg_name.split(VAR_SEP).collect();

            let (var_name, format) = match splitted.len() {
                1 => {
                    //first look if we can infer format from variable descriptions
                    if let Some(desc) = expected_var_descriptions.get(splitted[0]) {
                        if desc.get_direction() == VariableDirection::Required {
                            if let Some(dt) = desc.first_allowed_datatype() {
                                (splitted[0], dt.clone())
                            }
                            else {
                                (splitted[0], CompoundDataType::Singleton(DataType::String(None)))
                            }
                        }
                        else {
                            (splitted[0], CompoundDataType::Singleton(DataType::String(None)))
                        }
                    }
                    else {
                        (splitted[0], CompoundDataType::Singleton(DataType::String(None)))
                    }
                },
                2 => {
                    let dt = try!(CompoundDataType::parse(splitted[1]));
                    (splitted[0], dt)
                },
                _ => { return Err(format_err!("Incorrect variable definition format")); }
            };

            let data_as_str = Data::create_str(arg_content);
            let data = match format {
                CompoundDataType::Singleton(DataType::String(None)) => data_as_str,
                dt => {
                    try!(data_as_str.convert(&dt))
                }
            };

            datafeed.add_data(var_name.to_string(), data);
        }

        Ok(datafeed)
    }

    ///Execute a process, blocking
    pub fn launch_process(&mut self, process_name: &str, core: &mut Core, previous_state: Option<ExperimentState>)
                -> Result<Option<ExperimentState>, ProcessFailure> {
        {//clear data from previous experiments
            core.get_controller_mut().clear();
            core.get_monitor_mut().clear();
            core.get_communication_manager().close_all_connections();
        }
        let notifier = core.create_feedback_notifier();

        //this process description
        let process_desc = try!(self.processes.get(process_name)
                                                .ok_or(format_err!("Unknown process {}", process_name)))
                                                .clone();
        let count = process_desc.get_count();
        let start_pulse = match previous_state {
            Some(ref state) => state.get_pulse_index(),
            None => 0
        };

        info!("| Initializing process {}...", process_name);
        //create actors and prepare connections
        let mut modules = try!(self.init_process(process_name, core, previous_state));

        notifier.notify(&FeedbackNotification::ProcessStarted(process_name.to_string()))?;

        {
            //get monitor in its own theard
            //get a channel to get the monitor aggregate
            let (mon_tx, mon_rx) = core.get_monitor_mut().lift_to_aggregate_channel();
            //get the controller to sync
            let ctr = core.get_controller_mut();

            //start the actors
            //stop everything if fail
            try!(ctr.start_all().map_err(|e| {
                // let _ = mon_tx.send(MonitorMessage::Kill);
                let _ = ctr.kill_all();
                e
            }));

            //loop on executions
            let mut validated = start_pulse;
            for i in start_pulse..count {
                notifier.notify(&FeedbackNotification::Tick(i, count))?;
                info!("| | Starting execution {} of process {}...", i, process_name);
                //perform one execution, stop everything if fail
                //the monitor can interrupt the pulse in case of error
                try!(ctr.pulse(&mon_rx).map_err(|e|{
                    // let _ = mon_tx.send(MonitorMessage::Kill);
                    let _ = ctr.kill_all();
                    let _ = notifier.notify(&FeedbackNotification::ProcessCrashed(process_name.to_string()));
                    match Experiment::wait_for_final_state(&mon_rx) {
                        Ok(()) => {//ExperimentState built
                            match Experiment::modules2exp_state(&mut modules, validated) {
                                Ok(state) => ProcessFailure::new(e, state),
                                Err(_) => ProcessFailure::new(e, ExperimentState::new(validated))
                            }
                        },
                        Err(_) => {//No ExperimentState
                            ProcessFailure::new(e, ExperimentState::new(validated))
                        }
                    }
                }));

                //we have a new pulse to validate
                ctr.get_validator_mut().add_to_validate(ValidationStep::from_pulse(1));

                //look if any validation has arrived
                let validations = try!(ctr.get_validator_mut().try_validate().map_err(|e|{
                    // let _ = mon_tx.send(MonitorMessage::Kill);
                    let _ = ctr.kill_all();
                    let _ = notifier.notify(&FeedbackNotification::ProcessCrashed(process_name.to_string()));
                    match Experiment::wait_for_final_state(&mon_rx) {
                        Ok(()) => {//ExperimentState built
                            match Experiment::modules2exp_state(&mut modules, validated) {
                                Ok(state) => ProcessFailure::new(e, state),
                                Err(_) => ProcessFailure::new(e, ExperimentState::new(validated))
                            }
                        },
                        Err(_) => {//No ExperimentState
                            ProcessFailure::new(e, ExperimentState::new(validated))
                        }
                    }
                }));
                validated += validations as u64;//add to counter if any
            }

            //wait for all remaining validation
            //the monitor can interrupt the waiting if an error is detected
            try!(ctr.get_validator_mut().wait_for_total_validation(None, Some(&mon_rx)).map_err(|e|{
                info!("Error in final validation, the Monitor interrupted the final validation wait.");
                // let _ = mon_tx.send(MonitorMessage::Kill);
                let _ = ctr.kill_all();
                let _ = notifier.notify(&FeedbackNotification::ProcessCrashed(process_name.to_string()));
                match Experiment::wait_for_final_state(&mon_rx) {
                    Ok(()) => {//ExperimentState built
                        match Experiment::modules2exp_state(&mut modules, validated) {
                            Ok(state) => ProcessFailure::new(e, state),
                            Err(_) => ProcessFailure::new(e, ExperimentState::new(validated))
                        }
                    },
                    //No ExperimentState
                    Err(_) => ProcessFailure::new(e, ExperimentState::new(validated))
                }
            }));

            notifier.notify(&FeedbackNotification::ProcessEnded(process_name.to_string()))?;
            info!("| End of process {}", process_name);
            //Send the "end" signal at the end of the experiment
            try!(ctr.end(&mon_rx).map_err(|e|{
                // let _ = mon_tx.send(MonitorMessage::Kill);
                let _ = ctr.kill_all();
                e
            }));

            //manage all upcoming monitor messages up to the end state
            loop {
                match mon_rx.try_recv() {
                    Ok(m) => {
                        trace!("Message {:?} received at the end", m);
                        match m {
                            MonitorMessage::Error(e) => {//abort!
                                // let _ = mon_tx.send(MonitorMessage::Kill);
                                let _ = ctr.kill_all();
                                return Err(format_err!("Error at the end of the process {}: {}", process_name, e.to_string()).into());
                            },
                            MonitorMessage::End => {//valid end here
                                debug!("Process {} properly terminated", process_name);
                                break;//exit the loop on proper end
                            },
                            MonitorMessage::Undefined => {
                                // let _ = mon_tx.send(MonitorMessage::Kill);
                                let _ = ctr.kill_all();
                                return Err(format_err!("Process {} is not properly ending", process_name).into());
                            },
                            _ => {}
                        }
                    },
                    Err(_) => {}
                }
            }

            let final_state = Experiment::modules2exp_state(&mut modules, validated).ok();

            //Send the "kill" signal to end the threads. It will trigger the kill of the monitor thread
            ctr.kill_all()
                    .map_err(|e|
                        {
                            let _ = mon_tx.send(MonitorMessage::Kill);
                            e
                        })?;
            debug!("Actors killed");
            let closing_delay = time::Duration::from_millis(200);
            thread::sleep(closing_delay);

            Ok(final_state)
        }
    }

    /// Launch the experiment
    pub fn launch(&mut self, core: &mut Core) -> Result<(), Error> {

        info!("Starting experiment...");
        let mut process_states: HashMap<String, ExperimentState> = HashMap::new();

        let mut next_proc = {
            let entry_proc_decision = try!(self.decision_graph.get(ENTRY).ok_or(format_err!("No entry process for the experiment")));
            entry_proc_decision.get_success_process().to_string()
        };
        let notifier = core.create_feedback_notifier();
        notifier.notify(&FeedbackNotification::ExperimentStarted)?;

        loop {
            let prev_state = process_states.remove(&next_proc);

            //execute next process
            let success_res = self.launch_process(&next_proc, core, prev_state);

            //extract result
            //(Option<ProcessState>, Result<(), Error>)
            let (proc_state_opt, proc_err) = match success_res {
                Ok(estate_opt) => (estate_opt, Ok(())),
                Err(e) => {
                    let (error, estate) = e.dismantle();
                    (Some(estate), Err(error))
                }
            };

            //choose whate state to save according to resume_kind
            //this process description
            let process_desc = try!(self.processes.get(&next_proc)
                                                .ok_or(format_err!("Unknown process {}", next_proc)))
                                                .clone();
            let rkind = process_desc.get_resume_kind();

            //manage this process state if any
            //delete saved state if any
            process_states.remove(&next_proc);
            if rkind.is_state_saved() {
                if let Some(estate) = proc_state_opt {
                    process_states.insert(next_proc.clone(), estate);
                }
            }

            //choose next process
            let (next_proc_shad, last_proc_result) = {
                let next_decision = self.decision_graph.get(&next_proc).clone();
                match next_decision {
                    None => {//end of experiment
                        notifier.notify(&FeedbackNotification::ExperimentEnded)?;
                        info!("End of experiment.");
                        // return success_res.and_then(|_|Ok(())).map_err(|pf|pf.to_error());
                        return proc_err;
                    },
                    Some(d) => {
                        match proc_err {
                            Ok(()) => {//Process was a success
                                (d.get_success_process().to_string(), Ok(()) )//return (next process to execute, process result)
                            },
                            Err(error) => {//Process failed
                                warn!("Process **{}** failed with error {}", &next_proc, error);
                                (d.get_failure_process().to_string(), Err(error) )//return (next process to execute, process result)
                            }
                        }
                    }
                }
            };

            next_proc = next_proc_shad;

            if next_proc.is_empty() {
                return last_proc_result;
            }
        }
    }

    ///load a previously saved state if any
    fn load_previous_state(modules: &mut NameMapper<Arc<Mutex<dyn Module>>>, prev_state: Option<ExperimentState>) -> Result<(), Error> {
        match prev_state {
            None => Ok(()),
            Some(es) => {
                for (mod_name, module) in modules.iter_mut() {
                    let mstate = es.get_module_state(mod_name);
                    debug!("Load state {:?} for module {}.", mstate, mod_name);
                    let mut mod_guard = try!(module.lock().map_err(|e|format_err!("Cannot lock module {}: {}", mod_name, e)));
                    try!(mod_guard.load_state(mstate).map_err(|e|format_err!("Cannot load previous module {} state: {}", mod_name, e)));
                }
                Ok(())
            }
        }


    }

    ///Prepare the module (ex: create connection, ...)
    fn prepare(modules: &mut NameMapper<Arc<Mutex<dyn Module>>>, core: &mut Core) -> Result<(), Error> {
        for (_, module) in modules.iter_mut() {
            let mut mod_guard = try!(module.lock().map_err(|e|format_err!("{}", e)));
            try!(mod_guard.prepare(core.get_communication_manager()));
        }

        Ok(())
    }

    fn link_sources(actors: &mut NameMapper<ThreadedActor>, sources: &Vec<String>)
    -> Result<(Validator, Vec<SyncSender<ActorMessage>>), Error> {
        let mut vec = Vec::new();
        let mut valid = Validator::new();

        for source in sources {//these are explicit sources specified in the config
            if let Some(mut dest_actor) = actors.remove(source) {
                let (tx,rx): (SyncSender<ActorMessage>,Receiver<ActorMessage>) = sync_channel(1);
                let (tx_valid,rx_valid): (Sender<ValidationMessage>, Receiver<ValidationMessage>) = channel();

                //sync link
                try!(dest_actor.write_to_link(SYNC, rx, tx_valid, 0));
                valid.add_validation_rx(SYNC, rx_valid);

                //store the tx
                vec.push(tx);

                //add the actors back
                actors.insert(source, dest_actor);
            }
            else {
                warn!("Actor not found: {}", source);
            }
        }

        for (actor_name, actor) in actors.iter_mut() {//check if some actors are isolated (without source)
            if actor.get_sources_count() == 0 {//this actor has no source, it cannot be executed -> it is sync with sources
                let (tx,rx): (SyncSender<ActorMessage>,Receiver<ActorMessage>) = sync_channel(1);
                let (tx_valid,rx_valid): (Sender<ValidationMessage>, Receiver<ValidationMessage>) = channel();

                try!(actor.write_to_link(SYNC, rx, tx_valid, 0));
                valid.add_validation_rx(SYNC, rx_valid);

                //store the tx
                vec.push(tx);
                debug!("Actor {} has been made a source.", actor_name);
            }
        }

        Ok((valid, vec))
    }

    ///create modules from their descriptions and static_config
    ///process -> actors_mapping -> modules
    fn create_modules_in_process(&mut self, core: &mut Core, process: &Vec<String>)
    -> Result<NameMapper<Arc<Mutex<dyn Module>>>, Error> {
        debug!("Creating modules...");
        //extract module names to build: process -> actors_mapping -> modules
        let mut mod_names : HashSet<String> = HashSet::new();
        for cmd_name in process {
            let mod_cmd_pair = try!(self.actors_mapping.get(&cmd_name)
                                    .ok_or(format_err!("Command {} not found.", cmd_name)))
                                    .clone();
            mod_names.insert(mod_cmd_pair.get_module_name().to_string());
        }

        //build modules
        let mut mod_mapping: NameMapper<Arc<Mutex<dyn Module>>> = NameMapper::new();
        for mod_name in mod_names {//for each module name...
            let mod_desc = try!(self.module_descriptions.get(&mod_name)
                                .ok_or(format_err!("Module {} not found.", mod_name)))
                                .clone();//... get description and ...
            let module = try!(self.create_exp_instanced_module(&mod_name, &mod_desc, core));//...build module from description
            mod_mapping.insert(&mod_name, module);
        }
        debug!("{} modules built.", mod_mapping.len());
        Ok(mod_mapping)
    }

    ///Create threaded actors from modules and process description
    fn create_threaded_actors(&self, modules: &mut NameMapper<Arc<Mutex<dyn Module>>>, process: &Vec<String>, force: bool)
    -> Result<NameMapper<ThreadedActor>, Error> {
        let mut tactors : NameMapper<ThreadedActor> = NameMapper::new();
        for actor_name in process {//for each actor...
            //get command module pair
            let mod_cmd_pair = try!(self.actors_mapping.get(&actor_name)
                                    .ok_or(format_err!("Actor {} not found.", actor_name)))
                                    .clone();

            //clone and get module (handle)
            let arc_mod = try!(modules.get_mut(mod_cmd_pair.get_module_name())
                                    .ok_or(format_err!("Module {} not found.", mod_cmd_pair.get_module_name())))
                                    .clone();

            //create an actor from the module
            let mod_act = ModCmdActor::new(&actor_name, arc_mod, mod_cmd_pair.get_command_name());

            //get specified constants for this actor
            let constants = match self.constants.get_clone_by_name(actor_name) {
                Some(n) => n.to_hashmap(),
                None => HashMap::new()
            };

            //create threaded actor from actor
            let th_act = ThreadedActor::new(&actor_name, mod_act, constants, mod_cmd_pair.get_attributes().clone(), force);

            //add threaded actor to mapping for later use
            tactors.insert(actor_name, th_act);
        }
        Ok(tactors)
    }

    ///Create a module from a description and a config where to look for info
    fn create_exp_instanced_module(&mut self, mod_name: &str, mod_desc: &ModuleCreationDescription, core: &Core)
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        let at = StoreKey::from(INSTANCED).push(mod_name); //"instanced.ID"
        let module = try!(create_instanced_module(&mut self.static_config, &at, mod_desc.get_module_type(), mod_desc.get_args(), &self.ow_rules, core.get_toml_manager()));//create the module (Box<Module>)
        Ok(module)
    }

    fn create_feedx(actors: &mut NameMapper<ThreadedActor>, feedbacks: &Vec<FeedXDescription>) -> Result<(), Error> {
        for f_desc in feedbacks {
            let source = f_desc.get_source();
            let source_type = f_desc.get_source_type();
            let destination = f_desc.get_destination();
            let dest_type = f_desc.get_destination_type();
            let cardinality = f_desc.get_cardinality();

            //get actors
            if let Some(mut src_actor) = actors.remove(source) {
                if let Some(mut dest_actor) = actors.remove(destination) {
                    //add link if cardinality >= 1
                    if cardinality > 0 {
                        try!(src_actor.feedx_link(&mut dest_actor, source_type, dest_type, cardinality));
                    }
                    else { //if cardinality is 0 -> invalid
                        warn!("Feedback from {} to {} with cardinality 0", source, destination);
                    }

                    //add the actors back
                    actors.insert(destination, dest_actor);
                }
                else {
                    warn!("Actor not found for feedback: {}", destination);
                }
                //add the actors back
                actors.insert(source, src_actor);
            }
            else {
                warn!("Actor not found for feedback: {}", source);
            }
        }

        Ok(())
    }

    ///Link threaded actors together, a link may refer to non existent actors -> must not be a problem
    fn create_links(actors: &mut NameMapper<ThreadedActor>, links: &Vec<LinkDescription>) -> Result<(), Error>  {
        for link in links {
            //unpack link
            let source = link.get_source();
            let destination = link.get_destination();
            let var_source = link.get_source_var();
            let var_dest = link.get_dest_var();
            let cardinality = link.get_cardinality();

            //get actors
            if let Some(mut src_actor) = actors.remove(source) {
                if let Some(mut dest_actor) = actors.remove(destination) {
                    //add link if cardinality >= 1
                    if cardinality > 0 {
                        try!(src_actor.link(&mut dest_actor, var_source, var_dest, cardinality));
                    }
                    else { //if cardinality is 0 -> sync link
                        try!(src_actor.sync_link(&mut dest_actor));
                    }

                    //add the actors back
                    actors.insert(destination, dest_actor);
                }
                else if source == destination {
                    //add link if cardinality >= 1
                    if cardinality > 0 {
                        try!(src_actor.self_link(var_source, var_dest, cardinality));
                    }
                    else { //if cardinality is 0 -> sync link
                        try!(src_actor.self_sync_link());
                    }
                }
                else {
                    warn!("Actor not found (destination for {}.{}): {}.{}", source, var_source, destination, var_dest);
                }
                //add the actors back
                actors.insert(source, src_actor);
            }
            else {
                warn!("Actor not found (source for {}.{}): {}.{}", destination, var_dest, source, var_source);
            }
        }
        Ok(())
    }

    fn lift_actors_to_threads(core: &mut Core, mut actors: NameMapper<ThreadedActor>) {
        let mut keys : Vec<String> = Vec::new();
        for key in actors.get_keys() {
            keys.push(key.to_string());
        }

        for cmd_name in keys.iter() {
            if let Some(ta) = actors.remove(cmd_name) {
                ThreadedActor::launch_thread(ta, core);
            }

        }
    }
}

#[test]
fn test_experiment() {//requires a rpi2 plugged with UART
    extern crate env_logger;
    let _ = env_logger::init();

    let mut core = Core::setup("").unwrap();
    let mut exp = Experiment::load_experiment("stm32_run", &mut core).unwrap();

    exp.set_overwrite_rules(&vec![OverwriteRule::from_str("device_connect.port=\"/dev/ttyUSB0\"").unwrap()]);

    //need boards to test launch
    // exp.launch(&mut core).unwrap();
}
