// @Author: ronan
// @Date:   17-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 15-12-2016



pub mod experiment;
pub mod module_command_pair_description;
pub mod link_description;
pub mod module_creation_description;
pub mod exp_process_desc;
pub mod channel_mode;
pub mod resume_kind;
pub mod process_decision;
pub mod experiment_state;
pub mod feedx_description;

pub use self::experiment::Experiment;
