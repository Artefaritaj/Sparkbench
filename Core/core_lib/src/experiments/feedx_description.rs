// @Author: Lashermes Ronan <ronan>
// @Date:   29-03-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 29-03-2017
// @License: MIT

use hconfig::store::StoreNode;

use config_keys::*;

use sp_lib::interfaces::expressions::parser::{parse_static_i64, parse_static_string, parse_string_array};

use failure::Error;

#[derive(Debug,Clone)]
pub struct FeedXDescription {
    source: String,//source cmd
    source_type: String,
    dest: String,//destination cmd
    dest_type: String,
    cardinality: usize//how many ping to get a feedback event
}

impl FeedXDescription {
    pub fn new(source: &str, source_type: &str, dest: &str, dest_type: &str, card: i64) -> FeedXDescription {
        FeedXDescription { source: source.to_string(), source_type: source_type.to_string(), dest: dest.to_string(), dest_type: dest_type.to_string(), cardinality: card as usize}
    }

    pub fn get_source(&self) -> &str {
        &self.source
    }

    pub fn get_source_type(&self) -> &str {
        &self.source_type
    }

    pub fn get_destination(&self) -> &str {
        &self.dest
    }

    pub fn get_destination_type(&self) -> &str {
        &self.dest_type
    }

    pub fn get_cardinality(&self) -> usize {
        self.cardinality
    }

    pub fn parse_description(link_node: &StoreNode, config: &StoreNode) -> Result<Vec<FeedXDescription>, Error> {
        let desc = try!(parse_string_array(link_node));

        //card is optional (default is 1)
        let card = if desc.len() == 5 {
            try!(parse_static_i64(&desc[4], config))
        }
        else {
            1
        };

        //get source and dest (dest can be a vector)
        if desc.len() >= 4 {//source, source_type, dest, dest_type, card
            let splitted = desc[2].split(VEC_SEP);//list of destination
            let mut res: Vec<FeedXDescription> = Vec::new();
            for dest in splitted {
                let feedlink = FeedXDescription::new(
                                        &try!(parse_static_string(&desc[0], config)),
                                        &try!(parse_static_string(&desc[1], config)),
                                        &try!(parse_static_string(dest, config)),
                                        &try!(parse_static_string(&desc[3], config)),
                                        card);
                res.push(feedlink);
            }

            Ok(res)
        }
        else {
            Err(format_err!("The feedback description is not complete: [source, source_type, destination, dest_type, cardinality]"))
        }
    }
}
