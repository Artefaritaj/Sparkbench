// @Author: ronan
// @Date:   15-11-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

use config_keys::*;

use hconfig::store::{StoreNode, StoreKey};

use sp_lib::interfaces::expressions::parser::parse_string_array;

use experiments::link_description::LinkDescription;
use experiments::feedx_description::FeedXDescription;
use experiments::resume_kind::ResumeKind;

use failure::Error;

#[derive(Debug,Clone)]
///Describes one process in an experiment
pub struct ExpProcDesc {
    process: Vec<String>,//chain of commands
    sources: Vec<String>,
    links: Vec<LinkDescription>,
    feedx: Vec<FeedXDescription>,
    autolinks: bool,
    count: u64,//how many executions
    resume_kind: ResumeKind
}

impl ExpProcDesc {

    pub fn get_process(&self) -> &Vec<String> {
        &self.process
    }

    pub fn get_sources(&self) -> &Vec<String> {
        &self.sources
    }

    pub fn add_link(&mut self, new_link: LinkDescription) {
        self.links.push(new_link);
    }

    pub fn get_links(&self) -> &Vec<LinkDescription> {
        &self.links
    }

    pub fn get_links_mut(&mut self) -> &mut Vec<LinkDescription> {
        &mut self.links
    }

    pub fn get_feedx(&self) -> &Vec<FeedXDescription> {
        &self.feedx
    }

    pub fn get_feedx_mut(&mut self) -> &mut Vec<FeedXDescription> {
        &mut self.feedx
    }

    pub fn get_resume_kind(&self) -> ResumeKind {
        self.resume_kind
    }

    pub fn get_count(&self) -> u64 {
        self.count
    }

    pub fn is_autolinks(&self) -> bool {
        self.autolinks
    }

    pub fn new() -> ExpProcDesc {
        ExpProcDesc {process: Vec::new(), sources: Vec::new(), links: Vec::new(), feedx: Vec::new(), autolinks: true, count: 1, resume_kind: ResumeKind::StartOver }
    }

    pub fn parse(node: &StoreNode, config: &StoreNode) -> Result<ExpProcDesc, Error> {
        let process = try!(ExpProcDesc::parse_process(node));
        let sources = try!(ExpProcDesc::parse_sources(node, &process));
        let links = try!(ExpProcDesc::parse_links(node, config));
        let autolinks = ExpProcDesc::parse_autolinks(node);
        let count = try!(ExpProcDesc::parse_count(node));
        let feedx = try!(ExpProcDesc::parse_feedx(FEEDX, node, config));
        let resume_kind = try!(ExpProcDesc::parse_resume_kind(node));
        Ok(ExpProcDesc { process: process, sources: sources, links: links, feedx: feedx, autolinks: autolinks, count: count, resume_kind: resume_kind })
    }

    fn parse_autolinks(node: &StoreNode) -> bool {
        match node.get_node(&StoreKey::from(AUTOLINKS)) {
            Ok(n) => {
                match n.get_node_value().to_atomic_data().and_then(|ad|ad.to_boolean()) {
                   Ok(b) => b,
                    _ => true
                }
            },
            Err(_) => true
        }
    }

    fn parse_count(node: &StoreNode) -> Result<u64, Error> {
        match node.get_node(&StoreKey::from(COUNT)) {
            Ok(n) => {
                n.get_node_value().to_atomic_data()?.to_u64()
            },
            Err(_) => Ok(1)//default
        }
    }

    fn parse_resume_kind(node: &StoreNode) -> Result<ResumeKind, Error> {
        match node.get_node(&StoreKey::from(RESUME_KIND)) {
            Ok(n) => {
                Ok(ResumeKind::from(
                    &n.get_node_value().to_string_result()?
                    ))
            },
            Err(_) => Ok(ResumeKind::StartOver)
        }
    }

    fn parse_process(node: &StoreNode) -> Result<Vec<String>, Error> {
        let config = try!(node.get_node(&StoreKey::from(PROCESS)));
        let rule_args = try!(parse_string_array(config));
        if rule_args.is_empty() {
            Err(format_err!("Empty process is forbidden!"))
        }
        else {
            Ok(rule_args)
        }
    }

    fn parse_sources(node: &StoreNode, process: &Vec<String>) -> Result<Vec<String>, Error> {
        match node.get_node(&StoreKey::from(SOURCES)) {
            Ok(config) => {
                let rule_args = try!(parse_string_array(config));//get sources actor
                //verify that sources are valid actors
                for s in rule_args.iter() {
                    if !process.contains(s) {
                        return Err(format_err!("Source {} is not part of the process", s));
                    }
                }

                Ok(rule_args)
            },
            Err(_) => {//no explicit sources
                if process.is_empty() {
                    Err(format_err!("Empty process cannot have sources!"))
                }
                else {//source is first actor
                    // Ok(vec![process[0].clone()])
                    Ok(Vec::new())//isolated actors will be made source
                }

            }
        }

    }


    fn parse_links(node: &StoreNode, config: &StoreNode) -> Result<Vec<LinkDescription>, Error> {
        let mut links: Vec<LinkDescription> = Vec::new();
        match node.get_node(&StoreKey::from(LINKS)) {
            Ok(links_configs) => {
                for link in links_configs.get_node_children() {
                    let ldesc = try!(LinkDescription::parse_description(link, config));
                    links.push(ldesc);
                }
                Ok(links)
            },
            Err(_) => {//no links
                Ok(Vec::new())
            }
        }
    }

    fn parse_feedx(x: &str, node: &StoreNode, config: &StoreNode) -> Result<Vec<FeedXDescription>, Error> {
        let mut fbs: Vec<FeedXDescription> = Vec::new();
        match node.get_node(&StoreKey::from(x)) {
            Ok(fb_configs) => {
                for fb in fb_configs.get_node_children() {
                    let mut fbdescs = try!(FeedXDescription::parse_description(fb, config));
                    fbs.append(&mut fbdescs);
                }
                Ok(fbs)
            },
            Err(_) => {//no feedback
                Ok(Vec::new())
            }
        }
    }
}
