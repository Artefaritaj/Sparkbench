// @Author: ronan
// @Date:   19-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

use hconfig::store::{StoreNode, AtomicData, Data};

use sp_lib::interfaces::expressions::parser::{parse_static_i64, parse_static_string, parse_data_array};

use config_keys::SYNC;

use failure::Error;

#[derive(Debug,Clone)]
pub struct LinkDescription {
    source: String,//source cmd
    dest: String,//destination cmd
    var_source: String,//var for source
    var_dest: String,//var for destination
    cardinality: usize
}

impl LinkDescription {
    pub fn new(source: &str, var_source: &str, dest: &str, var_dest: &str, card: u64) -> LinkDescription {
        LinkDescription { source: source.to_string(), var_source: var_source.to_string(), dest: dest.to_string(), var_dest: var_dest.to_string(), cardinality: card as usize }
    }

    pub fn get_source(&self) -> &str {
        &self.source
    }

    pub fn get_destination(&self) -> &str {
        &self.dest
    }

    pub fn get_source_var(&self) -> &str {
        &self.var_source
    }

    pub fn get_dest_var(&self) -> &str {
        &self.var_dest
    }

    pub fn get_cardinality_string(&self) -> String {
        self.cardinality.to_string()
    }

    pub fn get_cardinality(&self) -> usize {
        self.cardinality
    }

    ///Check if links represent the same graph edge (do not care for cardinality)
    pub fn is_similar(&self, other: &LinkDescription) -> bool {
        self.source == other.source && self.dest == other.dest && self.var_source == other.var_source && self.var_dest == other.var_dest
    }

    pub fn parse_description(link_node: &StoreNode, config: &StoreNode) -> Result<LinkDescription, Error> {
        let desc = try!(parse_data_array(link_node));

        if desc.len() == 5 {//source, var source, dest, var dest, card
            match desc[4] {
                Data::Singleton(AtomicData::String(ref s)) => {
                    Ok(LinkDescription::new(&try!(parse_static_string(&try!(desc[0].to_string_result()), config)),
                                            &try!(parse_static_string(&try!(desc[1].to_string_result()), config)),
                                            &try!(parse_static_string(&try!(desc[2].to_string_result()), config)),
                                            &try!(parse_static_string(&try!(desc[3].to_string_result()), config)),
                                            try!(parse_static_i64(s, config)) as u64))
                },
                ref d => {
                    Ok(LinkDescription::new(&try!(parse_static_string(&try!(desc[0].to_string_result()), config)),
                                            &try!(parse_static_string(&try!(desc[1].to_string_result()), config)),
                                            &try!(parse_static_string(&try!(desc[2].to_string_result()), config)),
                                            &try!(parse_static_string(&try!(desc[3].to_string_result()), config)),
                                            d.to_atomic_data()?.to_u64()?))
                },
            }

        }
        else if desc.len() == 4 {//source, var source, dest, var dest (card = 1)
            Ok(LinkDescription::new(&try!(parse_static_string(&try!(desc[0].to_string_result()), config)),
                                    &try!(parse_static_string(&try!(desc[1].to_string_result()), config)),
                                    &try!(parse_static_string(&try!(desc[2].to_string_result()), config)),
                                    &try!(parse_static_string(&try!(desc[3].to_string_result()), config)),
                                    1))
        }
        else if desc.len() == 3 {//source, dest, card (vars are "sync")
            match desc[2] {
                Data::Singleton(AtomicData::String(ref s)) => {
                    Ok(LinkDescription::new(&try!(parse_static_string(&try!(desc[0].to_string_result()), config)),
                                            SYNC,
                                            &try!(parse_static_string(&try!(desc[1].to_string_result()), config)),
                                            SYNC,
                                            try!(parse_static_i64(s, config)) as u64))
                },
                ref d => {
                    Ok(LinkDescription::new(&try!(parse_static_string(&try!(desc[0].to_string_result()), config)),
                                            SYNC,
                                            &try!(parse_static_string(&try!(desc[1].to_string_result()), config)),
                                            SYNC,
                                            d.to_atomic_data()?.to_u64()?))
                }
            }

        }
        else if desc.len() == 2 { //source, dest (vars are "sync", card = 1)
            Ok(LinkDescription::new(&try!(parse_static_string(&try!(desc[0].to_string_result()), config)),
                                    SYNC,
                                    &try!(parse_static_string(&try!(desc[1].to_string_result()), config)),
                                    SYNC,
                                    1))
        }
        else {
            Err(format_err!("The link description is not complete: [source, source var, destination, dest var, cardinality]"))
        }
    }
}
