// @Author: ronan
// @Date:   18-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 19-10-2016

use hconfig::store::{StoreNode, StoreKey};
use std::collections::HashMap;
use config_keys::*;

use failure::Error;

#[derive(Clone,Debug,PartialEq)]
pub struct ModuleCommandPairDescription {
    module_name: String,
    command_name: String,
    attributes: HashMap<String, String>
}

impl ModuleCommandPairDescription {
    pub fn new(mod_name: &str, cmd_name: &str) -> ModuleCommandPairDescription {
        ModuleCommandPairDescription { module_name: mod_name.to_string(), command_name: cmd_name.to_string(), attributes: HashMap::new() }
    }

    pub fn with_attributes(mod_name: String, cmd_name: String, attributes: HashMap<String, String>) -> ModuleCommandPairDescription {
        ModuleCommandPairDescription { module_name: mod_name, command_name: cmd_name, attributes: attributes }
    }

    pub fn get_module_name(&self) -> &str {
        &self.module_name
    }

    pub fn get_command_name(&self) -> &str {
        &self.command_name
    }

    pub fn get_attributes(&self) -> &HashMap<String, String> {
        &self.attributes
    }

    fn parse_attribute(to_parse: &str) -> Result<(String, String), Error> {
        // info!("Attributes to parse: {}", to_parse);

        if to_parse.is_empty() {
            return Err(format_err!("Empty attributes are not authorized"));
        }

        let splitted: Vec<_> = to_parse.split(ASSIGN_SEP).collect();
        let left = if splitted.len() > 0 {
            splitted[0].to_string()
        }
        else {
            return Err(format_err!("Attribute error: {}", to_parse));
        };

        let right = if splitted.len() > 1 {
            splitted[1].to_string()
        }
        else {
            String::new()
        };

        // info!("Attributes parsed: {}={}", left, right);

        Ok((left, right))
    }

    pub fn parse_mcp(actor_node: &StoreNode) -> Result<(String, ModuleCommandPairDescription), Error> { //(cmd name, (module name, module command name))
        let actor_name = actor_node.get_node_key().to_string();

        //must have two children: {0} for device name and {1} for device's command name
        let children_count = actor_node.get_node_children().len();
        if children_count >= 2 {
            //module name
            let mod_node = try!(actor_node.get_node(&StoreKey::from("{0}")));
            let mod_name = try!(mod_node.get_node_value().to_string_result());

            //module command name
            let mod_cmd_node = try!(actor_node.get_node(&StoreKey::from("{1}")));
            let mod_cmd_name = try!(mod_cmd_node.get_node_value().to_string_result());

            let mut attrs: HashMap<String, String> = HashMap::new();
            for i in 2..children_count {
                let attribute_node = try!(actor_node.get_node(&StoreKey::from(&format!("{{{}}}", i))));
                let attr_string = try!(attribute_node.get_node_value().to_string_result());
                let (attr_name, attr_val) = try!(ModuleCommandPairDescription::parse_attribute(&attr_string));
                attrs.insert(attr_name, attr_val);
            }

            Ok((actor_name, ModuleCommandPairDescription::with_attributes(mod_name, mod_cmd_name, attrs)))
        }
        else {
            Err(format_err!("Reset and DUT process commands must define module and module's command: process command = [\"module\", \"module command\"]"))
        }
    }
}
