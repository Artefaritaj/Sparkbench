// @Author: Lashermes Ronan <ronan>
// @Date:   22-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 22-02-2017
// @License: MIT

use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::process::ModuleState;

#[derive(Debug,Clone)]
pub struct ExperimentState {
    pulse_index: u64,
    data: NameMapper<ModuleState>
}


impl ExperimentState {
    pub fn empty() -> ExperimentState {
        ExperimentState { pulse_index: 0, data: NameMapper::new() }
    }

    pub fn new(pulse_index: u64) -> ExperimentState {
        ExperimentState { pulse_index: pulse_index, data: NameMapper::new() }
    }

    pub fn add_module_state(&mut self, module_name: &str, state: ModuleState) {
        self.data.insert(module_name, state);
    }

    pub fn remove_module_state(&mut self, module_name: &str) -> Option<ModuleState> {
        self.data.remove(module_name)
    }

    pub fn get_module_state(&self, module_name: &str) -> Option<&ModuleState> {
        self.data.get(module_name)
    }

    pub fn get_pulse_index(&self) -> u64 {
        self.pulse_index
    }

    pub fn set_pulse_index(&mut self, index: u64) {
        self.pulse_index = index;
    }
}
