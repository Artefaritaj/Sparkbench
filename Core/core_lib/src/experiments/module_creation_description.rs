// @Author: ronan
// @Date:   19-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 19-10-2016

use hconfig::store::StoreNode;

#[derive(Debug,Clone)]
pub struct ModuleCreationDescription {
    mod_type: String,
    //args: Vec<String>
    args_node: StoreNode
}

impl ModuleCreationDescription {
    pub fn new(mod_type: String, args: StoreNode) -> ModuleCreationDescription {
        ModuleCreationDescription { mod_type: mod_type, args_node: args }
    }

    pub fn get_module_type(&self) -> &str {
        &self.mod_type
    }

    pub fn get_args(&self) -> &StoreNode {
        &self.args_node
    }
}
