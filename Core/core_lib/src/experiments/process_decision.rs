// @Author: Lashermes Ronan <ronan>
// @Date:   16-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 16-02-2017
// @License: MIT

use hconfig::store::{StoreNode, StoreKey};

use failure::Error;

///A process decision describes what to do if a given process succeed or fail
#[derive(Debug, Clone)]
pub struct ProcessDecisison {
    process_name:       String,
    proc_if_success:    String,
    proc_if_failure:    String//,
    // resume:             ResumeKind
}

impl ProcessDecisison {
    pub fn parse(node: &StoreNode) -> Result<ProcessDecisison, Error> {
        let proc_name = node.get_node_key().to_string();

        let children_count = node.get_node_children().len();
        let (suc_name, fail_name) = if children_count >= 2 {
            //module name
            let success_node = try!(node.get_node(&StoreKey::from("{0}")));
            let success_name = try!(success_node.get_node_value().to_string_result());

            //module command name
            let failure_node = try!(node.get_node(&StoreKey::from("{1}")));
            let failure_name = try!(failure_node.get_node_value().to_string_result());

            (success_name, failure_name)
        }
        else {
            return Err(format_err!("Reset and DUT process commands must define module and module's command: process command = [\"module\", \"module command\"]"));
        };

        // let resume = if children_count >= 3 {
        //     let resume_node = try!(node.get_node(&StoreKey::from("{2}")));
        //     let resume_name = try!(resume_node.get_node_value().to_string_result());
        //     ResumeKind::from(&resume_name)
        // }
        // else {
        //     ResumeKind::StartOver
        // };

        Ok(ProcessDecisison { process_name: proc_name, proc_if_success: suc_name, proc_if_failure: fail_name/*, resume: resume*/})
    }

    pub fn get_name(&self) -> &str {
        &self.process_name
    }

    pub fn get_success_process(&self) -> &str {
        &self.proc_if_success
    }

    pub fn get_failure_process(&self) -> &str {
        &self.proc_if_failure
    }

    // pub fn get_resume_kind(&self) -> ResumeKind {
    //     self.resume
    // }
}
