// @Author: Lashermes Ronan <ronan>
// @Date:   16-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 16-02-2017
// @License: MIT

///How to resume from an interrupted process
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ResumeKind {
    /// Do not save state
    StartOver,
    /// Save state if fails + skip failing exec
    ResumeSkip,
    ///Save state if fails
    Resume,
    Undefined,
    ///Save state even if success
    Persistent
}

impl ResumeKind {
    pub fn from(s: &str) -> ResumeKind {
        let lower = s.to_lowercase();
        match lower.as_str() {
            "start_over" | "startover" => ResumeKind::StartOver,
            "resume_skip" | "resumeskip" => ResumeKind::ResumeSkip,
            "resume" => ResumeKind::Resume,
            "persistent" => ResumeKind::Persistent,
            _ => ResumeKind::Undefined
        }
    }

    pub fn is_state_saved(&self) -> bool {
        match self {
            &ResumeKind::Resume | &ResumeKind::ResumeSkip | &ResumeKind::Persistent => true,
            _ => false
        }
    }

    pub fn is_force(&self) -> bool {
        match self {
            &ResumeKind::ResumeSkip => true,
            _ => false
        }
    }
}
