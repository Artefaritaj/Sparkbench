/*
 * File: range.rs
 * Project: src
 * Created Date: Thursday October 18th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 30th October 2018 3:20:25 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use range_val::RangeVal;

pub struct Range<T : RangeVal> {
    pub min: T,
    pub max: T
}

impl<T: RangeVal> Range<T> {
    pub fn new(val1: T, val2: T) -> Range<T> {
        if val1 < val2 {
            Range { min: val1, max: val2 }
        }
        else {
            Range { min: val2, max: val1 }
        }
    }

    pub fn is_inside_inclusive(&self, val: &T) -> bool {
        if &self.min <= val && val <= &self.max {
            true
        }
        else {
            false
        }
    }

    pub fn is_inside_exclusive(&self, val: &T) -> bool {
        if &self.min < val && val < &self.max {
            true
        }
        else {
            false
        }
    }

    /// extend the range with a new value
    /// if the new val is outside the range, it will either replace min or max and the method returns true
    pub fn extend(&mut self, val: T) -> bool {
        if val < self.min {
            self.min = val;
            return true
        }

        if val > self.max {
            self.max = val;
            return true;
        }

        false
    }

    pub fn size(&self) -> T {
        self.max - self.min
    }
}

#[test]
pub fn test_range() {
    let mut r1 = Range::new(-1.0f64, 1.0f64);

    assert_eq!(r1.is_inside_inclusive(&1f64), true);
    assert_eq!(r1.is_inside_exclusive(&1f64), false);

    r1.extend(2.0f64);

    assert_eq!(r1.is_inside_inclusive(&1.5), true);
}