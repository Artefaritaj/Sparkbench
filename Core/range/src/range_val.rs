/*
 * File: range_val.rs
 * Project: src
 * Created Date: Thursday October 18th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 30th October 2018 3:11:17 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use std::ops::Sub;

pub trait RangeVal : PartialOrd + PartialEq + Sub<Output=Self> + Sized + Clone + Copy {}

impl<T: PartialOrd + PartialEq + Sub<Output=T> + Copy + Clone> RangeVal for T {

}