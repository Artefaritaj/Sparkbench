/*
 * Author:   ronan.lashermes@inria.fr
 * Creation: Tue Nov 28 2017
 *
 * Copyright (c) 2017 INRIA
 */


#[derive(Fail, Debug)]
#[fail(display = "Failed to connect to {}: {}", conn_name, info)]
pub struct ConnectionError {
    conn_name: String,
    info: String
}

impl ConnectionError {
    pub fn new(conn_name: &str, info: &str) -> ConnectionError {
        ConnectionError { conn_name: conn_name.to_string(), info: info.to_string() }
    }
}


#[derive(Fail, Debug)]
#[fail(display = "The connection {} is missing", conn_name)]
pub struct ConnectionMissing {
    conn_name: String
}


impl ConnectionMissing {
    pub fn new(conn_name: &str) -> ConnectionMissing {
        ConnectionMissing { conn_name: conn_name.to_string()}
    }
}