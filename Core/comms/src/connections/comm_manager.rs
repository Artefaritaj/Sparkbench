// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 14-11-2016
// @License: MIT



use connections::connect::connect_from_config;
use connections::link::Link;
use connections::FeedbackNotifier;
use server_process::server::ServerMessage;
use connections::connect::popup_servers;

use std::collections::HashMap;
use std::hash::{Hash, Hasher};
use std::collections::hash_map::DefaultHasher;
use std::sync::Mutex;
use std::sync::Arc;
use std::path::Path;
use std::sync::mpsc::Sender;
// use std::time::Duration;
// use std::thread;

use hconfig::store::StoreNode;
use failure::Error;


///Grant access to connection link
pub struct CommManager {
    connections: HashMap<u64, Link>,
    servers: HashMap<u64, Vec<Sender<ServerMessage>>>,
    feedback_url: Option<String>
}


impl CommManager {
    pub fn new() -> CommManager {
        CommManager { connections: HashMap::new(), servers: HashMap::new(), feedback_url: None }
    }

    pub fn close_all_connections(&mut self) {
        self.connections.clear();
    }

    pub fn set_feedback_url(&mut self, url: Option<String>) {
        self.feedback_url = url
    }

    pub fn get_feedback_url(&self) -> &Option<String> {
        &self.feedback_url
    }

    pub fn create_feedback_notifier(&self) -> FeedbackNotifier {
        FeedbackNotifier::new(self.feedback_url.clone())
    }

    ///List the open serial ports
    pub fn serial_ports_available() -> Result<Vec<String>, Error> {
        //waiting for functionnality in serial crate
        //for now simple method for linux only

        let mut result: Vec<String> = Vec::new();
        let dev = Path::new("/dev");
        for file_res in try!( dev.read_dir().map_err(|e| format_err!("{}", e)) ) {
            match file_res {
                Ok(file) => {
                    let path = file.path();
                    path.file_name()//-> Opt OsSTr
                        .and_then(|n| n.to_str())//-> Opt str
                        .and_then(|n| {//select serial port -> Opt str
                            if n.starts_with("ttyS") || n.starts_with("ttyUSB") {
                                path.to_str()
                            } else {
                                None
                            }
                        })//push result
                        .and_then(|s| {
                            result.push(s.to_string());
                            Some(())
                        });
                },
                Err(e) => {return Err(format_err!("Serial ports enumeration error: {}", e.to_string()));}//skip
            }

        }

        Ok(result)
    }

    pub fn connect_to(&mut self, config: &StoreNode) -> Result<Link, Error> {
        let config_hash = CommManager::config_hash(config);
        //try to find an existing connections
        if self.connections.contains_key(&config_hash) {
            Ok(try!(self.connections.get_mut(&config_hash) .ok_or(format_err!("Failed to create connection: connection data unreachable."))).clone())
        }
        else//if not existing, create a new one
        {
            //popup server if needed
            let new_servers = if self.servers.contains_key(&config_hash) {
                None
            }
            else {
                Some(try!(popup_servers(config)))
            };

            if let Some(servers)  = new_servers {
                self.servers.insert(config_hash, servers);
            }

            //connect
            let new_conn = try!(connect_from_config(config));
            self.connections.insert(config_hash, Arc::new(Mutex::new(new_conn)));

            //clone the config to be used by the calling code
            Ok(try!(self.connections.get_mut(&config_hash) .ok_or(format_err!("Failed to create connection: unknown error."))).clone())
        }
    }

    pub fn kill_by_config(&mut self, config: &StoreNode) -> bool {
        self.kill_by_config_hash(CommManager::config_hash(config))
    }

    pub fn kill_by_config_hash(&mut self, config_hash: u64) -> bool {
        self.connections.remove(&config_hash).is_some()
    }

    ///Create a hash value from a config node specifying the connection properties
    pub fn config_hash(config: &StoreNode) -> u64 {
        let mut hasher = DefaultHasher::new();
        let config_hm = config.to_btreemap();//order is important to keep

        for (k,v) in config_hm {
            k.hash(&mut hasher);
            v.hash(&mut hasher);
        }
        hasher.finish()
    }
}

impl Drop for CommManager {
    fn drop(&mut self) {
        //info!("Drop CommManager!");
        for (_, servers) in self.servers.iter_mut() {//kill all servers
            for server in servers {
                let _ = server.send(ServerMessage::Kill);
            }
        }
        // thread::sleep(Duration::from_millis(1000));
    }
}

#[test]
//requires internet connection
fn test_comm_manager() {
    use hconfig::store::{Data, StoreKey};
    use std::thread;
    use std::sync::mpsc::channel;

    use config_keys::*;


    let mut mgr = CommManager::new();

    let mut eth_config = StoreNode::new(CONNECTION_CONFIG.to_string(), Data::Empty);
    eth_config.store_update(StoreKey::from(CONNECTION_LINK) , Data::create_str(LINK_ETHERNET));
    eth_config.store_update(StoreKey::from(CONNECTION_PROTOCOL) , Data::create_str(PROTOCOL_RAW));
    eth_config.store_update(StoreKey::from(LINK_ETHERNET).push(IPV4_ADDRESS), Data::create_str("www.google.com"));
    eth_config.store_update(StoreKey::from(LINK_ETHERNET).push(PORT), Data::create_i64(80));


    let lk1 = mgr.connect_to(&eth_config).unwrap();
    let lk2 = mgr.connect_to(&eth_config).unwrap();

    let lock1 = lk1.try_lock();
    let lock2 = lk2.try_lock();
    assert!(lock1.is_ok());
    assert!(lock2.is_err());//must not be able to get the lock twice (while lock1 in scope)

    mgr.kill_by_config(&eth_config);

    // let mutex_mgr = Arc::new(Mutex::new(mgr));

    let (tx, rx) = channel();
    for i in 0..10 {
        let tx = tx.clone();
        // let mutex_mgr = mutex_mgr.clone();
        // let eth_config = eth_config.clone();
        let link = mgr.connect_to(&eth_config).unwrap();

        thread::spawn(move || {
            // let mut mgr = mutex_mgr.lock().unwrap();
            // let _ = mgr.connect_to(&eth_config).unwrap();
            let link = link;
            let lock = link.lock();
            assert!(lock.is_ok());
            tx.send(i).unwrap();
        });
    }

    for _ in 0..10 {
        let _ = rx.recv().unwrap();
        //println!("{}", v);
    }
}

#[test]
fn test_enumerate() {
    let ports = CommManager::serial_ports_available().unwrap();
    println!("Ports: ");
    for p in ports.iter() {
        println!("{}", p);
    }
}
