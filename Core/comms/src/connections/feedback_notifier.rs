/*
 * Created on Fri Mar 23 2018
 *
 * Copyright (c) 2018 INRIA
 */

use failure::Error;
use comms_reqwest;
use serde::Serialize;

pub struct FeedbackNotifier {
    url: Option<String>
}

impl FeedbackNotifier {
    pub fn new(url: Option<String>) -> FeedbackNotifier {
        FeedbackNotifier { url }
    }

    pub fn notify<T: Serialize>(&self, f: &T) -> Result<(), Error> {
        if let Some(ref u) = self.url {
            let client = comms_reqwest::Client::new();
            let _ = client.post(u)
                .json(f)
                .send()?;
        }
        Ok(())
    }
}