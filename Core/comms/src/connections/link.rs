// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 23-09-2016
// @License: MIT



use connections::connect::Connected;
use std::sync::Mutex;
use std::sync::Arc;

pub type Link = Arc<Mutex<Box<dyn Connected>>>;
