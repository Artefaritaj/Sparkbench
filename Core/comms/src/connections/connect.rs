// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 24-03-2017
// @License: MIT

use hconfig::store::{StoreNode, StoreKey, Data, AtomicData};

use std::io::{Read, Write};
use std::net::{ SocketAddr, TcpStream, ToSocketAddrs/*, UdpSocket*/};
use std::time::Duration;
use std::sync::mpsc::Sender;

use config_keys::*;

use dimval::{Prefix};

use serial;
use serial::{SystemPort, PortSettings, BaudRate, CharSize, StopBits, Parity, FlowControl};
use serial::prelude::*;
use links::smartcard::Smartcard;
use connections::helpers;
use server_process::server::ServerMessage;
use server_process::server::pop_server;

use failure::Error;

pub enum TransportProtocol {
    TCP,
    UDP
}

impl TransportProtocol {
    pub fn parse(s: &str) -> TransportProtocol {
        match s.to_lowercase().as_ref() {
            "udp" => TransportProtocol::UDP,
            "tcp" => TransportProtocol::UDP,
            _ => TransportProtocol::UDP
        }
    }
}

pub trait Connected : Read + Write + Send {
    fn update_config(&mut self, config: &mut StoreNode);

    fn set_timeout(&mut self, to: Option<Duration>) -> Result<(), Error>;
    fn get_timeout(&self) -> Result<Option<Duration>, Error>;
}

impl Connected for TcpStream {
    ///Update timeouts (read and write) for tcp connection
    fn update_config(&mut self, config: &mut StoreNode) {
        let _ = apply_config_timeouts_tcp(config, self);
    }

    fn set_timeout(&mut self, to: Option<Duration>) -> Result<(), Error> {
        self.set_read_timeout(to).map_err(|e|format_err!("Set read timeout error: {}", e))
    }

    fn get_timeout(&self) -> Result<Option<Duration>, Error> {
        self.read_timeout().map_err(|e|format_err!("Get read timeout error: {}", e))
    }
}

// impl Connected for UdpSocket {
//     ///Update timeouts (read and write) for tcp connection
//     fn update_config(&mut self, config: &mut StoreNode) {
//         // let _ = apply_config_timeouts_tcp(config, self);
//     }

//     fn set_timeout(&mut self, to: Option<Duration>) -> Result<(), Error> {
//         self.set_read_timeout(to).map_err(|e|format_err!("Set read timeout error: {}", e))
//     }

//     fn get_timeout(&self) -> Result<Option<Duration>, Error> {
//         self.read_timeout().map_err(|e|format_err!("Get read timeout error: {}", e))
//     }
// }

impl Connected for SystemPort {

    ///Write bits (RTS,DTR) and read bits (CTS, DSR, RI, CD)
    fn update_config(&mut self, config: &mut StoreNode) {
        apply_rts(config, self);//write rts
        apply_dtr(config, self);//write dtr

        //read cts, dsr, ri, cd
        read_serial_bits_in_config(config, self);
    }

    fn set_timeout(&mut self, to: Option<Duration>) -> Result<(), Error> {
        if let Some(to_unwrap) = to {
            SerialPort::set_timeout(self, to_unwrap).map_err(|e|format_err!("Apply timeout error: {}", e))
        }
        else {
            Ok(())
        }
    }

    fn get_timeout(&self) -> Result<Option<Duration>, Error> {
        Ok(Some(self.timeout()))
    }
}

pub fn popup_servers(conn_config: &StoreNode) -> Result<Vec<Sender<ServerMessage>>, Error> {
    //check if root key valid
    let valid = match conn_config.get_node_key() {
        s => s == CONNECTION_CONFIG
    };

    if !valid { return Err(format_err!("The configuration is not a valid connection configuration.")); }

    match conn_config.get_value(&StoreKey::from(REQUIRE)).and_then(|n|n.to_string_result()) {
        Ok(require) => {
            let mut result = Vec::new();
            let servers_to_pop = require.split(VEC_SEP);
            for to_pop in servers_to_pop {
                result.push(try!(pop_server(to_pop, conn_config)));
                info!("Server {} has been launched.", to_pop);
            }
            Ok(result)
        },
        _ => { Ok(Vec::new()) }//no server to popup
    }
}

///Create a connection from a configuration tree
pub fn connect_from_config(config: &StoreNode) -> Result<Box<dyn Connected>, Error> {
    //check if root key valid
    let valid = match config.get_node_key() {
        s => s == CONNECTION_CONFIG
    };

    if !valid { return Err(format_err!("The configuration is not a valid connection configuration.")); }

    let link = config.get_value(&StoreKey::from(CONNECTION_LINK));

    let connected = match link.and_then(|n|n.to_string_result()) {//create connection according to link type
        Ok(s) => match s.as_ref() {
            LINK_SERIAL => connect_serial(config),
            LINK_ETHERNET => connect_ethernet(config),
            LINK_SMARTCARD => connect_smartcard(config),
            _ => Err(format_err!("No valid link type (serial, ethernet...) is defined in the configuration, instead: {}.", s))
        },
        _ => Err(format_err!("No link type specified."))
    };

    //connected.map(|c| ConfiguredConnected{conn: c, config_hash: create_config_hash(config)})
    connected
}

///Connect with serial
fn connect_serial(config: &StoreNode) -> Result<Box<dyn Connected>, Error> {
    //read port
    let port = try!(config.get_value(&StoreKey::from(LINK_SERIAL).push(PORT))
                                .map_err(|_| format_err!("No serial port specified.")))
                                .to_string();

    //create connection
    let mut conn = try!(serial::open(&port).map_err(|err| format_err!("Cannot connect to {}: {}", port, err)));

    let _ = conn.configure(&extract_serial_config(config));
    apply_timeout(config, &mut conn)?;

    Ok(Box::new(conn))
}

///Extract PortSettings from config
fn extract_serial_config(config: &StoreNode) -> PortSettings {

    //init settings with default value
    let mut settings = PortSettings {
        baud_rate: BaudRate::Baud115200,
        char_size: CharSize::Bits8,
        parity: Parity::ParityNone,
        stop_bits: StopBits::Stop1,
        flow_control: FlowControl::FlowNone,
    };

    let _ = apply_baudrate(config, &mut settings);
    apply_databits(config, &mut settings);
    apply_stopbits(config, &mut settings);
    apply_parity(config, &mut settings);
    apply_flow_control(config, &mut settings);

    settings
}

fn apply_rts(config: &StoreNode, conn: &mut dyn SerialPort) {
    match config.get_value(&StoreKey::from(LINK_SERIAL).push(SERIAL_RTS))
                .and_then(|n|n.to_atomic_data())
                .and_then(|ad|ad.to_boolean()) {
        Ok(b) => {let _ = conn.set_rts(b);},
        _ => {}
    }
}

fn apply_dtr(config: &StoreNode, conn: &mut dyn SerialPort) {
    match config.get_value(&StoreKey::from(LINK_SERIAL).push(SERIAL_DTR))
                .and_then(|n|n.to_atomic_data())
                .and_then(|ad|ad.to_boolean()) {
        Ok(b) => {let _ = conn.set_dtr(b);},
        _ => {}
    }
}

fn read_serial_bits_in_config(config: &mut StoreNode, conn: &mut dyn SerialPort) {

    match conn.read_cts() {
        Ok(b) => {config.store_update(StoreKey::from(LINK_SERIAL).push(SERIAL_CTS), Data::create_bool(b));},
        _ => {}
    }

    match conn.read_dsr() {
        Ok(b) => {config.store_update(StoreKey::from(LINK_SERIAL).push(SERIAL_DSR), Data::create_bool(b));},
        _ => {}
    }

    match conn.read_ri() {
        Ok(b) => {config.store_update(StoreKey::from(LINK_SERIAL).push(SERIAL_RI), Data::create_bool(b));},
        _ => {}
    }

    match conn.read_cd() {
        Ok(b) => {config.store_update(StoreKey::from(LINK_SERIAL).push(SERIAL_CD), Data::create_bool(b));},
        _ => {}
    }
}

fn apply_timeout(config: &StoreNode, conn: &mut dyn SerialPort) -> Result<(), Error> {

    let timeout_res = config.get_value(&StoreKey::from(LINK_SERIAL).push(TIMEOUT))
                        .or(config.get_value(&StoreKey::from(TIMEOUT)));

    let timeout = match timeout_res
                            .and_then(|n|n.to_atomic_data())
                            .and_then(|ad|ad.to_dim()) {
        Ok(dim) => Duration::from_millis(dim.to_i64_prefix(Prefix::Milli)? as u64),
        _ => Duration::from_millis(3000)
    };
    conn.set_timeout(timeout).map_err(|e|format_err!("Apply timeout error: {}", e))
}

fn apply_baudrate(config: &StoreNode, settings: &mut PortSettings) -> Result<(), Error> {
    let baudrate = match config.get_value(&StoreKey::from(LINK_SERIAL).push(BAUD_RATE))
                                    .and_then(|n|n.to_atomic_data())
                                    .and_then(|ad|ad.to_u64()) {
        Ok(u) => BaudRate::from_speed(u as usize),
        _ => BaudRate::Baud115200
    };
    settings.set_baud_rate(baudrate).map_err(|e|format_err!("Set baud rate error: {}", e))
}

fn apply_databits(config: &StoreNode, settings: &mut PortSettings) {
    let databits = match config.get_value(&StoreKey::from(LINK_SERIAL).push(DATABITS))
                                    .and_then(|n|n.to_atomic_data())
                                    .and_then(|ad|ad.to_u64()) {
        Ok(i) => helpers::char_size_from_int(i),
        _ => CharSize::Bits8
    };
    settings.set_char_size(databits)
}

fn apply_stopbits(config: &StoreNode, settings: &mut PortSettings) {
    let stopbits = match config.get_value(&StoreKey::from(LINK_SERIAL).push(STOPBITS))
                                .and_then(|n|n.to_atomic_data())
                                .and_then(|ad|ad.to_u64())  {
        Ok(u) => helpers::stop_bits_from_int(u),
        _ => StopBits::Stop1
    };
    settings.set_stop_bits(stopbits);
}

fn apply_parity(config: &StoreNode, settings: &mut PortSettings) {
    let parity = match config.get_value(&StoreKey::from(LINK_SERIAL).push(PARITY))
                             .and_then(|n|n.to_atomic_data()) {
        Ok(AtomicData::String(s)) => helpers::parity_from_str(&s),
        Ok(ad) => {
            match ad.to_u64() {
                Ok(u) => helpers::parity_from_int(u),
                _ => Parity::ParityNone
            }
        },
        _ => Parity::ParityNone
    };
    settings.set_parity(parity);
}

fn apply_flow_control(config: &StoreNode, settings: &mut PortSettings){
    let control = match config.get_value(&StoreKey::from(LINK_SERIAL).push(FLOW_CONTROL))
                                .and_then(|n|n.to_atomic_data()) {
        Ok(AtomicData::String(s)) => helpers::flow_control_from_str(&s),
        Ok(AtomicData::Boolean(b)) => helpers::flow_control_from_bool(b),
        Ok(ad) => {
            match ad.to_u64() {
                Ok(u) => helpers::flow_control_from_int(u),
                _ => FlowControl::FlowNone
            }
        },
        _ => FlowControl::FlowNone
    };
    settings.set_flow_control(control);
}

fn connect_ethernet(config: &StoreNode) -> Result<Box<dyn Connected>, Error> {
    let (transport, sock_add) = try!(extract_ethernet_connection(config));
    match transport {
        TransportProtocol::TCP => connect_ethernet_tcp(sock_add, config),
        TransportProtocol::UDP => Err(format_err!("UDP not implemented yet"))//connect_ethernet_udp(sock_add)
    }
}

///Connect with ethernet
fn connect_ethernet_tcp(sock_add: SocketAddr, config: &StoreNode) -> Result<Box<dyn Connected>, Error> {
    let mut conn = try!(TcpStream::connect(sock_add).map_err(|e| format_err!("{}", e)));
    apply_config_timeouts_tcp(config, &mut conn)?;//change timeouts if specified in config
    Ok(Box::new(conn))
}

// ///Connect with ethernet
// fn connect_ethernet_udp(sock_add: SocketAddr) -> Result<Box<Connected>, Error> {
//     let mut conn = try!(UdpSocket::connect(sock_add).map_err(|e| format_err!("{}", e)));
//     // apply_config_timeouts(config, &mut conn)?;//change timeouts if specified in config
//     Ok(Box::new(conn))
// }

fn extract_ethernet_connection(config: &StoreNode) -> Result<(TransportProtocol, SocketAddr), Error> {

    //read port
    let port = config.get_value(&StoreKey::from(LINK_ETHERNET).push(PORT))
                        .map_err(|_| format_err!("No key"))//useless but for the type system
                        .and_then(|n|n.to_atomic_data())
                        .and_then(|d| d.to_i64())//Data -> i64
                        .unwrap_or(80);//Default value

    //read ipv4 add
    let ipv4_res = config.get_value(&StoreKey::from(LINK_ETHERNET).push(IPV4_ADDRESS));
    let ipv6_res = config.get_value(&StoreKey::from(LINK_ETHERNET).push(IPV6_ADDRESS));

    let ip = try!(ipv4_res  .map_err(|_| format_err!("No IPV4 address"))
                            .or(ipv6_res    .map_err(|_|format_err!("No IPV6 address")))) // if no IPV4, try with IPV6
                            .to_string();



    let port_str = port.to_string();
    let ip_port = ip + ":" + &port_str;

    let sock_addr = ip_port.to_socket_addrs()
        .map_err(|err| format_err!("{}", err))
        .and_then(|mut it| it.next()
                                .ok_or(format_err!("Unable te resolve address.")))?;

    
    let transport = config.get_value(&StoreKey::from(LINK_ETHERNET).push(TRANSPORT))
                                .and_then(|t_data|Ok(t_data.to_string()))
                                .and_then(|t_str|Ok(TransportProtocol::parse(&t_str)))
                                .unwrap_or(TransportProtocol::TCP);
    Ok((transport, sock_addr))
}

fn apply_config_timeouts_tcp(config: &StoreNode, conn: &mut TcpStream) -> Result<(), Error> {
    //key can be read_timeout or timeout
    let read_to_res  = config.get_value(&StoreKey::from(LINK_ETHERNET).push(READ_TIMEOUT))
                        .or(config.get_value(&StoreKey::from(LINK_ETHERNET).push(TIMEOUT)))
                        .or(config.get_value(&StoreKey::from(TIMEOUT)));

    let read_to = match read_to_res
                            .and_then(|n|n.to_atomic_data())
                            .and_then(|ad|ad.to_dim()) {
        Ok(d) => Duration::from_millis(d.to_i64_prefix(Prefix::Milli)? as u64),
        _ => {return Ok(());}//do nothing
    };
    conn.set_read_timeout(Some(read_to)).map_err(|e|format_err!("Set read timeout error: {}", e))
}

///Connect with smartcard
fn connect_smartcard(config: &StoreNode) -> Result<Box<dyn Connected>, Error> {
    let smartcard = Smartcard::connect( 
                        config.get_node(&StoreKey::from(LINK_SMARTCARD))
                            .map_err(|_| format_err!("No smartcard config"))?
                        ).map_err(|e| format_err!("{}", e))?;
    Ok(Box::new(smartcard))
}

#[test]
fn test_connect() {
    let mut serial_config = StoreNode::new(CONNECTION_CONFIG.to_string(), Data::Empty);
    serial_config.store_update(StoreKey::from(CONNECTION_LINK) , Data::create_str(LINK_SERIAL));
    serial_config.store_update(StoreKey::from(CONNECTION_PROTOCOL) , Data::create_str(PROTOCOL_RAW));
    serial_config.store_update(StoreKey::from(LINK_SERIAL).push(PORT), Data::create_str("/det/ttyUSB19"));
    serial_config.store_update(StoreKey::from(LINK_SERIAL).push(BAUD_RATE), Data::create_i64(115200));

    let conn_serial = connect_from_config(&serial_config);
    assert!(conn_serial.is_err());
    println!("Serial error: {}",conn_serial.err().unwrap());

    let mut eth_config = StoreNode::new(CONNECTION_CONFIG.to_string(), Data::Empty);
    eth_config.store_update(StoreKey::from(CONNECTION_LINK) , Data::create_str(LINK_ETHERNET));
    eth_config.store_update(StoreKey::from(CONNECTION_PROTOCOL) , Data::create_str(PROTOCOL_RAW));
    eth_config.store_update(StoreKey::from(LINK_ETHERNET).push(IPV4_ADDRESS), Data::create_str("www.google.com"));
    eth_config.store_update(StoreKey::from(LINK_ETHERNET).push(PORT), Data::create_i64(80));
    let conn_eth = connect_from_config(&eth_config);
    assert!(conn_eth.is_ok(), "Test required internet connection.");
    //println!("Ethernet error: {}",conn_eth.err().unwrap());
}
