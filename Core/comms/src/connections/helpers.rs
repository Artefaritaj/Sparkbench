/*
 * Author:   ronan.lashermes@inria.fr
 * Creation: Tue Nov 28 2017
 *
 * Copyright (c) 2017 INRIA
 */


use serial::{CharSize, StopBits, Parity, FlowControl};

pub fn char_size_from_int(bits: u64) -> CharSize {
    match bits {
        5 => CharSize::Bits5,
        6 => CharSize::Bits6,
        7 => CharSize::Bits7,
        8 => CharSize::Bits8,
        _ => CharSize::Bits8
    }
}

pub fn stop_bits_from_int(bits: u64) -> StopBits {
    match bits {
        1 => StopBits::Stop1,
        2 => StopBits::Stop2,
        _ => StopBits::Stop1
    }
}

// pub fn stop_bits_from_float(bits: f64) -> StopBits {
//     let bits: i64 = bits as i64;
//     stop_bits_from_int(bits)
// }

pub fn parity_from_int(parity: u64) -> Parity {
    match parity {
        1 => Parity::ParityOdd,
        2 => Parity::ParityEven,
        _ => Parity::ParityNone
    }
}

pub fn parity_from_str(s: &str) -> Parity {
    match s {
        "odd" | "Odd" => Parity::ParityOdd,
        "even" | "Even" => Parity::ParityEven,
        _ => Parity::ParityNone
    }
}

pub fn flow_control_from_int(ctr: u64) -> FlowControl {
    match ctr {
        1 => FlowControl::FlowSoftware,
        2 => FlowControl::FlowHardware,
        _ => FlowControl::FlowNone
    }
}

pub fn flow_control_from_bool(ctr: bool) -> FlowControl {
    match ctr {
        true => FlowControl::FlowHardware,
        false => FlowControl::FlowNone
    }
}

pub fn flow_control_from_str(ctr: &str) -> FlowControl {
    match ctr {
        "Software" | "software" | "Xon" | "Xoff" | "Xon/Xoff"  => FlowControl::FlowSoftware,
        "Hardware" | "hardware" => FlowControl::FlowHardware,
        _ => FlowControl::FlowNone
    }
}