// @Author: ronan
// @Date:   23-12-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 23-12-2016

use connections::connect::Connected;

use hconfig::store::{StoreNode, StoreKey};
use smartcard::logic::{Reader, Card, Context};
use smartcard::parameters::{ShareMode, Protocol};
use config_keys::*;

use failure::Error;

use std::io::{Read, Write};
use std::io;
use std::sync::Arc;
use std::time::Duration;

pub struct Smartcard {
    reader: Reader,
    card: Card,
    buffer: Vec<u8>//necessary to expose Read + Write interface
}

impl Smartcard {

    pub fn get_reader_name(&self) -> &str {
        self.reader.get_name()
    }

    fn str2protocol(s: &str) -> Protocol {
        match s {
            T0          => Protocol::T0,
            T1          => Protocol::T1,
            ANY         => Protocol::Any,
            RAW         => Protocol::Raw,
            AUTO        => Protocol::Auto,
            UNDEFINED   => Protocol::Undefined,
            _           => Protocol::Undefined
        }
    }

    fn str2share_mode(s: &str) -> ShareMode {
        match s {
            EXCLUSIVE   => ShareMode::Exclusive,
            DIRECT      => ShareMode::Direct,
            SHARED      => ShareMode::Shared,
            AUTO        => ShareMode::Auto,
            _           => ShareMode::Auto
        }
    }

    pub fn connect(config_node: &StoreNode) -> Result<Smartcard, String> {
        //extract config info
        let protocol_str = try!(config_node.get_value(&StoreKey::from(PROTOCOL))
                                    .map_err(|e|e.to_string()))//Errors should be Err(String)
                                    .to_string();

        let protocol = Smartcard::str2protocol(&protocol_str);

        let share_str = try!(config_node.get_value(&StoreKey::from(SHARE))
                                .map_err(|e|e.to_string()))//Errors should be Err(String)
                                .to_string();

        let share = Smartcard::str2share_mode(&share_str);


        let reader_name = try!(config_node.get_value(&StoreKey::from(READER))
                                .map_err(|e|e.to_string()))//Errors should be Err(String)
                                .to_string();

        // println!("Protocol {:?}, share {:?}, reader {}", protocol, share, reader_name);

        //First we create the resource manager context. I think of it as 'the driver'.
        let context = Arc::new(try!(Context::establish_context_auto() .map_err(|e|e.to_string())));

        //The context allows to list all available card readers.
        let mut readers = try!(context.list_readers() .map_err(|e|e.to_string()));

        //get selected reader
        let reader = try!(if reader_name == AUTO {
            readers.pop().ok_or(format!("no readers found"))//take firt reader if AUTO
        }
        else {
            let mut found: Option<Reader> = None;
            for r in readers {
                if r.get_name() == reader_name {
                    found = Some(r);
                    break;
                }
            }
            found.ok_or(format!("no reader found with name {}", reader_name))
        });

        //From the reader, we can connect to its smartcard.
        let card = try!(reader.connect_to(context, share, protocol) .map_err(|e|e.to_string()));

        Ok(Smartcard { reader: reader, card: card, buffer: Vec::new()})
    }
}

impl Read for Smartcard {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let to_read = buf.len();
        let buf_len = self.buffer.len();
        let can_read = if buf_len > to_read { to_read } else { buf_len };

        if can_read > 0 {
            for i in 0..can_read {
                // buf[i] = try!(self.buffer.pop().ok_or(io::ErrorKind::UnexpectedEof));
                if self.buffer.is_empty() {
                    return Err(io::Error::new(io::ErrorKind::UnexpectedEof, "Empty buffer"));
                }
                else {
                    buf[i] = self.buffer.remove(0);
                }
            }
        }

        Ok(can_read)
    }
}

impl Write for Smartcard {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let mut answer = try!(self.card.send_raw_command(buf, 256) .map_err(|e| io::Error::new(io::ErrorKind::Interrupted, e.to_string())));
        self.buffer.append(&mut answer);
        Ok(buf.len())
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

impl Connected for Smartcard {
    ///Update the Smartcard from this new config
    fn update_config(&mut self, _: &mut StoreNode) {

    }

    fn set_timeout(&mut self, _: Option<Duration>) -> Result<(), Error> {
        Ok(())
    }

    fn get_timeout(&self) -> Result<Option<Duration>, Error> {
        Ok(None)
    }
}
