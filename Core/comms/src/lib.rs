// @Author: ronan
// @Date:   15-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 23-12-2016
// @License: MIT



extern crate serial;
extern crate hconfig;
extern crate config_keys;
extern crate smartcard;
extern crate dimval;
extern crate serde;

pub extern crate reqwest;

#[macro_use]
extern crate log;

#[macro_use] extern crate failure;

pub mod connections;
pub mod server_process;
pub mod errors;
pub mod links;

pub use self::connections::comm_manager::CommManager;
pub use self::connections::link::Link;
pub use reqwest as comms_reqwest;