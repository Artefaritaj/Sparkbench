/*
 * File: errors.rs
 * Project: src
 * Created Date: Monday November 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 6th November 2018 9:00:49 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

#[derive(Fail, Debug)]
#[fail(display = "Failed to lock connection to {}: {}", conn_name, info)]
pub struct ConnectionLockFailed {
    conn_name: String,
    info: String
}

impl ConnectionLockFailed {
    pub fn new(conn_name: &str, info: &str) -> ConnectionLockFailed {
        ConnectionLockFailed { conn_name: conn_name.to_string(), info: info.to_string() }
    }
}

#[derive(Fail, Debug)]
#[fail(display = "Failed to connect to {}: {}", conn_name, info)]
pub struct ConnectionFailed {
    conn_name: String,
    info: String
}

impl ConnectionFailed {
    pub fn new(conn_name: &str, info: &str) -> ConnectionFailed {
        ConnectionFailed { conn_name: conn_name.to_string(), info: info.to_string() }
    }
}