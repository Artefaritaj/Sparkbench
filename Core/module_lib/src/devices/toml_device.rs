// @Author: ronan
// @Date:   14-09-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 15-12-2016
// @License: MIT

use sp_lib::interfaces::modules::{Module, ModuleBuilder, Cmd, CommandModule, VariableDefinition, VariableDescription, VariableDirection};
use sp_lib::interfaces::modules::variables::parser::parse_variables;
use sp_lib::interfaces::expressions::parser::parse_string_array;
use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::commands::connection_preparer::ConnectionPreparer;
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::process::ModuleState;

use config_keys::*;

use hconfig::store::{StoreNode, StoreKey, Data, OverwriteRule};
use hconfig::io::toml_file::*;

use comms::connections::CommManager;
use failure::Error;

use std::sync::{Arc, Mutex};
use std::collections::HashMap;

pub struct TOMLDevice {
    uid: String,
    mod_cmd: CommandModule,
    ///static config is constant through the life of the device
    static_config: StoreNode,
}

impl Module for TOMLDevice {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        let mut vec: Vec<VariableDescription> = Vec::new();
        let cmd_opt = self.mod_cmd.get_command(cmd_name);
        if let Some(cmd) = cmd_opt {
            for produced in cmd.get_produced_var_definitions() {
                // vec.push(VariableDescription::new(&produced, VariableDirection::Produced));
                vec.push(VariableDescription::with_datatypes(produced.get_name(), VariableDirection::Produced, vec![produced.get_ext_type().clone()]));
            }
            for required in cmd.get_required_var_definitions() {
                // vec.push(VariableDescription::new(&required, VariableDirection::Required));
                vec.push(VariableDescription::with_datatypes(required.get_name(), VariableDirection::Required, vec![required.get_ext_type().clone()]));
            }
        }
        vec
    }

    fn prepare(&mut self, comms: &mut CommManager) -> Result<(), Error> {
        let nb = self.mod_cmd.prepare_connections(comms)?;
        debug!("{} connections created.", nb);
        Ok(())
    }

    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}

impl CommandExecuter for TOMLDevice {
    fn execute_cmd(&mut self, cmd_name: &str, vars: NameMapper<Vec<Data>>, attributes: &HashMap<String, String>)
    -> Result<NameMapper<Vec<Data>>, Error> {
        self.mod_cmd.execute_cmd(cmd_name, vars, attributes)
    }
}

impl ModuleBuilder for TOMLDevice {
    fn build(config: &mut StoreNode, at: &StoreKey, args_node: &StoreNode, ow_rules: &Vec<OverwriteRule>, toml_mgr: &TOMLManager)
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        let toml_files_node = try!(args_node.get_node(&StoreKey::from(TOML_FILES)) .map_err(|_| format_err!("The {} node is required for a toml device", TOML_FILES)));
        let args = try!(parse_string_array(toml_files_node));

        // ********* LOAD config ***************
        for toml_filename in args {
            // trace!("Loading TOML {}", toml_filename);

            //extract toml file from the args
            let path = try!(toml_mgr.get_clone_by_name(&toml_filename).ok_or(format_err!("Module {} not found", toml_filename)));
            // debug!("New file in toml_device: {}", path.display());
            let toml_config = try!(toml_file_to_storenode(&path).map_err(|e|format_err!("Error while parsing toml {}: {}", toml_filename, e)));

            //add this config to instance
            config.merge_prefix(&toml_config, at);
        }

        //add label
        let label_key = at.clone().push(LABEL);
        let label_name = at.last_sub_key();
        config.store_update(label_key, Data::create_str(label_name));

        //update with overwrite rules from the experiment file
        if let Ok(exp_ow_rules) = TOMLDevice::get_overwrite_rules(args_node) {
            OverwriteRule::apply_to_store_node(&exp_ow_rules, config)?;
        }

        //Modify the config with overwrite rules from the user
        try!(OverwriteRule::apply_to_store_node(ow_rules, config));

        // trace!("Creating module {:?}", at);

        //Create Module
        let instance_node = try!(config.get_node(at).map_err(|e|format_err!("{}", e)));
        let device = try!(TOMLDevice::new(instance_node).map_err(|e|format_err!("{}", e)));

        Ok(Arc::new(Mutex::new(device)))
    }
}

impl TOMLDevice {

    fn get_overwrite_rules(args_node: &StoreNode) -> Result<Vec<OverwriteRule>, Error> {
        let ow_node = try!(args_node.get_node(&StoreKey::from(OVERWRITE_RULES)) .map_err(|_| format_err!("The {} node is needed for extracting overwrite rules", TOML_FILES)));
        
        let mut rules: Vec<OverwriteRule> = Vec::new();
        //each node represent an ow rule
        for node in ow_node.get_node_children().iter() {
            rules.push(OverwriteRule::from_node(node));
        }

        Ok(rules)
    }

    ///Create a device from an instance config
    ///instance node key must be uid
    pub fn new(instance_node: &StoreNode) -> Result<TOMLDevice, Error> {
        //extract uid
        let uid = instance_node.get_node_key();

        //init command module
        let mut mod_cmd = CommandModule::new();

        //extract variables
        let vars_key = StoreKey::from(VARS);//the key to vars in hierarchy
        let vars = match instance_node.get_node(&vars_key) {//select the node with vars
            Ok(vars_node) => try!(parse_variables(vars_node, instance_node).map_err(|e| format_err!("Failing to parse variables from {}: {}", uid, e))),//extract vars from var node
            Err(_) => Vec::new()//no variables defined
        };

        //create namemapper for variable definitions
        let mut defs : NameMapper<VariableDefinition> = NameMapper::new();
        for v in vars {
            defs.insert(v.get_name(), v.clone());
        }

        //extract conn commands and add to mapper + extract connections config
        let connections_key = StoreKey::from(CONNECTIONS);//the key to the list of connections
        match instance_node.get_node(&connections_key) {//select the node with the list of connections
            Ok(connections_node) => {
                for conn in connections_node.get_node_children() {//for each connection
                    try!(TOMLDevice::extract_and_add_command(&mut mod_cmd, &defs, instance_node, conn));
                }
            },
            Err(_) => {}//no connections defined
        }

        Ok(TOMLDevice{ uid: uid.to_string(), static_config: instance_node.clone(), mod_cmd: mod_cmd})
    }


    fn extract_and_add_command(mod_cmd: &mut CommandModule, vars: &NameMapper<VariableDefinition>, instance_node: &StoreNode, conn: &StoreNode)
    -> Result<(), Error> {
        //extract commands from this connection
        let conn_name = conn.get_node_key();

        let cmds_key = StoreKey::from(CMDS);
        let cmds_node = try!(conn.get_node(&cmds_key));//select the cmds node (all children are cmds)
        for cmd_node in cmds_node.get_node_children() {//for each cmd
            // debug!("    - {}", cmd_node.get_node_key());
            let cmd = try!(Cmd::create(conn_name, cmd_node, instance_node, vars));
            mod_cmd.add_command(cmd);//parse and add command to mapper
        }

        //extract connection config
        let conn_config_node = try!(conn.get_node(&StoreKey::from(CONNECTION_CONFIG)));
        mod_cmd.add_connection_config(conn_name, conn_config_node.clone());
        Ok(())
    }

    pub fn list_available_commands(&self) -> Vec<String> {
        self.mod_cmd.list_available_commands()
    }

    pub fn get_static_config(&self) -> &StoreNode {
        &self.static_config
    }

    pub fn get_module_command(&mut self) -> &mut CommandModule{
        &mut self.mod_cmd
    }
}
