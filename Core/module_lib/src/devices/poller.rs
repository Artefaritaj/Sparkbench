// @Author: Lashermes Ronan <ronan>
// @Date:   20-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 20-04-2017
// @License: MIT

use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::errors::*;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::{parse_static_string, parse_string_array};
use sp_lib::interfaces::process::ModuleState;
use sp_lib::helpers::timeout::check_timeout;

use hconfig::store::{StoreKey, StoreNode, OverwriteRule, Data, AtomicData};
use hconfig::io::toml_file::*;

use std::sync::{Arc, Mutex};
use std::collections::HashMap;
use std::{thread, time};

use comms::connections::CommManager;
use failure::Error;

use dimval::{DimVal, Prefix};

use config_keys::*;

use chrono::prelude::*;
use chrono;

use num::Zero;

use devices::toml_device::TOMLDevice;

const GEN_CMD: &'static str = "poll";

const OUT_VAR_NAME: &'static str = "output";

pub struct Poller {
    uid: String,
    device: TOMLDevice,
    in_vars: Vec<String>,
    out_var_name: Option<String>,
    poll_delay: time::Duration,
    error_dist: f64,
    poll_cmd: String
}

impl ModuleBuilder for Poller {
    //["poller", "SMC100", "getXpos", "100ms", ("output")]
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, toml_mgr: &TOMLManager) 
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        //toml files
        let toml_files_node = try!(args.get_node(&StoreKey::from(TOML_FILES)) .map_err(|_| format_err!("The {} node is required for a poller", TOML_FILES)));
        let toml_files_str = try!(parse_string_array(toml_files_node));

        let uid = at.last_sub_key().to_string();

        //optionnal output var node
        let out_var_name = args.get_value(&StoreKey::from(OUTPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();

        //get poll_delay
        let poll_delay_str = try!(args.get_value(&StoreKey::from(POLL_DELAY))
                                    .and_then(|d|d.to_string_result())
                                    .and_then(|s|parse_static_string(&s, config)));

        let poll_delay = match poll_delay_str.parse::<DimVal>()
                                            .and_then(|x|x.filter_unit_str("s"))
                                            .and_then(|x|x.to_f64_prefix(Prefix::Milli)) {
            Ok(millis) => {
                time::Duration::from_millis(millis as u64)
            },
            Err(_) => {
                time::Duration::from_millis(10)
            }
        };

        //let's authorize errors by specifying tolerable error distance
        let error_dist_f64 = match args.get_value(&StoreKey::from(ERROR_DISTANCE)) {
            Ok(Data::Singleton(AtomicData::String(ref s))) => {
                let error_dist_str = try!(parse_static_string(s, config));
                let error_dist = try!(error_dist_str.parse::<DimVal>());
                error_dist.to_f64()?
            },
            Ok(d) => {
                    d.to_atomic_data()?.to_f64()?
            },
            _ => 0f64
        };

        //get and create device
        let mod_node = at.clone().push(MODULE);
        // ********* LOAD config ***************
        for toml_filename in toml_files_str {
            //extract toml file from the args
            let path = try!(toml_mgr.get_clone_by_name(&toml_filename).ok_or(format_err!("Module {} not found", toml_filename)));
            let toml_config = try!(toml_file_to_storenode(&path));

            //add this config to instance
            config.merge_prefix(&toml_config, &mod_node);
        }

        //Create Module
        let instance_node = try!(config.get_node(&mod_node).map_err(|e|format_err!("{}", e)));
        let device = try!(TOMLDevice::new(instance_node).map_err(|e|format_err!("{}", e)));

        //get command
        let command_name = try!(args.get_value(&StoreKey::from(COMMAND))
                                        .and_then(|d|d.to_string_result())
                                        .and_then(|s|parse_static_string(&s, config)));

        //manage vars
        //the expected in vars are the one **PRODUCED** by the polling command and that we are trying to match
        //(input is the expected result of the polling command)
        //and the **REQUIRED** variables required by the polling command
        let var_descs = device.get_variable_descriptions(&command_name);
        let mut in_vars: Vec<String> = Vec::new();
        for vdesc in var_descs.iter() {
            in_vars.push(vdesc.get_var_name().to_string());
        }

        let poller = Poller {uid: uid, device: device, in_vars: in_vars,
            out_var_name: out_var_name, poll_delay: poll_delay, error_dist: error_dist_f64,
            poll_cmd: command_name};
        Ok(Arc::new(Mutex::new(poller)))
    }
}

impl Module for Poller {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == GEN_CMD {
            let mut res = vec![VariableDescription::new(self.get_out_var_name(),VariableDirection::Produced)];
            for v in self.in_vars.iter() {
                res.push(VariableDescription::new(v, VariableDirection::Required));
            }
            res
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, comms: &mut CommManager) -> Result<(), Error> {
        self.device.prepare(comms)
    }

    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}

impl CommandExecuter for Poller {
    fn execute_cmd(&mut self, cmd_name: &str, mut inputs: NameMapper<Vec<Data>>, attrs: &HashMap<String, String>) 
    -> Result<NameMapper<Vec<Data>>, Error> {
        if cmd_name == GEN_CMD {

            let to = match attrs.get(POLL_TIMEOUT) {
                Some(val_str) => {//can unwrap because UNITS_SYMBOLS is statically defined (but error not catch at compilation if any)
                    match val_str.parse::<DimVal>().and_then(|x|x.filter_unit_str("s")).and_then(|x|x.to_f64_prefix(Prefix::Milli)) {
                        Ok(millis) => {
                            chrono::Duration::milliseconds(millis as i64)
                        },
                        Err(e) => {
                            warn!("An error occured while parsing POLL_TIMEOUT attribute: {}", e.to_string());
                            chrono::Duration::milliseconds(10000)
                        }
                    }

                },
                None => chrono::Duration::milliseconds(10000)//stay with default
            };
            let end = Utc::now() + to;

            let mut result : NameMapper<Vec<Data>> = NameMapper::new();

            //extract the vector we are trying to match
            let mut to_match: HashMap<String, Data> = HashMap::new();
            for in_v in self.device.get_variable_descriptions(&self.poll_cmd).iter() {
                //get next data vector
                let d = match inputs.remove(in_v.get_var_name()).and_then(|mut x|x.pop()) {
                    Some(d) => d,
                    None => { return Err(VariableMissingError::new(in_v.get_var_name(), "Variable missing in poller").into());}
                };
                to_match.insert(in_v.get_var_name().to_string(), d);
            }

            let mut matched = false;
            while matched == false {
                try!(check_timeout(end, format!("Polling timeout for {}", cmd_name)));

                //poll command
                let mut data_res = try!(self.device.execute_cmd(&self.poll_cmd, inputs.clone(), attrs));
                //trace!("New poll result: {:?}, expecting {:?}", data_res, to_match);

                //check result
                matched = true;
                for (name, data2match) in to_match.iter() {
                    let dpoll = match data_res.remove(name).and_then(|mut x|x.pop()) {
                        Some(d) => d,
                        None => {
                            matched = false;
                            thread::sleep(self.poll_delay);
                            continue;
                        }
                    };

                    if self.error_dist.is_zero() {
                        if dpoll != *data2match {
                            matched = false;
                            thread::sleep(self.poll_delay);
                            continue;
                        }
                    }
                    else {
                        let distance = (try!(dpoll.to_atomic_data()?.to_f64()) - try!(data2match.to_atomic_data()?.to_f64())).abs();
                        if distance > self.error_dist {
                            matched = false;
                            thread::sleep(self.poll_delay);
                            continue;
                        }
                    }
                }
            }

            //trace!("Match: {:?}", to_match);

            result.insert(self.get_out_var_name(), vec![Data::Empty;1]);
            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}

impl Poller {
    pub fn get_out_var_name(&self) -> &str {
        match self.out_var_name {
            Some(ref n) => n,
            None => OUT_VAR_NAME
        }
    }
}
