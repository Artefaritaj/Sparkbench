/*
 * File: mod.rs
 * Project: xyzstage
 * Created Date: Monday November 5th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 9th November 2018 11:48:12 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

pub mod xyzstage;
mod xyzstage_device;