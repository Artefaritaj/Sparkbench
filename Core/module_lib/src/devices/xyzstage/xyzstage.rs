/*
 * File: xyz_stage.rs
 * Project: devices
 * Created Date: Wednesday October 17th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 27th September 2019 10:28:16 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use sp_lib::interfaces::modules::{Module, ModuleBuilder, VariableDescription, VariableDirection};
use sp_lib::interfaces::expressions::parser::{parse_static_string};
use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::process::ModuleState;
use sp_lib::errors::*;

use config_keys::*;

use nalgebra::Point3;

use hconfig::store::{StoreNode, StoreKey, Data, OverwriteRule};
use hconfig::io::toml_file::*;

use comms::connections::CommManager;

use failure::Error;

use dimval::{DimVal, PrimaryUnit};

use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use devices::xyzstage::xyzstage_device::{XYZStageDevice, Axis};

pub struct XYZStage {
    uid: String,
    conn_config: Option<StoreNode>,
    stage: XYZStageDevice,
    last_pos: Option<Point3<f64>>
}

impl Module for XYZStage {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == MOVE {
            vec![VariableDescription::new(X,VariableDirection::Produced),
                VariableDescription::new(Y,VariableDirection::Produced),
                VariableDescription::new(Z,VariableDirection::Produced),
                VariableDescription::new(X,VariableDirection::Required),
                VariableDescription::new(Y,VariableDirection::Required),
                VariableDescription::new(Z,VariableDirection::Required)]
        }
        else if cmd_name == "pos" {
            vec![VariableDescription::new(X,VariableDirection::Produced),
                VariableDescription::new(Y,VariableDirection::Produced),
                VariableDescription::new(Z,VariableDirection::Produced)]
        }
        else {
            Vec::new()
        }
    }

    fn prepare(&mut self, comms: &mut CommManager) -> Result<(), Error> {
        if let Some(ref conn) = self.conn_config {
            self.stage.connect(comms, conn)?;
            debug!("{} connection created.", 1);

            let ready = self.stage.is_ready_all()?;
            if ready == false {
                Err(format_err!("Cannot prepare {}: homing must be performed on the stage before (e.g. with xyzpilot).", self.uid))
            }
            else {
                trace!("XY stage ready.");
                Ok(())
            }
        }
        else {
            Err(format_err!("Cannot connect to {} without a connection name.", self.uid))
        }
    }

    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}

fn f64_meter_to_dimval(val: f64) -> Result<DimVal, Error> {
    DimVal::from_f64_parts_raw(val, 0, PrimaryUnit::Length.into())
}

impl CommandExecuter for XYZStage {
    fn execute_cmd(&mut self, cmd_name: &str, mut inputs: NameMapper<Vec<Data>>, _: &HashMap<String, String>)
    -> Result<NameMapper<Vec<Data>>, Error> {
        if cmd_name == MOVE {
            //move to next position
            //get vars
            let x = match inputs.remove(X).and_then(|mut x|x.pop()) {
                Some(d) => d,
                None => { return Err(VariableMissingError::new(X, &format!("in command {} of {} module", cmd_name, self.uid)).into()); }
            };

            let y = match inputs.remove(Y).and_then(|mut x|x.pop()) {
                Some(d) => d,
                None => { return Err(VariableMissingError::new(Y, &format!("in command {} of {} module", cmd_name, self.uid)).into()); }
            };

            let z = match inputs.remove(Z).and_then(|mut x|x.pop()) {
                Some(d) => d,
                None => { return Err(VariableMissingError::new(Z, &format!("in command {} of {} module", cmd_name, self.uid)).into()); }
            };

            let xf = x.to_atomic_data()?.to_f64()?;
            let yf = y.to_atomic_data()?.to_f64()?;
            let zf = z.to_atomic_data()?.to_f64()?;
            let newpos = Point3::new(xf, yf, zf);

            let do_move = if let Some(lastpos) = self.last_pos {
                lastpos != newpos
            }
            else {
                true
            };

            if do_move {
                self.stage.z_up()?;

                trace!("Pos = {:?},{:?},{:?}", x, y, z);

                let mut pos_hm: HashMap<Axis, f64> = HashMap::new();
                pos_hm.insert(Axis::X, xf);
                pos_hm.insert(Axis::Y, yf);
                // pos_hm.insert(Axis::Z, z.to_atomic_data()?.to_f64()?);

                self.stage.move_absolute_parallel(&pos_hm)?;

                // self.stage.z_down()?;
                pos_hm.clear();
                pos_hm.insert(Axis::Z, zf);
                self.stage.move_absolute_parallel(&pos_hm)?;
                self.last_pos = Some(newpos);
            }
            
            let mut result: NameMapper<Vec<Data>> = NameMapper::new();
            
            if let Some(last_pos) = self.last_pos {
                //get resulting position
                result.insert(X, vec![
                        Data::create_dim(
                            f64_meter_to_dimval(last_pos.x)?
                        )
                    ]);

                result.insert(Y, vec![
                        Data::create_dim(
                            f64_meter_to_dimval(last_pos.y)?
                        )
                    ]);

                result.insert(Z, vec![
                        Data::create_dim(
                            f64_meter_to_dimval(last_pos.z)?
                        )
                    ]);
            }
            else {
                return Err(format_err!("Position cannot be obtained for {}.", self.uid));
            }

            Ok(result)
        }
        else if cmd_name == "pos" {
            let mut result: NameMapper<Vec<Data>> = NameMapper::new();
            let pos = self.stage.get_absolute_position()?;
            trace!("pos is {:?}", pos);

            result.insert(X, vec![
                    Data::create_dim(
                        f64_meter_to_dimval(pos.x)?
                    )
                ]);

            result.insert(Y, vec![
                    Data::create_dim(
                        f64_meter_to_dimval(pos.y)?
                    )
                ]);

            result.insert(Z, vec![
                    Data::create_dim(
                        f64_meter_to_dimval(pos.z)?
                    )
                ]);

            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}

impl ModuleBuilder for XYZStage {
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, ow_rules: &Vec<OverwriteRule>, toml_mgr: &TOMLManager)
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        
        let uid = at.last_sub_key().to_string();

        // ********* LOAD config ***************
        let stage_name = args.get_value(&StoreKey::from(CONTROLLER))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .unwrap_or(format!("SMC100"));

        //extract toml file from the args
        let path = try!(toml_mgr.get_clone_by_name(&stage_name).ok_or(format_err!("Module {} not found", stage_name)));
        let toml_config = try!(toml_file_to_storenode(&path).map_err(|e|format_err!("Error while parsing toml {}: {}", stage_name, e)));

        // trace!("{}.Controller config: {:?}", uid, toml_config);

        //add this config to instance
        config.merge_prefix(&toml_config, at);
           
        //update with overwrite rules from the experiment file
        if let Ok(exp_ow_rules) = XYZStage::get_overwrite_rules(args) {
            OverwriteRule::apply_to_store_node(&exp_ow_rules, config)?;
        }

        //Modify the config with overwrite rules from the user
        try!(OverwriteRule::apply_to_store_node(ow_rules, config));

        let mut conn_config: Option<StoreNode> = None;
        //extract conn commands and add to mapper + extract connections config
        let instance_node = config.get_node(at).map_err(|e|format_err!("{}", e))?;
        match instance_node.get_node(&StoreKey::from(CONNECTIONS)) {//select the node with the list of connections
            Ok(connections_node) => {
                for conn in connections_node.get_node_children() {//for each connection
                    conn_config = Some(conn.get_node(&StoreKey::from(CONNECTION_CONFIG))?.clone());
                    break;//keep only first connection
                }
            },
            Err(_) => {}//no connections defined
        }

        let stage = XYZStageDevice::new(instance_node)?;

        //Create Module
        let device = XYZStage {
            uid,
            conn_config,
            stage,
            last_pos: None
        };

        Ok(Arc::new(Mutex::new(device)))
    }
}

impl XYZStage {

    fn get_overwrite_rules(args_node: &StoreNode) -> Result<Vec<OverwriteRule>, Error> {
        let ow_node = try!(args_node.get_node(&StoreKey::from(OVERWRITE_RULES)) .map_err(|_| format_err!("The {} node is needed for extracting overwrite rules", TOML_FILES)));
        
        let mut rules: Vec<OverwriteRule> = Vec::new();
        //each node represent an ow rule
        for node in ow_node.get_node_children().iter() {
            rules.push(OverwriteRule::from_node(node));
        }

        Ok(rules)
    }
}
