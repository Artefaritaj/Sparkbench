// @Author: Lashermes Ronan <ronan>
// @Date:   23-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 23-02-2017
// @License: MIT

use hconfig::store::{StoreKey,StoreNode,OverwriteRule};
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::modules::{Module, ModuleBuilder};

use std::sync::{Arc, Mutex};

use devices::*;
use compute::*;
use io::*;

use failure::Error;

///Create an instanced module and update configuration.
///[at] should be the store key for the instance (id is the last level key)
pub fn create_instanced_module(config: &mut StoreNode, at: &StoreKey, module_type_str: &str, args: &StoreNode, ow_rules: &Vec<OverwriteRule>, toml_mgr: &TOMLManager)
        -> Result<Arc<Mutex<dyn Module>>, Error> {
    match module_type_str {
        "toml_device"       => TOMLDevice::build(config, at, args, ow_rules, toml_mgr),
        "xyzstage"          => XYZStage::build(config, at, args, ow_rules, toml_mgr),
        "mapper"            => Mapper::build(config, at, args, ow_rules, toml_mgr),
        "vec_u8_generator"  => VecU8Generator::build(config, at, args, ow_rules, toml_mgr),
        "replicator"        => Replicator::build(config, at, args, ow_rules, toml_mgr),
        "bin_saver"         => BinSaver::build(config, at, args, ow_rules, toml_mgr),
        "batch_saver"       => BatchSaver::build(config, at, args, ow_rules, toml_mgr),
        "text_saver"        => TextSaver::build(config, at, args, ow_rules, toml_mgr),
        "splitter"          => Splitter::build(config, at, args, ow_rules, toml_mgr),
        "aes"               => AES::build(config, at, args, ow_rules, toml_mgr),
        "diff"              => Diff::build(config, at, args, ow_rules, toml_mgr),
        "console_display"   => ConsoleDisplay::build(config, at, args, ow_rules, toml_mgr),
        "stepper"           => Stepper::build(config, at, args, ow_rules, toml_mgr),
        "poller"            => Poller::build(config, at, args, ow_rules, toml_mgr),
        "identity"          => Identity::build(config, at, args, ow_rules, toml_mgr),
        "map_saver"         => MapSaver::build(config, at, args, ow_rules, toml_mgr),
        "threshold_counter" => ThresholdCounter::build(config, at, args, ow_rules, toml_mgr),
        "delay_controller"  => DelayController::build(config, at, args, ow_rules, toml_mgr),
        "ip_socket"         => IpSocket::build(config, at, args, ow_rules, toml_mgr),
        "ip_post"           => IpPost::build(config, at, args, ow_rules, toml_mgr),
        "ip_feedback"       => IpFeedback::build(config, at, args, ow_rules, toml_mgr),
        "aes_nueva_gen"     => AESNuevaGen::build(config, at, args, ow_rules, toml_mgr),
        "nueva"             => Nueva::build(config, at, args, ow_rules, toml_mgr),
        _ => Err(format_err!("{} is not a known module type", module_type_str))
    }
}
