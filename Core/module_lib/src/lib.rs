// @Author: ronan
// @Date:   03-08-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 10-11-2016
// @License: MIT



extern crate sp_lib;
extern crate algo_lib;
extern crate hconfig;
extern crate range;
// extern crate range;
extern crate byteorder;
extern crate comms;
extern crate config_keys;
extern crate bincode;
extern crate serde;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate log;
#[macro_use] extern crate lazy_static;
extern crate chrono;
extern crate dimval;
extern crate num;
// #[macro_use] extern crate failure_derive;
#[macro_use] extern crate failure;
extern crate rand;
extern crate nalgebra;

pub mod io;
pub mod devices;
pub mod compute;
pub mod helpers;
pub mod errors;