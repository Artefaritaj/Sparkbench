// @Author: Lashermes Ronan <ronan>
// @Date:   10-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 10-04-2017
// @License: MIT

// @Author: ronan
// @Date:   20-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::errors::*;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::parse_static_string;
use sp_lib::interfaces::process::ModuleState;

use hconfig::store::{StoreKey,StoreNode,OverwriteRule,Data, CompoundDataType};

use config_keys::*;

use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use comms::connections::CommManager;
use failure::Error;

const GEN_CMD: &'static str = "display";
const IN_VAR_NAME: &'static str = "input";

#[derive(Debug)]
pub struct ConsoleDisplay {
    uid: String,
    announcement_text: Option<String>,
    in_var_name: Option<String>,
    var_format: CompoundDataType
}

impl ModuleBuilder for ConsoleDisplay {
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager)
    -> Result<Arc<Mutex<dyn Module>>, Error> {

        let uid = at.last_sub_key().to_string();

        let announcement_text = args.get_value(&StoreKey::from(TEXT))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();

        //optionnal input var node
        let in_var_name = args.get_value(&StoreKey::from(INPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();

        let var_format = args.get_value(&StoreKey::from(FORMAT))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|CompoundDataType::parse(&s))?;

        let module = ConsoleDisplay { uid: uid, announcement_text: announcement_text, in_var_name: in_var_name, var_format: var_format };

        Ok(Arc::new(Mutex::new(module)))
    }
}

impl Module for ConsoleDisplay {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == GEN_CMD {
            vec![VariableDescription::new(self.get_in_var_name(),VariableDirection::Required)]
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }

    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}

impl ConsoleDisplay {
    pub fn get_in_var_name(&self) -> &str {
        match self.in_var_name {
            Some(ref s) => s,
            None => IN_VAR_NAME
        }
    }
}

impl CommandExecuter for ConsoleDisplay {
    fn execute_cmd(&mut self, cmd_name: &str, inputs: NameMapper<Vec<Data>>, _: &HashMap<String, String>)
    -> Result<NameMapper<Vec<Data>>, Error> {
        if cmd_name == GEN_CMD {
            match inputs.get(self.get_in_var_name()) {
                Some(ref vec) => {
                    let prefix = match self.announcement_text {
                        Some(ref t) => t,
                        None => ""
                    };

                    for d in vec.iter() {
                        let formatted_d = d.convert(&self.var_format)?;
                        println!("{}{}", prefix, formatted_d.to_string_result()?);
                    }
                },
                None => {}//error ?
            }

            let result : NameMapper<Vec<Data>> = NameMapper::new();
            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}
