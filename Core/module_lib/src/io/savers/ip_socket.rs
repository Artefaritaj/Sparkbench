// @Author: Lashermes Ronan <ronan>
// @Date:   16-03-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 16-03-2017
// @License: MIT

use std::collections::HashMap;
use std::io::{Write};
use std::sync::{Mutex, Arc};

use config_keys::*;
use hconfig::store::*;
use comms::{CommManager, Link};

use sp_lib::interfaces::process::ModuleState;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::expressions::parser::{parse_string_array};
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::helpers::NameMapper;
use sp_lib::errors::*;
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;

use failure::Error;


const SAVE_CMD: &'static str = "send";


pub struct IpSocket {
    uid: String,
    connection_config_node: StoreNode,
    link: Option<Link>,
    in_vars_name: Vec<String>,
    in_vars_format: Vec<CompoundDataType>,
}

impl ModuleBuilder for IpSocket {

    ///0 => path, 1 => file_creation_mode
    fn build(_: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager) 
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        let uid = at.last_sub_key().to_string();

        let config_node = args.get_node(&StoreKey::from(CONNECTION_CONFIG))?;

        //var to save node
        let in_vars_node = args.get_node(&StoreKey::from(VARS))?;
        let in_vars = parse_string_array(in_vars_node)?;

        let in_vars_format_node = args.get_node(&StoreKey::from(FORMAT))?;
        let in_vars_format_res: Result<Vec<CompoundDataType>, Error> = parse_string_array(in_vars_format_node)?
                                                    .iter()
                                                    .map(|s|CompoundDataType::parse(s))
                                                    .collect();

        
        let poster = IpSocket { uid: uid, connection_config_node: config_node.clone(), in_vars_name: in_vars, in_vars_format: in_vars_format_res?, link: None };

        Ok(Arc::new(Mutex::new(poster)))
    }
}

impl Module for IpSocket {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == SAVE_CMD {
            let mut var_descs: Vec<VariableDescription> = Vec::new();

            for vname in self.in_vars_name.iter() {
                var_descs.push(VariableDescription::new(vname, VariableDirection::Required));
            }

            var_descs
        }
        else {
            Vec::new()
        }
    }

    ///create the file handle
    fn prepare(&mut self, comms: &mut CommManager) -> Result<(), Error> {
        self.link = Some(comms.connect_to(&self.connection_config_node)?);
        Ok(())
    }


    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }


    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}


impl CommandExecuter for IpSocket {
    fn execute_cmd(&mut self, cmd_name: &str, mut ivars: NameMapper<Vec<Data>>, _: &HashMap<String, String>) 
    -> Result<NameMapper<Vec<Data>>, Error> {
        if cmd_name == SAVE_CMD {


            let mut line_string = String::new();

            for (i, vname) in self.in_vars_name.iter().enumerate() {
                let d = try!(ivars.remove(vname)
                                    .and_then(|mut x|x.pop())
                                    .ok_or(VariableMissingError::new(vname, &format!("Variable to save must be present"))));
                let format = self.in_vars_format.get(i).ok_or(format_err!("No format defined for var {}", vname))?;
                let formatted_d = d.convert(format)?;
                let dstring = formatted_d.to_string_result()?;
                // let dstring = if attrs.contains_key(HUMAN) {//human readable format
                //     d.to_string()
                // }
                // else { //machine readable format
                //     d.to_atomic_data()?.to_f64().and_then(|d2|Ok(d2.to_string())).unwrap_or(d.to_string())
                // };

                if i != 0 {
                    line_string = format!("{}, \"{}\": \"{}\"", line_string, vname, dstring);
                }
                else {
                    line_string = format!("\"{}\": \"{}\"", vname, dstring);
                }

            }

            if let Some(ref l) = self.link {
                let mut link_guard = l.lock() .map_err(|e|format_err!("Cannot lock link: {}", e))?;
                try!(link_guard.write(&line_string.as_bytes()) .map_err(|e| format_err!("Failed to save data: {}", e)));
            }
            else {
                warn!("No connection for command {}", cmd_name)
            }

            Ok(NameMapper::new())//no output
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}
