// @Author: Lashermes Ronan <ronan>
// @Date:   03-05-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 03-05-2017
// @License: MIT

// @Author: Lashermes Ronan <ronan>
// @Date:   16-03-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 16-03-2017
// @License: MIT

use std::fs::{File, OpenOptions};
use std::path::Path;
use std::collections::HashMap;
use std::io::{Write, Seek, SeekFrom};
use std::sync::{Mutex, Arc};

use config_keys::*;
use hconfig::store::*;
use comms::CommManager;

use sp_lib::interfaces::process::ModuleState;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::expressions::parser::parse_static_string;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::helpers::NameMapper;
use sp_lib::errors::*;
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::interfaces::expressions::parser::parse_string_array;

use bincode;
use failure::Error;

use chrono::prelude::*;

const SAVE_CMD: &'static str = "save";
const EOL: &'static str = "\n";

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Serialize,Deserialize)]
enum FileCreationMode {
    Overwrite,
    Append,
    Rename
}

impl From<String> for FileCreationMode {
    fn from(s: String) -> FileCreationMode {
        match s.as_ref() {
            OVERWRITE => FileCreationMode::Overwrite,
            APPEND => FileCreationMode::Append,
            RENAME => FileCreationMode::Rename,
            e => {
                warn!("FileCreationMode error: {}", e);
                FileCreationMode::Rename
            }
        }
    }
}

#[derive(Debug)]
struct PreparationParameters {
    path: String,
    creation: FileCreationMode
}

///Dummy struct used to signal that a save occured
#[derive(Serialize,Deserialize,Debug)]
struct MapSaverSaveState {
    saved: bool
}


#[derive(Debug)]
pub struct MapSaver {
    uid: String,
    file: Option<File>,
    in_vars_name: Vec<String>,
    in_vars_format: Vec<CompoundDataType>,
    prep_params: PreparationParameters,
    separator: String
}

impl ModuleBuilder for MapSaver {

    ///0 => path, 1 => file_creation_mode
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager)
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        let uid = at.last_sub_key().to_string();
        //path
        let path = try!(args.get_value(&StoreKey::from(PATH))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config)));

        //creation mode
        let creation_str = args.get_value(&StoreKey::from(CREATION_MODE))
                                    .and_then(|d|d.to_string_result())
                                    .and_then(|s|parse_static_string(&s, config))
                                    .unwrap_or(APPEND.to_string());

        let creation = FileCreationMode::from(creation_str);
        let prep_params = PreparationParameters { path: path, creation: creation };

        //var to save node
        let in_vars_node = args.get_node(&StoreKey::from(VARS))?;
        let in_vars = parse_string_array(in_vars_node)?;

        let in_vars_format_node = args.get_node(&StoreKey::from(FORMAT))?;
        let in_vars_format_res: Result<Vec<CompoundDataType>, Error> = parse_string_array(in_vars_format_node)?
                                                    .iter()
                                                    .map(|s|CompoundDataType::parse(s))
                                                    .collect();

        //optional custom separator
        let sep = args.get_value(&StoreKey::from("separator"))
                        .and_then(|d|d.to_string_result())
                        .unwrap_or(format!(","));

        let saver = MapSaver { uid: uid, file: None, prep_params: prep_params, in_vars_name: in_vars, in_vars_format: in_vars_format_res?, separator: sep };

        Ok(Arc::new(Mutex::new(saver)))
    }
}

impl Module for MapSaver {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == SAVE_CMD {
            let mut var_descs: Vec<VariableDescription> = Vec::new();

            for vname in self.in_vars_name.iter() {
                var_descs.push(VariableDescription::new(vname, VariableDirection::Required));
            }

            var_descs
        }
        else {
            Vec::new()
        }
    }



    ///create the file handle
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {

        let local: DateTime<Local> = Local::now();
        let path_rename = format!("{}.{}", self.prep_params.path, local.to_rfc2822());
        let path_clone = self.prep_params.path.clone();
        let path = match self.prep_params.creation {
            FileCreationMode::Rename => {
                Path::new(&path_rename)
            },
            _ => Path::new(&path_clone)
        };


        if path.exists() {
            if self.prep_params.creation == FileCreationMode::Overwrite {//overwrite
                let f = try!(OpenOptions::new().write(true).truncate(true).create(true).open(path) .map_err(|e|format_err!("{}", e)));
                self.file = Some(f);
                try!(self.write_header());
            }
            else {//Append
                let mut f = try!(OpenOptions::new().write(true).read(true).open(path) .map_err(|e|format_err!("{}", e)));
                //reset cursor to end of file
                try!(f.seek(SeekFrom::End(0)) .map_err(|e|format_err!("{}", e)));
                self.file = Some(f);
            }
        }
        else {//rename
            let f = try!(File::create(path) .map_err(|e|format_err!("{}", e)));
            self.file = Some(f);
            try!(self.write_header());

            self.prep_params.creation = FileCreationMode::Append;
            self.prep_params.path = path_rename.clone();
        }




        Ok(())
    }


    fn load_state(&mut self, prev_state: Option<&ModuleState>) -> Result<(), Error> {
        match prev_state {
            Some(astate) => {
                let data_vec = astate.get_data();
                if data_vec.len() > 0 {
                    let state: MapSaverSaveState = try!(bincode::deserialize(&data_vec).map_err(|e|format_err!("{}", e)));
                    trace!("Map saver load state: {:?}", state);
                    self.prep_params.creation = FileCreationMode::Append;
                }
                Ok(())
            },
            None => {
                Ok(())
            }
        }
    }


    fn save_state(&mut self) -> Result<ModuleState, Error> {
        let state: MapSaverSaveState = MapSaverSaveState { saved: true };
        let data_vec: Vec<u8> = try!(bincode::serialize(&state).map_err(|e|format_err!("{}", e)));
        Ok(ModuleState::new(self.get_uid(), data_vec))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}

impl MapSaver {
    fn write_header(&mut self) -> Result<(), Error> {
        //write first line
        match self.file {
            Some(ref mut f) => {
                let mut header = String::new();
                for (i,vname) in self.in_vars_name.iter().enumerate() {
                    if i == 0 {
                        header = vname.to_string();
                    }
                    else {
                        header = format!("{}{}{}", header, self.separator, vname);
                    }
                }

                header += EOL;

                try!(f.write(&header.as_bytes()) .map_err(|e| format_err!("{}", e)));
            },
            None => {}
        }

        Ok(())
    }
}

impl CommandExecuter for MapSaver {
    fn execute_cmd(&mut self, cmd_name: &str, mut ivars: NameMapper<Vec<Data>>, _: &HashMap<String, String>)
    -> Result<NameMapper<Vec<Data>>, Error> {
        if cmd_name == SAVE_CMD {

            let mut line_string = String::new();

            for (i, vname) in self.in_vars_name.iter().enumerate() {
                let d = try!(ivars.remove(vname)
                                    .and_then(|mut x|x.pop())
                                    .ok_or(VariableMissingError::new(vname, &format!("Variable to save must be present"))));
                let format = self.in_vars_format.get(i).ok_or(format_err!("No format defined for var {}", vname))?;
                let formatted_d = d.convert(format)?;
                let dstring = formatted_d.to_string_result()?;
                // let dstring = if attrs.contains_key(HUMAN) {//human readable format
                //     d.to_string()
                // }
                // else { //machine readable format
                //     d.to_atomic_data()?.to_f64().and_then(|d2|Ok(d2.to_string())).unwrap_or(d.to_string())
                // };

                if i != 0 {
                    line_string = format!("{}{}{}", line_string, self.separator, dstring);
                }
                else {
                    line_string = dstring;
                }

            }

            line_string += EOL;

            match self.file {
                Some(ref mut f) => {
                    try!(f.write(&line_string.as_bytes()) .map_err(|e| format_err!("Failed to save data: {}", e)));
                },
                None => { return Err(format_err!("The file for saving does not exists")); }
            }

            Ok(NameMapper::new())//no output
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}
