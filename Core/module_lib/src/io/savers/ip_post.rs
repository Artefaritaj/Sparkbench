// @Author: Lashermes Ronan <ronan>
// @Date:   16-03-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 16-03-2017
// @License: MIT

use std::collections::HashMap;
use std::sync::{Mutex, Arc};

use config_keys::*;
use hconfig::store::*;
use comms::{CommManager, comms_reqwest};

use sp_lib::interfaces::process::ModuleState;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::expressions::parser::{parse_string_array, parse_static_string};
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::helpers::NameMapper;
use sp_lib::errors::*;
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;

use failure::Error;


const SAVE_CMD: &'static str = "send";

#[derive(Debug)]
pub struct IpPost {
    uid: String,
    url: String,
    in_vars_name: Vec<String>,
    in_vars_format: Vec<CompoundDataType>,
}

impl ModuleBuilder for IpPost {

    ///0 => path, 1 => file_creation_mode
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager) 
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        let uid = at.last_sub_key().to_string();

        let url = try!(args.get_value(&StoreKey::from(URL))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config)));

        //var to save node
        let in_vars_node = args.get_node(&StoreKey::from(VARS))?;
        let in_vars = parse_string_array(in_vars_node)?;

        let in_vars_format_node = args.get_node(&StoreKey::from(FORMAT))?;
        let in_vars_format_res: Result<Vec<CompoundDataType>, Error> = parse_string_array(in_vars_format_node)?
                                                    .iter()
                                                    .map(|s|CompoundDataType::parse(s))
                                                    .collect();

        
        let poster = IpPost { uid: uid, url: url, in_vars_name: in_vars, in_vars_format: in_vars_format_res? };

        Ok(Arc::new(Mutex::new(poster)))
    }
}

impl Module for IpPost {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == SAVE_CMD {
            let mut var_descs: Vec<VariableDescription> = Vec::new();

            for vname in self.in_vars_name.iter() {
                var_descs.push(VariableDescription::new(vname, VariableDirection::Required));
            }

            var_descs
        }
        else {
            Vec::new()
        }
    }

    ///create the file handle
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }


    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }


    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}


impl CommandExecuter for IpPost {
    fn execute_cmd(&mut self, cmd_name: &str, mut ivars: NameMapper<Vec<Data>>, _: &HashMap<String, String>) 
    -> Result<NameMapper<Vec<Data>>, Error> {
        if cmd_name == SAVE_CMD {


            let mut vars_hm: HashMap<String, String> = HashMap::new();

            for (i, vname) in self.in_vars_name.iter().enumerate() {
                let d = try!(ivars.remove(vname)
                                    .and_then(|mut x|x.pop())
                                    .ok_or(VariableMissingError::new(vname, &format!("Variable to save must be present"))));
                let format = self.in_vars_format.get(i).ok_or(format_err!("No format defined for var {}", vname))?;
                let formatted_d = d.convert(format)?;
                let dstring = formatted_d.to_string_result()?;
                vars_hm.insert(vname.to_string(), dstring);
            }

            
            let client = comms_reqwest::Client::new();
            let _ = client.post(&self.url)
                .json(&vars_hm)
                .send()?;

            Ok(NameMapper::new())//no output
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}
