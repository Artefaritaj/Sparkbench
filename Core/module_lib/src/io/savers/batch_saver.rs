/*
 * File: bin_saver copy.rs
 * Project: savers
 * Created Date: Monday October 21st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 21st October 2019 2:52:45 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */
// @Author: Lashermes Ronan <ronan>
// @Date:   16-03-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 16-03-2017
// @License: MIT

use std::fs::{File, OpenOptions};
use std::path::Path;
use std::collections::HashMap;
use std::io::{Write, Seek, SeekFrom};
use std::sync::{Mutex, Arc};

// use byteorder::{LittleEndian, ReadBytesExt};

use config_keys::*;
use hconfig::store::*;
use hconfig::io::byte_convert::ToBytes;
use comms::CommManager;

use sp_lib::interfaces::process::ModuleState;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::expressions::parser::parse_static_string;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::helpers::NameMapper;
use sp_lib::errors::*;
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;

// use chrono::prelude::*;
use failure::Error;

use bincode;

const SAVE_CMD: &'static str = "save";
const INPUT_VAR: &'static str = "input";



#[derive(Debug)]
struct PreparationParameters {
    basepath: String,
    batch_size: u32
}

#[derive(Serialize,Deserialize,Debug)]
struct BatchSaverSaveState {
    count: u32,
    len: Option<u32>
}


#[derive(Debug)]
pub struct BatchSaver {
    uid: String,
    file: Option<File>,
    vec_len: Option<u32>,
    vec_count: u32,
    in_var_name: Option<String>,
    prep_params: PreparationParameters
}

impl ModuleBuilder for BatchSaver {

    ///0 => path, 1 => file_creation_mode
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager) 
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        let uid = at.last_sub_key().to_string();

        //path
        let path = try!(args.get_value(&StoreKey::from(PATH))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config)));
                                
        //creation mode
        let batch_str = args.get_value(&StoreKey::from(BATCH))
                                    .and_then(|d|d.to_string_result())
                                    .and_then(|s|parse_static_string(&s, config))?;
        let batch_size = batch_str.parse::<u32>()?;

        let prep_params = PreparationParameters { basepath: path, batch_size };

        //optionnal input var node
        let in_var_name = args.get_value(&StoreKey::from(INPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();

        let saver = BatchSaver { uid: uid, file: None, prep_params: prep_params, vec_len: None, in_var_name: in_var_name, vec_count: 0 };

        Ok(Arc::new(Mutex::new(saver)))
    }
}

impl Module for BatchSaver {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == SAVE_CMD {
            vec![VariableDescription::new(self.get_in_var_name(),VariableDirection::Required)]
        }
        else {
            Vec::new()
        }
    }

    ///create the file handle
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {

        let path_str = format!("{}.{}", self.prep_params.basepath, self.get_batch_index());
        let path = Path::new(&path_str);

        //reserve space for header
        let header_space = vec![0u8;8];

        if path.exists() {
            let mut f = try!(OpenOptions::new().write(true).truncate(true).create(true).open(path) .map_err(|e|format_err!("{}", e)));
            try!(f.write(&header_space) .map_err(|e| format_err!("Failed to reserve space for header: {}", e.to_string())));
            self.file = Some(f);
            
        }
        else {//rename
            let mut f = try!(File::create(path) .map_err(|e|format_err!("{}", e)));
            try!(f.write(&header_space) .map_err(|e| format_err!("Failed to reserve space for header: {}", e.to_string())));
            self.file = Some(f);
        }

        Ok(())
    }


    fn load_state(&mut self, prev_state: Option<&ModuleState>) -> Result<(), Error> {
        match prev_state {
            Some(astate) => {
                let data_vec = astate.get_data();
                if data_vec.len() > 0 {
                    let state: BatchSaverSaveState = try!(bincode::deserialize(&data_vec).map_err(|e|format_err!("{}", e)));
                    self.vec_count = state.count;
                    self.vec_len = state.len;
                }
                Ok(())
            },
            None => {
                Ok(())
            }
        }
    }


    fn save_state(&mut self) -> Result<ModuleState, Error> {
        let state = BatchSaverSaveState { count: self.vec_count, len: self.vec_len };
        let data_vec: Vec<u8> = try!(bincode::serialize(&state).map_err(|e|format_err!("{}", e)));
        Ok(ModuleState::new(self.get_uid(), data_vec))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}



impl Drop for BatchSaver {
    fn drop(&mut self) {
        let count = if self.vec_count == self.prep_params.batch_size {
            self.vec_count
        }
        else {
            self.vec_count % self.prep_params.batch_size
        };
        let _ = self.finish_file(count);
    }
}

impl BatchSaver {
    pub fn get_in_var_name(&self) -> &str {
        match self.in_var_name {
            Some(ref s) => s,
            None => INPUT_VAR
        }
    }

    fn get_batch_index(&self) -> u32 {
        self.vec_count % self.prep_params.batch_size
    }

    fn finish_file(&mut self, count: u32) -> Result<(), Error> {
        //write header
        match self.file {
            Some(ref mut f) => {
                let _ = f.seek(SeekFrom::Start(0));

                //write length
                match self.vec_len {
                    Some(len) => {
                        match <u32 as ToBytes>::to_bytes(&len) {
                            Ok(buf) => {
                                let _ = f.write(&buf);
                            },
                            Err(_) => { return Err(format_err!("Cannot write vector length to header.")); }
                        }

                    },
                    None => {}//do nothing
                }

                //write count
                match <u32 as ToBytes>::to_bytes(&count) {
                    Ok(buf) => {
                        let _ = f.write(&buf);
                    },
                    Err(e) => { return Err(format_err!("Error while writing trace count: {}", e.to_string()))}
                }

                //sync_all
                let _ = f.sync_all();
            },
            None => {}//do nothing
        }

        Ok(())
    }
}

impl CommandExecuter for BatchSaver {
    fn execute_cmd(&mut self, cmd_name: &str, ivars: NameMapper<Vec<Data>>, _: &HashMap<String, String>) 
    -> Result<NameMapper<Vec<Data>>, Error> {
        if cmd_name == SAVE_CMD {
            let to_save = try!(ivars.get(self.get_in_var_name()).ok_or(format_err!("No input data to save")));
            for d in to_save.iter() {
                let new_batch_index = (self.vec_count + 1) % self.prep_params.batch_size;
                //should we open a new file ?
                if self.get_batch_index() != new_batch_index {
                    //close last file
                    self.finish_file(self.prep_params.batch_size)?;
                    //then open a new file
                    let path_str = format!("{}.{}", self.prep_params.basepath, new_batch_index);
                    let path = Path::new(&path_str);

                    //reserve space for header
                    let header_space = vec![0u8;8];

                    if path.exists() {
                        let mut f = try!(OpenOptions::new().write(true).truncate(true).create(true).open(path) .map_err(|e|format_err!("{}", e)));
                        try!(f.write(&header_space) .map_err(|e| format_err!("Failed to reserve space for header: {}", e.to_string())));
                        self.file = Some(f);
                        
                    }
                    else {//rename
                        let mut f = try!(File::create(path) .map_err(|e|format_err!("{}", e)));
                        try!(f.write(&header_space) .map_err(|e| format_err!("Failed to reserve space for header: {}", e.to_string())));
                        self.file = Some(f);
                    }
                }


                match self.file {
                    Some(ref mut f) => {
                        let vec_len = d.len() as u32;//get length of this vector
                        match self.vec_len {
                            Some(len) => {
                                if len != vec_len {//check if length is equal to other lengths
                                    return Err(format_err!("Incorrect vector length to be saved ({} instead of {})", vec_len, len));
                                }
                            },
                            None => {//if first vector, save length
                                self.vec_len = Some(vec_len);
                            }
                        }

                        try!(f.write(&try!(d.to_bytes())) .map_err(|e| format_err!("Failed to save data: {}", e.to_string())));
                        self.vec_count += 1;
                    },
                    None => { return Err(format_err!("The file for saving does not exists")); }
                }

            }

            Ok(NameMapper::new())//no output
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}
