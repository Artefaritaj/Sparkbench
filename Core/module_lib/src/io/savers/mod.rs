// @Author: Lashermes Ronan <ronan>
// @Date:   16-03-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 16-03-2017
// @License: MIT

pub mod bin_saver;
pub mod text_saver;
pub mod map_saver;
pub mod ip_socket;
pub mod ip_post;
pub mod ip_feedback;
pub mod batch_saver;