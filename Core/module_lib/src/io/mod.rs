// @Author: ronan
// @Date:   03-08-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 15-09-2016
// @License: GPL



// pub mod loaders;
pub mod savers;
pub mod console_display;

pub use self::savers::bin_saver::BinSaver;
pub use self::savers::batch_saver::BatchSaver;
pub use self::savers::text_saver::TextSaver;
pub use self::console_display::ConsoleDisplay;
pub use self::savers::map_saver::MapSaver;
pub use self::savers::ip_socket::IpSocket;
pub use self::savers::ip_post::IpPost;
pub use self::savers::ip_feedback::IpFeedback;