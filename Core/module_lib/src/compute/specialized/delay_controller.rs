/*
 * Author:   ronan.lashermes@inria.fr
 * Creation: Thu Dec 07 2017
 *
 * Copyright (c) 2017 INRIA
 */


use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::errors::*;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::parse_static_string;
use sp_lib::interfaces::process::ModuleState;

use hconfig::store::{StoreKey,StoreNode,OverwriteRule,Data/*,CompoundDataType,DataType*/};
use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use comms::connections::CommManager;
use failure::Error;
use dimval::DimVal;

// use config_keys::*;

const ACT_CMD: &'static str = "compute";

//inputs
const USER_DELAY: &'static str = "user_delay";
const BURST_COUNT: &'static str = "burst_count";
const PERIOD: &str = "period";

//outputs
const DELAY_COARSE: &str = "delay_coarse";
const DELAY_FINE: &'static str = "delay_fine";

//config
const COARSE_MIN: &str = "coarse_min";

#[derive(Debug)]
pub struct DelayController {
    uid: String,
    coarse_min: DimVal
}

impl ModuleBuilder for DelayController {
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager) 
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        let uid = at.last_sub_key().to_string();

        //optionnal input var node
        let coarse_min = args.get_value(&StoreKey::from(COARSE_MIN))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .and_then(|s|s.parse::<DimVal>())
                                .map_err(|_|format_err!("No coarse minimum value specified for this delay controller."))?;


        let dc = DelayController { uid, coarse_min };
        Ok(Arc::new(Mutex::new(dc)))
    }
}

impl Module for DelayController {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == ACT_CMD {
            vec![   VariableDescription::new(USER_DELAY, VariableDirection::Required),
                    VariableDescription::new(BURST_COUNT, VariableDirection::Required),
                    VariableDescription::new(PERIOD, VariableDirection::Required),
                    VariableDescription::new(DELAY_COARSE, VariableDirection::Produced),
                    VariableDescription::new(DELAY_FINE,VariableDirection::Produced)]
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }

    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}

fn compute_delays(user_delay: DimVal, _burst_count: u64, _period: DimVal, coarse_min: &DimVal) -> (DimVal, DimVal) {//(coarse, fine)
    let abs_delay = user_delay;// - (burst_count as i64) * period / 2;
    let fine = abs_delay.clone() % coarse_min;
    let coarse = abs_delay - &fine;
    (coarse, fine)
}

impl CommandExecuter for DelayController {
    fn execute_cmd(&mut self, cmd_name: &str, mut input: NameMapper<Vec<Data>>, _: &HashMap<String, String>) 
    -> Result<NameMapper<Vec<Data>>,Error> {
        if cmd_name == ACT_CMD {
            let mut result : NameMapper<Vec<Data>> = NameMapper::new();
            //get inputs
            let user_delay = match input.remove(USER_DELAY).and_then(|mut v|v.pop()) {
                Some(d) => d,
                None => Data::Empty
            }//.convert(&CompoundDataType::Singleton(DataType::Dim(0, String::new())))
            // .and_then(|data|data.to_atomic_data())
            .to_atomic_data()
            .and_then(|ad|ad.to_dim())?;

            let burst_count = input.remove(BURST_COUNT)
                                    .and_then(|mut v|v.pop())
                                    .ok_or(format_err!("No burst count variable provided"))
                                    .and_then(|data|data.to_atomic_data())
                                    .and_then(|ad|ad.to_u64())?;

            let period = match input.remove(PERIOD).and_then(|mut v|v.pop()) {
                Some(d) => d,
                None => Data::Empty
            }//.convert(&CompoundDataType::Singleton(DataType::Dim(0, String::new())))
            // .and_then(|data|data.to_atomic_data())
            .to_atomic_data()
            .and_then(|ad|ad.to_dim())?;

            let (coarse, fine) = compute_delays(user_delay, burst_count, period, &self.coarse_min);

            result.insert(DELAY_COARSE, vec![Data::create_dim(coarse);1]);
            result.insert(DELAY_FINE, vec![Data::create_dim(fine);1]);

            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}


#[test]
fn delay_controller() {
    use std::str::FromStr;

    let mut tc = DelayController { uid: format!("tester"), coarse_min: DimVal::from_str("4ns").unwrap() };

    let user_delay = Data::create_dim_str("123.4ns").unwrap();
    // let user_delay = Data::create_string("123.4ns".to_string());
    let period = Data::create_dim_str("3.5ns").unwrap();
    let burst_count = Data::create_u64(12);

    let mut cmd_in = NameMapper::new();
    cmd_in.insert("user_delay", vec![user_delay;1]);
    cmd_in.insert("period", vec![period;1]);
    cmd_in.insert("burst_count", vec![burst_count;1]);

    let hm = HashMap::new();

    // println!("Input: {:?}", cmd_in);

    let mut output_nm = tc.execute_cmd("compute", cmd_in, &hm).unwrap();

    
    // println!("Output: {:?}", output_nm);
    let coarse = output_nm.remove("delay_coarse")
                            .and_then(|mut v|v.pop())
                            .ok_or(format_err!("No coarse output"))
                            .and_then(|data|data.to_atomic_data())
                            .and_then(|ad|ad.to_dim())
                            .unwrap();
                            
    let fine = output_nm.remove("delay_fine")
                            .and_then(|mut v|v.pop())
                            .ok_or(format_err!("No fine output"))
                            .and_then(|data|data.to_atomic_data())
                            .and_then(|ad|ad.to_dim())
                            .unwrap();              
    assert_eq!(coarse, DimVal::from_str("100ns").unwrap());
    assert_eq!(fine, DimVal::from_str("2.4ns").unwrap());
    
}