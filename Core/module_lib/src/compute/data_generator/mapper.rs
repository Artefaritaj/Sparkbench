/*
 * File: mapper.rs
 * Project: data_generator
 * Created Date: Thursday November 8th 2018
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 27th September 2019 10:31:49 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2018 INRIA
 */

use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::errors::*;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::{parse_static_string, parse_static_i64};
use sp_lib::interfaces::process::ModuleState;

use hconfig::store::{StoreKey, StoreNode, OverwriteRule, Data, CompoundDataType, AtomicData};

use std::sync::{Arc, Mutex};
use std::collections::{HashMap, BTreeMap};

use comms::connections::CommManager;

use dimval::DimVal;
use failure::Error;

use num::{Zero};

use bincode;

use config_keys::*;


#[derive(Debug,Clone,PartialEq,Eq,Serialize,Deserialize)]
struct MapperState {
    positions: BTreeMap<String, DimVal>,//multi dimensional point
    // steps: BTreeMap<String, DimVal>
    count: usize
}

impl MapperState {
    pub fn next(&mut self, axes: &BTreeMap<String, MapperAxis>, max_count: usize) -> Result<bool, Error> {
        // println!("{} <= {}", self.count, max_count);
        if self.count < max_count {
            self.count += 1;
            // println!("still counting");
            Ok(false)
        }
        else {//end of axis
            // println!("End of axis");
            self.count = 1;
            //search for finished axes
            for (axis_name, position) in self.positions.iter_mut().rev() {//start by last axis
                let axis = axes.get(axis_name).ok_or(format_err!("No {} axis.", axis_name))?;

                if axis.step < DimVal::zero() { // then min is end of axis
                    if *position <= axis.min {
                        *position = axis.max.clone(); //reset axis at the end
                    }
                    else {
                        *position += &axis.step;
                        return Ok(false);//do not modify other axes
                    }
                }
                else {//step > 0, max is end
                    if *position >= axis.max {
                        *position = axis.min.clone();
                    }
                    else {
                        *position += &axis.step;
                        return Ok(false);//do not modify other axes
                    }
                }
            }

            Ok(true)
        }
    }
}

// #[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
// enum StepType {
//     Linear,
//     Snake
// }

#[derive(Debug,Clone)]
struct MapperAxis {
    min: DimVal,
    max: DimVal,
    step: DimVal,
    conv_to: Option<CompoundDataType>
}

const GEN_CMD: &'static str = "gen";

#[derive(Debug)]
pub struct Mapper {
    uid: String,
    axes: BTreeMap<String, MapperAxis>,//(var name, axis)
    valid_count: usize,
    last_state: MapperState,
    max_count: usize,
    state: MapperState
}

impl ModuleBuilder for Mapper {
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager)
    -> Result<Arc<Mutex<dyn Module>>, Error> 
    {

        let uid = at.last_sub_key().to_string();
        let axes_node = args.get_node(&StoreKey::from(AXES))?;
        let mut state: MapperState = MapperState { positions: BTreeMap::new(), count: 1 };

        let min_key = StoreKey::from(MIN);
        let max_key = StoreKey::from(MAX);
        let step_key = StoreKey::from(STEP);
        let conv_to_key = StoreKey::from(CONVERT_TO);

        let mut axes: BTreeMap<String, MapperAxis> = BTreeMap::new();

        for axis_node in axes_node.get_node_children() {
            let axis_name = axis_node.get_node_key().to_string();

            let min = axis_node.get_value(&min_key)
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .and_then(|s|s.parse::<DimVal>())
                                .map_err(|_|format_err!("No minimum value specified for {}.axes.{}", uid, axis_name))?;

            let max = axis_node.get_value(&max_key)
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .and_then(|s|s.parse::<DimVal>())
                                .map_err(|_|format_err!("No maximum value specified for {}.axes.{}", uid, axis_name))?;

             if min > max {
                return Err(format_err!("{} > {} is an error. Minimum must be lower than maximum. To get a decreasing stepper, use a negative step.", min, max));
            }

            let step = axis_node.get_value(&step_key)
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .and_then(|s|s.parse::<DimVal>())
                                .map_err(|_|format_err!("No step value specified for {}.axes.{}", uid, axis_name))?;

            let conv_to = args.get_value(&conv_to_key)
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .and_then(|conv_to_str|CompoundDataType::parse(&conv_to_str))
                                .ok();

            let init = if step < DimVal::zero() { max.clone() } else { min.clone() };
            let axis = MapperAxis { min, max, step, conv_to };
            axes.insert(axis_name.to_string(), axis);
            state.positions.insert(axis_name.to_string(), init);
        }

        let count = match args.get_value(&StoreKey::from(COUNT)) {
                Ok(Data::Singleton(AtomicData::String(ref s))) => {
                    try!(parse_static_i64(s, config)) as usize
                },
                Ok(d) => try!(d.to_atomic_data()?.to_u64()) as usize,
                _ => { 1 }
        };


        let mapper = Mapper {
            uid,
            axes,
            valid_count: 0,
            last_state: state.clone(),
            max_count: count,
            state
        };

        Ok(Arc::new(Mutex::new(mapper)))
    }
}

impl Module for Mapper {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == GEN_CMD {
            let mut vars: Vec<VariableDescription> = Vec::new();
            for (k, _) in self.axes.iter() {
                vars.push(VariableDescription::new(k,VariableDirection::Produced))
            }
            vars
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }

    fn load_state(&mut self, prev_state: Option<&ModuleState>) -> Result<(), Error> {
        match prev_state {
            Some(astate) => {
                let data_vec = astate.get_data();
                self.state = try!(bincode::deserialize(&data_vec).map_err(|e|format_err!("{}", e)));
                trace!("Mapper load state: {:?}", self.state);
                Ok(())
            },
            None => {
                Ok(())
            }
        }
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        let vcount = self.valid_count;
        let mut c = 0;

        // trace!("State {:?}", self.state);
        let mut state2save = self.last_state.clone();
        let mut end = false;

        while !end && c < vcount {
            c += 1;
            end = state2save.next(&self.axes, self.max_count)?;
        }
        trace!("Mapper save state: state2save = {:?} ({})", state2save, vcount);

        let data_vec: Vec<u8> = bincode::serialize(&state2save)
                                .map_err(|e| format_err!("{}", e))?;
        Ok(ModuleState::new(self.get_uid(), data_vec))
    }

    fn actor_closing(&mut self, _: &str, validated: &HashMap<String, usize>) {
        //debug
        // for (k, v) in validated.iter() {
        //     trace!("{} validations = {}", k, v);
        // }

        self.valid_count = validated.values().fold(std::usize::MAX, |min, x| if *x < min { *x } else { min });//minimum
        // trace!("New valid_count = {}", self.valid_count);
    }
}

impl CommandExecuter for Mapper {
    fn execute_cmd(&mut self, cmd_name: &str, _: NameMapper<Vec<Data>>, _: &HashMap<String, String>)
    -> Result<NameMapper<Vec<Data>>,Error> {
        if cmd_name == GEN_CMD {
            let mut result : NameMapper<Vec<Data>> = NameMapper::new();
            self.last_state = self.state.clone();

            //init vectors in result
            for (axis, _) in self.axes.iter() {
                result.insert(axis, Vec::new());
            }

            let mut end = false;
            while !end {
                //add state to result
                for (axis, position) in self.state.positions.iter() {
                    let conv_to_opt = self.axes.get(axis).and_then(|ma|ma.conv_to.clone());
                    
                    let mut current_data = Data::create_dim( position.clone() );
                    if let Some(ref conv_to) = conv_to_opt {
                        current_data = current_data.convert(conv_to)?;
                    }

                    if let Some(v) = result.get_mut(axis) {
                        v.push(current_data);
                    }
                }
                // println!("New position: {:?}", self.state);
                end = self.state.next(&self.axes, self.max_count)?;
            }

            // println!("Mapper end of command");

            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}

