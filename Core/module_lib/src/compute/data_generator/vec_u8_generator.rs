// @Author: ronan
// @Date:   20-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::helpers::byte_vectors::generate_random_byte_vector;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::errors::*;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::{parse_static_vecu8, parse_static_string, parse_static_i64};
use sp_lib::interfaces::process::ModuleState;

use hconfig::store::{StoreKey,StoreNode,OverwriteRule,Data, AtomicData};
use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use config_keys::*;

use comms::connections::CommManager;
use failure::Error;

#[derive(Debug,Clone)]
enum GeneratorType {
    Random(usize),//(number of bytes)
    Fixed(Vec<u8>)
}

const GEN_CMD: &'static str = "gen";
const OUT_VAR_NAME: &'static str = "vec";

#[derive(Debug)]
pub struct VecU8Generator {
    uid: String,
    gen_type: GeneratorType,
    out_var_name: Option<String>
}

impl ModuleBuilder for VecU8Generator {
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager) 
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        let gen_type = try!(args.get_value(&StoreKey::from(GENERATOR_TYPE))
            .and_then(|d|d.to_string_result())
            .and_then(|s|parse_static_string(&s, config))
            .map_err(|_|format_err!("No type (fixed, random, ...) specified for this vec generator")));

        let uid = at.last_sub_key().to_string();
        //optionnal output var node
        let out_var_name = args.get_value(&StoreKey::from(OUTPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();

        match gen_type.as_ref() {
            "fixed" => {
                let value = try!(args.get_value(&StoreKey::from(VALUE))
                                            .and_then(|d|d.to_string_result())
                                            .and_then(|s|parse_static_vecu8(&s, config))
                                            .map_err(|_|format_err!("Unknown or incorrect value for fixed vec generator.")));
                let mut vec_gen = VecU8Generator::new_fixed(&uid, value);
                vec_gen.set_out_var_name(out_var_name);
                Ok(Arc::new(Mutex::new(vec_gen)))
            },
            "random" => {
                let len = match args.get_value(&StoreKey::from(LEN)) {
                        Ok(Data::Singleton(AtomicData::String(ref s))) => {
                            try!(parse_static_i64(s, config)) as usize
                        },
                        Ok(d) => try!(d.to_atomic_data()?.to_u64()) as usize,
                        _ => { return Err(format_err!("Unknown len for random vec generator.")); }
                };

                let mut vec_gen = VecU8Generator::new_random(&uid, len);
                vec_gen.set_out_var_name(out_var_name);
                Ok(Arc::new(Mutex::new(vec_gen)))
            },
            x => Err(format_err!("The vec gen type {} is unwnown.", x))
        }
    }
}

impl Module for VecU8Generator {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == GEN_CMD {
            vec![VariableDescription::new(self.get_out_var_name(),VariableDirection::Produced)]
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }

    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}

impl CommandExecuter for VecU8Generator {
    fn execute_cmd(&mut self, cmd_name: &str, _: NameMapper<Vec<Data>>, _: &HashMap<String, String>) 
    -> Result<NameMapper<Vec<Data>>,Error> {
        if cmd_name == GEN_CMD {
            let vec = self.generate_vec();
            let mut result : NameMapper<Vec<Data>> = NameMapper::new();
            result.insert(self.get_out_var_name(), vec![Data::Raw(vec)]);
            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}

impl VecU8Generator {

    pub fn get_out_var_name(&self) -> &str {
        match self.out_var_name {
            Some(ref n) => n,
            None => OUT_VAR_NAME
        }
    }

    pub fn set_out_var_name(&mut self, out_var_name: Option<String>) {
        self.out_var_name = out_var_name;
    }

    pub fn new_fixed(uid: &str, value: Vec<u8>) -> VecU8Generator {
        VecU8Generator { uid: uid.to_string(), gen_type: GeneratorType::Fixed(value), out_var_name: None }
    }

    pub fn new_random(uid: &str, vec_len: usize) -> VecU8Generator {
        VecU8Generator { uid: uid.to_string(), gen_type: GeneratorType::Random(vec_len), out_var_name: None }
    }


    fn generate_vec(&self) -> Vec<u8> {
        match self.gen_type {
            GeneratorType::Fixed(ref v) => v.clone(),
            GeneratorType::Random(n) => generate_random_byte_vector(n)
        }
    }
}
