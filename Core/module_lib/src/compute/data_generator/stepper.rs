// @Author: ronan
// @Date:   20-10-2016
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 18-11-2016

use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::errors::*;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::parse_static_string;
use sp_lib::interfaces::process::ModuleState;

use hconfig::store::{StoreKey, StoreNode, OverwriteRule, Data, CompoundDataType};

use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use comms::connections::CommManager;

use dimval::DimVal;
use failure::Error;

use num::{Zero, One};

use bincode;

use config_keys::*;

#[derive(Debug,Clone,Copy,PartialEq,Eq,PartialOrd,Ord,Hash)]
enum StepperType {
    Linear,
    Snake,
    Fixed,
    Count
}

#[derive(Debug,Clone,Serialize,Deserialize)]
struct StepperState {
    current: DimVal,
    step: DimVal
}


const GEN_CMD: &'static str = "gen";
const OUT_VAR_NAME: &'static str = "output";

#[derive(Debug)]
pub struct Stepper {
    uid: String,
    stepper_type: StepperType,
    min: DimVal,
    max: DimVal,
    conv_to: Option<CompoundDataType>,
    out_var_name: Option<String>,
    valid_count: Option<usize>,
    last_state: Option<StepperState>,
    state: StepperState
}

impl ModuleBuilder for Stepper {
    //["stepper", "linear", "-20mm", "20mm", "1mm", ("f64"), ("output")]
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager)
    -> Result<Arc<Mutex<dyn Module>>, Error> {

        let step_type = args.get_value(&StoreKey::from(STEP_TYPE))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .map_err(|_|format_err!("No type (fixed, linear, snake, count) specified for this stepper"))?;
        let uid = at.last_sub_key().to_string();

        //optionnal output var node
        let out_var_name = args.get_value(&StoreKey::from(OUTPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();

        //parse min and max
        let min = args.get_value(&StoreKey::from(MIN))
                                .or(args.get_value(&StoreKey::from(VALUE)))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .and_then(|s|s.parse::<DimVal>())
                                .map_err(|_|format_err!("No minimum value specified for this stepper"))?;

        let max = args.get_value(&StoreKey::from(MAX))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .and_then(|s|s.parse::<DimVal>())
                                .or({
                                    if step_type == FIXED {
                                        Ok(min.clone() + DimVal::one())
                                    }
                                    else {
                                        Err(format_err!(""))
                                    }
                                })
                                .map_err(|_|format_err!("No maximum value specified for this stepper"))?;

        let step = args.get_value(&StoreKey::from(STEP))
                            .or(args.get_value(&StoreKey::from(COUNT)))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .and_then(|s|s.parse::<DimVal>())
                                .or({
                                    if step_type == FIXED {
                                        Ok(DimVal::one()+DimVal::one())
                                    }
                                    else {
                                        Err(format!(""))
                                    }
                                })
                                .map_err(|_|format_err!("No step/count value specified for this stepper"))?;



        if min > max && step_type != FIXED {
            return Err(format_err!("{} > {} is an error. First minimum, then maximum. To get a decreasing stepper, use a negative step (or count).", min, max));
        }

        let current_init = if step < DimVal::zero() {
            max.clone()
        }
        else {
            min.clone()
        };

        let conv_to = args.get_value(&StoreKey::from(CONVERT_TO))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .and_then(|conv_to_str|CompoundDataType::parse(&conv_to_str))
                                .ok();
        // let conv_to = try!(CompoundDataType::parse(&conv_to_str));

        match step_type.as_ref() {
            "count" => {
                if step != DimVal::zero() {
                    let real_step = (max.clone()-min.clone())/step;
                    let state = StepperState { current: current_init, step: real_step };
                    let stepper = Stepper { uid: uid, stepper_type: StepperType::Count,
                        min: min, max: max, conv_to: conv_to, out_var_name: out_var_name, state: state, valid_count: None, last_state: None };
                    Ok(Arc::new(Mutex::new(stepper)))
                }
                else {
                    Err(format_err!("Count {} is not an accepted value", step))
                }
            },
            "linear" => {
                let state = StepperState { current: current_init, step: step };
                let stepper = Stepper { uid: uid, stepper_type: StepperType::Linear,
                    min: min, max: max, conv_to: conv_to, out_var_name: out_var_name, state: state, valid_count: None, last_state: None };
                Ok(Arc::new(Mutex::new(stepper)))
            },
            "fixed" => {
                let state = StepperState { current: current_init, step: step };
                let stepper = Stepper { uid: uid, stepper_type: StepperType::Fixed,
                    min: min, max: max, conv_to: conv_to, out_var_name: out_var_name, state: state, valid_count: None, last_state: None };
                Ok(Arc::new(Mutex::new(stepper)))
            },
            "snake" => {
                let state = StepperState { current: current_init, step: step };
                let stepper = Stepper { uid: uid, stepper_type: StepperType::Snake,
                    min: min, max: max, conv_to: conv_to, out_var_name: out_var_name, state: state, valid_count: None, last_state: None };
                Ok(Arc::new(Mutex::new(stepper)))
            },
            _ => Err(format_err!("The stepper type is incorrect. Must be either fixed, linear, snake or count."))
        }
    }
}

impl Module for Stepper {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == GEN_CMD {
            vec![VariableDescription::new(self.get_out_var_name(),VariableDirection::Produced)]
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }

    fn load_state(&mut self, prev_state: Option<&ModuleState>) -> Result<(), Error> {
        match prev_state {
            Some(astate) => {
                let data_vec = astate.get_data();
                self.state = try!(bincode::deserialize(&data_vec).map_err(|e|format_err!("{}", e)));
                trace!("Stepper load state: {:?}", self.state);
                Ok(())
            },
            None => {
                Ok(())
            }
        }
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        match self.valid_count {
            None => {
                Ok(ModuleState::empty(self.get_uid()))
            },
            Some(vcount) => {
                let mut c = 0;

                let (mut current, new_step) = match self.last_state {
                    Some(ref state) => (state.current.clone(), state.step.clone()),
                    None => (self.state.current.clone(), self.state.step.clone())

                };

                while current <= self.max && current >= self.min && c < vcount {
                    c += 1;
                    current = current + new_step.clone();
                }
                trace!("Stepper save state: current = {}, step = {} ({})", current, new_step, vcount);
                let state2save = StepperState { current: current, step: new_step.clone() };
                let data_vec: Vec<u8> = bincode::serialize(&state2save)
                                        .map_err(|e| format_err!("{}", e))?;
                Ok(ModuleState::new(self.get_uid(), data_vec))
            }
        }

    }

    fn actor_closing(&mut self, _: &str, validated: &HashMap<String, usize>) {
        self.valid_count = match validated.get(self.get_out_var_name()) {
            Some(u) => Some(*u),
            None => None
        };
    }
}

impl CommandExecuter for Stepper {
    fn execute_cmd(&mut self, cmd_name: &str, _: NameMapper<Vec<Data>>, _: &HashMap<String, String>)
    -> Result<NameMapper<Vec<Data>>,Error> {
        if cmd_name == GEN_CMD {
            let mut result : NameMapper<Vec<Data>> = NameMapper::new();
            // trace!("Executing stepper {}, {:?}", self.uid, self.conv_to);
            let step = self.state.step.clone();
            let mut current = self.state.current.clone();
            // trace!("Current is {:?}", current);
            self.last_state = Some(StepperState { current: current.clone(), step: step.clone() });

            let mut vec = Vec::new();
            while current <= self.max && current >= self.min {
                // match self.conv_to {
                //     CompoundDataType::Singleton(DataType::I64) => {vec.push(Data::create_i64( try!(current.to_i64().map_err(|e|format_err!("{}", e)) )));},
                //     DataType::F64 => {vec.push(Data::create_f64( try!(current.to_f64().map_err(|e|format_err!("{}", e)) )));},
                //     DataType::Dim(_, _) => {vec.push(Data::create_dim( current.clone() ));},
                //     _ => {}
                // }
                let mut current_data = Data::create_dim( current.clone() );
                if let Some(ref conv) = self.conv_to {
                    current_data = current_data.convert(conv)?;
                }

                vec.push(current_data);
                //println!("New step: {}", current);
                current = current + step.clone();
            }

            //invert step if Snake
            if self.stepper_type == StepperType::Snake {
                    let new_step = -self.state.step.clone();
                    let new_current = if new_step < DimVal::zero() {
                        self.max.clone()
                    }
                    else {
                        self.min.clone()
                    };
                    self.state = StepperState { current: new_current, step: new_step };
            }
            else {//init again to deal with loaded state
                let current_init = if self.state.step < DimVal::zero() {
                    self.max.clone()
                }
                else {
                    self.min.clone()
                };
                self.state.current = current_init;
            }

            // trace!("Stepper execution result: {:?} @ {}", vec, self.get_out_var_name());


            result.insert(self.get_out_var_name(), vec);
            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}

impl Stepper {

    pub fn get_out_var_name(&self) -> &str {
        match self.out_var_name {
            Some(ref n) => n,
            None => OUT_VAR_NAME
        }
    }
}
