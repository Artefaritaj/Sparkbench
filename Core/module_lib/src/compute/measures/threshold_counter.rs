/*
 * Author:   ronan.lashermes@inria.fr
 * Creation: Thu Dec 07 2017
 *
 * Copyright (c) 2017 INRIA
 */


use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::errors::*;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::parse_static_string;
use sp_lib::interfaces::process::ModuleState;

use hconfig::store::{StoreKey,StoreNode,OverwriteRule,Data,CompoundDataType};
use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use comms::connections::CommManager;
use failure::Error;

use config_keys::*;

const ACT_CMD: &'static str = "count";
const IN_VAR_NAME: &'static str = "input";
const DATATYPE_VAR_NAME: &str = "datatype";
const THRESHOLD_VAR_NAME: &str = "threshold";
const OUT_VAR_NAME: &'static str = "count";

#[derive(Debug)]
pub struct ThresholdCounter {
    uid: String,
    in_datatype: CompoundDataType,
    in_var_name: Option<String>,
    out_var_name: Option<String>
}

impl ModuleBuilder for ThresholdCounter {
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager) 
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        let uid = at.last_sub_key().to_string();

        //optionnal input var node
        let in_var_name = args.get_value(&StoreKey::from(INPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();
        //optionnal output var node
        let out_var_name = args.get_value(&StoreKey::from(OUTPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();

        let dt_str = args.get_value(&StoreKey::from(DATATYPE_VAR_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))?;

        let dt = CompoundDataType::parse(&dt_str)?;


        let tc = ThresholdCounter { uid: uid, in_datatype: dt, in_var_name: in_var_name, out_var_name: out_var_name };
        Ok(Arc::new(Mutex::new(tc)))
    }
}

impl Module for ThresholdCounter {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == ACT_CMD {
            vec![   VariableDescription::new(self.get_in_var_name(), VariableDirection::Required),
                    VariableDescription::new(THRESHOLD_VAR_NAME, VariableDirection::Required),
                    VariableDescription::new(self.get_out_var_name(),VariableDirection::Produced)]
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }

    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}

impl CommandExecuter for ThresholdCounter {
    fn execute_cmd(&mut self, cmd_name: &str, mut input: NameMapper<Vec<Data>>, _: &HashMap<String, String>) 
    -> Result<NameMapper<Vec<Data>>,Error> {
        if cmd_name == ACT_CMD {
            let mut result : NameMapper<Vec<Data>> = NameMapper::new();
            let indata = match input.remove(self.get_in_var_name()).and_then(|mut v|v.pop()) {
                Some(d) => d,
                None => Data::Empty
            };

            let test_data = indata.convert(&self.in_datatype)?;

            let threshold = match input.remove(THRESHOLD_VAR_NAME).and_then(|mut v|v.pop()) {
                Some(d) => d,
                None => Data::Empty
            };

            let count: usize = 
            match test_data {
                Data::Empty => 0,
                Data::Raw(vec) => {
                    let th = threshold.to_atomic_data()?.to_u8()?;
                    vec.into_iter().filter(|v| *v > th).count()
                },
                Data::Singleton(ad) => {
                    if ad > threshold.to_atomic_data()? {
                        1
                    }
                    else {
                        0
                    }
                },
                Data::Array(vec) => {
                    vec.into_iter().filter(|v| *v > threshold).count()
                },
                Data::Table(tab) => {
                    tab.into_iter().filter(|&(_, ref v)| v > &threshold).count()
                }
            };

            result.insert(self.get_out_var_name(), vec![Data::create_u64(count as u64);1]);

            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}

impl ThresholdCounter {

    pub fn get_out_var_name(&self) -> &str {
        match self.out_var_name {
            Some(ref n) => n,
            None => OUT_VAR_NAME
        }
    }

    pub fn get_in_var_name(&self) -> &str {
        match self.in_var_name {
            Some(ref n) => n,
            None => IN_VAR_NAME
        }
    }
}


#[test]
fn threshold_counter() {
    use hconfig::store::DataType;

    let mut tc = ThresholdCounter { uid: format!("tester"), in_datatype: CompoundDataType::Array(DataType::I8), in_var_name: None, out_var_name: None };

    let input = Data::Raw(vec![0, 1, 2, 4, 5, 254, 255]);
    let threshold = Data::create_i8(3);

    let mut cmd_in = NameMapper::new();
    cmd_in.insert("input", vec![input;1]);
    cmd_in.insert("threshold", vec![threshold;1]);

    let hm = HashMap::new();

    let mut output_nm = tc.execute_cmd("count", cmd_in, &hm).unwrap();
    let output = output_nm.remove("count").and_then(|mut v|v.pop()).unwrap();
    let count = output.to_atomic_data().unwrap().to_u64().unwrap();
    assert_eq!(count, 2);
    
}