// @Author: Lashermes Ronan <ronan>
// @Date:   10-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 10-04-2017
// @License: MIT

use sp_lib::helpers::NameMapper;
use sp_lib::errors::*;
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::interfaces::process::ModuleState;
use sp_lib::interfaces::modules::{Module, ModuleBuilder, VariableDescription, VariableDirection};
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::parse_static_string;
use sp_lib::helpers::hex_string::to_hex_string;

use comms::CommManager;

use config_keys::*;

use hconfig::store::{Data, StoreNode, StoreKey, OverwriteRule};
use hconfig::io::byte_convert::ToBytes;

use algo_lib::aes::aes::aes128_encrypt_nueva_gen;

use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use failure::Error;

// use rand;
use rand::rngs::StdRng;
use rand::FromEntropy;

#[derive(Debug)]
pub struct AESNuevaGen {
    uid: String,
    out_var_name: Option<String>,
    rng: StdRng
}

const GEN_CMD: &'static str = "encrypt";
const OUT_VAR_NAME: &'static str = CIPHERTEXT;


impl ModuleBuilder for AESNuevaGen {
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager) -> Result<Arc<Mutex<dyn Module>>, Error> {
        let uid = at.last_sub_key().to_string();
        //optionnal output var node
        let out_var_name = args.get_value(&StoreKey::from(OUTPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();
        Ok(Arc::new(Mutex::new(AESNuevaGen { uid: uid, out_var_name: out_var_name, rng: StdRng::from_entropy() })))
    }
}

impl Module for AESNuevaGen {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == GEN_CMD {
            vec![   VariableDescription::new(PLAINTEXT, VariableDirection::Required),
                    VariableDescription::new(KEY, VariableDirection::Required),
                    VariableDescription::new(self.get_out_var_name(), VariableDirection::Produced)]
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }

    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}

impl CommandExecuter for AESNuevaGen {
    fn execute_cmd(&mut self, cmd_name: &str, mut inputs: NameMapper<Vec<Data>>, _: &HashMap<String, String>) -> Result<NameMapper<Vec<Data>>, Error> {
        if cmd_name == GEN_CMD {
            let plain = match inputs.remove(PLAINTEXT).and_then(|mut x|x.pop()) {
                Some(d) => try!(d.to_bytes()),
                None => { return Err(VariableMissingError::new(PLAINTEXT, &format!("in command {} of AES module", cmd_name)).into()); }
            };

            let key = match inputs.remove(KEY).and_then(|mut x|x.pop()) {
                Some(d) => try!(d.to_bytes()),
                None => { return Err(VariableMissingError::new(KEY, &format!("in command {} of AES module", cmd_name)).into()); }
            };

            let key_size = key.len() * 8;

            let mut result: NameMapper<Vec<Data>> = NameMapper::new();
            match key_size {
                128 => {
                    let cipher = aes128_encrypt_nueva_gen(&plain, &key, &mut self.rng);
                    result.insert(self.get_out_var_name(), vec![Data::Raw(cipher);1]);
                },
                x => { return Err(format_err!("Incorrect key size: {} bits ({})", x, to_hex_string(&key))); }
            }

            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}

impl AESNuevaGen {
    pub fn get_out_var_name(&self) -> &str {
        match self.out_var_name {
            Some(ref n) => n,
            None => OUT_VAR_NAME
        }
    }
}
