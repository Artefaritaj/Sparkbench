// @Author: Lashermes Ronan <ronan>
// @Date:   10-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 10-04-2017
// @License: MIT

use sp_lib::helpers::NameMapper;
use sp_lib::errors::*;
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::interfaces::process::ModuleState;
use sp_lib::interfaces::modules::{Module, ModuleBuilder, VariableDescription, VariableDirection};
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::parse_static_data;
use algo_lib::aes::aes::INVSBOX_TAB;

use comms::CommManager;

use config_keys::*;

use hconfig::store::{Data, StoreNode, StoreKey, OverwriteRule};
use hconfig::io::byte_convert::ToBytes;


use std::collections::HashMap;
use std::sync::{Arc, Mutex};
use std::f64;

use failure::Error;


#[derive(Clone,Copy)]
struct ErrorDistribution {
    counters: [usize; 256],
    total: usize
}

impl ErrorDistribution {
    fn entropy(&self) -> f64 {
        if self.total > 0 {
            let probabilities: Vec<f64> = self.counters.iter().map(|c| (*c as f64)/(self.total as f64)).collect();

            let me = probabilities.iter().map(|p| {
                if *p > 0f64 {
                    p*p.log(2.0)
                }
                else {
                    0f64
                }
            }).fold(0f64, |acc, x| acc + x);
            -me
        }
        else {
            0f64
        }
    }

    fn push_value(&mut self, val: u8) {
        self.total += 1;
        self.counters[val as usize] += 1;
    }

    fn new() -> ErrorDistribution {
        ErrorDistribution { counters: [0; 256], total: 0 }
    }
}

fn get_best_rank(entropies: [f64; 256], correct: u8) -> (u8, u8) {//(best, rank)
    let mut best = 0u8;
    let mut min_entropy = f64::MAX;

    let mut rank = 0u16;
    let correct_e = entropies[correct as usize];

    for i in 0..256 {
        let e = entropies[i];

        if e < min_entropy {
            min_entropy = e;
            best = i as u8;
        }

        if e <= correct_e {
            rank += 1;
        }
    }


    (best, (rank-1) as u8) //correct rank to fit u8
}

// #[derive(Debug)]
pub struct Nueva {
    uid: String,
    correct_key: Vec<u8>,
    error_dists: Vec<[ErrorDistribution; 256]> //error_dists[key_byte][key_value].entropy()
}

const GEN_CMD: &'static str = "analyze";
const RANKS: &'static str = "ranks";
const BEST_HYPOTHESIS: &'static str = "best_hypothesis";
const DIFF: &'static str = "diff";

impl ModuleBuilder for Nueva {
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager) -> Result<Arc<Mutex<dyn Module>>, Error> {
        let uid = at.last_sub_key().to_string();
        
        let key = args.get_value(&StoreKey::from(KEY_TO_FIND))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_data(&s, config)).map_err(|e|format_err!("Cannot parse data for module {}.key_to_find: {}", uid, e))?
                                .to_bytes()
                                .map_err(|e|format_err!("Cannot decode key_to_find for module {}: {}", uid, e))?;

        println!("key ok: {:?}", key);

        let key_size = key.len();
        let mut error_dists = Vec::new();
        for _ in 0..key_size {
            error_dists.push([ErrorDistribution::new(); 256]);
        }

        Ok(Arc::new(Mutex::new(Nueva { uid: uid, correct_key: key, error_dists: error_dists })))
    }
}

impl Module for Nueva {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == GEN_CMD {
            vec![   VariableDescription::new(CIPHERTEXT, VariableDirection::Required),
                    VariableDescription::new(FAULTY_CIPHERTEXT, VariableDirection::Required),
                    VariableDescription::new(BEST_HYPOTHESIS, VariableDirection::Produced),
                    VariableDescription::new(DIFF, VariableDirection::Produced),
                    VariableDescription::new(RANKS, VariableDirection::Produced)]
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }

    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}

impl CommandExecuter for Nueva {
    fn execute_cmd(&mut self, cmd_name: &str, mut inputs: NameMapper<Vec<Data>>, _: &HashMap<String, String>) -> Result<NameMapper<Vec<Data>>, Error> {
        if cmd_name == GEN_CMD {
            let ciphertext = match inputs.remove(CIPHERTEXT).and_then(|mut x|x.pop()) {
                Some(d) => try!(d.to_bytes()),
                None => { return Err(VariableMissingError::new(CIPHERTEXT, &format!("in command {} of AES module", cmd_name)).into()); }
            };

            let faulty_ciphertext = match inputs.remove(FAULTY_CIPHERTEXT).and_then(|mut x|x.pop()) {
                Some(d) => try!(d.to_bytes()),
                None => { return Err(VariableMissingError::new(FAULTY_CIPHERTEXT, &format!("in command {} of AES module", cmd_name)).into()); }
            };

            let key_size = self.correct_key.len();
            if ciphertext.len() != key_size || faulty_ciphertext.len() != key_size {
                return Err(format_err!("Ciphertexts do not have the correct size ({})", key_size));
            }

            let mut result: NameMapper<Vec<Data>> = NameMapper::new();

            let mut best_key_bytes: Vec<u8> = Vec::new();
            let mut ranks: Vec<u8> = Vec::new();
            let mut diff: Vec<u8> = Vec::new();

            //for each key byte
            for b in 0..key_size {
                let c = ciphertext[b];
                let d = faulty_ciphertext[b];
                let mut entropies = [0f64; 256];

                diff.push(c^d);

                if c != d {//update only if c != d
                    for k in 0..256usize {//for each key hypothesis
                        let m9 = INVSBOX_TAB[(c ^ (k as u8)) as usize];
                        let e = INVSBOX_TAB[(d ^ (k as u8)) as usize] ^ m9;
                        self.error_dists[b][k].push_value(e);//update error distribution
                    }

                    
                }//else do nothing

                for k in 0..256usize {//for each key hypothesis
                    entropies[k] = self.error_dists[b][k].entropy();
                }
                
                let (best, rank) = get_best_rank(entropies, self.correct_key[b]);
                best_key_bytes.push(best);
                ranks.push(rank);
            }

            
            // result.insert(RANKS, vec![Data::create_u8_array(ranks);1]);
            result.insert(BEST_HYPOTHESIS, vec![Data::Raw(best_key_bytes);1]);
            result.insert(RANKS, vec![Data::Raw(ranks);1]);
            result.insert(DIFF, vec![Data::Raw(diff);1]);

            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}


#[test]
fn test_entropy() {
    let mut error_dist = ErrorDistribution::new();
    assert_eq!(error_dist.entropy(), 0f64);

    error_dist.push_value(1);
    assert_eq!(error_dist.entropy(), 0f64);

    error_dist.push_value(2);
    assert_eq!(error_dist.entropy(), 1f64);
}