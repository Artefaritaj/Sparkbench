// @Author: Lashermes Ronan <ronan>
// @Date:   10-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 10-04-2017
// @License: MIT



use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::errors::*;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::parse_static_string;
use sp_lib::interfaces::process::ModuleState;

use config_keys::*;

use hconfig::store::{StoreKey,StoreNode,OverwriteRule,Data,AtomicData};

use std::sync::{Arc, Mutex};
use std::collections::HashMap;
use std::cmp;

use comms::connections::CommManager;
use failure::Error;

const GEN_CMD: &'static str = "diff";
const IN1_VAR_NAME: &'static str = "input1";
const IN2_VAR_NAME: &'static str = "input2";
const OUT_VAR_NAME: &'static str = "output";

#[derive(Debug)]
pub struct Diff {
    uid: String,
    in1_var_name: Option<String>,
    in2_var_name: Option<String>,
    out_var_name: Option<String>
}

impl ModuleBuilder for Diff {
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager) 
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        let uid = at.last_sub_key().to_string();

        //optionnal input var node
        let in1_var_name = args.get_value(&StoreKey::from(&format!("{}1", INPUT_NAME)))
                                    .and_then(|d|d.to_string_result())
                                    .and_then(|s|parse_static_string(&s, config))
                                    .ok();
        let in2_var_name = args.get_value(&StoreKey::from(&format!("{}2", INPUT_NAME)))
                                    .and_then(|d|d.to_string_result())
                                    .and_then(|s|parse_static_string(&s, config))
                                    .ok();
        //optionnal output var node
        let out_var_name = args.get_value(&StoreKey::from(OUTPUT_NAME))
                                    .and_then(|d|d.to_string_result())
                                    .and_then(|s|parse_static_string(&s, config))
                                    .ok();


        let diff = Diff { uid: uid, in1_var_name: in1_var_name, in2_var_name: in2_var_name, out_var_name: out_var_name };
        Ok(Arc::new(Mutex::new(diff)))
    }
}

impl Module for Diff {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == GEN_CMD {
            vec![   VariableDescription::new(self.get_in1_var_name(), VariableDirection::Required),
                    VariableDescription::new(self.get_in2_var_name(), VariableDirection::Required),
                    VariableDescription::new(self.get_out_var_name(),VariableDirection::Produced)]
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }

    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}

impl CommandExecuter for Diff {
    fn execute_cmd(&mut self, cmd_name: &str, mut input: NameMapper<Vec<Data>>, attributes: &HashMap<String, String>) 
    -> Result<NameMapper<Vec<Data>>, Error> {
        if cmd_name == GEN_CMD {
            let mut result : NameMapper<Vec<Data>> = NameMapper::new();
            let i1 = match input.remove(self.get_in1_var_name()).and_then(|mut x|x.pop()) {
                Some(d) => d,
                None => { 
                    return Err(VariableMissingError::new(self.get_in1_var_name(), 
                                                        &format!("in command {} of module Diff", cmd_name)).into());
                }
            };

            let i2 = match input.remove(self.get_in2_var_name()).and_then(|mut x|x.pop()) {
                Some(d) => d,
                None => { 
                    return Err(VariableMissingError::new(self.get_in2_var_name(), 
                                                        &format!("in command {} of module Diff", cmd_name)).into());
                    }
            };

            match i1 {
                Data::Raw(ref v1) => {
                    match i2 {
                        Data::Raw(ref v2) => {
                            let max_len = cmp::max(v1.len(), v2.len());
                            let mut res: Vec<u8> = Vec::new();

                            for i in 0..max_len {
                                let val1 = match v1.get(i) {
                                    Some(v) => *v,
                                    None => 0
                                };

                                let val2 = match v2.get(i) {
                                    Some(v) => *v,
                                    None => 0
                                };

                                res.push(val1 ^ val2);
                            }

                            if attributes.contains_key(NOT_ZERO) {
                                if !Diff::is_zero_vector(&res) {
                                    result.insert(self.get_out_var_name(), vec![Data::Raw(res);1]);
                                }
                            }
                            else {
                                result.insert(self.get_out_var_name(), vec![Data::Raw(res);1]);
                            }

                        },
                        _ => { return Err(format_err!("Data 2 must be a Byte Array as Data1"));}
                    }
                },
                Data::Singleton(AtomicData::I64(ref v1)) => {
                    match i2 {
                        Data::Singleton(AtomicData::I64(ref v2)) => {
                            let diff = v2 - v1;
                            if attributes.contains_key(NOT_ZERO) {
                                if diff != 0 {
                                    result.insert(self.get_out_var_name(), vec![Data::Singleton(AtomicData::I64(diff));1]);
                                }
                            }
                            else {
                                result.insert(self.get_out_var_name(), vec![Data::Singleton(AtomicData::I64(diff));1]);
                            }
                        },
                        _ => { return Err(format_err!("Data 2 must be a i64 as Data1"));}
                    }
                },
                _ => { return Err(format_err!("Data not supported for diff: {:?}", i1))}//TODO
            }

            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}

impl Diff {

    fn is_zero_vector(vec: &Vec<u8>) -> bool {
        for v in vec.iter() {
            if *v != 0 {
                return false;
            }
        }
        true
    }

    pub fn get_in1_var_name(&self) -> &str {
        match self.in1_var_name {
            Some(ref n) => n,
            None => IN1_VAR_NAME
        }
    }

    pub fn get_in2_var_name(&self) -> &str {
        match self.in2_var_name {
            Some(ref n) => n,
            None => IN2_VAR_NAME
        }
    }

    pub fn get_out_var_name(&self) -> &str {
        match self.out_var_name {
            Some(ref n) => n,
            None => OUT_VAR_NAME
        }
    }
}
