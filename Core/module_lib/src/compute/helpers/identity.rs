// @Author: ronan
// @Date:   23-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 23-02-2017

use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::errors::*;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::parse_static_string;
use sp_lib::interfaces::process::ModuleState;

use hconfig::store::{StoreKey,StoreNode,OverwriteRule,Data};
use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use comms::connections::CommManager;
use failure::Error;

use config_keys::*;

const GEN_CMD: &'static str = "replicate";
const IN_VAR_NAME: &'static str = "input";
const OUT_VAR_NAME: &'static str = "output";

#[derive(Debug)]
pub struct Identity {
    uid: String,
    in_var_name: Option<String>,
    out_var_name: Option<String>
}

impl ModuleBuilder for Identity {
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager)
    -> Result<Arc<Mutex<dyn Module>>, Error> {
        let uid = at.last_sub_key().to_string();

        //optionnal input var node
        let in_var_name = args.get_value(&StoreKey::from(INPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();
        //optionnal output var node
        let out_var_name = args.get_value(&StoreKey::from(OUTPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();

        let id = Identity { uid: uid, in_var_name: in_var_name, out_var_name: out_var_name };
        Ok(Arc::new(Mutex::new(id)))
    }
}

impl Module for Identity {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == GEN_CMD {
            vec![   VariableDescription::new(self.get_in_var_name(), VariableDirection::Required),
                    VariableDescription::new(self.get_out_var_name(),VariableDirection::Produced)]
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }

    fn load_state(&mut self, _: Option<&ModuleState>) -> Result<(), Error> {
        Ok(())
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        Ok(ModuleState::empty(self.get_uid()))
    }

    fn actor_closing(&mut self, _: &str, _: &HashMap<String, usize>) {

    }
}

impl CommandExecuter for Identity {
    fn execute_cmd(&mut self, cmd_name: &str, mut input: NameMapper<Vec<Data>>, _: &HashMap<String, String>)
    -> Result<NameMapper<Vec<Data>>,Error> {
        if cmd_name == GEN_CMD {
            let mut result : NameMapper<Vec<Data>> = NameMapper::new();
            let d = match input.remove(self.get_in_var_name()).and_then(|mut x|x.pop()) {
                Some(d) => d,
                None => Data::Empty
            };


            result.insert(self.get_out_var_name(), vec![d;1]);
            trace!("{} run: {:?} -> {:?}", self.uid, input, result);
            // trace!("New output for {}: {:?}", self.uid, result);
            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}

impl Identity {

    pub fn get_out_var_name(&self) -> &str {
        match self.out_var_name {
            Some(ref n) => n,
            None => OUT_VAR_NAME
        }
    }

    pub fn get_in_var_name(&self) -> &str {
        match self.in_var_name {
            Some(ref n) => n,
            None => IN_VAR_NAME
        }
    }
}
