// @Author: ronan
// @Date:   23-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 23-02-2017

use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::errors::*;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::{parse_static_i64, parse_static_string};
use sp_lib::interfaces::process::ModuleState;

use hconfig::store::{StoreKey,StoreNode,OverwriteRule,Data,AtomicData};
use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use comms::connections::CommManager;
use failure::Error;

use config_keys::*;

use bincode;

const GEN_CMD: &'static str = "replicate";
const IN_VAR_NAME: &'static str = "input";
const OUT_VAR_NAME: &'static str = "output";

#[derive(Debug)]
pub struct Replicator {
    uid: String,
    replication_count: usize,
    in_var_name: Option<String>,
    out_var_name: Option<String>,
    start_from: Option<usize>,
    last_start: usize,
    valid_count: Option<usize>
}

impl ModuleBuilder for Replicator {
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager)
    -> Result<Arc<Mutex<dyn Module>>, Error> {

        let uid = at.last_sub_key().to_string();


        //optionnal input var node
        let in_var_name = args.get_value(&StoreKey::from(INPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();
        //optionnal output var node
        let out_var_name = args.get_value(&StoreKey::from(OUTPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();


        let rep_count = match args.get_value(&StoreKey::from(REPLICATION_FACTOR)) {
                Ok(Data::Singleton(AtomicData::String(ref s))) => {
                    try!(parse_static_i64(s, config)) as usize
                },
                Ok(d) => try!(d.to_atomic_data()?.to_u64()) as usize,
                _ => { return Err(format_err!("No valid replication count for this value")); }
        };

        let replicator = Replicator::new(uid, rep_count, in_var_name, out_var_name);
        Ok(Arc::new(Mutex::new(replicator)))
    }
}

impl Module for Replicator {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == GEN_CMD {
            vec![   VariableDescription::new(self.get_in_var_name(), VariableDirection::Required),
                    VariableDescription::new(self.get_out_var_name(),VariableDirection::Produced)]
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }

    fn load_state(&mut self, prev_state: Option<&ModuleState>) -> Result<(), Error> {
        match prev_state {
            Some(astate) => {
                let data_vec = astate.get_data();
                let start: usize = try!(bincode::deserialize(&data_vec).map_err(|e|format_err!("{}", e)));
                self.start_from = Some(start);
                //bugfix, if fails after state loading and before first command execution
                self.last_start = start;
                trace!("Load replicator from {}", start);
                Ok(())
            },
            None => {
                Ok(())
            }
        }
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        match self.valid_count {
            Some(vcount) => {
                let to_save = vcount + self.last_start;
                //warn!("Rep save state {}+{}={}", self.last_start, vcount, to_save);
                let data_vec: Vec<u8> = try!(bincode::serialize(&to_save).map_err(|e|format_err!("{}", e)));
                Ok(ModuleState::new(self.get_uid(), data_vec))
            },
            None => Ok(ModuleState::empty(self.get_uid()))
        }
    }

    fn actor_closing(&mut self, _: &str, validated: &HashMap<String, usize>) {
        self.valid_count = match validated.get(self.get_out_var_name()) {
            Some(u) => Some(*u),
            None => None
        };
    }
}

impl CommandExecuter for Replicator {
    fn execute_cmd(&mut self, cmd_name: &str, mut input: NameMapper<Vec<Data>>, _: &HashMap<String, String>)
    -> Result<NameMapper<Vec<Data>>,Error> {
        if cmd_name == GEN_CMD {
            let mut result : NameMapper<Vec<Data>> = NameMapper::new();
            let d = match input.remove(self.get_in_var_name()).and_then(|mut x|x.pop()) {
                Some(d) => d,
                None => Data::Empty
            };

            // trace!("Replicator executed: X{} @{}", self.replication_count, self.get_var_name());
            self.last_start = match self.start_from {
                Some(from) => {
                    result.insert(self.get_out_var_name(), vec![d;self.replication_count-from]);
                    self.start_from = None;
                    from
                },
                None => {
                    result.insert(self.get_out_var_name(), vec![d;self.replication_count]);
                    0
                }
            };
            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}

impl Replicator {

    pub fn new(uid: String, rep_count: usize, in_var_name: Option<String>, out_var_name: Option<String>) -> Replicator {
        Replicator { uid: uid, replication_count: rep_count, in_var_name: in_var_name, out_var_name: out_var_name, last_start: 0, start_from: None, valid_count: None }
    }

    pub fn get_in_var_name(&self) -> &str {
        match self.in_var_name {
            Some(ref n) => n,
            None => IN_VAR_NAME
        }
    }

    pub fn get_out_var_name(&self) -> &str {
        match self.out_var_name {
            Some(ref n) => n,
            None => OUT_VAR_NAME
        }
    }
}
