// @Author: ronan
// @Date:   23-02-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 23-02-2017

use sp_lib::helpers::name_mapper::NameMapper;
use sp_lib::interfaces::modules::{Module, VariableDescription, VariableDirection, ModuleBuilder};
use sp_lib::interfaces::modules::commands::command_executer::CommandExecuter;
use sp_lib::errors::*;
use sp_lib::resources::toml::toml_manager::TOMLManager;
use sp_lib::interfaces::expressions::parser::{parse_static_i64, parse_static_string};
use sp_lib::interfaces::process::ModuleState;

use hconfig::store::{StoreKey,StoreNode,OverwriteRule,Data,AtomicData};
use std::sync::{Arc, Mutex};
use std::collections::HashMap;

use comms::connections::CommManager;
use failure::Error;

use config_keys::*;

use bincode;

const GEN_CMD: &'static str = "split";
const IN_VAR_NAME: &'static str = "input";
const OUT_VAR_NAME: &'static str = "output";

#[derive(Serialize,Deserialize,Debug,Clone)]
struct SplitterSaveState {
    start_from: usize,
    last_input: Vec<Data>
}

#[derive(Debug, Clone, Copy)]
pub enum SplitType {
    DivideBy(usize),
    MaxSize(usize)
}

#[derive(Debug)]
pub struct Splitter {
    uid: String,
    split_type: SplitType,
    in_var_name: Option<String>,
    out_var_name: Option<String>,
    save_state: Option<SplitterSaveState>,
    valid_count: Option<usize>
}

impl ModuleBuilder for Splitter {
    fn build(config: &mut StoreNode, at: &StoreKey, args: &StoreNode, _: &Vec<OverwriteRule>, _: &TOMLManager)
    -> Result<Arc<Mutex<dyn Module>>, Error> {

        let div_by = match args.get_value(&StoreKey::from(DIVIDE_BY)) {
            Ok(Data::Singleton(AtomicData::String(ref s))) => Some(try!(parse_static_i64(s, config)) as usize),
            Ok(d) => Some(try!(d.to_atomic_data()?.to_u64()) as usize),
            _ => None
        };

        let max_size = match args.get_value(&StoreKey::from(MAX_SIZE)) {
            Ok(Data::Singleton(AtomicData::String(ref s))) => Some(try!(parse_static_i64(s, config)) as usize),
            Ok(d) => Some(try!(d.to_atomic_data()?.to_u64()) as usize),
            _ => None
        };

        let split_opt = max_size  .map(|u| Some(SplitType::MaxSize(u)))
                                .unwrap_or(div_by.map(|u| Some(SplitType::DivideBy(u)))
                                                 .unwrap_or(None));
        let split = try!(split_opt.ok_or(format_err!("divide_by or max_size must be specified")));

        let uid = at.last_sub_key().to_string();

        //optionnal input var node
        let in_var_name = args.get_value(&StoreKey::from(INPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();
        //optionnal output var node
        let out_var_name = args.get_value(&StoreKey::from(OUTPUT_NAME))
                                .and_then(|d|d.to_string_result())
                                .and_then(|s|parse_static_string(&s, config))
                                .ok();


        let splitter = Splitter::new(uid, split, in_var_name, out_var_name);
        Ok(Arc::new(Mutex::new(splitter)))
    }
}

impl Module for Splitter {
    fn get_uid(&self) -> &str {
        &self.uid
    }

    fn get_variable_descriptions(&self, cmd_name: &str) -> Vec<VariableDescription> {
        if cmd_name == GEN_CMD {
            vec![   VariableDescription::new(self.get_in_var_name(), VariableDirection::Required),
                    VariableDescription::new(self.get_out_var_name(),VariableDirection::Produced)]
        }
        else {
            Vec::new()
        }
    }

    //No comms
    fn prepare(&mut self, _: &mut CommManager) -> Result<(), Error> {
        Ok(())
    }

    fn load_state(&mut self, prev_state: Option<&ModuleState>) -> Result<(), Error> {
        trace!("Splitter loading previous state: {:?}", prev_state);
        match prev_state {
            Some(astate) => {
                let data_vec = astate.get_data();
                if data_vec.is_empty()
                {
                    self.save_state = None;
                }
                else {
                    let save_state: SplitterSaveState = try!(bincode::deserialize(&data_vec));
                    self.save_state = Some(save_state);
                }
                Ok(())
            },
            None => {
                self.save_state = None;
                Ok(())
            }
        }
    }

    fn save_state(&mut self) -> Result<ModuleState, Error> {
        let last_data = match self.save_state {
            Some(ref data) => data.clone(),
            None => {
                // trace!("Splitter saving empty state: save_state={:?}", self.save_state);
                return Ok(ModuleState::empty(self.get_uid()));
            }
        };

        match self.valid_count {
            Some(vcount) => {
                let save_state = SplitterSaveState { start_from: vcount, last_input: last_data.last_input};
                let data_vec: Vec<u8> = try!(bincode::serialize(&save_state).map_err(|e|format_err!("{}", e)));
                // trace!("Splitter saving state: save_state={:?}", save_state);
                Ok(ModuleState::new(self.get_uid(), data_vec))
            },
            None => {
                // trace!("Splitter saving empty state: save_state={:?}", self.save_state);
                Ok(ModuleState::empty(self.get_uid()))
            }
        }

    }

    fn actor_closing(&mut self, _: &str, validated: &HashMap<String, usize>) {
        self.valid_count = match validated.get(self.get_out_var_name()) {
            Some(u) => Some(*u),
            None => None
        };
    }
}

fn split_vec_div(in_vec: Vec<Data>, div_by: usize) -> Vec<Data> {
    let mut result: Vec<Data> = Vec::new();

    for d in in_vec {
        match d {
            Data::Array(arr) => {
                let mut vec = arr;
                let out_vec_len = vec.len() / div_by;
                while vec.len() >= out_vec_len {
                    let new_vec = vec.split_off(out_vec_len);
                    result.push(Data::Array(vec));
                    vec = new_vec;
                }
            },
            Data::Raw(arr) => {
                let mut vec = arr;
                let out_vec_len = vec.len() / div_by;
                while vec.len() >= out_vec_len {
                    let new_vec = vec.split_off(out_vec_len);
                    result.push(Data::Raw(vec));
                    vec = new_vec;
                }
            },
            _ => {}
        }
    }
    result
}

fn split_vec_max(in_vec: Vec<Data>, max_size: usize) -> Vec<Data> {
    let mut result: Vec<Data> = Vec::new();

    for d in in_vec {
        match d {
            Data::Array(arr) => {
                let mut vec = arr;
                while vec.len() >= max_size {
                    let new_vec = vec.split_off(max_size);
                    result.push(Data::Array(vec));
                    vec = new_vec;
                }
            },
            Data::Raw(arr) => {
                let mut vec = arr;
                while vec.len() >= max_size {
                    let new_vec = vec.split_off(max_size);
                    result.push(Data::Raw(vec));
                    vec = new_vec;
                }
            },
            _ => {}
        }
    }
    result
}

fn split_vec(in_vec: Vec<Data>, split: SplitType) -> Vec<Data> {
    match split {
        SplitType::DivideBy(div_by) => split_vec_div(in_vec, div_by),
        SplitType::MaxSize(max_size) => split_vec_max(in_vec, max_size)
    }
}

impl CommandExecuter for Splitter {
    fn execute_cmd(&mut self, cmd_name: &str, mut input: NameMapper<Vec<Data>>, _: &HashMap<String, String>)
    -> Result<NameMapper<Vec<Data>>, Error> {
        if cmd_name == GEN_CMD {
            let mut result : NameMapper<Vec<Data>> = NameMapper::new();

            let in_vec: Vec<Data> = match input.remove(self.get_in_var_name()) {
                Some(vec) => vec,
                None => Vec::new()
            };

            // trace!("Replicator executed: X{} @{}", self.replication_count, self.get_var_name());
            match self.save_state {
                Some(ref state) => {
                    let mut missing_vecs = split_vec(state.clone().last_input, self.split_type);
                    if missing_vecs.len() > state.start_from {
                        let to_send = missing_vecs.split_off(state.start_from);
                        result.insert(self.get_out_var_name(), to_send);
                    }

                    result.insert(self.get_out_var_name(), split_vec(in_vec, self.split_type));

                },
                None => {
                    result.insert(self.get_out_var_name(), split_vec(in_vec, self.split_type));
                }
            }
            self.save_state = None;
            Ok(result)
        }
        else {
            Err(CommandUnknown::new(cmd_name).into())
        }
    }
}

impl Splitter {

    pub fn new(uid: String, split: SplitType, in_var_name: Option<String>, out_var_name: Option<String>) -> Splitter {
        Splitter { uid: uid, split_type: split, in_var_name: in_var_name, out_var_name: out_var_name, save_state: None, valid_count: None }
    }

    pub fn get_out_var_name(&self) -> &str {
        match self.out_var_name {
            Some(ref n) => n,
            None => OUT_VAR_NAME
        }
    }

    pub fn get_in_var_name(&self) -> &str {
        match self.in_var_name {
            Some(ref n) => n,
            None => IN_VAR_NAME
        }
    }
}
