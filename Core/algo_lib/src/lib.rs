// @Author: Lashermes Ronan <ronan>
// @Date:   10-04-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 10-04-2017
// @License: MIT

extern crate sp_lib;
extern crate rand;
#[macro_use] extern crate lazy_static;

pub mod aes;
