'use strict'

import { app, BrowserWindow } from 'electron'
const {dialog} = require('electron')
// import '../../semantic/dist/semantic.min.css'
// import $ from 'jquery'
// import '../../semantic/dist/semantic.min.js'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000,
    webPreferences: {webSecurity: false}
  })

  app.showExitPrompt = false

  mainWindow.setMenu(null)
  mainWindow.loadURL(winURL)

  mainWindow.on('close', (e) => {
    if (app.showExitPrompt) {
      e.preventDefault() // Prevents the window from closing
      dialog.showMessageBox({
        type: 'question',
        buttons: ['Cancel', 'Quit'],
        title: 'Confirm',
        message: 'Unsaved data will be lost. Are you sure you want to quit?'
      }, function (response) {
        if (response === 1) { // Runs the following if 'Yes' is clicked
          app.showExitPrompt = false
          mainWindow.close()
        }
      })
    }
  })

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

// app.on('before-quit', (evt) => {
//   console.log(store)
//   if(store.getters['Workspace/has_modification'] === true) {
//     console.log('prevent')
//     evt.preventDefault()
//   }
//   evt.preventDefault()
// })

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
