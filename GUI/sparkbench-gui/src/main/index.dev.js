/**
 * This file is used specifically and only for development. It installs
 * `electron-debug` & `vue-devtools`. There shouldn't be any need to
 *  modify this file, but it can be used to extend your development
 *  environment.
 */

/* eslint-disable */

// Set environment for development
process.env.NODE_ENV = 'development'

// Install `electron-debug` with `devtron`
// require('electron-debug')({ showDevTools: true })
require('electron-debug')({ showDevTools: false })

// Install `vue-devtools`
// Not supported for electron 2.0.0 yet
// require('electron').app.on('ready', () => {
//   let installExtension = require('electron-devtools-installer')
//   installExtension.default(installExtension.VUEJS_DEVTOOLS)
//     .then(() => {})
//     .catch(err => {
//       console.log('Unable to install `vue-devtools`: \n', err)
//     })
// })

// context menu
const {app, BrowserWindow} = require('electron');
 
require('electron-context-menu')({
    prepend: (params, browserWindow) => [{
        label: 'Rainbow',
        // Only show it when right-clicking images 
        visible: params.mediaType === 'image'
    }]
});


// Require `main` process to boot app
require('./index')

