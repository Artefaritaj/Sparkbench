import sparkbench from './sparkbench.js'
var http = require('http')
const EventEmitter = require('events')
var _ = require('lodash')
var analysis_components = require('@/components/Analyses/index.js').default

var server_add = 'http://localhost'
var port = 8081

export default {
  run_experiment: run_experiment
}

var server = undefined

function run_experiment (vue, workspace_files, exp_name, proc_name) {
  vue.$store.commit('Data/clear')

  if (server) {
    sparkbench.run_experiment(exp_name, proc_name, server_add + ':' + port)
  } else {
    start_feedback_server(function (data) {
      vue.$store.commit('Data/push_data', data)
    })
    sparkbench.run_experiment(exp_name, proc_name, server_add + ':' + port)
  }
}

function start_feedback_server (feedback_callback) {
  // launch feedback server
  server = http.createServer(function (req, res) {
    var body = ''

    req.on('data', function (data) {
      body += data
      // Too much POST data, kill the connection!
      // 1e6 ~~~ 1MB
      if (body.length > 1e6) {
        req.connection.destroy()
      }
    })

    req.on('end', function () {
      if (feedback_callback) {
        var jsonObj = JSON.parse(body)
        feedback_callback(jsonObj)
      }
    })

    res.end()
  })

  server.listen(port)
}
