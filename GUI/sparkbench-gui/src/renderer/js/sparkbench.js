var cproc = require('child_process')
var exec = cproc.exec
var axios = require('axios')
var _ = require('lodash')

var spark_server = axios.create({
  baseURL: 'http://localhost:8080',
  crossDomain: true,
  timeout: 100000,
  headers: {
    'Access-Control-Allow-Origin': '*'
  }
})

export default {
  get_bench_files_location: get_bench_files_location,
  get_toml_path_from_name: get_toml_path_from_name,
  run_experiment: run_experiment
}

function execute (command, callback) {
  exec(command, function (error, stdout, stderr) {
    if (error) {
      console.log('Error: ' + error)
    }
    callback(stdout.trim())
  })
}

// get the root path of where bench files are
function get_bench_files_location (callback) {
  spark_server.post('', {
    'command': 'where'
  })
    .then(function (response) {
      var path = _.get(response, ['data', 'path'])
      callback(path)
    })
    .catch(function (error) {
      console.log(error)
    })
    // execute('sparkbench-cli -m where', callback)
}

// get the path of a toml config file from its name
function get_toml_path_from_name (name, callback) {
  // execute('sparkbench-cli -m database -p ' + name , callback)
  spark_server.post('', {
    'command': 'database',
    'option': 'path',
    'filename': name
  })
    .then(function (response) {
      var path = _.get(response, ['data', 'path'])
      callback(path)
    })
    .catch(function (error) {
      console.log(error)
    })
}

function run_experiment (name, process_name, feedback_url) {
  if (name) {
    var command_payload = {
      'command': 'experiment',
      'load': name
    }
    if (feedback_url) {
      command_payload['feedback_url'] = feedback_url
    }
    if (process_name) {
      command_payload['process'] = process_name
    }

    spark_server.post('', command_payload)
      .catch(function (error) {
        console.log(error)
      })
  }
}
