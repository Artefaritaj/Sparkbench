const {dialog} = require('electron').remote
var fs = require('fs')
var pt = require('path')
var _ = require('lodash')

import workspace from './workspace.js'

export default {
  open_file: open_file,
  save_file: save_file,
  save_as_file: save_as_file,
  new_file: new_file
}

function open_file (store) {
  var options = {
    defaultPath: store.state.SparkbenchCLI.bench_files_path,
    filters: [
      {names: 'TOML configuration', extensions: ['toml']},
      {names: 'All Files', extensions: ['*']}
    ],
    properties: ['openFile']
  }
  dialog.showOpenDialog(options, function (fileNames) {
    if (fileNames) {
      var fileName = fileNames[0]
      store.dispatch('Workspace/set_workspace_file', fileName)
    }
  })
}

function save_file (store, filename) {
  var filepath = _.get(store.state.Workspace.files, [filename, 'path'])
  var data = _.get(store.state.Workspace.files, [filename, 'data'])
  if (filepath && filepath !== '*') {
    workspace.write_TOML(filepath, data)
    store.commit('Workspace/clear_modified', filename)
  } else {
    save_as_file(store, filename)
  }
}

function save_all_files (store) {
  Object.keys(store.state.Workspace.files).map(n => {
    save_file(store, n)
  })
}

function save_as_file (store, filename) {
  var data = _.get(store.state.Workspace.files, [filename, 'data'])
  var options = {
    defaultPath: store.state.SparkbenchCLI.bench_files_path,
    filters: [
      {names: 'TOML configuration', extensions: ['toml']},
      {names: 'All Files', extensions: ['*']}
    ]
  }
  dialog.showSaveDialog(options, function (selectedFile) {
    if (selectedFile) {
      workspace.write_TOML(selectedFile, data)
      store.commit('Workspace/clear_modified', filename)
      store.commit('Workspace/set_file_path', {filename: filename, path: selectedFile})
    }
  })
}

function new_file (store) {
  var options = {
    defaultPath: store.state.SparkbenchCLI.bench_files_path,
    filters: [
      {names: 'TOML configuration', extensions: ['toml']},
      {names: 'All Files', extensions: ['*']}
    ]
  }
  dialog.showSaveDialog(options, function (selectedFile) {
    if (selectedFile) {
      let filename = pt.parse(selectedFile).name
      store.commit('Workspace/add_referenced_file', {filename: filename, configpath: selectedFile, data: {}})
    }
  })
}
