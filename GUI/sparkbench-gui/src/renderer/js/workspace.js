var toml = require('toml')
var fs = require('fs')
var pt = require('path')

var tomlify = require('tomlify-j0.4')
var sparkbench = require('./sparkbench.js')
var _ = require('lodash')

export default {
  parseTOML: parseTOML,
  getModulesCount: getModulesCount,
  getActorsCount: getActorsCount,
  getProcessesCount: getProcessesCount,
  getConstantsCount: getConstantsCount,
  getExperimentTransitionsCount: getExperimentTransitionsCount,
  getVariablesCount: getVariablesCount,
  getConnectionsCount: getConnectionsCount,
  get_referenced_filenames: get_referenced_filenames,
  getConnections: getConnections,
  write_TOML: write_TOML
}

function parseTOML (fullPath) {
  var data = toml.parse(fs.readFileSync(fullPath).toString())
  // console.log(data)
  return data
}

function write_TOML (path, toml_data) {
  var text = tomlify.toToml(toml_data,
    {
      space: 2,
      // replace function to have integers in TOML instead of floats everywhere
      replace: function (key, value) {
        if (Number.isInteger(value)) {
          return value.toFixed(0)
        }
        return false
      }
    })
  fs.writeFile(path, text, function (err) {
    if (err) {
      console.log('Failed to write TOML file to ' + path + ': ' + err)
    }
  })
}

// deduplicates array
function dedup (a) {
  var prims = {'boolean': {}, 'number': {}, 'string': {}}, objs = []

  return a.filter(function (item) {
    var type = typeof item
    if (type in prims) { return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true) } else { return objs.indexOf(item) >= 0 ? false : objs.push(item) }
  })
}

// toml_files, toml_device
function get_referenced_filenames (data) {
  if (data) {
    var modules = data.modules
    if (modules) {
      var array_results = Object.keys(modules).map(function (key, index) {
        var m = modules[key]
        if (m.type && m.type === 'toml_device') {
          return m.toml_files
        }
      }).filter(x => x != undefined)
      var merged = [].concat.apply([], array_results)
      return dedup(merged)
    }
  }
  return []
}

function countMembers (data) {
  if (data) {
    return Object.keys(data).length
  } else {
    return 0
  }
}

function getModules (files) {
  if (files) {
    var array_results = Object.keys(files).map(function (key, index) {
      // foreach config
      var config = files[key].data
      if (config) {
        if (config.modules) {
          return Object.keys(config.modules)
        }
      }
      return undefined
    }).filter(x => x != undefined)
    var merged = [].concat.apply([], array_results)
    return dedup(merged)
  } else {
    return []
  }
}

function getModulesCount (files) {
  return getModules(files).length
}

function getActors (files) {
  if (files) {
    var array_results = Object.keys(files).map(function (key, index) {
      // foreach config
      var config = files[key].data
      if (config) {
        if (config.actors) {
          return Object.keys(config.actors)
        }
      }
      return undefined
    }).filter(x => x != undefined)
    var merged = [].concat.apply([], array_results)
    return dedup(merged)
  } else {
    return []
  }
}

function getActorsCount (files) {
  return getActors(files).length
}

function getProcesses (files) {
  if (files) {
    var array_results = Object.keys(files).map(function (key, index) {
      // foreach config
      var config = files[key].data
      if (config) {
        if (config.processes) {
          return Object.keys(config.processes)
        }
      }
      return undefined
    }).filter(x => x != undefined)
    var merged = [].concat.apply([], array_results)
    return dedup(merged)
  } else {
    return []
  }
}

function getProcessesCount (files) {
  return getProcesses(files).length
}

function getConstants (files) {
  if (files) {
    var array_results = Object.keys(files).map(function (key, index) {
      // foreach config
      var config = files[key].data
      if (config) {
        if (config.constants) {
          return Object.keys(config.constants)
        }
      }
      return undefined
    }).filter(x => x != undefined)
    var merged = [].concat.apply([], array_results)
    return dedup(merged)
  } else {
    return []
  }
}

function getConstantsCount (files) {
  return getConstants(files).length
}

function getExperimentalTransitions (files) {
  if (files) {
    var array_results = Object.keys(files).map(function (key, index) {
      // foreach config
      var config = files[key].data
      if (config) {
        if (config.experiment) {
          return Object.keys(config.experiment)
        }
      }
      return undefined
    }).filter(x => x != undefined)
    var merged = [].concat.apply([], array_results)
    return dedup(merged)
  } else {
    return []
  }
}

function getExperimentTransitionsCount (files) {
  return getExperimentalTransitions(files).length
}

function getVariables (files) {
  if (files) {
    var array_results = Object.keys(files).map(function (key, index) {
      // foreach config
      var config = files[key].data
      if (config) {
        if (config.vars) {
          return Object.keys(config.vars)
        }
      }
      return undefined
    }).filter(x => x != undefined)
    var merged = [].concat.apply([], array_results)
    return dedup(merged)
  } else {
    return []
  }
}

function getVariablesCount (files) {
  return getVariables(files).length
}

function getConnections (files) {
  if (files) {
    var array_results = Object.keys(files).map(function (key, index) {
      // foreach config
      var config = files[key].data
      // console.log(config)
      if (config) {
        if (config.connections) {
          // console.log('Conns: ' + Object.keys(config.connections))
          return Object.keys(config.connections)
        }
      }
      return undefined
    }).filter(x => x != undefined)
    var merged = [].concat.apply([], array_results)
    // console.log('Merged conns: ' + merged)
    return dedup(merged)
  } else {
    return []
  }
}

function getConnectionsCount (files) {
  return getConnections(files).length
}
