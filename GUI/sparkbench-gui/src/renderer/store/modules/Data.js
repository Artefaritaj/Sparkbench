import { stat } from 'fs'

var _ = require('lodash')

const state = {
  data: {}
}

const mutations = {
  clear (state) {
    state.data = {}
  },
  push_data (state, d) {
    // console.log(d)
    if (_.isPlainObject(d)) {
      Object.keys(d).map(key => {
        if (!state.data[key]) {
          state.data[key] = []
        }
        state.data[key].push(d[key])
      })
      state.data = {...state.data} // force vue reactivity
    }
  }
}

const actions = {
  // someAsyncTask ({ commit }) {
  //   // do something async
  //   commit('INCREMENT_MAIN_COUNTER')
  // }
}

export default {
  namespaced: true,
  state,
  mutations
  // actions
}
