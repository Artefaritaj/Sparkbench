const state = {
  open: false
}

const mutations = {
  show (state) {
    state.open = true
  },
  close (state) {
    state.open = false
  },
  toggle (state) {
    state.open = !state.open
  }
}

export default {
  namespaced: true,
  state,
  mutations
}
