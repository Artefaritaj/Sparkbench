const state = {
  bench_files_path: ''
}

const mutations = {
  set_bench_files_path (state, path) {
    state.bench_files_path = path
  }
}

const actions = {
  // someAsyncTask ({ commit }) {
  //   // do something async
  //   commit('INCREMENT_MAIN_COUNTER')
  // }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
