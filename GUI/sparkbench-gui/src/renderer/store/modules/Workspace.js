
// var toml = require('toml')
// var fs   = require('fs')

// function parseTOML(fullPath) {
//     var data = toml.parse(fs.readFileSync(fullPath).toString())
//     console.log(data)
//     return data
// }

import workspace from '../../js/workspace.js'
import sparkbench from '../../js/sparkbench.js'

const { app } = require('electron').remote

// import Vue from 'vue'

var fs = require('fs')
var pt = require('path')
var _ = require('lodash')

const state = {
  wsToml: '',
  files: {}
}

function mark_file_modified (state, filename) {
  _.set(state, ['files', filename, 'modified'], true)
  state.files = { ...state.files}// force Vue reactivity
  app.showExitPrompt = true
}

const mutations = {
  add_referenced_file (state, {filename, configpath, data}) {
    if (state.files) {
      state.files = { ...state.files,
        [filename]: {
          path: configpath, modified: false, data: data, main: false
        }}
    }
  },
  set_workspace (state, {data, wsPath}) {
    if (wsPath) {
      let filename = pt.parse(wsPath).name
      state.wsToml = wsPath
      state.files = {}
      state.files = { ...state.files,
        [filename]: {
          path: wsPath, modified: false, data: data, main: true
        }}
    } else {
      state.files = {}
      state.wsToml = ''
    }
  },
  delCmd (state, {file, connection, previousCmdName}) {
    delete state.files[file].data.connections[connection].cmds[previousCmdName]
    mark_file_modified(state, file)
    // state.files = { ...state.files}//force Vue reactivity
  },
  createCmd (state, {file, connection, name, to_send, to_receive}) {
    _.set(state, ['files', file, 'data', 'connections', connection, 'cmds', name], [to_send, to_receive])
    mark_file_modified(state, file)
    // state.files = { ...state.files}//force Vue reactivity
  },
  updateCmd (state, {file, connection, previousCmdName, newCmd}) {
    _.set(state, ['files', file, 'data', 'connections', connection, 'cmds', newCmd.name], [newCmd.to_send, newCmd.to_receive])
    if (newCmd.name != previousCmdName) {
      delete state.files[file].data.connections[connection].cmds[previousCmdName]
    }
    mark_file_modified(state, file)
    // state.files = { ...state.files}//force Vue reactivity
  },
  updateSerialConfig (state, {file, connection, link, port, baudrate, databits, stopbits, parity, flowcontrol}) {
    _.set(state, ['files', file, 'data', 'connections', connection, 'connection_config', 'link'], 'serial')
    _.set(state, ['files', file, 'data', 'connections', connection, 'connection_config', link, 'port'], port)
    _.set(state, ['files', file, 'data', 'connections', connection, 'connection_config', link, 'baudrate'], baudrate)
    _.set(state, ['files', file, 'data', 'connections', connection, 'connection_config', link, 'databits'], databits)
    _.set(state, ['files', file, 'data', 'connections', connection, 'connection_config', link, 'stopbits'], stopbits)
    _.set(state, ['files', file, 'data', 'connections', connection, 'connection_config', link, 'parity'], parity)
    _.set(state, ['files', file, 'data', 'connections', connection, 'connection_config', link, 'flowcontrol'], flowcontrol)
    mark_file_modified(state, file)
    // state.files = { ...state.files}//force Vue reactivity
  },
  updateEthernetConfig (state, {file, connection, link, ip_address, port}) {
    _.set(state, ['files', file, 'data', 'connections', connection, 'connection_config', 'link'], 'ethernet')
    _.set(state, ['files', file, 'data', 'connections', connection, 'connection_config', link, 'ipv4_address'], ip_address)
    _.set(state, ['files', file, 'data', 'connections', connection, 'connection_config', link, 'port'], port)
    mark_file_modified(state, file)
    // state.files = { ...state.files}//force Vue reactivity
  },
  changeConnectionLink (state, {file, connection, link}) {
    _.set(state, ['files', file, 'data', 'connections', connection, 'connection_config', 'link'], link)
    mark_file_modified(state, file)
    // state.files = { ...state.files}//force Vue reactivity
  },
  clear_modified (state, filename) {
    _.set(state, ['files', filename, 'modified'], false)
    // _.set(state, ['files', filename, 'on_disk'], _.get(state, ['files', filename, 'data']))

    // update if we show prompt preventing quit with unsaved data
    app.showExitPrompt = getters.has_modification(state)
    state.files = { ...state.files}// force Vue reactivity
  },
  set_file_path (state, {filename, path}) {
    _.set(state, ['files', filename, 'path'], path)
    state.files = { ...state.files}// force Vue reactivity
  },
  updateAnalyses (state, analyses) {
    _.set(state, ['files', getters.main(state), 'data', 'analyses'], analyses)
    mark_file_modified(state, getters.main(state))
  }
}

const getters = {
  has_modification: state => {
    return Object.keys(state.files).map(n => {
      if (_.get(state, ['files', n, 'modified']) === true) {
        return true
      } else {
        return false
      }
    }).reduce((acc, x) => acc || x)
  },
  is_file_modified: (state) => (file) => {
    return _.get(state, ['files', file, 'modified'])
  },
  main: state => {
    return pt.parse(state.wsToml).name
  }
}

const actions = {
  set_workspace_file ({ commit }, wsPath) {
    if (wsPath) {
      let data = workspace.parseTOML(wsPath)

      commit('set_workspace', {wsPath: wsPath, data: data})

      var referenced_filenames = workspace.get_referenced_filenames(data)
      referenced_filenames.map(function (filename) {
        sparkbench.get_toml_path_from_name(filename, function (configpath) {
          if (configpath && fs.existsSync(configpath)) {
            let ref_data = workspace.parseTOML(configpath)
            commit('add_referenced_file', {filename: filename, configpath: configpath, data: ref_data})
          }
        })
      })
    } else {
      commit('set_workspace', {data: {}, wsPath: ''})
    }
  },
  clear_workspace ({ commit }) {
    commit('set_workspace', '', {data: {}, wsPath: ''})
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
