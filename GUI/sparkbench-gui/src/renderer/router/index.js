import Vue from 'vue'
import Router from 'vue-router'

import AppMenu from '@/components/AppMenu/AppMenu'
import LandingPage from '@/components/Home/LandingPage'

import Modules from '@/components/Modules/Modules'
import ActorsTop from '@/components/Actors/ActorsTop'
import Processes from '@/components/Processes/Processes'
import Experiment from '@/components/Experiment/Experiment'

import Variables from '@/components/Variables/Variables'
import Connections from '@/components/Connections/Connections'

import Preferences from '@/components/Preferences/Preferences'

var analysis_components = require('@/components/Analyses/index.js').default

Vue.use(Router)

function createRoutes () {
  // first create static routes
  var routes = [
    {
      path: '/',
      name: 'landing-page',
      components: {
        app_menu: AppMenu,
        default: LandingPage
      }
    },
    {
      path: '/modules',
      name: 'modules',
      components: {
        app_menu: AppMenu,
        default: Modules
      }
    },
    {
      path: '/actors',
      name: 'actors',
      components: {
        app_menu: AppMenu,
        default: ActorsTop
      }
    },
    {
      path: '/processes',
      name: 'processes',
      components: {
        app_menu: AppMenu,
        default: Processes
      }
    },
    {
      path: '/experiment',
      name: 'experiments',
      components: {
        app_menu: AppMenu,
        default: Experiment
      }
    },
    {
      path: '/variables',
      name: 'variables',
      components: {
        app_menu: AppMenu,
        default: Variables
      }
    },
    {
      path: '/connections',
      name: 'connections',
      components: {
        app_menu: AppMenu,
        default: Connections
      }
    },
    {
      path: '/preferences',
      name: 'preferences',
      components: {
        app_menu: AppMenu,
        default: Preferences
      }
    },
    {
      path: '*',
      redirect: '/'
    }
  ]

  // then create dynamic routes for analyses
  Object.keys(analysis_components).map(cname => {
    let this_analysis = analysis_components[cname]
    routes.push({
      path: '/analysis/' + cname,
      name: 'analysis/' + this_analysis.name,
      components: {
        app_menu: AppMenu,
        default: this_analysis.ui
      }
    })
  })

  // return all routes
  return routes
}

export default new Router({
  routes: createRoutes()
})
