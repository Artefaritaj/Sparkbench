import AesNuevaConfig from './AesNuevaConfig'
import AesNuevaUi from './AesNuevaUi'
import Vue from 'vue'
import experiment from '../../../js/experiment.js'

var ByteBuffer = require('bytebuffer')

// var diff_data = []

export default {
  name: 'AES-NUEVA',
  ui: AesNuevaUi,
  config: AesNuevaConfig
  // start: start,
  // diff_data: diff_data
}

// a and b must have the same size
// function xor (a, b) {
//     var res = Buffer.allocUnsafe(a.length)

//     for (var i = 0; i < a.length; ++i) {
//         res[i] = a[i] ^ b[i]
//     }

//     return res
// }

// function start() {

// }
