
import PinConfig from './PinConfig'
import PinUi from './PinUi'

export default {
  name: 'PIN',
  ui: PinUi,
  config: PinConfig,
  start: start
}

function start () {
  console.log('start pin')
}
